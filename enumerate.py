import glob, os

def rename(dir, pattern, titlePattern):
  i = 0
  for pathAndFilename in glob.iglob(os.path.join(dir, pattern)):

    title, ext = os.path.splitext(os.path.basename(pathAndFilename))
    number = "%05d" % (i,)

    os.rename(pathAndFilename,
              os.path.join(dir, titlePattern % number + '.jpg'))
    i += 1

rename(r'img/zoom-modules', r'*.jpg', r'img-%s')
