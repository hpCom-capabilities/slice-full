
$(function($){
var slice = (function(){

  /* ---- Constants ---- */
  var frames = 415,
    stops = [
      {
        name: 'Overview',
        startframe: 30
      }, {
        name: 'Design',
        startframe: 70
      }, {
        name: 'Versatility',
        startframe: 105
      }, {
        name: 'Wireless Charging',
        startframe: 131,
        parttwo: 153
      }, {
        name: 'Communications',
        startframe: 198,
        parttwo: 230
      }, {
        name: 'Modules',
        startframe: 265
      }, {
        name: 'Security',
        startframe: 315
      }, {
        name: 'Desktop',
        startframe: 355
      }, {
        name: 'Meeting',
        startframe: 380
      }, {
        name: 'Finish',
        startframe: 415
      }
    ],
    zooms =   [
      {
        name: 'Design',
        slug: 'design',
        framesMid: 100,
        frames: 146,
        tempo: .12,
        copyClass: '1-0',
        openDelay: 4800,
        closeDelay: 2500,
        preloadPerc:.5
      },
      {
        name: 'Modules',
        slug: 'modules',
        frameStops: [
          {frame: 40, openDelay: 2000, closeDelay: 8000},
          {frame: 82, openDelay: 2000, closeDelay: 8000},
          {frame: 108, openDelay: 1200, closeDelay: 8000},
          {frame: 133, openDelay: 1200, closeDelay: 6000},
          {frame: 162}
        ],
        frames: 162,
        tempo: .12,
        copyClass: '5',
        preloadPerc:.5
      },
      {
        name: 'Security',
        slug: 'security',
        frameStops: [
          {frame: 90, openDelay: 1500, closeDelay: 10000},
          {frame: 121}
        ],
        frames: 121,
        tempo: .125,
        copyClass: '6',
        preloadPerc:.5
      }
    ],
    $module = $('#slice-module-x2'),
    $cont = $module.find('.slice-int-mod-cont'),
    $contbg = $module.find('.slice-opened-bg'),
    $stage = $module.find('.slice-stage'),
    $image = $module.find('#slice-image'),
    $window = $(window),
    timeout,
    $viewport = "",
    $mobileStage = $module.parents('.section').find('.slice-mobile-stage'),
    $slides = $mobileStage.find('.slice-slide:not(.slice-slide-intro)'),
    mobileIndex = 0;

  /* ---- Initialization ---- */
  function init(viewport) {
    if(!$viewport) {
      $viewport = viewport;
    }
    $module.parents('.section').addClass('fnf-module');
	var parentMobile = $mobileStage.parent('.molecule-999');
	$cont.append($mobileStage);
	parentMobile.remove();
    if (viewport === 'mobile') {
      setMobileView();
    } else {
      setDesktopView();
    }
    _buildButtons(viewport);
    _setIcons();

    $cont.off('click tap', '.slice-overlay-trig');
    $cont.on('click tap', '.slice-overlay-trig', function () {
      _openModule(viewport);
    });
  }

  function setDesktopView() {
      $stage.removeClass('ghost');
      $mobileStage.addClass('ghost');
      $module.find('.slice-int-mod-cont .slice-copy-intro').css('background-image', 'url(https://hp-ui-slice-full-dev.s3.amazonaws.com/img/main/img-00000.jpg)');
      $cont.css('height', 'auto');      
      $viewport = 'desktop';
  }

  function setMobileView() {

    $('body').addClass('touch-device');

    $stage.addClass('ghost');
    $mobileStage.removeClass('ghost');  
    _setImagesUrls ();
    //_alignMobile();
    $viewport = 'mobile';
  }

  function reInit(viewport) {
    if ($viewport == viewport) {
      return;
    }
    if (_isOpened($cont)) {
      _closeModule($viewport);     
    }
    if($viewport == 'desktop' && viewport == 'mobile') {
      setMobileView();      
    } else if($viewport == 'mobile' && viewport == 'desktop') {
      setDesktopView();       
    } 
    $cont.off('click tap', '.slice-overlay-trig');
    $cont.on('click tap', '.slice-overlay-trig', function () {
      _openModule(viewport);
    });       
  }

  function _applyEventListeners(viewport) {
    $cont.off('click', '.slice-zoom');
    $cont.off('click', '.slice-onward');

    if (viewport === 'desktop') {
      $cont.on('click', '.slice-zoom', function () {
        _loadZoom($(this).data('zoom'));
      });
      $cont.on('click', '.slice-onward', function () {
        _gotoStop($(this).data('next-stop'));
      });
      $window.on('keyup', _throttle(function (e) {
        e.preventDefault();
        e.stopPropagation();
        if (_isOpened($cont) && $stage.attr('data-active') === 'false') {
          var stop = parseInt($stage.attr('data-stop'));
          if (e.key == "ArrowUp" || e.key == "ArrowLeft") {
            if (stop > 0) {
              _gotoStop(stop - 1);
            }
          } else if (e.key == "ArrowDown" || e.key == "ArrowRight") {
            if (stop < 9) {
              _gotoStop(stop + 1);
            }
          } else if (e.keyCode >= 48 && e.keyCode <= 58) {
            var currentStop = _getActiveStop(),
              targetStop = e.key - 1,
              delta;
            if (e.keyCode === 48) {
              targetStop = 9;
            }
            delta = targetStop - currentStop;
            if (Math.abs(delta) === 1) {
              _gotoStop(targetStop);
            } else if (delta == 0) {
              return false;
            } else {
              _jumpStop(targetStop)
            }
          } else if (e.key == " ") {
            var $targetZoom = $('.slice-copy-' + stop).find('.zoom');
            if ($targetZoom) {
              var zoom = $targetZoom.data('zoom');
              _loadZoom(zoom);
            }
          } else if (e.key == "Escape") {
            _closeModule('desktop');
          }
        }
      }, 500));
    } else {

      $cont.on('click tap', '.slice-onward', function () {
        go(0, 8, true);
      });

    //
    //  /* -- Viewport Resize -- */
    //  $window.on('load resize', _throttle(function () {
    //    //var clientHeight = document.documentElement.clientHeight,
    //    //  innerHeight = window.innerHeight,
    //    //  orientation = _getOrientation(),
    //    //  lowProfile = false;
    //    //
    //    //if (orientation === 'portrait') {
    //    //  lowProfile = (clientHeight < innerHeight);
    //    //} else {
    //    //  lowProfile = (clientHeight < innerHeight);
    //    //}
    //    //
    //    //console.log('low profile: ' + lowProfile);
    //    //
    //    //if (lowProfile) {
    //    //  $cont.addClass('bars-shown');
    //    //} else {
    //    //  $cont.removeClass('bars-shown');
    //    //}
    //
    //  }, 200));
    //
    }

    $window.on('resize', _throttle(function () {
      if (_isOpened($cont)) {
        _centerModule(viewport);
      }
    }, 500));

    $cont.off('click tap', '.slice-close-overlay');
    $cont.on('click tap', '.slice-close-overlay', function(){
      _closeModule(viewport);
    });
  }

  /*Had to set images in js,because in css tridion change absolute url*/
  function _setImagesUrls () {
    var mobileImg = {
      ".slice-slide-intro" : "1_premodule.jpg",
      ".slice-slide-0" : "2_overview.jpg",
      ".slice-slide-1" : "3_design.jpg",
      ".slice-slide-2" : "4_connectivity.jpg",
      ".slice-slide-3" : "5_versatility.jpg",
      ".slice-slide-4" : "6_wireless_charging.jpg",
      ".slice-slide-5" : "7_communications.jpg",
      ".slice-slide-6" : "8_modules.jpg",
      ".slice-slide-7" : "9_wireless_connections.jpg",
      ".slice-slide-8" : "10_odd_module.jpg",
      ".slice-slide-9" : "11_speaker_module.jpg",
      ".slice-slide-10" : "12_vesa_plate.jpg",
      ".slice-slide-11" : "13_security.jpg",
      ".slice-slide-12" : "14_fingertip.jpg",
      ".slice-slide-13" : "15_hp_slice.jpg",
      ".slice-slide-14" : "16_hp_slice_for_meeting_rooms.jpg",
      ".slice-slide-15" : "17_final_frame.jpg"
    },
    urlPart = "https://hp-ui-slice-full-dev.s3.amazonaws.com/img/mobile/";

    $.each( mobileImg, function( key, value ) {
        var url = urlPart + value;
        $module.find(key).css('background-image', 'url(' + url + ')');
    });
    
  }

  function _setIcons() {
    var icons = {
      ".slice-int-mod-cont .slice-scroll-on" : "scroll-on.png",
      ".close-slice" : "close-overlay.png"
    }, 
    partUrl = "https://hp-ui-slice-full-dev.s3.amazonaws.com/css/";
    $.each(icons, function( key, value ) {
        var url = partUrl + value;
        $module.find(key).css('background-image', 'url(' + url + ')');
    });

    $(".slice-int-mod-cont .slice-buttons, .slice-int-mod-cont .slice-mobile-buttons").css('cursor', "url(https://hp-ui-slice-full-dev.s3.amazonaws.com/css/icon.ico),ns-resize;");
  }






  /* ---- Animation Reel Methods ---- */
  /* -- Main Reel Invocation -- */
  function _invokeMainReel(frame) {
    var preload = 0;
    $image.reel({
        wheelable: true,
        scrollable: true,
        draggable: false,
        throwable: false,
        timeout: 0,
        tempo: 12,
        frames: frames,
        frame: 0,
        loops: false,
        revolution: 10000,
        vertical: true,
        horizontal: false,
        delay: 9999,
        path: 'https://hp-ui-slice-full-dev.s3.amazonaws.com/img/main/',
        laziness: 1,
        preload: 'linear',
        concurrent_requests: 6
      })
      .bind('loaded', function () { return false; })
      .bind('preloaded', function () {
        if (preload === stops[0].startframe) { setTimeout(function(){ _gotoStop(0); }, 0); }
        preload++
      })
      .bind('frameChange', function() { if ($stage.attr('data-active') == 'true') { _hideCopy(); }  })
      .bind('wheel', function(e) {
        e.preventDefault();
        e.stopPropagation();

        if ($stage.attr('data-active') === 'false') {
          var stop = parseInt($stage.attr('data-stop')),
            deltaY = e.originalEvent.deltaY;

          if (deltaY > 15 && stop < 9) {
            _gotoStop(stop + 1);
          } else if (deltaY <= -40 && stop > 0) {
            _gotoStop(stop - 1);
          }
        }
      })
      .bind('stop', function () {
        $stage.attr('data-active', 'false');
        clearTimeout(timeout);
        var activeStop = _getActiveStop('desktop');

        if(_isOpened($cont) && $image.reel('frame') == stops[activeStop].startframe) {
          _showCopy(activeStop);
          $module.find('.slice-buttons').removeClass('ghost');
          $module.find('.slice-no-scroll').addClass('ghost');
        }

        if (stops[activeStop].parttwo){
          timeout = setTimeout( function() { _gotoFrame(stops[activeStop].parttwo); }, 1500)
        }
      })
      .hammer().bind("swipeup swipedown panup pandown", function(e) {
        if ($stage.attr('data-active') === 'false') {
          var stop = parseInt($stage.attr('data-stop')),
              deltaY = e.gesture.deltaY;
          if (deltaY > 10 && stop < 9) {
            _gotoStop(stop + 1);
          } else if (deltaY <= -10 && stop > 0) {
            _gotoStop(stop - 1);
          }
        }
      });
  }

  /* -- Zoom Reel Invocation -- */
  function _invokeZoomReel(z, zoomSlug) {
    _hideCopy();
    var frames = zooms[z].frames - 1,
        zoomID = '#' + zoomSlug,
        $zoomID = $(zoomID),
        stallTil = Math.ceil(zooms[z].frames * zooms[z].preloadPerc),
        preload = 0;

    $zoomID.reel({
        wheelable: false,
        scrollable: false,
        draggable: false,
        throwable: false,
        cursor: 'default',
        timeout: 0,
        tempo: 12,
        frames: frames,
        frame: 0,
        loops: false,
        vertical: true,
        horizontal: false,
        delay: 9999,
        path: 'https://hp-ui-slice-full-dev.s3.amazonaws.com/img/' + zoomSlug + '/',
        laziness: 1,
        preload: 'linear',
        concurrent_requests: 6
      })
      .bind('loaded', function () { return false; })
      .bind('preloaded', function () {

        if(/*@cc_on!@*/false) {
          // Only IE, preload everything
          stallTil = zooms[z].frames;
        }

        if (preload == stallTil) {
          setTimeout(function() {
            if (zooms[z].frameStops){
              var frameDelay = 0;

              for (var i = 0; i < zooms[z].frameStops.length; i++ ) {
                var frameData = {
                  $targetCopy: $('.slice-copy-' + zooms[z].copyClass + '-' + (i)),
                  frame: zooms[z].frameStops[i].frame - 1,
                  openDelay: frameDelay + zooms[z].frameStops[i].openDelay,
                  closeDelay: frameDelay + zooms[z].frameStops[i].closeDelay,
                  delay: frameDelay
                };

                (function (e) {
                  setTimeout(function() {
                    _gotoFrame(e.frame, zoomID, zooms[z].tempo);
                    e.$targetCopy.removeClass('ghost');
                  }, e.delay);

                  if (i !== zooms[z].frameStops.length - 1) {
                    setTimeout(function () { e.$targetCopy.find('.anim').removeClass('ghost'); }, e.openDelay);
                    setTimeout(function () { e.$targetCopy.find('.anim').addClass('ghost'); }, e.closeDelay);
                  }
                })(frameData);

                frameDelay = frameData.closeDelay;
              }
            } else {
              _gotoFrame(frames, zoomID, zooms[z].tempo);
              $module.find('.slice-copy-' + zooms[z].copyClass).removeClass('ghost');
              setTimeout(function () {
                $module.find('.slice-copy-' + zooms[z].copyClass).find('.anim').removeClass('ghost');
                setTimeout(function () {
                  $module.find('.slice-copy-' + zooms[z].copyClass).find('.anim').addClass('ghost');
                }, zooms[z].closeDelay);
              }, zooms[z].openDelay);
            }
          }, 0);
        }
        preload++
      })
      .bind('stop', function () {
        if (frames === $zoomID.data('frame')) { _unloadZoom(); }
      });
  }

  /* ---- Visibility Methods ---- */
  /* -- Is Element Open -- */
  function _isOpened (el) {
    return el.hasClass('opened');
  }

  /* ---- Button Functions ---- */
  /* -- Set up sidebar buttons -- */
  function _buildButtons(viewport) {

    var markup = '',
      $buttonCont; 
	  //buttons for desktop were moved to html for setting up on tridion translatable

    if (viewport === 'mobile') {
      $buttonCont = $module.find('.slice-mobile-buttons'),
      length = $slides.length;
    }
    if ($buttonCont && !$buttonCont.children().length) { 
      for (i = 0; i < length; i++) {
        var first = (i==0) ? 'selected' : '',
          name = '';
        if (viewport !== 'mobile') {
          name = stops[i].name;
        }
        markup = $('<a  class="' + first + ' slice-marker slice-marker-'
          + i + '" title="' + name + '"><span>' + name + '</span></a>');
        (function(i) {
           markup.on('click', function(){
            buttonClick(i,viewport);
         });
        }(i)); 
        $buttonCont.append(markup);   
      }
    }
	$module.find('.slice-buttons a').on('click', function () {
		var index = $(this).index();
		buttonClick(index);
	});
  }

  /* -- Button Click Event -- */
  function buttonClick(stop, viewport) {
    var currentStop = _getActiveStop(viewport),
        delta = stop - currentStop;
    if (viewport === 'mobile') {
      go(stop);
    } else {
      if (Math.abs(delta) === 1) {
        _gotoStop(stop);
      } else if (delta == 0) {
        return false;
      } else {
        _jumpStop(stop)
      }
    }
  }

  /* -- Set selected button -- */
  function _makeSelected(stop) {
    $module.find('.slice-marker').removeClass('selected');
    $module.find('.slice-marker-' + stop).addClass('selected');
  }

  /* ---- Animation Methods ---- */
  /* -- Animate to frame -- */
  function _gotoFrame(frame, target, tempo) {
    clearTimeout(timeout);
    target = target || '#slice-image';
    tempo = tempo || 0.065;
    $(target).trigger('reach', [frame, tempo]);
  }

  /* -- Animate to stop -- */
  function _gotoStop(stop) {
    $stage.attr('data-active', 'true');
    _setActiveStop(stop);
    _gotoFrame(stops[stop].startframe);
  }

  /* -- Go directly to a frame -- */
  function _jumpFrame(frame) {
    clearTimeout(timeout);
    $image.trigger('stop');
    $image.reel('frame', frame);
  }

  /* -- Fade directly to a stop -- */
  function _jumpStop(stop) {
    _setActiveStop(stop);
    $stage.attr('data-active', 'true');
    $module.find('.slice-white-overlay').removeClass('ghost');

    var frame = stops[stop].startframe;
    setTimeout(function(){
      _jumpFrame(frame);
      setTimeout(function(){
        $module.find('.slice-white-overlay').addClass('ghost');
        $stage.attr('data-active', 'false');
        _showCopy(stop);
      }, 150);
    }, 300);
  }

  /* -- Get active stop context -- */
  function _getActiveStop() {
    return $stage.attr('data-stop');
  }

  /* -- Set active stop context -- */
  function _setActiveStop(stop) {
    $stage.attr('data-stop', stop);
  }

  /* ---- Copy Methods ---- */
  /* -- Show a stop's copy -- */
  function _showCopy (stop) {
    _hideCopy();
    _waterfall(stop);
    _makeSelected(stop);
  }

  /* -- Hide all copy -- */
  function _hideCopy(viewport) {
    if (viewport === 'mobile') {
      $module.find('.slice-slide.inner, .slice-slide.inner .anim').addClass('ghost');
    } else {
      $module.find('.slice-copy.inner, .slice-copy.inner .anim').addClass('ghost');
    }
  }

  /* -- Animate copy reveal -- */
  /* -- Animate copy reveal -- */
  function _waterfall(stop, mobile) {
    if (mobile) {
      var $target = $module.find('.slice-slide-' + (stop));
    } else {
      var $target = $module.find('.slice-copy-' + (stop));
    }
    $target.removeClass('ghost').find('.ghost').each(function(){
      var $this = $(this),
        timeout = $this.data('anim');

      setTimeout(function(){
        $this.removeClass('ghost');
      }, timeout);
    });
  }

  /* ---- Module Methods ---- */
  /* -- Open Module -- */
  function _openModule(viewport) {

    _applyEventListeners(viewport);

    if (viewport === 'mobile') {

      $module.find('.slice-white-overlay').removeClass('ghost');
      $module.find('.slice-slide').addClass('ghost');
      $cont.addClass('opened');

      setTimeout(function(){
        $module.find('.slice-no-scroll').addClass('ghost');
        $module.find('.slice-slide-0, .slice-mobile-buttons').removeClass('ghost');
        $cont.css('height', _getHeight());
        setTimeout(function(){
          $module.find('.slice-white-overlay').addClass('ghost');
          $module.find('.slice-slide:not(.slice-slide-intro)').removeClass('ghost');
          _waterfall(mobileIndex, true);
        }, 300);
      }, 300);

    } else {
      $cont.css('height', 'auto');
      _invokeMainReel();
    }

    $('body').addClass('overlay-lock');
    $cont.addClass('opened');
    $contbg.addClass('opened');
    $module.find('.slice-copy-intro').addClass('ghost');
    $module.find('.slice-close-overlay').removeClass('ghost');

    setTimeout(function(){
      _hideCopy();
      _buildButtons(viewport);
      _centerModule(viewport);
    }, 0);

  }

  /* -- Close Module -- */
  function _closeModule(viewport) {
    if (viewport == 'mobile') {
      $module.find('.slice-white-overlay').removeClass('ghost');
      $stage.attr('data-stop', 0);
      setTimeout(function(){
        _resetAll('mobile');
        setTimeout(function(){
          $module.find('.slice-white-overlay').addClass('ghost');
        }, 300);
      }, 300);

    } else {

      _unloadZoom();
      $module.find('.slice-white-overlay').removeClass('ghost');
      $module.find('.slice-copy').addClass('ghost');
      setTimeout(function () {
        _resetAll();
        setTimeout(function () {
          $module.find('.slice-white-overlay').addClass('ghost');
        }, 150);
      }, 300);

    }
  }

  /* -- Load inner zoom reel -- */
  function _loadZoom (z) {
    $stage.attr('data-active', 'true');
    $module.find('.slice-copy-' + zooms[z].copyClass).removeClass('ghost');
    $module.find('.slice-buttons, .slice-close-overlay').addClass('ghost');
    $module.find('.slice-close-zoom').removeClass('ghost');

    var zoomSlug = 'zoom-' + zooms[z].slug;
    $module.find('#' + zoomSlug).removeClass('ghost').addClass('opened');
    _invokeZoomReel(z, zoomSlug);
  }

  /* -- Unload inner zoom reel -- */
  function _unloadZoom () {
    $stage.attr('data-active', 'false');

    $module.find('.slice-zoom-reel').each(function(){
      if ($(this).hasClass('opened')){
        $(this).addClass('ghost').removeClass('opened').unreel();
      }
    });
    $module.find('.slice-buttons, .slice-close-overlay').removeClass('ghost');
    $module.find('.slice-close-zoom').addClass('ghost');

    var stop = $stage.attr('data-stop');
    _showCopy(stop);
  }

  /* -- Back to square 1 -- */
  function _resetAll(viewport) {
    if (viewport === 'mobile') {
      $slides
        .removeClass('show-copy')
        .css('height', _getHeight())
        .each(function () {
          translate = 'translate3d(0, 0, 0)';
          _applyTransform(this, translate);
        });
      $module.find('.slice-slide-copy').each(function () {
        translate = 'translate3d(0, ' + _getHeight() + 'px, 0)';
        _applyTransform(this, translate);
      });
      mobileIndex = 0;

      $module.find('.slice-slide, .slice-close-overlay, .slice-mobile-buttons').addClass('ghost');
      $cont.removeClass('opened');
      //_alignMobile();
      $cont.css('height', 'auto');

    } else {

      _makeSelected('0');
      _jumpFrame(0);
      $module.find('.slice-close-overlay, .slice-buttons, .slice-copy').addClass('ghost');

    }

    _centerModule(viewport);

    $stage.attr('data-stop', 0);
    $contbg.removeClass('opened');
    $cont.removeClass('opened');
    $('body').removeClass('overlay-lock');
    $module.find('.slice-no-scroll, .slice-intro').removeClass('ghost');

  }

  function _applyTransform(elem, translate) {
    elem.style.transform = translate;
    elem.style.mozTransform = translate;
    elem.style.webkitTransform = translate;
  }

  /* ---- Alignment Methods ---- */
  /* -- Center Module -- */
  function _centerModule(viewport) {
      if (viewport === 'mobile') {
        var elementOffsetTop = $mobileStage.offset().top,
          truMid = (elementOffsetTop);
      } else {
        var winHeight = window.innerHeight,
          winHalf = (winHeight / 2),
          contHeight = $cont.height(),
          contMiddle = contHeight / 2,
          elementOffsetTop = $cont.offset().top,
          truMid = (elementOffsetTop + contMiddle - winHalf);
      }
      //$('html, body').scrollTop(truMid);
      $('html,body').animate({
          scrollTop: (truMid)
      });
  }

  /* -- Align Mobile -- */
  function _alignMobile() {
    $cont.css('height', _getHeight());
    $mobileStage.css('height', _getHeight());
    $slides.css('height', _getHeight());
    show(mobileIndex);    
  }

  /* -- Viewport Orientation Methods -- */
  function _getHeight() {
    return Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
  }

  function _getWidth() {
    return Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
  }

  function _getOrientation() {
    var orientation = 'landscape';
    if (_getHeight() > _getWidth()) {
      orientation = 'portrait';
    }
    return orientation;
  }

  /* ---- Utility Functions ---- */
  function _throttle(fn, threshhold, scope) {
    threshhold || (threshhold = 250);
    var last,
        deferTimer;
    return function () {
      var context = scope || this;
      var now = +new Date,
          args = arguments;
      if (last && now < last + threshhold) {
        clearTimeout(deferTimer);
        deferTimer = setTimeout(function () {
          last = now;
          fn.apply(context, args);
        }, threshhold);
      } else {
        last = now;
        fn.apply(context, args);
      }
    };
  }

  $(window).on("orientationchange",function(){    
    setTimeout(function() {
        _alignMobile();
    }, 100);
  });









  /**
   * Carousel
   * @param container
   * @param direction
   * @constructor
   */
  this.container = $mobileStage[0];
  this.direction = Hammer.DIRECTION_VERTICAL;

  this.panes = $module.parents('.section').find('.slice-mobile-stage .slice-slide:not(.slice-slide-intro)');
  this.containerSize = _getHeight();




  /**
   * show a pane
   * @param {Number} showIndex
   * @param {Number} direction
   * @param {Number} [percent] percentage visible
   \     */
  var show = function(showIndex, direction, percent, animate, copy) {
    showIndex = Math.max(0, Math.min(showIndex, this.panes.length - 1));
    percent = (percent * 1.25) || 0;
    var pos, translate, $targetPane,
      target = 0;

    if (direction === 8) {
      target = showIndex;
      pos = (this.containerSize / 100) * percent;
    } else if (direction === 16) {
      target = showIndex - 1;
      pos = ((this.containerSize / 100) * percent) - this.containerSize;
    }

    $targetPane = $('.slice-slide-' + target);
    if(copy) {
      $targetPane = $targetPane.find('.slice-slide-copy');
      pos = pos + this.containerSize;
    } else {
      mobileIndex = showIndex;
    }

    translate = 'translate3d(0, ' + pos + 'px, 0)';
    $targetPane.css('transform',  translate);
  };

  var go = function(showIndex, direction, copy){
    showIndex = Math.max(0, Math.min(showIndex, this.panes.length - 1));

    if (_isOpened($cont)) {

      var copyTarget = document.querySelector('div.slice-slide-' + showIndex);


      if (copy) {
        if (direction === 8) {
          copyTarget.classList.add('show-copy');
        } else if (direction === 16) {
          copyTarget.classList.remove('show-copy');
        }
      } else {

        var slideTargets = document.querySelectorAll('div.slice-slide:not(.slice-slide-intro)');
        for (i = 0; i < slideTargets.length; i++) {
          if (i < showIndex) {
            slideTargets[i].style.transform = 'translate3d(0, -' + (this.containerSize * 1.25) + 'px, 0)';
            slideTargets[i].classList.add('show-copy');
          } else if (i > showIndex) {
            slideTargets[i].style.transform = 'translate3d(0, ' + (this.containerSize * .20) + 'px, 0)';
          } else {
            slideTargets[i].style.transform = 'translate3d(0, 0, 0)';
          }
        }
      }

      $module.find('.slice-mobile-buttons a').removeClass('selected');
      $module.find('.slice-mobile-buttons a.slice-marker-' + showIndex).addClass('selected');

    }
    mobileIndex = showIndex;
  };

  var onPan = function (ev) {
    var delta = ev.deltaY;
    var direction = ev.direction;
    var percent = (100 / this.containerSize) * delta;
    var animate = false;
    var copy = true;
    var target = document.querySelector('div.slice-slide-' + mobileIndex);

    if ((direction == 8 && target.classList.contains('show-copy')) || (direction == 16 && !target.classList.contains('show-copy'))) {
      copy = false;
    }

    if (ev.type == 'panend' || ev.type == 'pancancel') {
      if (Math.abs(percent) > 10 &&  ev.type == 'panend' && !copy) {
        mobileIndex += (percent < 0) ? 1 : -1;
      } else if (Math.abs(percent) > 10 && ev.type == 'panend' && !copy) {
        mobileIndex += (percent < 0) ? 1 : -1;
      }
      go(mobileIndex, direction, copy);
    }
  };
  if (this.container) {
    this.hammer = new Hammer.Manager(this.container);
    this.hammer.add(new Hammer.Pan({ direction: this.direction, threshold: 10 }));
    this.hammer.on("panstart panmove panend pancancel", Hammer.bindFn(onPan, this));
  }












  /* ---- Exposed Functions ---- */
  return {
    init: init,
    reInit: reInit,
    buttonClick: buttonClick
  };
})();

  $(function() {

      function initScreen () {
        if (isMobileDevice()) {
          slice.init('mobile');
        } else {
          if (window.innerWidth < 800) {          
            slice.init('mobile');
          } else {
            slice.init('desktop');
          }
        }       
      }

      function resize() {
        if (!isMobileDevice()) {
          if (window.innerWidth < 800) {
            slice.reInit('mobile');         
          } else {
            slice.reInit('desktop');          
          }
        }     
      }

      function isMobileDevice(){
          return (( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) ? true : false);
      }

      if (slice){ 
        $(window).on('resize', function () {
            setTimeout(function() {
                resize();
            }, 100);
        }); 
        initScreen();
      }
  });

});  
