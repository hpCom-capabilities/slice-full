(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _scrollTo = require('./scroll-to');

Object.keys(_scrollTo).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _scrollTo[key];
    }
  });
});

var _pageConfig = require('./page-config');

Object.keys(_pageConfig).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _pageConfig[key];
    }
  });
});

var _utils = require('./utils');

Object.keys(_utils).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _utils[key];
    }
  });
});

var _metrics = require('./metrics');

Object.keys(_metrics).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _metrics[key];
    }
  });
});

var _sliderHelper = require('./slider-helper');

Object.keys(_sliderHelper).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _sliderHelper[key];
    }
  });
});

},{"./metrics":3,"./page-config":4,"./scroll-to":5,"./slider-helper":6,"./utils":8}],2:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
var eventBus = exports.eventBus = {
  on: function on(eventName, handler) {
    var _this = this;

    $(this).on(eventName, handler);
    return function () {
      _this.off(eventName, handler);
    };
  },
  off: function off(eventName, handler) {
    $(this).off(eventName, handler);
  },
  trigger: function trigger(eventName) {
    for (var _len = arguments.length, data = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
      data[_key - 1] = arguments[_key];
    }

    $(this).triggerHandler(eventName, [].concat(data));
  }
};

},{}],3:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Metrics = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _baseComponent = require('../../components/base-component');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Metrics = exports.Metrics = function (_BaseComponent) {
  _inherits(Metrics, _BaseComponent);

  _createClass(Metrics, [{
    key: 'defaults',
    get: function get() {
      return $.extend({}, _get(Metrics.prototype.__proto__ || Object.getPrototypeOf(Metrics.prototype), 'defaults', this), {
        metricEvent: "click",
        trackPrefix: "2.0",
        trackName: "new.link",
        metaToCollect: [{ name: "bu" }, { name: "sub_bu" }, { name: "simple_title" }],
        attrsToCollect: [{ name: "identifier" }, { name: "title" }]
      });
    }
  }]);

  function Metrics() {
    var _ref;

    _classCallCheck(this, Metrics);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    var _this = _possibleConstructorReturn(this, (_ref = Metrics.__proto__ || Object.getPrototypeOf(Metrics)).call.apply(_ref, [this].concat(args)));

    $(function () {
      return _this._init();
    });
    return _this;
  }

  _createClass(Metrics, [{
    key: '_init',
    value: function _init() {
      var _this2 = this;

      var meta = $("meta"),
          metaValue = '',
          selector = '.flex2-molecule[data-metrics-identifier] ' + 'a[href]:not([href*="slideshare"], [href="#"], [href*="javascript:void(0)"]), ' + '.flex2-molecule[data-metrics-identifier] a.js_overlay_trigger, ' + '.flex2-molecule[data-metrics-identifier] a.action-trigger, ' + '.flex2-molecule[data-metrics-identifier] a.iframe-popup-trigger, ' + '.js_menu_holder a[href]:not([href="#"], [href*="javascript:void(0)"]),' + 'a[data-metrics-identifier]';
      this.languageCode = document.documentElement.lang.split("-")[0]; //ja||us
      this.countryCode = document.documentElement.lang.split("-")[1]; //jp||en
      this.metaValues = [];
      this.links = [];
      this.timer = null;

      for (var q = 0; q < this.options.metaToCollect.length; q++) {
        metaValue = meta.filter("*[name='" + this.options.metaToCollect[q].name + "']").attr("content");
        if (metaValue) {
          if (this.options.metaToCollect[q].name == 'simple_title') {
            if (metaValue.length > 40) {
              metaValue = metaValue.substr(0, 40);
            }
          }
          this.metaValues.push(metaValue);
        }

        if (this.options.metaToCollect[q].name == 'sub_bu') {
          this.metaValues.push(this.countryCode);
          this.metaValues.push(this.languageCode);
        }
      }
      $(document).on('click', selector, function (e) {
        _this2.delayedTrackCall(e);
      });
      $(document).on('mousedown touchstart', '.metalocator-buy-wrapper', function (e) {
        _this2.delayedTrackCall(e);
      });

      $(document).on('sendMetric', function (e) {
        _self.sendMetric(e.$link);
      });
    }
  }, {
    key: 'delayedTrackCall',
    value: function delayedTrackCall(e) {
      var _this3 = this;

      if (!this.timer) {
        this.timer = setTimeout(function () {
          clearTimeout(_this3.timer);
          _this3.timer = null;
        }, 100);
        if (e.which !== 3) {
          this.sendMetric($(e.currentTarget));
        }
      }
    }
  }, {
    key: 'sendMetric',
    value: function sendMetric(elementToTrack) {
      var metricTargetProps = [],
          metricsType = '',
          metricValue = "",
          props = this.options.attrsToCollect;

      metricTargetProps = this.collectTrackingValue(elementToTrack, props);
      metricValue = this.options.trackPrefix + "/" + this.metaValues.join("/") + "/" + metricTargetProps;
      metricsType = elementToTrack.attr('data-metrics-link-type') || '';

      //handle collapse/expand links, ignore sending metrics for expanded state
      if (elementToTrack.attr('data-disable-metrics')) {
        return;
      }

      //videos & popups
      if (elementToTrack.hasClass('js_overlay_trigger') || elementToTrack.hasClass('iframe-popup-trigger')) {
        metricsType = 'link';
      }

      if (metricsType) {
        try {
          console.log('trackMetrics:' + this.options.trackName + ', name: ' + metricValue + ', type: ' + metricsType);
          trackMetrics(this.options.trackName, { name: metricValue, type: metricsType });
        } catch (excpt) {
          console.log(excpt.message);
        };
      } else {
        console.log('trackMetrics has NOT beet called cause data-metrics-link-type param is not set to a link');
      }

      return true;
    }
  }, {
    key: 'collectTrackingValue',
    value: function collectTrackingValue(target, props) {

      var valueArray = [],
          value = "";

      if (target.length === 0) {
        return value;
      }

      for (var i = 0; i < props.length; i++) {
        var metricCnt = '';

        if (props[i].name == 'identifier') {
          if (target.parents('.flex2-molecule[data-metrics-identifier]').length) {
            metricCnt = target.parents('.flex2-molecule[data-metrics-identifier]').attr('data-metrics-identifier');
          } else {
            if (target.attr('data-metrics-identifier') !== 'undefined') {
              metricCnt = target.attr('data-metrics-identifier');
            } else {
              metricCnt = '';
            }
          }
        }

        if (props[i].name == 'title') {

          if (target.parents('.flex2-molecule').length) {
            metricCnt = target.parents('.flex2-molecule').attr("data-metrics-" + props[i].name);
          } else if (target.parents('.js_menu_holder').length) {
            metricCnt = target.parents('.js_menu_holder').attr("data-metrics-" + props[i].name);
          }

          var targetTitle = target.attr("data-metrics-" + props[i].name);

          if (targetTitle) {
            if (target.hasClass('js_overlay_trigger') || target.hasClass('iframe-popup-trigger')) {
              if (targetTitle.length > 20) {
                targetTitle = targetTitle.substr(0, 20);
              }
            }
            if (metricCnt) {
              metricCnt = metricCnt + '/' + targetTitle;
            } else {
              metricCnt = targetTitle;
            }
          }
        }

        if (metricCnt) {
          valueArray.push(metricCnt);
        }
      }

      if (valueArray.length) {
        value = valueArray.join('/');
      }

      return value;
    }
  }]);

  return Metrics;
}(_baseComponent.BaseComponent);

},{"../../components/base-component":11}],4:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var PageConfig = exports.PageConfig = function () {
  function PageConfig() {
    _classCallCheck(this, PageConfig);

    this.data = {};

    //Adding global HP if it is not defined.
    !window.HP && (window.HP = {});

    //Adding pageConfig, cause on some pages there is inline usage of HP.pageConfig -> errors if not defined
    window.HP.pageConfig = this;
  }

  _createClass(PageConfig, [{
    key: "has",
    value: function has(key) {
      return Object.prototype.hasOwnProperty.call(this.data, key);
    }
  }, {
    key: "set",
    value: function set(key, value) {
      if (value === undefined || value === this.data[key]) return false;

      if (this.has(key) && _typeof(this.get(key)) === "object" && (typeof value === "undefined" ? "undefined" : _typeof(value)) === "object") {
        for (var prop in value) {
          this.data[key][prop] = value[prop];
        }
      } else {
        this.data[key] = value;
      }

      return this.data[key];
    }
  }, {
    key: "get",
    value: function get(key) {
      return this.data[key];
    }
  }, {
    key: "drop",
    value: function drop(key) {
      key = key || null;

      if (key === null) {
        delete this.data;
        this.data = {};
      } else if (this.has(key)) {
        delete this.data[key];
      }
    }
  }]);

  return PageConfig;
}();

},{}],5:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var ScrollTo = exports.ScrollTo = function () {
  function ScrollTo() {
    _classCallCheck(this, ScrollTo);

    this.resolveAnchorsRows();
    this._initMobileScrolling();
    this.animating = false;
  }

  _createClass(ScrollTo, [{
    key: 'resolveAnchorsRows',
    value: function resolveAnchorsRows() {
      var _this = this;

      //editors can not put an anchor to a ROW. 
      $(function () {
        //if anchor is set via custom block with id -> set it to closest ROW
        $('.row-anchor-helper').each(function (index, el) {
          var $el = $(el),
              id = $el.attr('id');

          if (id) {
            $el.closest('.section').attr('data-anchor', id);
            $el.remove();
          }
        });
        //if anchor is set via module data-anchor with no "anchor-to-module" classname
        // -> set it to closest ROW
        $('.flex2-molecule:not(.anchor-to-module)[data-anchor]').each(function (index, el) {
          var $el = $(el),
              anchor = $el.attr('data-anchor');

          $el.removeAttr('data-anchor').closest('.section').attr('data-anchor', anchor);
        });
        $(window).trigger('anchors-resolved');
        window.GLOBALS.anchorsResolved = true;
        _this.checkAnchorInUrl();
      });
    }
  }, {
    key: '_initMobileScrolling',
    value: function _initMobileScrolling() {
      window.requestAnimFrame = function () {
        return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || function (callback) {
          window.setTimeout(callback, 1000 / 60);
        };
      }();
    }
  }, {
    key: 'navigate',
    value: function navigate(hash, cb, duration, el, offset) {
      var $el = void 0,
          anchorEl = void 0,
          top = void 0,
          updatedNavHeight = void 0,
          newTop = void 0;
      offset = offset || 0;
      hash = el ? '' : hash.replace('#', '');
      anchorEl = el || $('div[data-anchor="' + hash + '"]');

      $el = anchorEl.length ? anchorEl : $('#' + hash);

      if ($el.length) {
        top = Math.floor($el.offset().top + 1);
        updatedNavHeight = $('.molecule-lb-835').outerHeight();
        newTop = +top - +updatedNavHeight + offset;
        this._animate(newTop, duration, cb);
        return true;
      }
      return false;
    }
  }, {
    key: 'navigateToOffset',
    value: function navigateToOffset(offset, duration, cb, includeNavHeight) {
      var scrollTop = $(window).scrollTop(),
          newTop = scrollTop + offset;

      includeNavHeight && (newTop -= $('.molecule-lb-835').outerHeight());
      this._animate(newTop, duration, cb);
    }
  }, {
    key: 'navigateToSection',
    value: function navigateToSection(hash, cb, duration, el, offset) {
      var $el = void 0,
          anchorEl = void 0,
          top = void 0,
          newTop = void 0;
      offset = offset || 0;
      hash = el ? '' : hash.replace('#', '');
      anchorEl = el || $('div[data-anchor="' + hash + '"]');

      $el = anchorEl.length ? anchorEl : $('#' + hash);

      if ($el.length) {
        top = Math.floor($el.offset().top + 1);
        newTop = +top + offset;
        this._animate(newTop, duration, cb);
        return true;
      }
      return false;
    }
  }, {
    key: 'navigateTo',
    value: function navigateTo(offset, duration, cb) {
      this._animate(offset, duration, cb);
    }
  }, {
    key: '_animate',
    value: function _animate(newTop) {
      var duration = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 600;
      var cb = arguments[2];

      this.animating = true;

      if (GLOBALS.Utils.device.isMobile) {
        this._animateMobile(newTop, duration, 'easeInOutCubic', cb);
      } else {
        this._animateDefault(newTop, duration, 'easeInOutCubic', cb);
      }
    }
  }, {
    key: '_animateDefault',
    value: function _animateDefault(newTop, duration, easing, cb) {
      var _this2 = this;

      $('html, body').stop().clearQueue().animate({
        scrollTop: newTop + 'px'
      }, duration, easing, function () {
        typeof cb === 'function' && cb();
        _this2.animating = false;
      });
    }
  }, {
    key: '_animateMobile',
    value: function _animateMobile(newTop, duration, easing, cb) {
      this._scrollToMobile(newTop, duration, easing, function () {
        this.animating = false;
        cb && cb();
      }.bind(this));
    }
  }, {
    key: '_scrollToMobile',
    value: function _scrollToMobile(scrollToY, duration, easing, cb) {
      var scrollY = window.scrollY || window.pageYOffset,
          scrollTo = scrollToY || 0,
          currentTime = 0,
          time = duration / 1000,
          easingEquations = {
        easeInOutCubic: function easeInOutCubic(pos) {
          if ((pos /= 0.5) < 1) return 0.5 * Math.pow(pos, 3);
          return 0.5 * (Math.pow(pos - 2, 3) + 2);
        }
      };

      easing = easing || 'easeInOutCubic';

      function tick() {
        currentTime += 1 / 60;

        var p = currentTime / time,
            t = easingEquations[easing](p);

        if (p < 1) {
          requestAnimFrame(tick);
          window.scrollTo(0, scrollY + (scrollTo - scrollY) * t);
        } else {
          window.scrollTo(0, scrollTo);
          cb && cb();
        }
      }
      tick();
    }
  }, {
    key: 'checkAnchorInUrl',
    value: function checkAnchorInUrl() {
      var _this3 = this;

      var urlAnchor = window.location.hash;
      if (urlAnchor) {
        urlAnchor = urlAnchor && urlAnchor.replace(/^#!/, '#');
        $(window).on('load', function () {
          return _this3.navigate(urlAnchor);
        });
      }
    }
  }]);

  return ScrollTo;
}();

},{}],6:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var SliderHelper = exports.SliderHelper = function () {
  function SliderHelper() {
    _classCallCheck(this, SliderHelper);
  }

  _createClass(SliderHelper, [{
    key: 'alignArrowsToEl',


    /**
     * @param {jquery DOM el} slider - slider to work with
     * @param {object} options - configuration
     * @description - alignes position of arrow to the middle of a certain item within a slider
     *                Item is specified in options.alignToElSelector. Item is a single DOM el from 
     *                first slide for all slides
     */
    value: function alignArrowsToEl(slider, options) {
      var arrows = slider.find('.slick-arrow').css('margin-top', 0),
          alignToEl = slider.find('.slick-current ' + options.alignToElSelector).first();

      function _align() {
        var arrowHeight = arrows.first().height(),
            alignToElHeight = alignToEl.height();

        alignToElHeight && arrows.css('top', (alignToElHeight - arrowHeight) / 2);
      }
      _align();

      slider.on('setPosition', _.debounce(function () {
        return _align();
      }, 50));
    }

    /**
     * @param {jquery DOM el} takeHeightFromEl - element, which height should be used to apply to the other el
     * @param {jquery DOM el} applyHeightToEl - element, that should accept a new height from takeHeightFromEl
     * @description - alignes heights of multiple elements {applyHeightToEls}, based on {takeHeightFromEl} height
     */

  }, {
    key: 'alignElementsHeight',
    value: function alignElementsHeight(slider, takeHeightFromEl, applyHeightToEls) {
      if (!takeHeightFromEl || !applyHeightToEls) return;
      function _align() {
        var height = takeHeightFromEl.height();

        applyHeightToEls.height(height);
      }
      _align();

      slider.on('setPosition', _.debounce(function () {
        return _align();
      }, 50));
    }
  }]);

  return SliderHelper;
}();

},{}],7:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Stickable = exports.Stickable = function () {
  _createClass(Stickable, [{
    key: 'options',
    get: function get() {
      return {
        type: 'regular', // regular|bounding. Defines type of skickable behaviour
        stickyClass: 'sticky',
        offsetTop: 120,
        offsetBottom: 0
      };
    }
  }]);

  function Stickable(stickableEl, staticParent, opt) {
    _classCallCheck(this, Stickable);

    this.opts = $.extend({}, this.options, opt);
    this.el = stickableEl;
    this.staticParent = staticParent || stickableEl.closest('.section');
    this._init();
  }

  _createClass(Stickable, [{
    key: '_init',
    value: function _init() {
      var _this = this;

      this.calculate = function () {
        return _this._calculate()[_this.opts.type]();
      };
      this.calculate();

      $(window).on('scroll', this.calculate);
      this._destroy = function () {
        return $(window).off('scroll', _this.calculate);
      };
    }
  }, {
    key: '_calculate',
    value: function _calculate() {
      var _this2 = this;

      return {
        'regular': function regular() {
          var top = _this2.staticParent.offset().top;
          _this2.el[$(window).scrollTop() + 1 > top ? 'addClass' : 'removeClass'](_this2.opts.stickyClass);
        },
        'bounding': function bounding() {
          var clientRect = _this2.staticParent[0].getBoundingClientRect(),
              wHeight = $(window).height();
          _this2.el[clientRect.top + _this2.opts.offsetTop < wHeight && clientRect.bottom - _this2.opts.offsetBottom > wHeight ? 'addClass' : 'removeClass'](_this2.opts.stickyClass);
        },
        'bottom': function bottom() {
          var scrollTop = $(window).scrollTop();
          _this2.el[scrollTop > _this2.opts.offsetTop ? 'addClass' : 'removeClass'](_this2.opts.stickyClass);
        }
      };
    }
  }, {
    key: 'destroy',
    value: function destroy() {
      this.el.removeClass(this.opts.stickyClass);
      this._destroy();
    }
  }]);

  return Stickable;
}();

},{}],8:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Utils = exports.Utils = function () {
  function Utils() {
    _classCallCheck(this, Utils);

    this._init();
  }

  _createClass(Utils, [{
    key: '_init',
    value: function _init() {
      this.device = {
        isMobile: 'ontouchstart' in window || navigator.msMaxTouchPoints || false,
        isLandscape: function isLandscape() {
          return window.matchMedia("(orientation: landscape)").matches;
        }
      };
      if (this.device.isMobile) {
        $(function () {
          return $('body').addClass('lb-mobile');
        });
      }

      //Extend video options to have "isPlaying" param
      Object.defineProperty(HTMLMediaElement.prototype, 'isPlaying', {
        get: function get() {
          return !!(this.currentTime > 0.01 && !this.paused && !this.ended && this.readyState > 2);
        }
      });
      this._initCache();
    }
  }, {
    key: '_parseQuery',
    value: function _parseQuery(qstr) {
      var query = {},
          pairs = (qstr[0] === '?' ? qstr.substr(1) : qstr).split('&');

      for (var i = 0; i < pairs.length; i++) {
        var pair = pairs[i].split('=');
        query[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1] || '');
      }
      return query;
    }
  }, {
    key: '_initCache',
    value: function _initCache() {
      var _this = this;

      var cache = {};
      this.cache = {
        set: function set(name, value) {
          cache[name] = [value];
        },
        push: function push(name, value) {
          if (cache[name]) {
            cache[name].push(value);
          } else {
            _this.cache.set(name, value);
          }
        },
        get: function get(name) {
          return cache[name] || [];
        },
        remove: function remove(name) {
          delete cache[name];
        }
      };
    }
  }]);

  return Utils;
}();

},{}],9:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Banners = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _baseComponent = require('../base-component.js');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Banners = exports.Banners = function (_BaseComponent) {
  _inherits(Banners, _BaseComponent);

  _createClass(Banners, [{
    key: 'defaults',
    get: function get() {
      return $.extend({}, _get(Banners.prototype.__proto__ || Object.getPrototypeOf(Banners.prototype), 'defaults', this), {
        DEFAULT_FLIP_BP: '720'
      });
    }
  }], [{
    key: 'selector',
    get: function get() {
      return '.section.full[style*="url"][data-secondary-background]:not(.custom-master):not(.custom-slave)';
    }
  }]);

  function Banners() {
    var _ref;

    _classCallCheck(this, Banners);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    var _this = _possibleConstructorReturn(this, (_ref = Banners.__proto__ || Object.getPrototypeOf(Banners)).call.apply(_ref, [this].concat(args)));

    _this._init();
    return _this;
  }

  _createClass(Banners, [{
    key: '_init',
    value: function _init() {
      this._initBgFlip();
    }
  }, {
    key: '_initBgFlip',
    value: function _initBgFlip() {
      var customBP = this._getWrapBreakpoint(),
          mobileQuery = window.matchMedia('screen and (max-width: ' + customBP + 'px)'),
          url = this.el.css('background-image').replace(/url\(['"]*(.*?)['"]*\)/, '$1');
      this.el.attr('data-primary-background', url);
      this._changeBackgrounds(mobileQuery);
      mobileQuery.addListener(this._changeBackgrounds.bind(this));
    }
  }, {
    key: '_changeBackgrounds',
    value: function _changeBackgrounds(mql) {
      var image = mql.matches ? 'data-secondary-background' : 'data-primary-background';
      this.el.css('background-image', 'url("' + this.el.attr(image) + '")');
    }
  }, {
    key: '_getWrapBreakpoint',
    value: function _getWrapBreakpoint() {
      var result = /flip-bgs-breakpoint-(.*?)(\D|$)/.exec(this.el.attr('class'));
      if (result) {
        return result[1];
      }
      return this.options.DEFAULT_FLIP_BP;
    }
  }]);

  return Banners;
}(_baseComponent.BaseComponent);

},{"../base-component.js":11}],10:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CustomHeight = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _baseComponent = require('../base-component.js');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var CustomHeight = exports.CustomHeight = function (_BaseComponent) {
  _inherits(CustomHeight, _BaseComponent);

  _createClass(CustomHeight, null, [{
    key: 'selector',
    get: function get() {
      return '.section.custom-height:not(.parallax-panel)';
    }
  }]);

  function CustomHeight() {
    var _ref;

    _classCallCheck(this, CustomHeight);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    var _this = _possibleConstructorReturn(this, (_ref = CustomHeight.__proto__ || Object.getPrototypeOf(CustomHeight)).call.apply(_ref, [this].concat(args)));

    _this._init();
    return _this;
  }

  _createClass(CustomHeight, [{
    key: '_init',
    value: function _init() {
      this._initCustomHeight();
    }
  }, {
    key: '_initCustomHeight',
    value: function _initCustomHeight() {
      //pick up inline height from .section and replace it with min-height 
      var style = this.el.attr('style');
      style && this.el.attr('style', style.replace(/height/ig, 'min-height'));
    }
  }]);

  return CustomHeight;
}(_baseComponent.BaseComponent);

},{"../base-component.js":11}],11:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.BaseComponent = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _eventBus = require('../common/scripts/event-bus');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var BaseComponent = exports.BaseComponent = function () {
  _createClass(BaseComponent, [{
    key: 'defaults',
    get: function get() {
      return {};
    }
  }]);

  function BaseComponent(element, options) {
    _classCallCheck(this, BaseComponent);

    this.el = $(element);
    this.options = $.extend({}, this.defaults, options);

    if (this.ROOT_CLASS) {
      this.el.addClass(this.ROOT_CLASS);
    }
  }

  _createClass(BaseComponent, [{
    key: 'on',
    value: function on(eventName, handler) {
      var _this = this;

      this.el.on(eventName, handler);
      // Returning unbind function
      return function () {
        return _this.off(eventName, handler);
      };
    }
  }, {
    key: 'off',
    value: function off(eventName, handler) {
      this.el.off(eventName, handler);
    }
  }, {
    key: 'trigger',
    value: function trigger(eventName) {
      for (var _len = arguments.length, data = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
        data[_key - 1] = arguments[_key];
      }

      this.el.trigger(eventName, [].concat(data));
    }
  }, {
    key: 'broadcast',
    value: function broadcast(eventName) {
      for (var _len2 = arguments.length, data = Array(_len2 > 1 ? _len2 - 1 : 0), _key2 = 1; _key2 < _len2; _key2++) {
        data[_key2 - 1] = arguments[_key2];
      }

      this.trigger.apply(this, [eventName].concat(data));
      _eventBus.eventBus.trigger.apply(_eventBus.eventBus, [eventName, this].concat(data));
    }
  }, {
    key: 'onResize',
    value: function onResize(cb, delay) {
      var method = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'debounce';

      var handler = _.debounce(cb, debounceDelay);
      $(window).on('resize', handler);
      return function () {
        return $(window).off('resize', handler);
      };
    }
  }]);

  return BaseComponent;
}();

},{"../common/scripts/event-bus":2}],12:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CustomFont = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _baseComponent = require('../../components/base-component.js');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var CustomFont = exports.CustomFont = function (_BaseComponent) {
  _inherits(CustomFont, _BaseComponent);

  _createClass(CustomFont, [{
    key: 'defaults',
    get: function get() {
      return {
        medias: {
          desktop: 'screen and (min-width: 1281px)',
          tablet: 'screen and (max-width: 1280px) and (min-width: 721px)',
          mobile: 'screen and (max-width: 720px)'
        }
      };
    }
  }], [{
    key: 'selector',
    get: function get() {
      return '[style*="tablet"], [style*="mobile"]';
    }
  }]);

  function CustomFont() {
    var _ref;

    _classCallCheck(this, CustomFont);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    var _this = _possibleConstructorReturn(this, (_ref = CustomFont.__proto__ || Object.getPrototypeOf(CustomFont)).call.apply(_ref, [this].concat(args)));

    _this._init();
    return _this;
  }

  _createClass(CustomFont, [{
    key: '_init',
    value: function _init() {
      var _this2 = this;

      this._retrieveCustomStyles();
      this._updateDataAttrs();
      for (var i in this.options.medias) {
        var media = window.matchMedia(this.options.medias[i]);
        media.addListener(function (media) {
          return _this2.update(media);
        });
        this.update(media);
      }
    }
  }, {
    key: '_retrieveCustomStyles',
    value: function _retrieveCustomStyles() {
      var style = this.el.attr('style');
      if (style) {
        var desktop = style.replace(/\/\*(.*?)\*\//ig, ''),
            tablet = style.match(/\/\*tablet(.*?)\*\//),
            mobile = style.match(/\/\*mobile(.*?)\*\//);

        tablet = tablet ? tablet[1] : '';
        mobile = mobile ? mobile[1] : '';
        desktop && this.el.attr('data-style-desktop', desktop);
        tablet && this.el.attr('data-style-tablet', tablet);
        mobile && this.el.attr('data-style-mobile', mobile);
      }
    }
  }, {
    key: '_updateDataAttrs',
    value: function _updateDataAttrs() {
      !this.el.attr('data-style-desktop') && this.el.attr('data-style-desktop', '');
      if (!this.el.attr('data-style-mobile') && this.el.attr('data-style-tablet')) {
        this.el.attr('data-style-mobile', this.el.attr('data-style-tablet'));
      }
    }
  }, {
    key: 'update',
    value: function update(media) {
      if (media.matches) {
        switch (media.media) {
          case this.options.medias.desktop:
            {
              this.setStyle('desktop');
              break;
            }
          case this.options.medias.tablet:
            {
              this.setStyle('tablet');
              break;
            }
          case this.options.medias.mobile:
            {
              this.setStyle('mobile');
              break;
            }
        }
      }
    }
  }, {
    key: 'setStyle',
    value: function setStyle(screen) {
      var style = this.el.data('style-' + screen) || this.el.data('style-desktop');
      this.el.attr('style', style);
    }
  }]);

  return CustomFont;
}(_baseComponent.BaseComponent);

},{"../../components/base-component.js":11}],13:[function(require,module,exports){
'use strict';

/*NOTE, order matters. If you need to update html of some element before other 
 *components will be initialized - put it's logic on top
 */

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _customFont = require('./common/custom-font');

Object.keys(_customFont).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _customFont[key];
    }
  });
});

var _grid = require('../grid/scripts/grid');

Object.keys(_grid).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _grid[key];
    }
  });
});

var _sliders = require('./sliders/sliders');

Object.keys(_sliders).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _sliders[key];
    }
  });
});

var _banners = require('./banners/banners');

Object.keys(_banners).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _banners[key];
    }
  });
});

var _customHeight = require('./banners/custom-height');

Object.keys(_customHeight).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _customHeight[key];
    }
  });
});

var _overlays = require('./overlays/overlays');

Object.keys(_overlays).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _overlays[key];
    }
  });
});

},{"../grid/scripts/grid":21,"./banners/banners":9,"./banners/custom-height":10,"./common/custom-font":12,"./overlays/overlays":15,"./sliders/sliders":18}],14:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Overlay = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _baseComponent = require('../../base-component.js');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Overlay = exports.Overlay = function (_BaseComponent) {
  _inherits(Overlay, _BaseComponent);

  _createClass(Overlay, [{
    key: 'defaults',
    get: function get() {
      return $.extend({}, _get(Overlay.prototype.__proto__ || Object.getPrototypeOf(Overlay.prototype), 'defaults', this), {
        closeButtonSelector: '.js_pop_close',
        overlayWrapperClass: 'overlay-wrapper body',
        openedClass: 'opened',
        noScrollClass: 'no-scroll'
      });
    }
  }], [{
    key: 'OPEN_EVENT',
    get: function get() {
      return 'overlay-opened';
    }
  }, {
    key: 'CLOSE_EVENT',
    get: function get() {
      return 'overlay-closed';
    }
  }]);

  function Overlay() {
    var _ref;

    _classCallCheck(this, Overlay);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    var _this = _possibleConstructorReturn(this, (_ref = Overlay.__proto__ || Object.getPrototypeOf(Overlay)).call.apply(_ref, [this].concat(args)));

    _this.closeButton = _this.el.find(_this.options.closeButtonSelector);
    _this.openers = $('a[rel="' + _this.el.attr('id') + '"]');
    _this.el.wrap('<div class="' + _this.options.overlayWrapperClass + '"></div>').parent().appendTo(document.body);
    _this._initListeners();
    _this._innerModules = _this.el.find('[class*="flex2-molecule"]');
    return _this;
  }

  _createClass(Overlay, [{
    key: '_initListeners',
    value: function _initListeners() {
      var _this2 = this;

      this.openers.on('click', function (e) {
        return _this2.open(e);
      });
      this.closeButton.on('click', function () {
        return _this2.close();
      });
      this.el.parent().on('click', function (e) {
        $(e.target).hasClass('overlay-wrapper') && _this2.close();
      });
    }
  }, {
    key: 'open',
    value: function open(e) {
      var _this3 = this;

      this._innerModules.trigger('overlay.beforeopen');
      this._onOpen();
      this._innerModules.trigger('overlay.opened');

      //one time listen for ESC pressed to close overlay
      $(document).one('keyup.overlay', function (e) {
        e.keyCode == 27 && _this3.close();
      });
      e.preventDefault();
    }
  }, {
    key: '_onOpen',
    value: function _onOpen() {
      this._preventScrolling();

      this.el.addClass(this.options.openedClass);
      this.el.parent().addClass(this.options.openedClass);
    }
  }, {
    key: 'close',
    value: function close() {
      $(document).off('keyup.overlay');
      this._onClose();
    }
  }, {
    key: '_onClose',
    value: function _onClose() {
      this.el.removeClass(this.options.openedClass);
      this.el.parent().removeClass(this.options.openedClass);
      this._innerModules.trigger('overlay.closed');
      this._releaseScrolling();
    }
  }, {
    key: '_preventScrolling',
    value: function _preventScrolling() {
      $(document.body).addClass(this.options.noScrollClass);
    }
  }, {
    key: '_releaseScrolling',
    value: function _releaseScrolling() {
      $(document.body).removeClass(this.options.noScrollClass);
    }
  }]);

  return Overlay;
}(_baseComponent.BaseComponent);

},{"../../base-component.js":11}],15:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.OverlaysFactory = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _baseComponent = require('../base-component.js');

var _overlay = require('./overlay/overlay.js');

var _videoOverlay = require('./video-overlay/video-overlay.js');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var OverlaysFactory = exports.OverlaysFactory = function () {
  _createClass(OverlaysFactory, [{
    key: 'options',
    get: function get() {
      return {
        closeOverlayIconTpl: '' + '<a href="javascript:void(0);" class="js_pop_close popup-close" title="close button">' + '<span class="screenReading">Close</span>' + '</a>'
      };
    }
  }, {
    key: 'overlayTypes',
    get: function get() {
      return {
        videoOverlaySelector: '.video-container',
        contentOverlayClass: 'content-overlay'
      };
    }
  }], [{
    key: 'selector',
    get: function get() {
      return '.overlay-popup, .content-overlay';
    }
  }]);

  function OverlaysFactory(el) {
    _classCallCheck(this, OverlaysFactory);

    this._init(el);
  }

  _createClass(OverlaysFactory, [{
    key: '_init',
    value: function _init(el) {
      this.el = $(el);
      //Initiate video Overlay
      if (this.el.find(this.overlayTypes.videoOverlaySelector).length && !this.el.hasClass(this.overlayTypes.contentOverlayClass)) {
        new _videoOverlay.VideoOverlay(el);
        return;
      }
      //Initiate content Overlay
      if (this.el.hasClass(this.overlayTypes.contentOverlayClass)) {
        this._prepareContent() && new _overlay.Overlay(this.el[0]);
        return;
      }

      //Initiate regular Overlay, assuming that it is not a special one
      new _overlay.Overlay(el);
    }
  }, {
    key: '_prepareContent',
    value: function _prepareContent() {
      this._removeInlineScripts();
      var popupId = this.el.find('[data-content-overlay-id]').attr('data-content-overlay-id'),
          openers = $('a[href="#' + popupId + '"]');
      if (popupId && openers.length) {
        this.el = this.el.removeClass(this.overlayTypes.contentOverlayClass).wrap('<div class="overlay-popup ' + this.overlayTypes.contentOverlayClass + '"></div>').parent().attr('id', popupId).append(this.options.closeOverlayIconTpl);
        openers.attr('rel', popupId);
      }
      return this.el;
    }

    //remove inline scripts within overlay's content to prevent double evaluation

  }, {
    key: '_removeInlineScripts',
    value: function _removeInlineScripts() {
      this.el.find('script').remove();
    }
  }]);

  return OverlaysFactory;
}();

},{"../base-component.js":11,"./overlay/overlay.js":14,"./video-overlay/video-overlay.js":16}],16:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.VideoOverlay = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _overlay = require('../overlay/overlay.js');

var _youtubePlayer = require('../../video/youtube-player/youtube-player.js');

var _brightcovePlayer = require('../../video/brightcove-player/brightcove-player.js');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var VideoOverlay = exports.VideoOverlay = function (_Overlay) {
  _inherits(VideoOverlay, _Overlay);

  _createClass(VideoOverlay, [{
    key: 'ROOT_CLASS',

    // This class is added automatically to this.el component
    get: function get() {
      return 'video-overlay';
    }
  }, {
    key: 'defaults',
    get: function get() {
      return $.extend({}, _get(VideoOverlay.prototype.__proto__ || Object.getPrototypeOf(VideoOverlay.prototype), 'defaults', this), {
        youtubeVideoSelector: '.youtube-video-template',
        brightcoveVideoSelector: '.bc-video-template',
        videoContainerSelector: '.video-container'
      });
    }
  }]);

  function VideoOverlay() {
    var _ref;

    _classCallCheck(this, VideoOverlay);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    var _this = _possibleConstructorReturn(this, (_ref = VideoOverlay.__proto__ || Object.getPrototypeOf(VideoOverlay)).call.apply(_ref, [this].concat(args)));

    _this._spinner = _this._createVideoSpinner();
    return _this;
  }

  _createClass(VideoOverlay, [{
    key: '_createVideoPlayer',
    value: function _createVideoPlayer() {
      var youtubeVideoElement = this.el.find(this.options.youtubeVideoSelector);
      var brightcoveVideoElement = this.el.find(this.options.brightcoveVideoSelector);
      if (youtubeVideoElement.length) {
        return new _youtubePlayer.YoutubePlayer(youtubeVideoElement);
      } else if (brightcoveVideoElement.length) {
        return new _brightcovePlayer.BrightcovePlayer(brightcoveVideoElement);
      }

      console.warn('No supported video element in VideoOverlay');
      return {};
    }
  }, {
    key: '_createVideoSpinner',
    value: function _createVideoSpinner() {
      if (!this._videoSpinner) {
        this.spinner = $('<div/>', { 'class': 'spinner' });
        this.el.find(this.options.videoContainerSelector).append(this.spinner);
      }
    }
  }, {
    key: '_onOpen',
    value: function _onOpen() {
      var _this2 = this;

      if (!this._videoPlayer) {
        this._videoPlayer = this._createVideoPlayer();
        this._videoPlayer.init(function () {
          return _this2._hideSpinner();
        });
        //if player was not initialized - don't autostart video on mobiles
        if (GLOBALS.Utils.device.isMobile) {
          _get(VideoOverlay.prototype.__proto__ || Object.getPrototypeOf(VideoOverlay.prototype), '_onOpen', this).call(this);
          return false;
        }
      }
      this._startVideo();
      _get(VideoOverlay.prototype.__proto__ || Object.getPrototypeOf(VideoOverlay.prototype), '_onOpen', this).call(this);
    }
  }, {
    key: '_onClose',
    value: function _onClose() {
      this._pauseVideo();
      _get(VideoOverlay.prototype.__proto__ || Object.getPrototypeOf(VideoOverlay.prototype), '_onClose', this).call(this);
    }
  }, {
    key: '_startVideo',
    value: function _startVideo() {
      this._videoPlayer.play();
    }
  }, {
    key: '_pauseVideo',
    value: function _pauseVideo() {
      this._videoPlayer.pause();
    }
  }, {
    key: '_hideSpinner',
    value: function _hideSpinner() {
      this.spinner.fadeOut();
    }
  }]);

  return VideoOverlay;
}(_overlay.Overlay);

},{"../../video/brightcove-player/brightcove-player.js":19,"../../video/youtube-player/youtube-player.js":20,"../overlay/overlay.js":14}],17:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/*Custom LbSlider*/
var LbSlider = exports.LbSlider = function () {
  _createClass(LbSlider, [{
    key: 'defaults',
    get: function get() {
      return {
        sliderClass: 'lb-slider animate',
        noArrowsClass: 'no-arrows',
        currentSlide: 0,
        visibleSlides: 0,
        customClass: '',
        alignArrowsToEl: null,
        hideArrowsWhenCantSlide: true,
        onSlide: function onSlide() {}
      };
    }
  }]);

  function LbSlider(el, options) {
    _classCallCheck(this, LbSlider);

    this.o = $.extend({}, this.defaults, options);
    this._init(el);
  }

  _createClass(LbSlider, [{
    key: '_init',
    value: function _init(el) {
      this._bootstrap(el);
      this._onResize();
      this._activateSlide(this.o.currentSlide);
      this._initListeners();
    }
  }, {
    key: '_bootstrap',
    value: function _bootstrap(el) {
      this.s = {
        el: el,
        slides: $(el).children(),
        currentSlide: this.o.currentSlide,
        nextIndex: 0,
        slidesCount: el.children().length,
        slideWidth: 0,
        trackWidth: 0,
        visibleSlides: this.o.visibleSlides,
        currentTranslate: 0
      };
      this.s.el.addClass(this.o.sliderClass + ' ' + this.o.customClass);
      this._addLbSliderTrack();
      this._addControls();
    }
  }, {
    key: '_addLbSliderTrack',
    value: function _addLbSliderTrack() {
      this.s.list = $('<div />', { class: 'lb-slider-list' });
      this.s.track = $('<div />', { class: 'lb-slider-track' });
      this.s.list.append(this.s.track.append(this.s.slides));
      this.s.el.append(this.s.list.append(this.s.track));
    }
  }, {
    key: '_getTrackWidth',
    value: function _getTrackWidth() {
      return this.s.slideWidth * this.s.slidesCount;
    }
  }, {
    key: '_getSlideWidth',
    value: function _getSlideWidth() {
      if (!this.s.visibleSlides) {
        var regexpr = new RegExp(/span(\d*)/ig),
            slideClass = this.s.slides.first().attr('class'),
            test = regexpr.exec(slideClass);

        this.s.visibleSlides = test && test[1] ? 24 / +test[1] : 1;
      }

      return this.s.el.width() / this.s.visibleSlides;
    }
  }, {
    key: '_addControls',
    value: function _addControls() {
      this.s.nextArrow = $('<button />', { class: 'next', text: 'Next' });
      this.s.prevArrow = $('<button />', { class: 'prev', text: 'Prev' });
      this.s.el.append(this.s.prevArrow).append(this.s.nextArrow);

      //align arrows to some element within slider (e.g. to the middle of the img)
      this.o.alignArrowsToEl && GLOBALS.SliderHelper.alignArrowsToEl(this.s.el, {
        arrows: this.s.nextArrow.add(this.s.prevArrow),
        alignToEl: this.s.slides.first().find(this.o.alignArrowsToEl)
      });
    }
  }, {
    key: '_onResize',
    value: function _onResize() {
      this._updateModel();
      this._updateDOM();
    }
  }, {
    key: '_updateModel',
    value: function _updateModel() {
      this.s.slideWidth = this._getSlideWidth();
      this.s.trackWidth = this._getTrackWidth();
      this.s.maxTranslate = this.s.trackWidth - this.s.el.width();
      this.s.maxTranslate = this.s.maxTranslate < 0 ? 0 : this.s.maxTranslate;
    }
  }, {
    key: '_updateDOM',
    value: function _updateDOM() {
      this.s.slides.width(this.s.slideWidth);
      this.s.track.width(this.s.trackWidth);
      this.slideTo(this.s.currentSlide);

      //hide arrows when no content to scroll
      if (this.o.hideArrowsWhenCantSlide) {
        this.s.el[this.s.el.width() >= this.s.trackWidth ? 'addClass' : 'removeClass'](this.o.noArrowsClass);
      }

      this.s.el.trigger('setPosition');
    }
  }, {
    key: '_initListeners',
    value: function _initListeners() {
      var _this = this;

      $(window).on('resize orientationchange load', _.debounce(function () {
        return _this._onResize();
      }, 50));
      this.s.nextArrow.on('click', function () {
        return _this.slideNext();
      });
      this.s.prevArrow.on('click', function () {
        return _this.slidePrev();
      });
      this.s.slides.on('click', function (e) {
        return _this.handleSlideClick(e);
      });
      this._initMobileEvents();
    }
  }, {
    key: 'handleSlideClick',
    value: function handleSlideClick(e) {
      this.slideTo($(e.currentTarget).index());
    }
  }, {
    key: 'slideNext',
    value: function slideNext() {
      this.s.nextIndex = this.s.currentSlide + 1;
      this.s.nextIndex >= this.s.slidesCount && (this.s.nextIndex = 0);

      this.slideTo();
    }
  }, {
    key: 'slidePrev',
    value: function slidePrev() {
      this.s.nextIndex = this.s.currentSlide - 1;
      this.s.nextIndex < 0 && (this.s.nextIndex = this.s.slidesCount - 1);

      this.slideTo();
    }
  }, {
    key: 'slideTo',
    value: function slideTo(index) {
      var ind = index !== undefined ? index : this.s.nextIndex,
          moveX = this._getMoveX(ind);

      //scrollToElement only if it is not currently visible
      this.s.currentTranslate = moveX;

      //slide to px
      this._slide(moveX);

      //reset nextIndex once sliding initialized
      this.s.nextIndex = 0;

      //update currentSlide
      this.s.currentSlide = ind;

      //activate slide
      this._activateSlide(ind);
    }
  }, {
    key: '_slide',
    value: function _slide(to, stickToSlide, currentTranslate) {
      var scrollTo = stickToSlide ? this._getClosestItemTranslate(to) : to;
      this.s.track.css('transform', 'translate3d(' + scrollTo + 'px,0,0)');
    }
  }, {
    key: '_getClosestItemTranslate',
    value: function _getClosestItemTranslate(to) {
      return -1 * this.s.slideWidth * Math.round(Math.abs(to) / this.s.slideWidth);
    }
  }, {
    key: '_getMoveX',
    value: function _getMoveX(ind) {
      var moveX = this._indexIntoPx(ind);

      if (moveX < 0) return 0;
      if (moveX >= this.s.maxTranslate) return this.s.maxTranslate * -1;

      return moveX * -1;
    }
  }, {
    key: '_indexIntoPx',
    value: function _indexIntoPx(index) {
      return index * this.s.slideWidth;
    }
  }, {
    key: '_activateSlide',
    value: function _activateSlide(index) {
      var i = index || this.s.currentSlide;
      this.s.slides.removeClass('active').eq(i).addClass('active');

      //callback
      this.o.onSlide(i);
    }
  }, {
    key: '_initMobileEvents',
    value: function _initMobileEvents() {
      var _this2 = this;

      var translate = 0,
          touch = {
        inAction: false,
        startSliderPos: 0,
        startPointerPos: 0,
        preLastTime: 0,
        preLastPos: 0,
        lastTime: 0,
        lastPos: 0
      };

      this.s.el.on('touchstart', function (e) {
        touch.inAction = false;
        translate = touch.startSliderPos = _this2._getCurrentTranslate();
        touch.startPointerPos = e.originalEvent.changedTouches[0].clientX;
      });

      this.s.el.on('touchmove', function (e) {
        _this2.s.el.removeClass('animate');
        touch.inAction = true;

        touch.preLastTime = touch.lastTime;
        touch.preLastPos = touch.lastPos;
        touch.lastTime = e.timeStamp;
        touch.lastPos = e.originalEvent.changedTouches[0].clientX;
        translate = touch.startSliderPos + touch.lastPos - touch.startPointerPos;

        //sliding out of available area
        if (translate > 0) {
          _this2._slide(translate / 4);
        } else if (Math.abs(translate) > _this2.s.maxTranslate) {
          _this2._slide(-_this2.s.maxTranslate + (_this2.s.maxTranslate + translate) / 4);
        } else {
          _this2._slide(translate);
        }
      });

      this.s.el.on('touchend', function () {
        if (touch.inAction) {
          var time = touch.lastTime - touch.preLastTime,
              distance = touch.lastPos - touch.preLastPos,
              isNegative = distance < 0 ? -1 : 1,
              power = Math.abs(distance / time * 10),
              sp = power > 5 ? (power * 1725 - 6225) / 32 * isNegative : power * 15 * isNegative;

          _this2.s.el.addClass('animate');

          var slideTo = translate + sp;

          //handle out of left screen edge
          if (translate > 0 || slideTo > 0) {
            slideTo = 0;
          }

          //handle out of right screen edge
          if (translate < -_this2.s.maxTranslate || slideTo < -_this2.s.maxTranslate) {
            slideTo = -_this2.s.maxTranslate;
          }

          _this2._slide(slideTo, true, translate);
        }
      });
    }
  }, {
    key: '_getCurrentTranslate',
    value: function _getCurrentTranslate() {
      var regExpr = new RegExp(/translate3d\((.*?)px/),
          style = this.s.track.attr('style'),
          test = regExpr.exec(style);

      if (test) return +test[1];

      return 0;
    }
  }]);

  return LbSlider;
}();

},{}],18:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SlidersFactory = undefined;

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _baseComponent = require('../base-component.js');

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/*
 * @description Introduces factory that creates a certain type of slider depending on a selector
 */
var SlidersFactory = exports.SlidersFactory = function () {
  _createClass(SlidersFactory, [{
    key: 'sliderTypes',
    get: function get() {
      return {
        regular: 'js-slider',
        adaptive: 'adaptive-slider'
      };
    }
  }], [{
    key: 'selector',
    get: function get() {
      return '.js-slider, .adaptive-slider';
    }
  }]);

  function SlidersFactory(el) {
    _classCallCheck(this, SlidersFactory);

    this._init(el);
  }

  _createClass(SlidersFactory, [{
    key: '_init',
    value: function _init(el) {
      if ($(el).hasClass(this.sliderTypes.regular)) {
        new RegularSlider(el);
      }
      if ($(el).hasClass(this.sliderTypes.adaptive)) {
        new AdaptiveSlider($(el).find('>.row'));
      }
    }
  }]);

  return SlidersFactory;
}();

/*
 * @description Regular Lookbook(HPI like) slider, that is created for any .js-slider selector
 */


var RegularSlider = function (_BaseComponent) {
  _inherits(RegularSlider, _BaseComponent);

  _createClass(RegularSlider, [{
    key: 'defaults',
    get: function get() {
      return $.extend({}, _get(RegularSlider.prototype.__proto__ || Object.getPrototypeOf(RegularSlider.prototype), 'defaults', this), {
        infinite: true,
        interval: 5000,
        autostart: false,
        navType: 'both',
        readyClass: 'ready',
        bulletedDotsClass: 'bulleted-dots',
        rowSliderClass: 'row-slider',
        lightColorClass: 'font-color-theme1',
        enableArrowsMobileClass: 'enable-arrows-mobile',
        carouselCounterSelector: '.display-carousel-counter',
        amountSlides: 0
      });
    }
  }]);

  function RegularSlider() {
    var _ref;

    _classCallCheck(this, RegularSlider);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    var _this = _possibleConstructorReturn(this, (_ref = RegularSlider.__proto__ || Object.getPrototypeOf(RegularSlider)).call.apply(_ref, [this].concat(args)));

    _this._init();
    return _this;
  }

  _createClass(RegularSlider, [{
    key: '_init',
    value: function _init() {
      this.counterEl = null;
      this._updateOptions();
      this._build();
      this._setCounter();
      this._onReady();
    }

    //Merge default options with data-options

  }, {
    key: '_updateOptions',
    value: function _updateOptions() {
      this.options = $.extend({}, this.options, this.el.data());

      //initialize autorotate manually via classname. Useful for autorotation with only ARROWS
      this.el.closest('.carousel-auto-rotate').length && (this.options.autostart = 'true');
    }
  }, {
    key: '_setCounter',
    value: function _setCounter() {
      var _this2 = this;

      if (this.el.closest(this.options.carouselCounterSelector).length || this.el.find(this.options.carouselCounterSelector).length) {
        var slick = this.el.slick('getSlick');
        this.options.amountSlides = slick.slideCount;
        this.counterEl = $('<span />', { class: 'pagingInfo' });
        this.el.on('reInit afterChange', function (e, slick, currentSlide) {
          return _this2._updateCounter(currentSlide);
        }).append(this.counterEl);

        this._updateCounter(slick.currentSlide);
      };
    }
  }, {
    key: '_updateCounter',
    value: function _updateCounter(currentSlide) {
      this.counterEl.text(currentSlide + 1 + '/' + this.options.amountSlides);
    }
  }, {
    key: '_build',
    value: function _build() {
      this.el.on('init', this._afterChange).on('beforeChange', this._beforeChange).on('afterChange', this._afterChange).slick({
        dots: this.options.navType === 'both' || this.options.navType === 'dotted',
        arrows: this.options.navType === 'both' || this.options.navType === 'arrow' || this.el.attr('data-autostart') === 'true',
        autoplay: this.options.autostart,
        touchMove: false,
        autoplaySpeed: this.options.interval,
        infinite: true,
        touchThreshold: 100
      });
    }
  }, {
    key: '_beforeChange',
    value: function _beforeChange(e, slick) {
      slick.$slides.eq(slick.currentSlide).find('.flex2-molecule').trigger('slider.viewportout');
    }
  }, {
    key: '_afterChange',
    value: function _afterChange(e, slick) {
      slick.$slides.eq(slick.currentSlide).find('.flex2-molecule').trigger('slider.viewportin');
    }
  }, {
    key: '_onReady',
    value: function _onReady() {
      var _this3 = this;

      this.el.find('.section.slick-slide').length && this.el.addClass(this.options.rowSliderClass);
      this.el.find('.section.font-color-theme1').length && this.el.addClass(this.options.lightColorClass);
      this.el.find('.section.preserve-row-content').length && this.el.addClass('preserve-row-content');
      this.el.addClass(this.options.readyClass);

      //add bulleted dots apperance only if NO KW 'carousel-add-lines' is applied and NO autorotate.
      if (!this.el.closest('.carousel-add-lines').length && !this.options.autostart) {
        this.el.addClass(this.options.bulletedDotsClass);
      }

      /*if only ARROWS are visible on desktop or a 'carousel-arrows-on-mobile' KW is applied
        - show arrows on mobiles*/
      if (this.el.closest('.carousel-arrows-on-mobile').length || this.options.navType === 'arrow' || this.el.find('.carousel-arrows-on-mobile').length) {
        this.el.find('.slick-arrow').addClass(this.options.enableArrowsMobileClass);
      }

      $(window).on("resize", function () {
        return _.debounce(function () {
          return _this3.el.slick('setPosition');
        }, 100);
      });
    }
  }]);

  return RegularSlider;
}(_baseComponent.BaseComponent);

/*
 * @description Adaptive Lookbook slider, that is created for any .adaptive-slider selector.
 *        Turns cells in a slider once
 */


var AdaptiveSlider = function (_BaseComponent2) {
  _inherits(AdaptiveSlider, _BaseComponent2);

  _createClass(AdaptiveSlider, [{
    key: 'defaults',
    get: function get() {
      return {
        initializedClassname: 'slick-initialized',
        showNextPrevSlides: 'show-next-prev',
        initBelowBreakpoint: 720,
        readyClass: 'ready',
        disableInfinitySelector: '.noloop',
        initialSlideSelector: '[class*="initial-slide"]',
        bulletedDotsClass: 'bulleted-dots',
        types: {
          regular: 'adaptive',
          custom: 'nextPrev'
        }
      };
    }
  }]);

  function AdaptiveSlider() {
    var _ref2;

    _classCallCheck(this, AdaptiveSlider);

    for (var _len2 = arguments.length, args = Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
      args[_key2] = arguments[_key2];
    }

    var _this4 = _possibleConstructorReturn(this, (_ref2 = AdaptiveSlider.__proto__ || Object.getPrototypeOf(AdaptiveSlider)).call.apply(_ref2, [this].concat(args)));

    _this4._init();
    return _this4;
  }

  _createClass(AdaptiveSlider, [{
    key: '_init',
    value: function _init() {
      this.opts = {
        dots: true,
        touchMove: false,
        infinite: !this.el.parents(this.options.disableInfinitySelector).length,
        initialSlide: this._getInitialSlide(),
        touchThreshold: 100,
        slide: 'div:not(.ignore)'
      };
      this.showNextPrevOpts = {
        centerMode: true,
        centerPadding: '28%',
        slidesToShow: 1,
        responsive: [{
          breakpoint: 1350,
          settings: {
            centerPadding: '20%'
          }
        }, {
          breakpoint: 720,
          settings: {
            centerPadding: 0
          }
        }]
      };
      this.options.type = this._getSliderType();
      this._checkInitialization();
      this._onReady();
    }

    //detects initial slide index from classname initial-slide-XXX

  }, {
    key: '_getInitialSlide',
    value: function _getInitialSlide() {
      var initialIndex = 0,
          initialSlideIndicatorEl = this.el.parents(this.options.initialSlideSelector);

      if (initialSlideIndicatorEl.length) {
        initialIndex = /initial-slide-(\d{1,})/.exec(initialSlideIndicatorEl.attr('class'))[1];
        initialIndex = +initialIndex - 1;
      }
      return initialIndex;
    }
  }, {
    key: '_getSliderType',
    value: function _getSliderType() {
      return this.el.parent().hasClass(this.options.showNextPrevSlides) ? this.options.types.custom : this.options.types.regular;
    }
  }, {
    key: '_checkInitialization',
    value: function _checkInitialization() {
      //Custom apperance. Show partially next and prev slides
      if (this.el.parent().hasClass(this.options.showNextPrevSlides)) {

        //Paired sliders START
        if (this.el.parent().hasClass('slider-nav') && $('.slider-for').length) {
          this.el.addClass('slider-nav').parent().removeClass('slider-nav');
          this.showNextPrevOpts = $.extend({}, this.showNextPrevOpts, {
            asNavFor: '.slider-for',
            dots: false
          });
        }
        if (this.el.parent().hasClass('slider-for') && $('.slider-nav').length) {
          this.el.addClass('slider-for').parent().removeClass('slider-for');
          this.showNextPrevOpts = $.extend({}, this.showNextPrevOpts, {
            asNavFor: '.slider-nav',
            arrows: false,
            speed: 0
          });
        }
        //Paired sliders END

        this.opts = $.extend({}, this.opts, this.showNextPrevOpts);
        this._build();
        return;
      }
      if (!this._isInitialized() && this._shouldBeInitialized()) {
        this._build();
        return;
      }
      if (this._isInitialized() && !this._shouldBeInitialized()) {
        this._destroy();
      }
    }
  }, {
    key: '_isInitialized',
    value: function _isInitialized() {
      return this.el.hasClass(this.options.initializedClassname);
    }
  }, {
    key: '_shouldBeInitialized',
    value: function _shouldBeInitialized() {
      return window.innerWidth < this.options.initBelowBreakpoint;
    }
  }, {
    key: '_build',
    value: function _build() {
      var _this5 = this;

      this.el.slick(this.opts);
      this.opts.initialSlide && setTimeout(function () {
        return _this5.el.slick('slickGoTo', _this5.opts.initialSlide);
      });
    }
  }, {
    key: '_destroy',
    value: function _destroy() {
      this.el.slick('unslick');
    }
  }, {
    key: '_onReady',
    value: function _onReady() {
      var _this6 = this;

      this.el.addClass(this.options.bulletedDotsClass + ' ' + this.options.readyClass);
      if (this.options.type === this.options.types.regular) {
        $(window).on('resize', _.debounce(function () {
          return _this6._checkInitialization();
        }, 100));
      }
    }
  }]);

  return AdaptiveSlider;
}(_baseComponent.BaseComponent);

},{"../base-component.js":11}],19:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.BrightcovePlayer = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _baseComponent = require("../../base-component");

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var bcAPILoaded = $.Deferred();

var BrightcovePlayer = exports.BrightcovePlayer = function (_BaseComponent) {
  _inherits(BrightcovePlayer, _BaseComponent);

  _createClass(BrightcovePlayer, [{
    key: "defaultData",
    get: function get() {
      return {
        "data-embed": "default",
        "controls": ""
      };
    }
  }]);

  function BrightcovePlayer(element) {
    _classCallCheck(this, BrightcovePlayer);

    var _this = _possibleConstructorReturn(this, (BrightcovePlayer.__proto__ || Object.getPrototypeOf(BrightcovePlayer)).call(this, element));

    _this._playerInitialized = $.Deferred();
    _this._captionsAvailable = $.Deferred();
    _this._bcPlayer = null;
    _this._captions = false;
    _this._videoUniqueId = _.uniqueId('bc-id-');
    return _this;
  }

  _createClass(BrightcovePlayer, [{
    key: "init",
    value: function init(cb) {
      var _this2 = this;

      this._handleCaptions();
      this._preparePlayerHtml();

      return this.loadAPIIfNeeded().then(function () {
        return _this2.createNewPlayer();
      }).then(function (player) {
        _this2._bcPlayer = player;
        _this2._playerInitialized.resolve(_this2);
        _this2._initMetrics();
        _this2._bcPlayer.on('play', function () {
          _this2._captionsAvailable.resolve();
        });
        cb && cb();

        return _this2._playerInitialized.promise();
      });
    }
  }, {
    key: "_preparePlayerHtml",
    value: function _preparePlayerHtml() {
      var playerAttrs = $.extend({}, this.defaultData, this._getParams()),
          playerEl = $('<video />', { 'id': this._videoUniqueId, 'class': "video-js" });

      for (var i in playerAttrs) {
        playerEl.attr(i, playerAttrs[i]);
      }
      this.el.replaceWith(playerEl);

      this.el = playerEl;
      this.accountId = playerAttrs['data-account'];
      this.playerId = playerAttrs['data-player'];
    }
  }, {
    key: "_handleCaptions",
    value: function _handleCaptions() {
      var _this3 = this;

      this._captions = this.el.attr('data-captions') === 'true';
      this._captions && this._captionsAvailable.then(function () {
        _this3._showCaptions();
      });
    }
  }, {
    key: "_getParams",
    value: function _getParams() {
      var _this4 = this;

      //fallback to support already existed HTML5 BC videos with OLD data format.
      if (this.el.attr('data-video')) {
        return GLOBALS.Utils._parseQuery(this.el.attr('data-video'));
      }
      var requiredParams = [{
        name: 'account',
        val: 'account'
      }, {
        name: 'player',
        val: 'playerID'
      }, {
        name: 'video-id',
        val: 'video-id'
      }],
          params = {};

      requiredParams.forEach(function (param, index) {
        params['data-' + param.name] = _this4.el.attr('data-' + param.val);
      });
      return params;
    }
  }, {
    key: "loadAPIIfNeeded",
    value: function loadAPIIfNeeded() {
      var APILoaded = $.Deferred(),
          APISrc = this._getPlayerInitScript();

      $.getScript(APISrc, function () {
        APILoaded.resolve();
      });
      return APILoaded.promise();
    }
  }, {
    key: "createNewPlayer",
    value: function createNewPlayer(element, options) {
      var playerReady = $.Deferred();

      if (typeof videojs != 'undefined') {
        videojs(this._videoUniqueId).ready(function () {
          //NOTE, don't change to arrow syntax, this is required
          playerReady.resolve(this);
        });
      };

      return playerReady.promise();
    }
  }, {
    key: "_getPlayerInitScript",
    value: function _getPlayerInitScript() {
      return "//players.brightcove.net/" + this.accountId + "/" + this.playerId + "_default/index.min.js";
    }
  }, {
    key: "_showCaptions",
    value: function _showCaptions() {
      var tracks = this._bcPlayer.textTracks();
      for (var i = 0; i < tracks.length; i++) {
        var track = tracks[i];
        if (track.kind === 'captions' && this._trackLangMatchesPage(track.language)) {
          track.mode = 'showing';
        }
      }
    }
  }, {
    key: "_trackLangMatchesPage",
    value: function _trackLangMatchesPage(language) {
      if (language) {
        var trackLang = language.toLowerCase(),
            trackLangSplited = trackLang.split('-'),
            pageLang = $('html').attr('lang') ? $('html').attr('lang').toLowerCase() : 'en-us';

        if (trackLangSplited.length > 1) {
          return trackLang === pageLang;
        }
        return trackLangSplited[0] === pageLang.split('-')[0];
      }
    }
  }, {
    key: "_getCurrentLanguage",
    value: function _getCurrentLanguage() {
      var lang = $('html').attr('lang') || 'en';
      return lang.split('-')[0];
    }
  }, {
    key: "play",
    value: function play(cb) {
      var _this5 = this;

      this._playerInitialized.then(function () {
        _this5._bcPlayer.play();
        cb && cb();
      });
    }
  }, {
    key: "pause",
    value: function pause() {
      var _this6 = this;

      this._playerInitialized.then(function () {
        _this6._bcPlayer.pause();
      });
    }
  }, {
    key: "stop",
    value: function stop() {
      var _this7 = this;

      this._playerInitialized.then(function () {
        _this7._bcPlayer.pause();
        _this7._bcPlayer.currentTime(0.01);
      });
    }
  }, {
    key: "_initMetrics",
    value: function _initMetrics() {
      var _this8 = this;

      var events = ['playing', 'pause', 'ended'],
          videoTitle = '',
          videoDurationInSeconds = void 0,
          videoQueuePointInSeconds = void 0,
          hasBeenEnded = true;

      //duration is available only once metadata is loaded
      this._bcPlayer.on('loadedmetadata', function () {
        videoDurationInSeconds = Math.floor(_this8._bcPlayer.duration() || 0);
        videoTitle = _this8._bcPlayer.mediainfo ? _this8._bcPlayer.mediainfo.name : '';
      });

      //attach events to capture
      events.forEach(function (event) {
        _this8._bcPlayer.on(event, function (event) {
          return handleEvent.call(_this8, event);
        });
      });

      function handleEvent(event) {
        //handle events only if metadata is loaded
        if (videoDurationInSeconds) {
          videoQueuePointInSeconds = Math.floor(this._bcPlayer.currentTime() || 0);
          switch (event.type) {
            case 'playing':
              {
                //if video has beed ended - then fire 'open' before next 'play' event
                if (hasBeenEnded) {
                  hasBeenEnded = false;
                  send('open', videoTitle, videoDurationInSeconds);
                }
                send('play', videoTitle, videoQueuePointInSeconds);
                break;
              }
            case 'pause':
              {
                //prevent sending 'stop' event on video 'ended' event
                videoQueuePointInSeconds != videoDurationInSeconds && send('stop', videoTitle, videoQueuePointInSeconds);
                break;
              }
            case 'ended':
              {
                /* if video has beed ended - change flag hasBeenEnded to true, to be able trigger 'open'
                   before the next 'play event'*/
                hasBeenEnded = true;
                send('close', videoTitle, videoQueuePointInSeconds);
                break;
              }
          }
        }
      }

      function send(eventType, title, duration) {
        console.log(eventType, title, duration);
        try {
          trackVideoMetrics(eventType, title, duration);
        } catch (e) {
          console.log('Metrics are NOT sent: ' + e.message);
        }
      }
    }
  }]);

  return BrightcovePlayer;
}(_baseComponent.BaseComponent);

},{"../../base-component":11}],20:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.YoutubePlayer = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _baseComponent = require('../../base-component');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var YOUTUBE_API_ENDPOINT = 'https://www.youtube.com/iframe_api';

var youtubeAPILoaded = $.Deferred();

var YoutubePlayer = exports.YoutubePlayer = function (_BaseComponent) {
  _inherits(YoutubePlayer, _BaseComponent);

  _createClass(YoutubePlayer, [{
    key: 'ytPlayerDefaults',
    get: function get() {
      return {
        playerVars: {
          rel: 0
        }
      };
    }
  }, {
    key: 'dataAttributes',
    get: function get() {
      return {
        videoId: 'data-id'
      };
    }
  }, {
    key: 'videoStates',
    get: function get() {
      return {
        playerLastState: -1,
        UNSTARTED: -1,
        ENDED: 0,
        PLAYING: 1,
        PAUSED: 2,
        BUFFERING: 3,
        CUED: 5
      };
    }
  }], [{
    key: 'PLAY_EVENT',
    get: function get() {
      return 'youtube-player-play';
    }
  }, {
    key: 'PAUSE_EVENT',
    get: function get() {
      return 'youtube-player-pause';
    }
  }, {
    key: 'STOP_EVENT',
    get: function get() {
      return 'youtube-player-stop';
    }
  }]);

  function YoutubePlayer(element) {
    _classCallCheck(this, YoutubePlayer);

    var _this = _possibleConstructorReturn(this, (YoutubePlayer.__proto__ || Object.getPrototypeOf(YoutubePlayer)).call(this, element));

    _this.states = $.extend({}, _this.videoStates);
    _this._playerInitialized = $.Deferred();
    _this._youtubePlayer = null;

    // Retrieve options from DOM element
    _this.options = _this._retrieveOptions(_this.el, _this.dataAttributes);
    _this.options = $.extend({}, _this.options, _this.ytPlayerDefaults);
    return _this;
  }

  _createClass(YoutubePlayer, [{
    key: '_retrieveOptions',
    value: function _retrieveOptions(element, attributes) {
      Object.keys(attributes).forEach(function (item) {
        attributes[item] = element.attr(attributes[item]);
      });
      return attributes;
    }
  }, {
    key: 'init',
    value: function init(cb) {
      var _this2 = this;

      var videoElement = this.el[0];

      return loadAPIIfNeeded().then(function () {
        return createNewPlayer(videoElement, _this2.options);
      }).then(function (player) {
        _this2._youtubePlayer = player;
        _this2._youtubePlayer.addEventListener("onStateChange", function (data) {
          return _this2._onPlayerStateChange(data);
        });
        _this2._playerInitialized.resolve(_this2);
        cb && cb();
        return _this2._playerInitialized.promise();
      });
    }
  }, {
    key: '_onPlayerStateChange',
    value: function _onPlayerStateChange(event) {
      var videoDurationInSeconds = Math.floor(this._youtubePlayer.getDuration() - 0.01 || 0),
          videoQueuePointInSeconds = Math.round(this._youtubePlayer.getCurrentTime() || 0),
          videoTitle = this._youtubePlayer.getVideoData().title;
      try {
        if (this.states.playerLastState == this.states.UNSTARTED) {
          console.log('***********LB: Youtube player UNSTARTED **********', 'open' + ',' + videoTitle + ',' + videoDurationInSeconds);
          trackVideoMetrics('open', videoTitle, videoDurationInSeconds);
        }
        if (event.data == this.states.PLAYING) {
          console.log('***********LB: Youtube player PLAYING **********', 'play' + ',' + videoTitle + ',' + videoQueuePointInSeconds);
          trackVideoMetrics('play', videoTitle, videoQueuePointInSeconds);
        } else if (event.data == this.states.PAUSED) {
          console.log('***********LB: Youtube player PAUSED **********', 'stop' + ',' + videoTitle + ',' + videoQueuePointInSeconds);
          trackVideoMetrics('stop', videoTitle, videoQueuePointInSeconds);
        } else if (event.data == this.states.ENDED) {
          console.log('***********LB: Youtube player ENDED **********', 'close' + ',' + videoTitle);
          trackVideoMetrics('close', videoTitle);
        }
      } catch (e) {
        console.log(e.message);
      }
      if (event.data == this.states.ENDED) {
        this.states.playerLastState = this.states.UNSTARTED;
      } else {
        this.states.playerLastState = event.data;
      }
    }
  }, {
    key: 'play',
    value: function play(cb) {
      var _this3 = this;

      this._playerInitialized.then(function () {
        _this3._youtubePlayer.playVideo();
        _this3.broadcast(YoutubePlayer.PLAY_EVENT);
        cb && cb();
      });
    }
  }, {
    key: 'pause',
    value: function pause() {
      var _this4 = this;

      this._playerInitialized.then(function () {
        _this4._youtubePlayer.pauseVideo();
        _this4.broadcast(YoutubePlayer.PAUSE_EVENT);
      });
    }
  }, {
    key: 'stop',
    value: function stop() {
      var _this5 = this;

      this._playerInitialized.then(function () {
        _this5._youtubePlayer.stopVideo();
        _this5.broadcast(YoutubePlayer.STOP_EVENT);
      });
    }
  }]);

  return YoutubePlayer;
}(_baseComponent.BaseComponent);

function loadAPIIfNeeded() {
  if (!window.YT) {
    window.onYouTubeIframeAPIReady = function () {
      return youtubeAPILoaded.resolve();
    };
    $.getScript(YOUTUBE_API_ENDPOINT);
  } else {
    if (!window.YT.loaded) {
      window.onYouTubeIframeAPIReady = function () {
        return youtubeAPILoaded.resolve();
      };
    } else {
      youtubeAPILoaded.resolve();
    }
  }
  return youtubeAPILoaded.promise();
}

function createNewPlayer(element, options) {
  var playerReady = $.Deferred();

  options.events = options.events || {};
  options.events.onReady = function (event) {
    playerReady.resolve(event.target);
  };
  options.events.onError = function () {
    return playerReady.reject();
  };
  console.log(options);
  new window.YT.Player(element, options);

  return playerReady.promise();
}

},{"../../base-component":11}],21:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Grid = exports.Grid = function () {
  _createClass(Grid, [{
    key: 'selectors',
    get: function get() {
      return {
        equalCells: '.equal-cells',
        collageAppearance: '.collage-layout'
      };
    }
  }]);

  function Grid() {
    _classCallCheck(this, Grid);

    this._init();
  }

  _createClass(Grid, [{
    key: '_init',
    value: function _init() {
      var _this = this;

      $(this.selectors.equalCells).each(function (index, el) {
        var cells = $(el).find('> .row > [class*="span"]');
        var wrapper = $(el);
        _this._updateEqualCells(cells, wrapper);
        _this._correctVerticalSpacings(cells);
        $(window).on('resize load', _.throttle(function () {
          return _this._updateEqualCells(cells, wrapper);
        }, 50));
      });
      $(this.selectors.collageAppearance).each(function (index, el) {
        var sliderEl = $(el).find('> .row'),
            cells = sliderEl.find('> [class*="span"]'),
            wideCell = cells.filter(function (index, el) {
          return $(el).hasClass('span16');
        });

        wideCell.length && $(el).addClass('wide-cell-position-' + wideCell.index());
        cells.each(function (index, el) {
          return $(el).attr('data-index', $(el).index());
        });
        sliderEl.on('destroy', function () {
          cells.sort(function (a, b) {
            return +$(a).attr('data-index') - +$(b).attr('data-index');
          }).appendTo(sliderEl);
        });
      });
    }
  }, {
    key: '_updateEqualCells',
    value: function _updateEqualCells(cells, wrapper) {

      /*if equal cells is used with adaptive slider - ignore height recalculation.
      height is set to auto!important in CSS*/
      if (window.innerWidth < 720 && wrapper.hasClass('adaptive-slider')) {
        return false;
      }

      var groups = _.groupBy(cells, function (el) {
        return $(el).offset().top;
      });
      if (Object.keys(groups).length != cells.length) {
        for (var i in groups) {
          calculateHeight($(groups[i]));
        }
      } else {
        cells.height('auto');
      }

      function calculateHeight(cells) {
        var max = 0;
        cells.height('auto');
        max = cells.eq(0).height();
        for (var _i = 1; _i < cells.length; _i++) {
          max = cells.eq(_i).height() > max ? cells.eq(_i).height() : max;
        }
        cells.height(max);
      }
    }
  }, {
    key: '_correctVerticalSpacings',
    value: function _correctVerticalSpacings(cells) {
      cells.each(function (index, el) {
        var childMolecule = $(el).find('>[class*="molecule"]');
        if (childMolecule.hasClass('mtop-mol')) {
          $(el).addClass('mtop-mol');
        }
        if (childMolecule.hasClass('mbot-mol')) {
          $(el).addClass('mbot-mol');
        }
      });
    }
  }]);

  return Grid;
}();

},{}],22:[function(require,module,exports){
'use strict';

var _helpers = require('./molecules/helpers');

var _molecules = require('./molecules');

var MoleculesList = _interopRequireWildcard(_molecules);

var _index = require('./components/index.js');

var ComponentsList = _interopRequireWildcard(_index);

var _common = require('./common/scripts/common.js');

var Common = _interopRequireWildcard(_common);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

//Initialize common functionality
window.GLOBALS = {};
for (var commonItem in Common) {
  GLOBALS[commonItem] = new Common[commonItem]();
}

$(function () {
  // Initialize all molecules
  for (var moleculeName in MoleculesList) {
    (function (Molecule) {
      $(Molecule.selector).each(function (_, $el) {
        new Molecule($el);
      });
    })(MoleculesList[moleculeName]);
  }

  $(window).trigger('modules.initialized');

  // Initialize all components
  for (var componentName in ComponentsList) {
    (function (Component) {
      if (Component.selector) {
        $(Component.selector).each(function (_, $el) {
          return (0, _helpers.runAsync)(function () {
            new Component($el);
          });
        });
      } else {
        new Component();
      }
    })(ComponentsList[componentName]);
  }
});

},{"./common/scripts/common.js":1,"./components/index.js":13,"./molecules":28,"./molecules/helpers":27}],23:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CollapsibleRows = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _baseComponent = require('../../../components/base-component.js');

var _stickable = require('../../../common/scripts/stickable.js');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var CollapsibleRows = exports.CollapsibleRows = function (_BaseComponent) {
  _inherits(CollapsibleRows, _BaseComponent);

  _createClass(CollapsibleRows, [{
    key: 'defaults',
    get: function get() {
      return $.extend({}, _get(CollapsibleRows.prototype.__proto__ || Object.getPrototypeOf(CollapsibleRows.prototype), 'defaults', this), {
        collapseIconClass: '.collapsible-row-collapse-icon',
        collapseIconTpl: this.el.find('.collapsible-row-collapse-icon-wrap').length ? this.el.find('.collapsible-row-collapse-icon-wrap') : '<div class="collapsible-row-collapse-icon-wrap"><a class="collapsible-row-collapse-icon" href="javascript:void(0)" data-metrics-identifier="drawer module" data-metrics-title="show less" data-metrics-link-type="link"></a></div>',
        lightThemeKeyword: 'appearance-theme-2',
        lightThemeClass: 'light-theme'
      });
    }
  }], [{
    key: 'selector',
    get: function get() {
      return '.molecule-999 .collapsible-row';
    }
  }]);

  function CollapsibleRows() {
    var _ref;

    _classCallCheck(this, CollapsibleRows);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    var _this = _possibleConstructorReturn(this, (_ref = CollapsibleRows.__proto__ || Object.getPrototypeOf(CollapsibleRows)).call.apply(_ref, [this].concat(args)));

    _this._init();
    return _this;
  }

  _createClass(CollapsibleRows, [{
    key: '_init',
    value: function _init() {
      this.trigger = this.el.find('>a');
      this.triggerWrapper = this.trigger.closest('[class*="flex2-molecule"]').addClass('collapsible-row-trigger-wrapper');

      this.triggerWrapper.closest('.row').css('line-height', '0');
      this._prepareCollapsibleRow();
      this._prepareCollapseIcon();
      this._initListeners();
    }
  }, {
    key: '_prepareCollapsibleRow',
    value: function _prepareCollapsibleRow() {
      var lastRowParent = this.el.parents('.row').last(),
          lightTheme = this.el.closest('.flex2-molecule').hasClass(this.options.lightThemeKeyword) ? this.options.lightThemeClass : '';

      //detect whether scructure is .section > .row + .section > .row OR .section > .row + .row;
      //condidition is truth for .section > .row + .row structure
      if (lastRowParent.next('.row').length) {
        this.row = lastRowParent.next('.row');
      } else {
        this.row = lastRowParent.parent().next('.section');
      }
      this.row.addClass('collapsible-row-container ' + lightTheme);
    }
  }, {
    key: '_prepareCollapseIcon',
    value: function _prepareCollapseIcon() {
      this.row.append(this.options.collapseIconTpl);
      this.collapseIcon = this.row.find(this.options.collapseIconClass);
    }
  }, {
    key: '_initListeners',
    value: function _initListeners() {
      var _this2 = this;

      this.trigger.on('click', function () {
        _this2._expandRow();
      });

      this.collapseIcon.on('click', function () {
        _this2.collapseIcon.removeClass('initialized');
        _this2._collapseRow();
      });
    }
  }, {
    key: '_expandRow',
    value: function _expandRow() {
      var _this3 = this;

      this.triggerWrapperHeight = this.triggerWrapper.outerHeight(true);

      this.row.on('transitionend webkitTransitionEnd', function (e) {
        if (e.originalEvent.propertyName == 'max-height' && $(e.target).is(_this3.row)) {
          _this3.row.off('transitionend webkitTransitionEnd');
          _this3.row.css('max-height', 'none');
        }
      });

      this.row.css('max-height', this.row[0].scrollHeight);

      this.triggerWrapper.on('transitionend webkitTransitionEnd', function (e) {
        if (e.originalEvent.propertyName == 'margin-top' && $(e.target).hasClass('collapsible-row-trigger-wrapper')) {
          _this3.triggerWrapper.off('transitionend webkitTransitionEnd');
          _this3.triggerWrapper.addClass('disabled');
          _this3.stickableCollapseBtn = new _stickable.Stickable(_this3.collapseIcon, null, { type: 'bounding' });
          _this3.collapseIcon.addClass('initialized');
        }
      });

      this.triggerWrapper.addClass('inactive').css({ 'margin-top': -this.triggerWrapperHeight + 'px' });
      GLOBALS.ScrollTo.navigate(null, null, 1000, this.row, -this.triggerWrapperHeight);
    }
  }, {
    key: '_collapseRow',
    value: function _collapseRow() {
      var _this4 = this;

      setTimeout(function () {
        return _this4.stickableCollapseBtn.destroy();
      }, 300);
      var navigateOffset = this.row.offset().top - $(window).scrollTop() + this.triggerWrapperHeight - 300;

      this.triggerWrapper.on('transitionend webkitTransitionEnd', function (e) {
        if (e.originalEvent.propertyName == 'margin-top' && $(e.target).hasClass('collapsible-row-trigger-wrapper')) {
          _this4.triggerWrapper.off('transitionend webkitTransitionEnd');
          _this4.triggerWrapper.removeClass('inactive disabled');
        }
      });
      this.row.css('max-height', this.row.height());
      setTimeout(function () {
        _this4.row.css('max-height', 0);
        _this4.triggerWrapper.css('margin-top', 0);
      });

      GLOBALS.ScrollTo.navigateToOffset(navigateOffset, 1000, null, true);
    }
  }]);

  return CollapsibleRows;
}(_baseComponent.BaseComponent);

},{"../../../common/scripts/stickable.js":7,"../../../components/base-component.js":11}],24:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.InlineVideo = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _baseComponent = require('../../../components/base-component.js');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var InlineVideo = exports.InlineVideo = function (_BaseComponent) {
  _inherits(InlineVideo, _BaseComponent);

  _createClass(InlineVideo, [{
    key: 'defaults',
    get: function get() {
      return {
        offsetTop: 100,
        loop: false,
        offsetBottom: 100,
        playInViewportClassname: 'play-once-in-viewport',
        loopInViwportClassname: 'loop-in-viwport',
        replayIconSelector: '.replay-video',
        playIconTpl: '<a class="play js_overlay_trigger" href="javascript:void(0)" title="play video" data-metrics-tcmid="245-1909760" data-metrics-tags="asset," data-metrics-link-type="" data-metrics-title="Video w/o related videos at the end"></a>'
      };
    }
  }], [{
    key: 'selector',
    get: function get() {
      return '.molecule-999 .video-module';
    }
  }]);

  function InlineVideo() {
    var _ref;

    _classCallCheck(this, InlineVideo);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    var _this = _possibleConstructorReturn(this, (_ref = InlineVideo.__proto__ || Object.getPrototypeOf(InlineVideo)).call.apply(_ref, [this].concat(args)));

    _this.mobile = GLOBALS.Utils.device.isMobile;
    _this._init();
    return _this;
  }

  _createClass(InlineVideo, [{
    key: '_init',
    value: function _init() {
      var _this2 = this;

      this.video = this.el.find('video').length && this.el.find('video')[0];
      if (this.video) {
        this.parentModule = this.el.closest('.flex2-molecule');
        this.replayIcon = this.el.find(this.options.replayIconSelector);

        /* for mobiles we need to show play icon with poster image cause autoplay
         * is not working and we need to trigger video to play manually*/
        if (this.mobile) {
          this.replayIcon.after(this.options.playIconTpl);
          this.playIcon = this.el.find('a.play');
        } else {
          this.playInViewport = this.el.hasClass(this.options.playInViewportClassname);
          this.loopInViewport = this.el.hasClass(this.options.loopInViwportClassname);
        }

        //Ignore playing in a viewport for mobile devices
        if (!this.mobile && this.playInViewport) {
          this.outOfViewport = true;

          this.viewportParent = this.el.closest('.slick-slider');
          this.viewportParent = this.viewportParent.length ? this.viewportParent : this.el;
          this.viewportParent.viewportChecker({
            repeat: true,
            offset: 100,
            callbackFunction: function callbackFunction(obj, e) {
              if (e === 'add') {
                if (_this2.outOfViewport) {
                  _this2._playVideo();
                  _this2.outOfViewport = false;
                }
              } else {
                _this2._pauseVideo();
                _this2.outOfViewport = true;
              }
            }
          });
        }
        //play video in active slide only for desktops
        if (!this.mobile) {
          this.parentModule.on('slider.viewportin', function () {
            return setTimeout(function () {
              return _this2._playVideo();
            }, 100);
          });
          //pause video in inactive slide for both desktops and mobiles
          this.parentModule.on('slider.viewportout', function () {
            return setTimeout(function () {
              return _this2._pauseVideo();
            }, 100);
          });
        } else {
          this.parentModule.on('slider.viewportout', function () {
            return setTimeout(function () {
              if (_this2.video.isPlaying) {
                _this2._showPlayIcon();
              }
              _this2._pauseVideo();
            }, 100);
          });
        }

        this._initVideoEvents();
      }
    }
  }, {
    key: '_playVideo',
    value: function _playVideo() {
      var _this3 = this;

      if (!this.mobile && this.playInViewport) {
        if (this.viewportParent.hasClass('visible')) {
          if (!this.video.isPlaying) {
            if (this.el.closest('.slick-slider').length) {
              if (this.el.closest('.slick-active').length) {
                setTimeout(function () {
                  return _this3._play();
                }, 50);
              }
            } else {
              setTimeout(function () {
                return _this3._play();
              }, 50);
            }
          }
        }
      } else {
        if (!this.video.isPlaying) {
          this._play();
        }
      }
    }
  }, {
    key: '_play',
    value: function _play() {
      this._hideReplayIcon();
      this._hidePlayIcon();
      this.video.play();
    }
  }, {
    key: '_pauseVideo',
    value: function _pauseVideo() {
      if (this.video.isPlaying) {
        this.video.pause();
      }
    }
  }, {
    key: '_initVideoEvents',
    value: function _initVideoEvents() {
      var _this4 = this;

      if (!this.mobile) {
        $(this.video).on('loadedmetadata', function () {
          _this4.video.currentTime = 0.01;
        }).on('ended', function () {
          if (!_this4.loopInViewport) {
            _this4._showReplayIcon();
          } else {
            _this4._playVideo();
          }
        });
      } else {
        this._showPlayIcon();
        $(this.video).on('ended', function () {
          return _this4._showReplayIcon();
        });
      }

      this.replayIcon && this.replayIcon.on('click', function (e) {
        if (!_this4.video.isPlaying) {
          _this4._playVideo();
        }
      });

      this.playIcon && this.playIcon.on('click', function (e) {
        _this4._playVideo();
      });
    }
  }, {
    key: '_showPlayIcon',
    value: function _showPlayIcon() {
      this.playIcon && this.playIcon.addClass('visible');
    }
  }, {
    key: '_hidePlayIcon',
    value: function _hidePlayIcon() {
      this.playIcon && this.playIcon.removeClass('visible');
    }
  }, {
    key: '_showReplayIcon',
    value: function _showReplayIcon() {
      this.replayIcon && this.replayIcon.addClass('visible');
    }
  }, {
    key: '_hideReplayIcon',
    value: function _hideReplayIcon() {
      this.replayIcon && this.replayIcon.removeClass('visible');
    }
  }]);

  return InlineVideo;
}(_baseComponent.BaseComponent);

},{"../../../components/base-component.js":11}],25:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ScrollToNext = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _baseComponent = require('../../../components/base-component.js');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ScrollToNext = exports.ScrollToNext = function (_BaseComponent) {
  _inherits(ScrollToNext, _BaseComponent);

  _createClass(ScrollToNext, [{
    key: 'defaults',
    get: function get() {
      return {
        scrollDuration: 1000,
        scrollToSectionClassName: 'scroll-to-section-arrow'
      };
    }
  }], [{
    key: 'selector',
    get: function get() {
      return '.molecule-999 a.scroll-to-next';
    }
  }]);

  function ScrollToNext() {
    var _ref;

    _classCallCheck(this, ScrollToNext);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    var _this = _possibleConstructorReturn(this, (_ref = ScrollToNext.__proto__ || Object.getPrototypeOf(ScrollToNext)).call.apply(_ref, [this].concat(args)));

    _this._init();
    return _this;
  }

  _createClass(ScrollToNext, [{
    key: '_init',
    value: function _init() {
      this.$section = this.el.closest('.section');
      this.$wrapper = this.el.closest('.molecule-999');
      if (this.$section.length) {
        this.$section.addClass(this.options.scrollToSectionClassName);
        this._prepareSection();
      }

      this._initListeners();
    }
  }, {
    key: '_prepareSection',
    value: function _prepareSection() {
      if (this.$wrapper.hasClass('show-above-1280')) {
        this.$section.addClass('arrow-above-1280');
      }
      if (this.$wrapper.hasClass('show-below-1280')) {
        this.$section.addClass('arrow-below-1280');
      }
      if (this.$wrapper.hasClass('show-above-720')) {
        this.$section.addClass('arrow-above-720');
      }
      if (this.$wrapper.hasClass('show-below-720')) {
        this.$section.addClass('arrow-below-720');
      }
    }
  }, {
    key: '_initListeners',
    value: function _initListeners() {
      var _this2 = this;

      this.el.on('click', function (e) {
        GLOBALS.ScrollTo.navigateToSection($(e.currentTarget).attr('href'), null, _this2.options.scrollDuration);
        e.preventDefault();
      });
    }
  }]);

  return ScrollToNext;
}(_baseComponent.BaseComponent);

},{"../../../components/base-component.js":11}],26:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ScrollToTop = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _baseComponent = require('../../../components/base-component.js');

var _stickable = require('../../../common/scripts/stickable.js');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ScrollToTop = exports.ScrollToTop = function (_BaseComponent) {
  _inherits(ScrollToTop, _BaseComponent);

  _createClass(ScrollToTop, [{
    key: 'defaults',
    get: function get() {
      return {
        offsetTop: 500,
        scrollDuration: 600
      };
    }
  }], [{
    key: 'selector',
    get: function get() {
      return '.molecule-999 a.scroll-to-top';
    }
  }]);

  function ScrollToTop() {
    var _ref;

    _classCallCheck(this, ScrollToTop);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    var _this = _possibleConstructorReturn(this, (_ref = ScrollToTop.__proto__ || Object.getPrototypeOf(ScrollToTop)).call.apply(_ref, [this].concat(args)));

    _this._init();
    return _this;
  }

  _createClass(ScrollToTop, [{
    key: '_init',
    value: function _init() {
      new _stickable.Stickable(this.el, null, { type: 'bottom', offsetTop: +this.el.attr('data-show-offset') || this.options.offsetTop });
      this._initListeners();
    }
  }, {
    key: '_initListeners',
    value: function _initListeners() {
      var _this2 = this;

      this.el.on('click', function () {
        return GLOBALS.ScrollTo.navigateTo(0, +_this2.el.attr('data-scroll-duration') || _this2.options.scrollDuration);
      });
    }
  }]);

  return ScrollToTop;
}(_baseComponent.BaseComponent);

},{"../../../common/scripts/stickable.js":7,"../../../components/base-component.js":11}],27:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.runAsync = runAsync;
function runAsync(fn) {
  window.setTimeout(fn, 0);
}

},{}],28:[function(require,module,exports){
'use strict';
/*NOTE order matters. Some modules should be initialized prior to the others. Don't change it unless very sure*/

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _moleculeLb = require('./lb-209/molecule-lb-209');

Object.keys(_moleculeLb).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _moleculeLb[key];
    }
  });
});

var _moleculeLb2 = require('./lb-300/molecule-lb-300');

Object.keys(_moleculeLb2).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _moleculeLb2[key];
    }
  });
});

var _moleculeLb3 = require('./lb-302/molecule-lb-302');

Object.keys(_moleculeLb3).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _moleculeLb3[key];
    }
  });
});

var _moleculeLb4 = require('./lb-413/molecule-lb-413');

Object.keys(_moleculeLb4).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _moleculeLb4[key];
    }
  });
});

var _moleculeLb5 = require('./lb-501/molecule-lb-501');

Object.keys(_moleculeLb5).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _moleculeLb5[key];
    }
  });
});

var _moleculeLb6 = require('./lb-510/molecule-lb-510');

Object.keys(_moleculeLb6).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _moleculeLb6[key];
    }
  });
});

var _moleculeLb7 = require('./lb-823/molecule-lb-823');

Object.keys(_moleculeLb7).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _moleculeLb7[key];
    }
  });
});

var _moleculeLb8 = require('./lb-827/molecule-lb-827');

Object.keys(_moleculeLb8).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _moleculeLb8[key];
    }
  });
});

var _moleculeLb9 = require('./lb-830/molecule-lb-830');

Object.keys(_moleculeLb9).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _moleculeLb9[key];
    }
  });
});

var _moleculeLb10 = require('./lb-835/molecule-lb-835');

Object.keys(_moleculeLb10).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _moleculeLb10[key];
    }
  });
});

var _moleculeLb11 = require('./lb-836/molecule-lb-836');

Object.keys(_moleculeLb11).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _moleculeLb11[key];
    }
  });
});

var _moleculeLb836Theme = require('./lb-836/molecule-lb-836-theme-2');

Object.keys(_moleculeLb836Theme).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _moleculeLb836Theme[key];
    }
  });
});

var _moleculeLb12 = require('./lb-701/molecule-lb-701');

Object.keys(_moleculeLb12).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _moleculeLb12[key];
    }
  });
});

var _moleculeLb13 = require('./lb-702/molecule-lb-702');

Object.keys(_moleculeLb13).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _moleculeLb13[key];
    }
  });
});

var _collapsibleRow = require('./999/collapsible-row/collapsible-row');

Object.keys(_collapsibleRow).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _collapsibleRow[key];
    }
  });
});

var _scrollToTop = require('./999/scroll-to-top/scroll-to-top');

Object.keys(_scrollToTop).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _scrollToTop[key];
    }
  });
});

var _inlineVideo = require('./999/inline-video/inline-video');

Object.keys(_inlineVideo).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _inlineVideo[key];
    }
  });
});

var _scrollToNext = require('./999/scroll-to-next/scroll-to-next');

Object.keys(_scrollToNext).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _scrollToNext[key];
    }
  });
});

var _moleculeLb14 = require('./lb-406/molecule-lb-406');

Object.keys(_moleculeLb14).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _moleculeLb14[key];
    }
  });
});

},{"./999/collapsible-row/collapsible-row":23,"./999/inline-video/inline-video":24,"./999/scroll-to-next/scroll-to-next":25,"./999/scroll-to-top/scroll-to-top":26,"./lb-209/molecule-lb-209":29,"./lb-300/molecule-lb-300":30,"./lb-302/molecule-lb-302":31,"./lb-406/molecule-lb-406":32,"./lb-413/molecule-lb-413":33,"./lb-501/molecule-lb-501":34,"./lb-510/molecule-lb-510":35,"./lb-701/molecule-lb-701":36,"./lb-702/molecule-lb-702":37,"./lb-823/molecule-lb-823":38,"./lb-827/molecule-lb-827":41,"./lb-830/molecule-lb-830":42,"./lb-835/molecule-lb-835":43,"./lb-836/molecule-lb-836":45,"./lb-836/molecule-lb-836-theme-2":44}],29:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Molecule209 = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _helpers = require('../helpers');

var _baseComponent = require('../../components/base-component.js');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Molecule209 = exports.Molecule209 = function (_BaseComponent) {
  _inherits(Molecule209, _BaseComponent);

  _createClass(Molecule209, [{
    key: 'defaults',
    get: function get() {
      return $.extend({}, _get(Molecule209.prototype.__proto__ || Object.getPrototypeOf(Molecule209.prototype), 'defaults', this), {
        sectionSelector: '.section',
        wrapperSelector: '.molecule-ocmhpifootnotes__wrapper',
        footnotesSectionClass: 'molecule-ocmhpifootnotes-section',
        layoutWidth: 1280
      });
    }
  }], [{
    key: 'selector',
    get: function get() {
      return '.molecule-lb-209';
    }
  }]);

  function Molecule209() {
    var _ref;

    _classCallCheck(this, Molecule209);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    var _this = _possibleConstructorReturn(this, (_ref = Molecule209.__proto__ || Object.getPrototypeOf(Molecule209)).call.apply(_ref, [this].concat(args)));

    _this._init();
    return _this;
  }

  _createClass(Molecule209, [{
    key: '_init',
    value: function _init() {
      var _this2 = this;

      this._section = this.el.parents(this.options.sectionSelector);

      (0, _helpers.runAsync)(function () {
        _this2._alignWithLayout();
        _this2._section.addClass(_this2.options.footnotesSectionClass);
      });
    }
  }, {
    key: '_alignWithLayout',
    value: function _alignWithLayout() {
      var maxWidth = this._getLayoutWidth(),
          wrapper = this.el.find(this.options.wrapperSelector);

      wrapper.css('max-width', maxWidth);
    }
  }, {
    key: '_getLayoutWidth',
    value: function _getLayoutWidth() {
      var result = /layout-(.*?)(\D|$)/.exec(this._section.attr('class'));
      if (result) {
        return +result[1];
      }
      return this.options.layoutWidth;
    }
  }]);

  return Molecule209;
}(_baseComponent.BaseComponent);

},{"../../components/base-component.js":11,"../helpers":27}],30:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Molecule300 = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _baseComponent = require('../../components/base-component.js');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Molecule300 = exports.Molecule300 = function (_BaseComponent) {
  _inherits(Molecule300, _BaseComponent);

  _createClass(Molecule300, [{
    key: 'defaults',
    get: function get() {
      return $.extend({}, _get(Molecule300.prototype.__proto__ || Object.getPrototypeOf(Molecule300.prototype), 'defaults', this), {});
    }
  }], [{
    key: 'selector',
    get: function get() {
      return '.molecule-lb-300';
    }
  }]);

  function Molecule300() {
    var _ref;

    _classCallCheck(this, Molecule300);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    var _this = _possibleConstructorReturn(this, (_ref = Molecule300.__proto__ || Object.getPrototypeOf(Molecule300)).call.apply(_ref, [this].concat(args)));

    _this._init();
    return _this;
  }

  _createClass(Molecule300, [{
    key: '_init',
    value: function _init() {
      if (this.el.find('a.button').length > 1) {
        this.el.find('a.button').wrap('<div class="cta-wrapper"></div>');
      }
    }
  }]);

  return Molecule300;
}(_baseComponent.BaseComponent);

},{"../../components/base-component.js":11}],31:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Molecule302 = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _baseComponent = require('../../components/base-component.js');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Molecule302 = exports.Molecule302 = function (_BaseComponent) {
  _inherits(Molecule302, _BaseComponent);

  _createClass(Molecule302, [{
    key: 'defaults',
    get: function get() {
      return $.extend({}, _get(Molecule302.prototype.__proto__ || Object.getPrototypeOf(Molecule302.prototype), 'defaults', this), {
        rowSelector: '.row',
        childrenSelector: '> *',
        imageSelector: 'img',
        imageHolderSelector: '.image-holder',
        contentWrapperSelector: '.content-wrapper',

        clonedClass: 'cloned',
        hiddenClass: 'hidden-container',
        noImageClass: 'no-image',
        noAllSpacingsClass: 'drop-all-spacings'
      });
    }
  }], [{
    key: 'selector',
    get: function get() {
      return '.molecule-lb-302';
    }
  }]);

  function Molecule302() {
    var _ref;

    _classCallCheck(this, Molecule302);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    var _this = _possibleConstructorReturn(this, (_ref = Molecule302.__proto__ || Object.getPrototypeOf(Molecule302)).call.apply(_ref, [this].concat(args)));

    _this._init();
    return _this;
  }

  _createClass(Molecule302, [{
    key: '_init',
    value: function _init() {
      var image = this.el.find(this.options.imageSelector);
      var contentWrapper = this.el.find(this.options.contentWrapperSelector);
      var hasAnyTextContent = contentWrapper.find(this.options.childrenSelector).length;

      if (image.length) {
        this.el.find(this.options.imageHolderSelector).css({
          'background-image': 'url("' + image.attr('src') + '")'
        });

        // NOTE: Append cloned copy only if there is an image together with content
        if (hasAnyTextContent) {
          contentWrapper.find(' > a, > .molecule-lb-406').wrap('<div class="cta-wrapper"></div>');
          var contentCopy = contentWrapper.clone(true, true).addClass(this.options.clonedClass + ' ' + this.options.hiddenClass);

          contentCopy.find('.overlay-popup').remove();
          this.el.append(contentCopy);
        }
      } else {
        this.el.addClass(this.options.noImageClass);
      }
      //in case if module has both classnames - there should not be noAllSpacingsClass on a ROW
      var preserveWidth = this.el.attr('class').match(/image-as-is|respect-image-metadata-width/g);
      if (!preserveWidth || preserveWidth && preserveWidth.length < 2) {
        this.el.parents(this.options.rowSelector).addClass(this.options.noAllSpacingsClass);
      }
    }
  }]);

  return Molecule302;
}(_baseComponent.BaseComponent);

},{"../../components/base-component.js":11}],32:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Molecule406 = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _baseComponent = require('../../components/base-component.js');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Molecule406 = exports.Molecule406 = function (_BaseComponent) {
  _inherits(Molecule406, _BaseComponent);

  _createClass(Molecule406, [{
    key: 'defaults',
    get: function get() {
      return $.extend({}, _get(Molecule406.prototype.__proto__ || Object.getPrototypeOf(Molecule406.prototype), 'defaults', this), {
        moduleSelector: '.molecule-lb-406',
        dropSelector: '.dropdown-menu',
        centerModuleClassname: 'align-module-center',
        alignRightClassname: 'align-module-right',
        triggerSelector: '.dropdown-toggle',
        clickableHelperSelector: '.clickable'
      });
    }
  }], [{
    key: 'selector',
    get: function get() {
      return '.molecule-lb-406';
    }
  }]);

  function Molecule406() {
    var _ref;

    _classCallCheck(this, Molecule406);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    var _this = _possibleConstructorReturn(this, (_ref = Molecule406.__proto__ || Object.getPrototypeOf(Molecule406)).call.apply(_ref, [this].concat(args)));

    _this._init();
    return _this;
  }

  _createClass(Molecule406, [{
    key: '_init',
    value: function _init() {
      this._handleAlignment();
      this._initListeners();
      this.closeTimeout = null;
    }
  }, {
    key: '_handleAlignment',
    value: function _handleAlignment() {
      var clickableEl = $('<div />', { class: 'clickable' });
      this.el.find(this.options.triggerSelector).before(clickableEl);
      if (this.el.hasClass(this.options.centerModuleClassname)) {
        this.el.parent().addClass('text-center');
      }
      if (this.el.hasClass(this.options.alignRightClassname)) {
        this.el.parent().addClass('text-right');
      }
    }
  }, {
    key: '_prepareMobile',
    value: function _prepareMobile() {
      var tpl = _.template(this.options.mobileTpl),
          items = this.el.find('ul').clone().html(),
          btn = this.el.find('li.small-btn').clone().html(),
          firstAnchor = this.el.find('li:first-child a'),
          dropTrigger = '<a class="drop-trigger" href="javascript: void(0)">' + firstAnchor.text() + '</a>';

      this.el.append(tpl({ items: items, btn: btn, dropTrigger: dropTrigger }));
    }
  }, {
    key: '_initListeners',
    value: function _initListeners() {
      var _this2 = this;

      this.el.find(this.options.clickableHelperSelector).on('click', function (e) {
        $(_this2.options.moduleSelector).not(_this2.el).removeClass('active');
        if (_this2.el.hasClass('active')) {
          _this2.el.removeClass('active');
        } else {
          _this2.el.addClass('active');
          _this2._initCloseOnBodyCLick();
        }
      });
      this.el.on('focusout', function () {
        _this2.closeTimeout = setTimeout(function () {
          _this2.el.removeClass('active');
        }, 400);
      });
      this.el.find(this.options.dropSelector).on('click', 'a', function () {
        _this2.el.trigger('focusout');
      });
    }
  }, {
    key: '_initCloseOnBodyCLick',
    value: function _initCloseOnBodyCLick() {
      var _this3 = this;

      $('body').off('click.dd').on('click.dd', function (e) {
        if (!$(e.target).closest(_this3.options.moduleSelector).length) {
          $(_this3.options.moduleSelector).removeClass('active');
          $('body').off('click.dd');
          clearTimeout(_this3.closeTimeout);
        }
      });
    }
  }]);

  return Molecule406;
}(_baseComponent.BaseComponent);

},{"../../components/base-component.js":11}],33:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Molecule413 = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _baseComponent = require('../../components/base-component.js');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Molecule413 = exports.Molecule413 = function (_BaseComponent) {
  _inherits(Molecule413, _BaseComponent);

  _createClass(Molecule413, [{
    key: 'defaults',
    get: function get() {
      return $.extend({}, _get(Molecule413.prototype.__proto__ || Object.getPrototypeOf(Molecule413.prototype), 'defaults', this), {
        customAppearanceClassname: 'appearance-theme-2'
      });
    }
  }], [{
    key: 'selector',
    get: function get() {
      return '.molecule-lb-413';
    }
  }]);

  function Molecule413() {
    var _ref;

    _classCallCheck(this, Molecule413);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    var _this = _possibleConstructorReturn(this, (_ref = Molecule413.__proto__ || Object.getPrototypeOf(Molecule413)).call.apply(_ref, [this].concat(args)));

    _this._init();
    return _this;
  }

  _createClass(Molecule413, [{
    key: '_init',
    value: function _init() {
      if (this.el.hasClass(this.options.customAppearanceClassname)) {
        this._initCustomAppearance();
      } else {
        this._wrapImage();
      }
    }
  }, {
    key: '_initCustomAppearance',
    value: function _initCustomAppearance() {
      var img = this.el.find('img'),
          height = +this.el.find('img').attr('height'),
          background = 'url(' + img.attr('src') + ')';
      img.parent().css({
        'background-image': background,
        height: height
      });

      this._setInitialized();
    }
  }, {
    key: '_wrapImage',
    value: function _wrapImage() {
      this.el.find('img').each(function (i, el) {
        return $(el).wrap('<div />');
      });
    }
  }, {
    key: '_setInitialized',
    value: function _setInitialized() {
      this.el.addClass('initialized');
    }
  }]);

  return Molecule413;
}(_baseComponent.BaseComponent);

},{"../../components/base-component.js":11}],34:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Molecule501 = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _baseComponent = require('../../components/base-component.js');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Molecule501 = exports.Molecule501 = function (_BaseComponent) {
  _inherits(Molecule501, _BaseComponent);

  _createClass(Molecule501, [{
    key: 'defaults',
    get: function get() {
      return $.extend({}, _get(Molecule501.prototype.__proto__ || Object.getPrototypeOf(Molecule501.prototype), 'defaults', this), {
        textOverImageClassname: 'text-over-image'
      });
    }
  }], [{
    key: 'selector',
    get: function get() {
      return '.molecule-lb-501';
    }
  }]);

  function Molecule501() {
    var _ref;

    _classCallCheck(this, Molecule501);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    var _this = _possibleConstructorReturn(this, (_ref = Molecule501.__proto__ || Object.getPrototypeOf(Molecule501)).call.apply(_ref, [this].concat(args)));

    _this._init();
    return _this;
  }

  _createClass(Molecule501, [{
    key: '_init',
    value: function _init(el) {
      if (this.el.hasClass(this.options.textOverImageClassname)) {
        this.el.find('> *:not(.videoitem):not(.clf)').wrapAll('<div class="module-content"></div>');
      }
    }
  }]);

  return Molecule501;
}(_baseComponent.BaseComponent);

},{"../../components/base-component.js":11}],35:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Molecule510 = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _baseComponent = require('../../components/base-component.js');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Molecule510 = exports.Molecule510 = function (_BaseComponent) {
  _inherits(Molecule510, _BaseComponent);

  _createClass(Molecule510, null, [{
    key: 'selector',
    get: function get() {
      return '.molecule-lb-510';
    }
  }]);

  function Molecule510() {
    var _ref;

    _classCallCheck(this, Molecule510);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    var _this = _possibleConstructorReturn(this, (_ref = Molecule510.__proto__ || Object.getPrototypeOf(Molecule510)).call.apply(_ref, [this].concat(args)));

    _this._init();
    return _this;
  }

  _createClass(Molecule510, [{
    key: '_init',
    value: function _init() {
      this._setCollapsibleIndicator();
      this._updateHtml();
      this._initListeners();
    }
  }, {
    key: '_setCollapsibleIndicator',
    value: function _setCollapsibleIndicator() {
      if ($.trim(this.el.find('.description').text())) {
        this.el.find('.description').wrap('<div class="description-wrapper"/>');
        this.el.addClass('with-icon');
      } else {
        if (!this.el.find('.title *').length) {
          this.el.parent().addClass('ignore');
        }
      }
    }
  }, {
    key: '_updateHtml',
    value: function _updateHtml() {
      var src = this.el.find('img').attr('src'),
          imgWrapper = this.el.find('.img-wrapper');

      imgWrapper.css('background-image', 'url(' + src + ')');
      this.el.addClass('initialized');
    }
  }, {
    key: '_initListeners',
    value: function _initListeners() {
      var _this2 = this;

      var parent = this.el.closest('.section.collage-layout');
      this.el.find('.expand-collapse-wrapper').on('click', function () {
        parent.find('.molecule-lb-510').not(_this2.el).each(function (index, el) {
          return _this2._hideDescription($(el));
        });
        if (_this2.el.hasClass('active')) {
          _this2._hideDescription(_this2.el);
        } else {
          _this2._showDescription(_this2.el);
        }
      });
    }
  }, {
    key: '_hideDescription',
    value: function _hideDescription($el) {
      if ($el.hasClass('active')) {
        $el.find('.description').stop().clearQueue().animate({ opacity: 0 }, 150);
        $el.removeClass('active').find('.description-wrapper').slideUp(300);
        this._enableMetrics($el);
      }
    }
  }, {
    key: '_showDescription',
    value: function _showDescription($el) {
      $el.addClass('active');
      $el.find('.description-wrapper').slideDown(300, function () {
        $el.find('.description').stop().clearQueue().animate({ 'opacity': 1 });
      });
      this._disableMetrics($el);
    }
  }, {
    key: '_enableMetrics',
    value: function _enableMetrics($el) {
      setTimeout(function () {
        return $el.find('.action-trigger').removeAttr('data-disable-metrics');
      });
    }
  }, {
    key: '_disableMetrics',
    value: function _disableMetrics($el) {
      setTimeout(function () {
        return $el.find('.action-trigger').attr('data-disable-metrics', 'disabled');
      });
    }
  }]);

  return Molecule510;
}(_baseComponent.BaseComponent);

},{"../../components/base-component.js":11}],36:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Molecule701 = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _baseComponent = require('../../components/base-component.js');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Molecule701 = exports.Molecule701 = function (_BaseComponent) {
  _inherits(Molecule701, _BaseComponent);

  _createClass(Molecule701, [{
    key: 'defaults',
    get: function get() {
      return $.extend({}, _get(Molecule701.prototype.__proto__ || Object.getPrototypeOf(Molecule701.prototype), 'defaults', this), {
        pageRowSelector: '.section',
        pageSectionSelector: '.flex-content',
        configElSelector: '.row-gradient-config'
      });
    }
  }], [{
    key: 'selector',
    get: function get() {
      return '.molecule-lb-701';
    }
  }]);

  function Molecule701() {
    var _ref;

    _classCallCheck(this, Molecule701);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    var _this = _possibleConstructorReturn(this, (_ref = Molecule701.__proto__ || Object.getPrototypeOf(Molecule701)).call.apply(_ref, [this].concat(args)));

    _this._init();
    return _this;
  }

  _createClass(Molecule701, [{
    key: '_init',
    value: function _init() {
      this._collectOptions();
      this._getBg();
      this._setBg();
    }
  }, {
    key: '_collectOptions',
    value: function _collectOptions() {
      this.configEl = this.el.find(this.options.configElSelector);
      this.options = $.extend({}, this.options, this.configEl.data());
    }
  }, {
    key: '_getBg',
    value: function _getBg() {
      this.options.bg = '';
      if (this.options.start) {
        this.options.bg += 'background: ' + this.options.start + ';';
        if (this.options.end) {
          var direction = this.options.direction === 'vertical' ? 'bottom' : 'right';
          this.options.bg += 'background: linear-gradient(to ' + direction + ', ' + this.options.start + ' 0%,' + this.options.end + ' 100%);';
        }
      }
    }
  }, {
    key: '_setBg',
    value: function _setBg() {
      var applyTo = this.options.area !== 'area-page-row' ? 'pageSection' : 'pageRow';
      var applyToEl = this.el.closest(this.options[applyTo + 'Selector']);
      this.options.bg && applyToEl.attr('style', this.options.bg);
    }
  }]);

  return Molecule701;
}(_baseComponent.BaseComponent);

},{"../../components/base-component.js":11}],37:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Molecule702 = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _baseComponent = require('../../components/base-component.js');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Molecule702 = exports.Molecule702 = function (_BaseComponent) {
  _inherits(Molecule702, _BaseComponent);

  _createClass(Molecule702, null, [{
    key: 'selector',
    get: function get() {
      return '.molecule-lb-702';
    }
  }]);

  function Molecule702() {
    var _ref;

    _classCallCheck(this, Molecule702);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    var _this = _possibleConstructorReturn(this, (_ref = Molecule702.__proto__ || Object.getPrototypeOf(Molecule702)).call.apply(_ref, [this].concat(args)));

    _this._init();
    return _this;
  }

  _createClass(Molecule702, [{
    key: '_init',
    value: function _init() {
      var _this2 = this;

      if (!GLOBALS.Utils.device.isMobile) {
        this.videoWrapper = this.el.find('.ambient-video-wrapper');
        this.config = this.videoWrapper.find('.video-config').data();
        this.el.closest('.section').prepend(this.videoWrapper).addClass('ambient-video-section');
        setTimeout(function () {
          return _this2._createVideo();
        });
      } else {
        this.el.remove();
      }
    }
  }, {
    key: '_createVideo',
    value: function _createVideo() {
      var _this3 = this;

      this.video = document.createElement('video');
      this.config.mp4 && this._addSource(this.config.mp4, 'video/mp4');
      this.config.webm && this._addSource(this.config.webm, 'video/webm');
      this.video.setAttribute('autoplay', 'autoplay');
      this.video.setAttribute('muted', 'muted');
      this.video.setAttribute('loop', 'loop');
      this.videoWrapper.append(this.video);
      this.video.addEventListener('loadeddata', function () {
        _this3.el.closest('.section').addClass('video-loaded');
        _this3._initAspectRatio();
      });
    }
  }, {
    key: '_initAspectRatio',
    value: function _initAspectRatio() {
      var _this4 = this;

      this.ratio = this.video.videoWidth / this.video.videoHeight;
      $(window).on('resize', _.debounce(function () {
        return _this4._calcVideoWidth();
      }, 100));
      this._calcVideoWidth();
    }
  }, {
    key: '_calcVideoWidth',
    value: function _calcVideoWidth() {
      var minWidth = this.ratio * this.videoWrapper.height();
      $(this.video).css('min-width', minWidth);
    }
  }, {
    key: '_addSource',
    value: function _addSource(src, type) {
      var source = document.createElement('source');
      source.src = src;
      source.type = type;
      this.video.appendChild(source);
    }
  }, {
    key: '_playVideo',
    value: function _playVideo() {
      this.video.play();
    }
  }]);

  return Molecule702;
}(_baseComponent.BaseComponent);

},{"../../components/base-component.js":11}],38:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Molecule823 = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _baseComponent = require('../../components/base-component.js');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Molecule823 = exports.Molecule823 = function (_BaseComponent) {
  _inherits(Molecule823, _BaseComponent);

  _createClass(Molecule823, null, [{
    key: 'selector',
    get: function get() {
      return '.molecule-823, .molecule-lb-823';
    }

    /*get defaults() {
      return $.extend({}, super.defaults, {
        fixedColumnSelector: 'fixed-column'
      });
    }*/

  }]);

  function Molecule823() {
    var _ref;

    _classCallCheck(this, Molecule823);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    var _this = _possibleConstructorReturn(this, (_ref = Molecule823.__proto__ || Object.getPrototypeOf(Molecule823)).call.apply(_ref, [this].concat(args)));

    _this._init();
    return _this;
  }

  _createClass(Molecule823, [{
    key: '_init',
    value: function _init() {
      var cellsLength = this.el.find('.rows-wrapper .first_row > div').length;
      this.el.addClass('columns-' + cellsLength);
      /*if (this.el.hasClass(this.options.fixedColumnSelector)) {
      	this.el.closest('.section').addClass('table-columns-' + cellsLength);
      	this.el.find('>.rows-wrapper').stickyTableColumns({
      		columns: 2,
      		startFrom: 1
      	});
      }*/
    }
  }]);

  return Molecule823;
}(_baseComponent.BaseComponent);

},{"../../components/base-component.js":11}],39:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ImageGallery = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _helpers = require('../helpers');

var _baseComponent = require('../../components/base-component.js');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ImageGallery = exports.ImageGallery = function (_BaseComponent) {
  _inherits(ImageGallery, _BaseComponent);

  _createClass(ImageGallery, [{
    key: 'ROOT_CLASS',


    // This class is added automatically to this.el component
    get: function get() {
      return 'image-gallery';
    }
  }, {
    key: 'defaults',
    get: function get() {
      return $.extend({}, _get(ImageGallery.prototype.__proto__ || Object.getPrototypeOf(ImageGallery.prototype), 'defaults', this), {
        enableArrowsMobileClass: 'enable-arrows-mobile',
        carouselCounterClass: 'display-carousel-counter',
        amountSlides: 0
      });
    }
  }]);

  function ImageGallery() {
    var _ref;

    _classCallCheck(this, ImageGallery);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    var _this = _possibleConstructorReturn(this, (_ref = ImageGallery.__proto__ || Object.getPrototypeOf(ImageGallery)).call.apply(_ref, [this].concat(args)));

    _this._init();
    return _this;
  }

  _createClass(ImageGallery, [{
    key: '_init',
    value: function _init() {
      //lazy init when in content-overlay
      if (this.el.closest('.content-overlay').length) {
        this._lazyInit();
      } else {
        this._instantInit();
      }
    }
  }, {
    key: '_lazyInit',
    value: function _lazyInit() {
      var _this2 = this;

      this.el.one('overlay.opened', function () {
        return _this2._instantInit();
      });
      //get rid of extra grid spacings
      this.el.one('overlay.beforeopen', function () {
        return _this2.el.closest('.content-overlay').find('>.section').addClass('drop-all-spacings');
      });
    }
  }, {
    key: '_instantInit',
    value: function _instantInit() {
      this.switcherEl = null;
      this.counterEl = null;
      this.faderHelperEl = null;
      this.thumbnailList = this.el.find('.thumbnails-list');
      this.imageContainer = this.el.find('.image-container');
      this.gallerySlider = this.el.find('.content-list');
      this.initialMedia = this._checkMedia();
      this._setImages();
      this._renderImages();
      this._wrapForScroll();
      this._prepareHTML();
      this.initListeners();
      this._addSwitcher();
      this._buildSlider();
      this._setCounter();
      this.onReady();
    }
  }, {
    key: 'initListeners',
    value: function initListeners() {
      var _this3 = this;

      this.thumbnailList.find('.item').on('mouseenter', function (e) {
        _this3._scrollTo($(e.currentTarget));
      });

      this.thumbnailList.find('.item').on('click', function (e) {
        var el = $(e.currentTarget);

        _this3.thumbnailList.find('.item').removeClass("active");
        el.addClass("active");
        console.log(el);
        _this3.gallerySlider.slick('slickGoTo', el.index());
      });

      this.gallerySlider.on('init reInit', function (e, slick, currentSlide) {
        return _this3._setActive(slick.currentSlide);
      });
      this.gallerySlider.on('beforeChange', function (e, slick, currentSlide, nextSlide) {
        return _this3._setActive(nextSlide);
      });
    }
  }, {
    key: '_prepareHTML',
    value: function _prepareHTML() {
      if (this.el.parents('.content-overlay').length) {
        this.faderHelperEl = $('<div />', { class: 'fader-helper' });
        this.el.find('.content-wrapper').prepend(this.faderHelperEl);
      }
    }
  }, {
    key: '_scrollTo',
    value: function _scrollTo(tab) {
      var scrolligPanel = this.el.find('.scrolling-panel');

      if (scrolligPanel.width() < scrolligPanel.get(0).scrollWidth) {
        var elPosLeft = Math.floor(tab.position().left),
            posLeft = elPosLeft - parseInt(this.thumbnailList.css("padding-left"));

        scrolligPanel.stop(true, true).delay(100).animate({
          scrollLeft: posLeft
        }, 300);
      }
    }
  }, {
    key: '_checkMedia',
    value: function _checkMedia() {
      var media;
      if (window.innerWidth > 720) {
        media = "desktop";
      } else {
        media = "mobile";
      }
      return media;
    }
  }, {
    key: '_crossBreakpoint',
    value: function _crossBreakpoint() {
      var media = this._checkMedia();
      if (media == this.initialMedia) {
        return;
      } else {
        this.initialMedia = media;
        this._renderImages();
      }
    }
  }, {
    key: '_setImages',
    value: function _setImages() {
      var _this4 = this;

      this.imageContainer.each(function (index, el) {
        var elem = $(el),
            container = elem.find("div"),
            options = {};

        container.each(function (index, it) {
          var item = $(it);
          if (item.hasClass("desktop")) {
            options.srcDesktop = item.data("src");
          } else if (item.hasClass("mobile")) {
            options.srcMobile = item.data("src");
          }
          options.alt = item.data("alt") || "";
          item.remove();
        });

        elem.append(_this4._createImage(options));
      });
    }
  }, {
    key: '_renderImages',
    value: function _renderImages() {
      this.imageContainer.find('img').each(function (index, image) {
        var imageItem = $(image),
            desktopSrc = imageItem.data("src-desktop"),
            mobileSrc = imageItem.data("src-mobile"),
            src = "";
        if (window.innerWidth > 720) {
          src = desktopSrc;
        } else {
          src = mobileSrc ? mobileSrc : desktopSrc;
        }

        imageItem.attr("src", src);
      });
    }
  }, {
    key: '_createImage',
    value: function _createImage(_ref2) {
      var srcDesktop = _ref2.srcDesktop,
          srcMobile = _ref2.srcMobile,
          alt = _ref2.alt;

      var image = $('<img />', {
        alt: alt,
        "data-src-desktop": srcDesktop,
        "data-src-mobile": srcMobile,
        class: srcMobile ? "hasMobileImage" : ""
      });
      return image;
    }
  }, {
    key: '_wrapForScroll',
    value: function _wrapForScroll() {
      var panel = $('<div />', {
        class: 'scrolling-panel'
      });
      this.thumbnailList.parents('.thumbnails-wrapper').wrapInner(panel);
    }
  }, {
    key: '_buildSlider',
    value: function _buildSlider() {
      var _this5 = this;

      this.gallerySlider.on('init', function (e, slick) {
        return _this5._onInit(e, slick);
      }).slick({
        touchMove: false,
        infinite: false,
        touchThreshold: 20,
        initialSlide: 0,
        fade: true
      });
      this.el.find('.slick-arrow').addClass(this.options.enableArrowsMobileClass);
    }
  }, {
    key: '_onInit',
    value: function _onInit(e, slick) {
      if (this.faderHelperEl) {
        GLOBALS.SliderHelper.alignElementsHeight(this.gallerySlider, this.gallerySlider.find('.slick-current .image-container'), this.faderHelperEl);
      }
    }
  }, {
    key: '_setCounter',
    value: function _setCounter() {
      var _this6 = this;

      if (this.el.hasClass(this.options.carouselCounterClass)) {
        var slick = this.gallerySlider.slick('getSlick');
        this.options.amountSlides = slick.slideCount;
        this.counterEl = $('<span />', { class: 'pagingInfo' });
        this.gallerySlider.on('reInit afterChange', function (e, slick, currentSlide) {
          return _this6._updateCounter(currentSlide);
        }).append(this.counterEl);

        this._updateCounter(slick.currentSlide);
      };
    }
  }, {
    key: '_updateCounter',
    value: function _updateCounter(currentSlide) {
      this.counterEl.text(currentSlide + 1 + '/' + this.options.amountSlides);
    }
  }, {
    key: '_setActive',
    value: function _setActive(slide) {
      var _this7 = this;

      this.thumbnailList.find('.item').removeClass("active");
      var activeTab = $(this.thumbnailList.find('.item').eq(slide)).addClass('active');

      this._scrollTo(activeTab);
      setTimeout(function () {
        return _this7._updateUnderline(activeTab);
      }, 100);
    }
  }, {
    key: '_updateUnderline',
    value: function _updateUnderline(activeTab) {
      var active = activeTab ? activeTab : this.thumbnailList.find('.item.active'),
          posLeft = active.position().left - (this.thumbnailList.outerWidth() - this.thumbnailList.width()) / 2;
      this.switcherEl.css({ width: active.width() }).css({ 'margin-left': posLeft });
    }
  }, {
    key: '_addSwitcher',
    value: function _addSwitcher() {
      this.switcherEl = $('<div/>', { class: 'switcher-inner' });
      var switcherWrapper = $('<div />', { class: 'switcher-wrapper' }).append(this.switcherEl);
      this.thumbnailList.append(switcherWrapper);
    }
  }, {
    key: 'onReady',
    value: function onReady() {
      var _this8 = this;

      this.gallerySlider.slick('setPosition');

      $(window).on('resize', _.debounce(function () {
        _this8._crossBreakpoint();
        _this8.gallerySlider.slick('setPosition');
        _this8._updateUnderline();
      }, 250));
    }
  }]);

  return ImageGallery;
}(_baseComponent.BaseComponent);

},{"../../components/base-component.js":11,"../helpers":27}],40:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.VideoGallery = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _baseComponent = require('../../components/base-component');

var _youtubePlayer = require('../../components/video/youtube-player/youtube-player');

var _brightcovePlayer = require('../../components/video/brightcove-player/brightcove-player');

var _lbSlider = require('../../components/sliders/lb-slider');

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var VideoGallery = exports.VideoGallery = function (_BaseComponent) {
  _inherits(VideoGallery, _BaseComponent);

  _createClass(VideoGallery, [{
    key: 'ROOT_CLASS',


    // This class is added automatically to this.el component
    get: function get() {
      return 'video-gallery';
    }
  }, {
    key: 'defaults',
    get: function get() {
      return $.extend({}, _get(VideoGallery.prototype.__proto__ || Object.getPrototypeOf(VideoGallery.prototype), 'defaults', this), {
        enableArrowsMobileClass: 'enable-arrows-mobile',
        playIconTpl: function playIconTpl() {
          return '<a class="play js_overlay_trigger" href="javascript:void(0)" title="play video" data-metrics-link-type="link" data-metrics-title="Play video"></a>';
        }
      });
    }
  }]);

  function VideoGallery() {
    var _ref;

    _classCallCheck(this, VideoGallery);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    var _this = _possibleConstructorReturn(this, (_ref = VideoGallery.__proto__ || Object.getPrototypeOf(VideoGallery)).call.apply(_ref, [this].concat(args)));

    _this._posters = [];
    _this._players = [];
    _this._videoGallery = null;
    _this._init();
    return _this;
  }

  _createClass(VideoGallery, [{
    key: '_init',
    value: function _init() {
      //lazy init when in content-overlay
      if (this.el.closest('.content-overlay').length) {
        this._lazyInit();
      } else {
        this._instantInit();
      }
    }
  }, {
    key: '_lazyInit',
    value: function _lazyInit() {
      var _this2 = this;

      this.el.one('overlay.opened', function () {
        return _this2._instantInit();
      });
      //get rid of extra grid spacings
      this.el.one('overlay.beforeopen', function () {
        return _this2.el.closest('.content-overlay').find('>.section').addClass('drop-all-spacings');
      });
      this.el.on('overlay.closed', function () {
        return _this2._refreshGallery();
      });
    }
  }, {
    key: '_instantInit',
    value: function _instantInit() {
      this._contentItems = this.el.find('.content-list .item');
      this._prepareHTML();
      this._initVideoItems();
      this._buildSliders();
    }
  }, {
    key: '_prepareHTML',
    value: function _prepareHTML() {
      this.faderHelperEl = $('<div />', { class: 'fader-helper' });
      this.el.find('.content-wrapper').prepend(this.faderHelperEl);
    }
  }, {
    key: '_initVideoItems',
    value: function _initVideoItems() {
      this._videoGallery = new VideoItemsFactory(this._contentItems);
    }
  }, {
    key: '_buildSliders',
    value: function _buildSliders() {
      var _this3 = this;

      this._mainSlider = this.el.find('.content-list');
      this._thumbSlider = this.el.find('.thumbnails-list').addClass('default');

      this._mainSlider.on('init', function (e, slick) {
        return _this3._onInit(e, slick);
      }).on('beforeChange', function () {
        return _this3._beforeChange.apply(_this3, arguments);
      }).on('afterChange', function (e, slick) {
        return _this3._afterChange(e, slick);
      }).slick({
        touchMove: false,
        infinite: true,
        touchThreshold: 100,
        swipe: false,
        fade: true
      });

      var mainSliderSlick = this._mainSlider.slick('getSlick');

      //init thumbs slider
      this._thumbsLbSlider = new _lbSlider.LbSlider(this._thumbSlider, {
        visibleSlides: 3,
        onSlide: function onSlide() {}, //mainSliderSlick.slickGoTo.bind(mainSliderSlick),
        customClass: 'small-arrows',
        alignArrowsToEl: 'a > img'
      });
    }
  }, {
    key: '_refreshGallery',
    value: function _refreshGallery() {
      //pause all videos and show posters
      this._videoGallery.pauseAll(true);

      //activate first slide
      this._mainSlider.slick('slickGoTo', 0, true);
    }
  }, {
    key: '_onInit',
    value: function _onInit(e, slick) {
      var _this4 = this;

      //update position & size on initialization
      setTimeout(function () {
        return slick.setPosition();
      }, 100);

      //position arrows relatively to image
      GLOBALS.SliderHelper.alignArrowsToEl(this._mainSlider, { alignToElSelector: '.video-container' });

      //align black helper element's height with a height of videos
      GLOBALS.SliderHelper.alignElementsHeight(this._mainSlider, this._mainSlider.find('.slick-current .video-container'), this.faderHelperEl);

      //show arrows for mobiles
      this._mainSlider.find('.slick-arrow').addClass(this.options.enableArrowsMobileClass + ' animate-top');

      //set counter
      this._mainSlider.append('<span class="pagingInfo" class="pagingInfo">1/' + slick.slideCount + '</span>');
      this._counter = this._mainSlider.find('.pagingInfo');

      //counter placement is updated after each setPosition call
      this._mainSlider.on('setPosition', function (lolo) {
        return _this4._updateCounterPlacement();
      });
    }

    //TODO handle arrows properly for a new slider

  }, {
    key: '_onInitThumbsSlider',
    value: function _onInitThumbsSlider(e, slick) {
      GLOBALS.SliderHelper.alignArrowsToEl(this._thumbSlider, { alignToElSelector: 'a > img' });
      this._thumbSlider.find('.slick-arrow').addClass(this.options.enableArrowsMobileClass + ' animate-top');
    }
  }, {
    key: '_beforeChange',
    value: function _beforeChange(e, slick, prevIndex, nextIndex) {
      this._videoGallery.pauseAll();
      this._thumbsLbSlider.slideTo(nextIndex);
      this._updateCounter(slick.slideCount, nextIndex);
    }
  }, {
    key: '_afterChange',
    value: function _afterChange(e, slick) {
      this._videoGallery.showPosters(slick.currentSlide);
    }
  }, {
    key: '_updateCounter',
    value: function _updateCounter(slideCount, currentSlide) {
      this._counter.text(currentSlide + 1 + '/' + slideCount);
    }
  }, {
    key: '_updateCounterPlacement',
    value: function _updateCounterPlacement() {
      var newTop = this._mainSlider.find('.slick-current .video-container').first().height();

      newTop && this._counter.css('top', newTop);
    }
  }]);

  return VideoGallery;
}(_baseComponent.BaseComponent);

var VideoItemsFactory = function () {
  function VideoItemsFactory(items) {
    _classCallCheck(this, VideoItemsFactory);

    this._init(items);
  }

  _createClass(VideoItemsFactory, [{
    key: '_init',
    value: function _init(items) {
      this._items = items;
      this._videos = [];
      this._lazyInit(0);
    }
  }, {
    key: '_lazyInit',
    value: function _lazyInit(index) {
      var _this5 = this;

      var el = this._items.eq(index);
      if (el.length) {
        this._videos.push(new VideoItem(el));
        setTimeout(function () {
          return _this5._lazyInit(++index);
        }, 100);
      }
    }
  }, {
    key: 'pauseAll',
    value: function pauseAll(showPoster) {
      this._videos.forEach(function (video) {
        return video.pauseVideo(showPoster);
      });
    }
  }, {
    key: 'showPosters',
    value: function showPosters(excludeIndex) {
      this._videos.forEach(function (video, i) {
        return i !== excludeIndex && video.showPoster();
      });
    }
  }]);

  return VideoItemsFactory;
}();

var VideoItem = function () {
  _createClass(VideoItem, [{
    key: 'options',
    get: function get() {
      return {
        youtubeVideoSelector: '.youtube-video-template',
        brightcoveVideoSelector: '.bc-video-template',
        mobileQuery: window.matchMedia('screen and (max-width: 960px)'),
        playIconTpl: function playIconTpl() {
          return '<a class="play js_overlay_trigger" href="javascript:void(0)" title="play video" data-metrics-link-type="link" data-metrics-title="Play video"></a>';
        }
      };
    }
  }]);

  function VideoItem(el) {
    _classCallCheck(this, VideoItem);

    this._init(el);
  }

  _createClass(VideoItem, [{
    key: '_init',
    value: function _init(el) {
      this.el = el;
      this._poster = null;
      this._player = null;
      this._initPoster();
      this._initPlayer();
      this._initListeners();
    }
  }, {
    key: '_initPoster',
    value: function _initPoster() {
      var desktopBg = this.el.find('.desktop').attr('data-src'),
          mobileBg = this.el.find('.mobile').attr('data-src') || desktopBg,
          posterEl = $('<div />', {
        'class': 'img',
        'style': this._getBgStyle(desktopBg, mobileBg)
      });

      this._poster = this.el.find('.image-container').append(posterEl).append(this.options.playIconTpl()).data({
        bgs: [desktopBg, mobileBg]
      });
    }
  }, {
    key: '_updatePosterImg',
    value: function _updatePosterImg() {
      this._poster.find('.img').attr('style', this._getBgStyle.apply(this, _toConsumableArray(this._poster.data().bgs)));
    }
  }, {
    key: '_initPlayer',
    value: function _initPlayer(el) {
      this._player = this._createVideoPlayer();
      this._player.init();
    }
  }, {
    key: '_initListeners',
    value: function _initListeners() {
      var _this6 = this;

      this.el.find('.play').on('click', function () {
        return _this6.playVideo();
      });
      this.options.mobileQuery.addListener(function () {
        return _this6._updatePosterImg();
      });
    }
  }, {
    key: '_createVideoPlayer',
    value: function _createVideoPlayer() {
      var youtubeVideoElement = this.el.find(this.options.youtubeVideoSelector),
          brightcoveVideoElement = this.el.find(this.options.brightcoveVideoSelector);

      if (youtubeVideoElement.length) {
        return new _youtubePlayer.YoutubePlayer(youtubeVideoElement);
      } else if (brightcoveVideoElement.length) {
        return new _brightcovePlayer.BrightcovePlayer(brightcoveVideoElement);
      }
      return {};
    }
  }, {
    key: '_getBgStyle',
    value: function _getBgStyle(imgUrlDesktop, imgUrlMobile) {
      var imgUrl = this.options.mobileQuery.matches ? imgUrlMobile : imgUrlDesktop;
      return 'background: url(' + imgUrl + ') 50% 50% / cover no-repeat scroll;';
    }
  }, {
    key: '_hidePoster',
    value: function _hidePoster() {
      this._poster.fadeOut(800);
    }
  }, {
    key: 'showPoster',
    value: function showPoster() {
      this._poster.fadeIn(400);
    }
  }, {
    key: 'playVideo',
    value: function playVideo() {
      this._player.play();
      this._hidePoster();
    }

    /*
     * @param {boolean} showPoster - indicates whether to show poster after video has been paused
     *                               and rewinds video to the beginning
     */

  }, {
    key: 'pauseVideo',
    value: function pauseVideo(showPoster) {
      if (this._player) {
        if (!showPoster) {
          this._player.pause();
        } else {
          this._player.stop();
          this.showPoster();
        }
      }
    }
  }]);

  return VideoItem;
}();

},{"../../components/base-component":11,"../../components/sliders/lb-slider":17,"../../components/video/brightcove-player/brightcove-player":19,"../../components/video/youtube-player/youtube-player":20}],41:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Molecule827Factory = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _baseComponent = require('../../components/base-component.js');

var _moleculeLb827VideoGallery = require('./molecule-lb-827-video-gallery.js');

var _moleculeLb827ImageGallery = require('./molecule-lb-827-image-gallery.js');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Molecule827Factory = exports.Molecule827Factory = function (_BaseComponent) {
  _inherits(Molecule827Factory, _BaseComponent);

  _createClass(Molecule827Factory, [{
    key: 'defaults',
    get: function get() {
      return $.extend({}, _get(Molecule827Factory.prototype.__proto__ || Object.getPrototypeOf(Molecule827Factory.prototype), 'defaults', this), {
        videoGalleryIndicator: '.video-container'
      });
    }
  }], [{
    key: 'selector',
    get: function get() {
      return '.molecule-lb-827';
    }
  }]);

  function Molecule827Factory() {
    var _ref;

    _classCallCheck(this, Molecule827Factory);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    var _this = _possibleConstructorReturn(this, (_ref = Molecule827Factory.__proto__ || Object.getPrototypeOf(Molecule827Factory)).call.apply(_ref, [this].concat(args)));

    _this._init();
    return _this;
  }

  _createClass(Molecule827Factory, [{
    key: '_init',
    value: function _init() {
      if (this.el.find(this.options.videoGalleryIndicator).length) {
        new _moleculeLb827VideoGallery.VideoGallery(this.el);
      } else {
        new _moleculeLb827ImageGallery.ImageGallery(this.el);
      }
    }
  }]);

  return Molecule827Factory;
}(_baseComponent.BaseComponent);

},{"../../components/base-component.js":11,"./molecule-lb-827-image-gallery.js":39,"./molecule-lb-827-video-gallery.js":40}],42:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Molecule830nonr = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _baseComponent = require('../../components/base-component.js');

var _stickable = require('../../common/scripts/stickable.js');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Molecule830nonr = exports.Molecule830nonr = function (_BaseComponent) {
  _inherits(Molecule830nonr, _BaseComponent);

  _createClass(Molecule830nonr, [{
    key: 'defaults',
    get: function get() {
      return $.extend({}, _get(Molecule830nonr.prototype.__proto__ || Object.getPrototypeOf(Molecule830nonr.prototype), 'defaults', this), {
        disableStickableClassname: 'non-stickable',
        swapAtBreakpoint: '720',
        mobileQuery: window.matchMedia('screen and (max-width: 720px)')
      });
    }
  }], [{
    key: 'selector',
    get: function get() {
      return '.molecule-lb-830';
    }
  }]);

  function Molecule830nonr() {
    var _ref;

    _classCallCheck(this, Molecule830nonr);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    var _this = _possibleConstructorReturn(this, (_ref = Molecule830nonr.__proto__ || Object.getPrototypeOf(Molecule830nonr)).call.apply(_ref, [this].concat(args)));

    _this._init();
    return _this;
  }

  _createClass(Molecule830nonr, [{
    key: '_init',
    value: function _init() {
      this.nav = this.el.find('nav');

      if (this._hasSubmenu()) {
        this._prepareContent();
        this.dropTrigger = this.el.find('.title a');
        this._initListeners();
      } else {
        this.el.addClass('no-submenu');
      }

      if (!this.el.hasClass(this.options.disableStickableClassname)) {
        this._initStickableBehaviour();
      }
    }
  }, {
    key: '_hasSubmenu',
    value: function _hasSubmenu() {
      return !!this.el.find('ul.level2').length;
    }
  }, {
    key: '_prepareContent',
    value: function _prepareContent() {
      var copyContent = '';
      var wrapNav = $('<div />', { class: 'nav-wrapper' });

      this.level3Container = $('<div />', { class: 'level3-container' });
      this.el.find('.level2 > .selectable > .expand-area').each(function (i, item) {
        var el = $(item),
            id = el.attr('id');
        copyContent += '<div id="' + id + '-desktop" class="expand-area">' + el.html() + '</div>';
      });
      this.level3Container.html(copyContent);
      wrapNav.append(this.el.find('.level2'));
      wrapNav.find('.level2').wrap('<div class="level1-container"/>');
      wrapNav.append(this.level3Container);
      this.nav.append(wrapNav);

      this.drop = wrapNav;
      this.allDesktopDrops = wrapNav.find('.level3-container .expand-area');
      this.mobileDrops = wrapNav.find('.selectable .expand-area');
      this.allTriggers = wrapNav.find('.level2 >li.selectable > a');
      this.level2Container = this.el.find('.level2');
    }
  }, {
    key: '_initListeners',
    value: function _initListeners() {
      var _this2 = this;

      this.el.find('.level2 > .selectable a').on('click', function (e) {
        var el = $(e.currentTarget),
            preffix = _this2._getPreffix(),
            relId = $(e.currentTarget).attr('rel') + preffix,
            drop = $('#' + relId);

        if (el.hasClass('active')) {
          _this2.allDesktopDrops.removeClass('active');
          _this2.mobileDrops.slideUp();
          _this2.allTriggers.removeClass('active');
          _this2.level2Container.removeClass('active');
        } else {
          //preffix truthy only for desktops
          if (preffix) {
            _this2.allDesktopDrops.removeClass('active');
            _this2.level2Container.addClass('active');
            drop.addClass('active');
          } else {
            _this2.mobileDrops.slideUp();
            drop.slideDown();
          }
          _this2.allTriggers.removeClass('active');
          el.addClass('active');
        }
      });
      this.dropTrigger.on('click', function (e) {
        var el = $(e.currentTarget);
        e.preventDefault();
        if (el.hasClass('active')) {
          _this2._closeAll(true);
        } else {
          el.addClass('active');
          _this2.drop.slideDown(function () {
            _this2.drop.css('display', 'flex');
          });
        }
      });

      $('body').on('click', function (e) {
        _this2._clickOutsideCb(e);
      });
      this.options.mobileQuery.addListener(function (e) {
        return _this2._closeAll(e);
      });
    }
  }, {
    key: '_getPreffix',
    value: function _getPreffix() {
      var preffix = window.matchMedia('screen and (min-width: ' + this.options.swapAtBreakpoint + 'px)').matches ? '-desktop' : '';
      return preffix;
    }
  }, {
    key: '_initStickableBehaviour',
    value: function _initStickableBehaviour() {
      this.stickableEl = this.el.find('nav');
      new _stickable.Stickable(this.stickableEl, this.stickableEl.parent());
    }
  }, {
    key: '_closeAll',
    value: function _closeAll() {
      var _this3 = this;

      this.mobileDrops.slideUp();
      this.drop.slideUp(function () {
        _this3.allTriggers.removeClass('active');
        _this3.allDesktopDrops.removeClass('active');
        _this3.level2Container.removeClass('active');
      });

      if (arguments.length) {
        this.dropTrigger.removeClass('active');
      }
    }
  }, {
    key: '_clickOutsideCb',
    value: function _clickOutsideCb(e) {
      if (!$(e.target).closest('.molecule-lb-830').length) {
        this._closeAll(true);
      }
    }
  }]);

  return Molecule830nonr;
}(_baseComponent.BaseComponent);

},{"../../common/scripts/stickable.js":7,"../../components/base-component.js":11}],43:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Molecule835 = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _baseComponent = require('../../components/base-component.js');

var _stickable = require('../../common/scripts/stickable.js');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Molecule835 = exports.Molecule835 = function (_BaseComponent) {
  _inherits(Molecule835, _BaseComponent);

  _createClass(Molecule835, [{
    key: 'defaults',
    get: function get() {
      return $.extend({}, _get(Molecule835.prototype.__proto__ || Object.getPrototypeOf(Molecule835.prototype), 'defaults', this), {
        rowSelector: '.section',
        alternativeThemeClass: 'appearance-theme-2',
        alternativeThemeRowClass: 'main-nav-835-theme-2',
        rowClass: 'main-nav-835',
        mobileTpl: '<div class="mobile-nav">' + '<div class="anchors">' + '<%= dropTrigger %>' + '<div class="drop"><ul><%= items %></ul></div>' + '</div>' + '<% if (btn) { %>' + '<div class="btn-wrapper small-btn">' + '<%= btn %>' + '</div>' + '<% } %>' + '</div>'
      });
    }
  }], [{
    key: 'selector',
    get: function get() {
      return '.molecule-lb-835';
    }
  }]);

  function Molecule835() {
    var _ref;

    _classCallCheck(this, Molecule835);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    var _this = _possibleConstructorReturn(this, (_ref = Molecule835.__proto__ || Object.getPrototypeOf(Molecule835)).call.apply(_ref, [this].concat(args)));

    _this._init();
    return _this;
  }

  _createClass(Molecule835, [{
    key: '_init',
    value: function _init() {
      if (this.el.hasClass(this.options.alternativeThemeClass)) {
        this.parentSection = this.el.closest(this.options.rowSelector).addClass(this.options.alternativeThemeRowClass);
        this._activateLink();
        $(this.el).wrapInner("<div class='inner-wrapper'></div>");
        if (!this.el.hasClass('non-stickable')) {
          this._initStickableBehaviour();
          this._initHeightUpdate();
        }
        return;
      }
      var length = this.el.find('.links > li').length;
      this.el.addClass('width-' + length);
      this.el.closest(this.options.rowSelector).addClass(this.options.rowClass);
      this._markExternalLinks();
      this._prepareBuyButton();
      this._prepareMobile();
      this._initListeners();
      if (!this.el.hasClass('non-stickable')) {
        this._initStickableBehaviour();
      }
      this.activeNavIndex = 0;
      this._clickOutsideCb = this._clickOutsideCb.bind(this);
    }
  }, {
    key: '_activateLink',
    value: function _activateLink() {
      var currentLocation = window.location.href;
      this.el.find('.links > li > a').each(function (index, el) {
        if ($(el).attr('href') === currentLocation) {
          $(el).addClass('active');
        }
      });
    }
  }, {
    key: '_markExternalLinks',
    value: function _markExternalLinks() {
      this.el.find('.links > li > a').each(function (index, el) {
        if ($(el).attr('href').indexOf("#") !== 0) {
          $(el).addClass('external-page-link');
        }
      });
    }
  }, {
    key: '_prepareBuyButton',
    value: function _prepareBuyButton() {
      var lastLink = this.el.find('.links > li:last-child > a'),
          drop = this.el.find('.molecule-lb-406');

      if (lastLink.length && lastLink.attr('href').indexOf('#') !== 0) {
        lastLink.addClass('button primary').closest('li').addClass('small-btn');
      }
      if (drop.length) {
        drop.parent().addClass('drop-link');
      }
    }
  }, {
    key: '_prepareMobile',
    value: function _prepareMobile() {
      var tpl = _.template(this.options.mobileTpl),
          items = this.el.find('.links').clone().html(),
          btn = this.el.find('li.small-btn, li.drop-link').clone().html(),
          firstAnchor = this.el.find('.links > li:first-child > a'),
          dropTrigger = '<a class="drop-trigger" href="javascript: void(0)">' + firstAnchor.text() + '</a>';

      this.el.append(tpl({ items: items, btn: btn, dropTrigger: dropTrigger }));
    }
  }, {
    key: '_initListeners',
    value: function _initListeners() {
      var _this2 = this;

      this.dropTrigger = this.el.find('.anchors > a');
      this.drop = this.el.find('.drop');
      this.mobileNav = this.el.find('.mobile-nav');
      this.anchorTriggers = this.el.find('.links > li > a:not(.button, .external-page-link), .mobile-nav .drop > ul > li > a:not(.button, .external-page-link)');
      this.anchorSections = this._collectSections();

      this.dropTrigger.on('click', function (el) {
        if (_this2.mobileNav.hasClass('active')) {
          _this2._collapseDrop();
        } else {
          _this2._expandDrop();
        }
      });

      this.anchorTriggers.on('click', function (e) {
        _this2.dropTrigger.html($(e.currentTarget).text());
        _this2._collapseDrop();
        GLOBALS.ScrollTo.navigate($(e.currentTarget).attr('href'), _this2._updateActiveAnchor.bind(_this2), 1000);
        e.preventDefault();
      });

      $(window).on('scroll', function () {
        if (!GLOBALS.ScrollTo.animating) {
          _this2._updateActiveAnchor();
        }
      });
    }
  }, {
    key: '_updateActiveAnchor',
    value: function _updateActiveAnchor() {
      var currentNavHeight = this.el.outerHeight(),
          activeSection = void 0;
      for (var i = this.anchorSections.length - 1; i >= 0; i--) {
        var clientRect = this.anchorSections[i][0].getBoundingClientRect();

        if (clientRect.top < currentNavHeight) {
          if (i !== this.anchorSections.length - 1) {
            activeSection = this.anchorSections[i];
            break;
          } else {
            if (clientRect.bottom > currentNavHeight) {
              activeSection = this.anchorSections[i];
            }
            break;
          }
        };
      }
      if (activeSection) {
        var activeSectionAnchor = activeSection.data('anchor'),
            activeTriggers = this.anchorTriggers.filter(function (index, el) {
          return $(el).attr('href') === '#' + activeSectionAnchor;
        });
        this.anchorTriggers.removeClass('active');
        this.dropTrigger.html(activeTriggers.eq(0).text());
        activeTriggers.addClass('active');
      } else {
        this.anchorTriggers.removeClass('active');
      }
    }
  }, {
    key: '_expandDrop',
    value: function _expandDrop() {
      $('body').on('click', this._clickOutsideCb);
      this.mobileNav.addClass('active');
      this.drop.slideDown(200);
    }
  }, {
    key: '_collapseDrop',
    value: function _collapseDrop() {
      $('body').off('click', this._clickOutsideCb);
      this.mobileNav.removeClass('active');
      this.drop.slideUp(200);
    }
  }, {
    key: '_initStickableBehaviour',
    value: function _initStickableBehaviour() {
      new _stickable.Stickable(this.el);
    }
  }, {
    key: '_initHeightUpdate',
    value: function _initHeightUpdate() {
      var _this3 = this;

      var row = this.parentSection.find('>.row');
      var updateHeight = function updateHeight() {
        row.height(_this3.el.height());
      };
      $(window).on('load resize', _.debounce(function () {
        return updateHeight();
      }, 100));
      updateHeight();
    }
  }, {
    key: '_clickOutsideCb',
    value: function _clickOutsideCb(e) {
      if (!$(e.target).closest('.molecule-lb-835').length) {
        this._collapseDrop();
      }
    }
  }, {
    key: '_collectSections',
    value: function _collectSections() {
      var sections = [];
      this.el.find('.links > li > a:not(.button, .external-page-link)').each(function (index, el) {
        var anchorId = $(el).attr('href').replace('#', ''),
            anchorSeaction = $('[data-anchor="' + anchorId + '"]');
        anchorSeaction.length && sections.push(anchorSeaction);
      });
      return sections;
    }
  }]);

  return Molecule835;
}(_baseComponent.BaseComponent);

},{"../../common/scripts/stickable.js":7,"../../components/base-component.js":11}],44:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Molecule836Theme2 = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _baseComponent = require('../../components/base-component.js');

var _stickable = require('../../common/scripts/stickable.js');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Molecule836Theme2 = exports.Molecule836Theme2 = function (_BaseComponent) {
  _inherits(Molecule836Theme2, _BaseComponent);

  _createClass(Molecule836Theme2, [{
    key: 'defaults',
    get: function get() {
      return $.extend({}, _get(Molecule836Theme2.prototype.__proto__ || Object.getPrototypeOf(Molecule836Theme2.prototype), 'defaults', this), {
        rowSelector: '.section',
        rowClass: 'section-with-tabs-theme-2',
        stickyWrapperSelector: '.sticky-wrapper',
        contentClass: 'tab-content content-theme-2',
        activeClass: 'tab-content-active',
        activeTabClass: 'active-tab',
        contentSelector: '.flex-content',
        linksContainerSelector: '.tabs-container',
        linksSelector: 'a',
        sliderSelector: '.slider-carousel-arrows-and-dots',
        underlineSliderTpl: '<div class="underline-slider">' + '<span class="switcher"></span>' + '</div>'
      });
    }
  }], [{
    key: 'selector',
    get: function get() {
      return '.molecule-lb-836.appearance-theme-2';
    }
  }]);

  function Molecule836Theme2() {
    var _ref;

    _classCallCheck(this, Molecule836Theme2);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    var _this = _possibleConstructorReturn(this, (_ref = Molecule836Theme2.__proto__ || Object.getPrototypeOf(Molecule836Theme2)).call.apply(_ref, [this].concat(args)));

    _this._init();
    return _this;
  }

  _createClass(Molecule836Theme2, [{
    key: '_init',
    value: function _init() {

      this._addUnderlineSlider();
      this._wrapForScrolling();
      this._collectContents();
      this._wrapLinks();
      this.isScrollable = false;
      this._initListeners();
      this._wrapSticky();
      this._initStickableBehaviour();
      this._checkScrollable();
      this.processContentsBackground();
      var self = this;
      this.tabHashes = {};

      this.links.each(function (index, link) {
        if (link.hash) {
          self.tabHashes[link.hash] = true;
        }
      });

      if (!this._openTabByHash()) {
        this.links.first().trigger('click', { initialClick: true });
      }
    }

    //here detecting tabs sections with content

  }, {
    key: '_collectContents',
    value: function _collectContents() {
      this.container = this.el.find(this.options.linksContainerSelector);
      this.links = this.container.find(this.options.linksSelector);
      this.contents = this.el.parents(this.options.contentSelector).nextAll(this.options.contentSelector).slice(0, this.links.length).addClass(this.options.contentClass);

      this.el.closest(this.options.rowSelector).addClass(this.options.rowClass);
    }
  }, {
    key: '_wrapLinks',
    value: function _wrapLinks() {
      this.links.each(function (index, el) {
        $(el).wrapInner("<span></span>");
      });
    }
  }, {
    key: '_openTabByHash',
    value: function _openTabByHash(hash, forceScroll) {
      var tab = this._findTabByHash(hash),
          self = this;

      if (!tab.length) {
        return false;
      }

      if (!tab.parent().hasClass(this.options.activeTabClass)) {
        tab.each(function (i, $el) {
          self._updateActiveTab($el);
          self._updateActiveContent($el);
          self._updateUnderline($el);
        });
      }

      var target = $(this.el),
          stickyNavBefore = self._getPrevStickyNavHeight() || 0,
          topNumber = target.offset().top - stickyNavBefore;
      if (!target.hasClass('no-initial-scroll') || forceScroll) {
        window.setTimeout(function () {
          GLOBALS.ScrollTo.navigateTo(topNumber, 1000);
        }, 200);
      }

      return true;
    }
  }, {
    key: '_findTabByHash',
    value: function _findTabByHash(hash) {
      hash = hash || window.location.hash;
      return $($.grep(this.tabTriggers, function (item) {
        return hash && item.hash === hash;
      }));
    }
  }, {
    key: '_addUnderlineSlider',
    value: function _addUnderlineSlider() {
      var tpl = _.template(this.options.underlineSliderTpl);
      this.el.find('ul').append(tpl);
    }
  }, {
    key: '_wrapForScrolling',
    value: function _wrapForScrolling() {
      this.el.find('ul').wrap('<div class="scrolling-panel"></div>');
      this.scrollableDiv = this.el.find('.scrolling-panel');
    }
  }, {
    key: '_checkScrollable',
    value: function _checkScrollable() {
      var parentUl = this.el.find('ul');
      if (document.body.clientWidth < parentUl.width()) {
        this.isScrollable = true;
      } else {
        this.isScrollable = false;
      }
    }
  }, {
    key: '_getPrevStickyNavHeight',
    value: function _getPrevStickyNavHeight() {
      var prevParent = this.el.parents(this.options.contentSelector).prevAll('.flex-content').not('.tab-content'),
          prevParentNear = prevParent.children().find(".molecule-lb-835, .molecule-lb-836, .molecule-lb-830").not(".non-stickable"),
          maxHeight = 0;

      if (prevParentNear.length > 0) {
        maxHeight = $(prevParentNear.eq(0)).height();
      }
      return maxHeight;
    }
  }, {
    key: '_initListeners',
    value: function _initListeners() {
      var _this2 = this;

      this.tabTriggers = this.el.find('a');
      var self = this,
          $htmlOrBody = $('html, body');

      // Any link on a page may be targeting tabs
      $('body').on('click', 'a', function (e) {
        var link = e.currentTarget;
        if (self.tabTriggers.index(link) == -1) {
          if (link.hash && link.hash in self.tabHashes) {
            self._openTabByHash(link.hash, true);
            window.location.hash = $(link).attr('href');
            e.preventDefault();
          }
        }
      });

      this.tabTriggers.on('click', function (e, params) {
        var clicked = $(e.currentTarget);
        _this2._updateActiveTab(clicked);
        _this2._updateActiveContent(clicked);
        _this2._updateUnderline(clicked);

        if (!params || !params.initialClick) {
          if (clicked.attr('href').length > 1) {
            window.location.hash = clicked.attr('href');
          }
          var target = $(_this2.el),
              stickyNavBefore = self._getPrevStickyNavHeight() || 0,
              topNumber = target.offset().top - stickyNavBefore;
          window.setTimeout(function () {
            GLOBALS.ScrollTo.navigateTo(topNumber, 1000);
          }, 0);
        }

        $(window).trigger("resize");
      });

      this.tabTriggers.on('mouseenter', function (e, params) {
        var focused = $(e.currentTarget);

        if (self.isScrollable) {
          setTimeout(function () {
            self._scrollTo(focused);
          }, 500);
        }
      });

      $(window).on("resize", function () {
        var self = this;
        setTimeout(function () {
          var activeLink = self.el.find("." + self.options.activeTabClass).find("a");
          self._checkScrollable();
          self._updateMargins();
          self._centerActiveItem(activeLink);
        }, 250);
      }.bind(this));

      $(window).bind("load", function () {
        var activeLink = this.el.find("." + this.options.activeTabClass).find("a");

        this._updateUnderline(activeLink);
        this._checkScrollable();
        this._updateMargins();
        this._centerActiveItem(activeLink);
      }.bind(this));

      $htmlOrBody.on(['scroll', 'mousedown', 'wheel', 'DOMMouseScroll', 'mousewheel', 'keyup', 'touchmove'].join(' '), function stopTabsAnimations() {
        $htmlOrBody.stop();
      });
    }
  }, {
    key: '_updateActiveTab',
    value: function _updateActiveTab(el) {
      var activeTab = $(el),
          index = this.links.index(activeTab);

      if (index != -1) {
        this.tabTriggers.parents('li').removeClass(this.options.activeTabClass);
        $(this.links.parent('li').eq(index)).addClass(this.options.activeTabClass);
      }
    }
  }, {
    key: '_updateActiveContent',
    value: function _updateActiveContent(el) {
      var clickedLink = $(el),
          index = this.links.index(clickedLink);

      if (index != -1) {
        this.contents.removeClass(this.options.activeClass).eq(index).addClass(this.options.activeClass);
      }
    }
  }, {
    key: '_updateUnderline',
    value: function _updateUnderline(el) {
      var clickedLink = $(el),
          index = this.links.index(clickedLink),
          switcher = this.el.find('.underline-slider .switcher'),
          parentUl = this.el.find('ul'),
          offsetTarget = parseInt(clickedLink.offset().left - parentUl[0].getBoundingClientRect().left - parentUl.css("padding-left").replace("px", "")) + "px",
          widthTarget = parseInt(clickedLink.width()) + "px";

      if (index != -1) {
        switcher.css("width", widthTarget).css("margin-left", offsetTarget);
      }
    }
  }, {
    key: '_scrollTo',
    value: function _scrollTo(el) {
      var $el = $(el),
          posLeft = void 0;

      posLeft = parseInt($($el).offset().left - (this.scrollableDiv.width() - $el.outerWidth(true)) / 2 + this.scrollableDiv.scrollLeft());

      this.scrollableDiv.stop().animate({
        scrollLeft: posLeft
      }, 300);
    }
  }, {
    key: '_getCenter',
    value: function _getCenter(el) {
      var $el = $(el),
          centering = parseInt(document.body.clientWidth / 2 - $el.width() / 2);
      return centering;
    }
  }, {
    key: '_updateMargins',
    value: function _updateMargins() {
      var parentUl = this.el.find("ul"),
          lastElIndex = parentUl.find("li").last().index(),
          spaceForCentering = void 0,
          paddingProp = void 0,
          marginProp = void 0,
          marginSpace = void 0,
          items = parentUl.find("li");

      if (this.isScrollable) {
        items.each(function (index, el) {
          if (index == 0) {
            paddingProp = "padding-left";
            marginProp = "margin-left";
            spaceForCentering = this._getCenter(el);
          } else if (index == lastElIndex) {
            paddingProp = "padding-right";
            marginProp = "margin-right";
            spaceForCentering = this._getCenter(el);
          } else {
            return;
          }

          marginSpace = parseInt(spaceForCentering - parentUl.css(paddingProp).replace("px", "")) + "px";
          parentUl.css(marginProp, marginSpace);
        }.bind(this));
      } else {
        $(parentUl).css("margin", 0);
      }
    }
  }, {
    key: '_centerActiveItem',
    value: function _centerActiveItem(item) {
      var activeLink = $(item),
          self = this;

      setTimeout(function () {
        self._scrollTo(activeLink);
      }, 300);
    }
  }, {
    key: '_wrapSticky',
    value: function _wrapSticky() {
      $(this.el).wrapInner('<div class="sticky-wrapper"></div>');
    }
  }, {
    key: '_initStickableBehaviour',
    value: function _initStickableBehaviour() {
      if (!this.el.hasClass('non-stickable')) {
        new _stickable.Stickable(this.el.find(this.options.stickyWrapperSelector));
      }
    }
  }, {
    key: 'processContentsBackground',
    value: function processContentsBackground() {
      var self = this;
      this.contents.each(function () {
        var $tabContent = $(this),
            sectionInSlider = $tabContent.find(self.options.sliderSelector),
            className = '';

        if (sectionInSlider.length && sectionInSlider[0].className.indexOf('bkg-color-') > -1) {
          sectionInSlider.each(function () {
            var classes = this.className.split(' ');
            for (var i = 0; i < classes.length; i++) {
              if (classes[i].indexOf('bkg-color-') > -1) {
                className = classes[i];
              }
            }
          });
          $tabContent.find('.section').addClass(className);
        }
      });
    }
  }]);

  return Molecule836Theme2;
}(_baseComponent.BaseComponent);

},{"../../common/scripts/stickable.js":7,"../../components/base-component.js":11}],45:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Molecule836 = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _baseComponent = require('../../components/base-component.js');

var _stickable = require('../../common/scripts/stickable.js');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Molecule836 = exports.Molecule836 = function (_BaseComponent) {
  _inherits(Molecule836, _BaseComponent);

  _createClass(Molecule836, [{
    key: 'defaults',
    get: function get() {
      return $.extend({}, _get(Molecule836.prototype.__proto__ || Object.getPrototypeOf(Molecule836.prototype), 'defaults', this), {
        rowSelector: '.section',
        rowClass: 'section-with-tabs',
        stickyWrapperSelector: '.sticky-wrapper',
        contentClass: 'tab-content',
        activeClass: 'tab-content-active',
        activeTabClass: 'active-tab',
        contentSelector: '.flex-content',
        linksContainerSelector: '.tabs-container',
        linksSelector: 'a',
        activeMobile: 'mob-active',
        linksContainerMobile: '.mobile-tabs-container .drop',
        sliderSelector: '.slider-carousel-arrows-and-dots',
        mobileTpl: '<div class="mobile-tabs">' + '<div class="mobile-tabs-container">' + '<%= dropTrigger %>' + '<div class="drop"><ul><%= items %></ul></div>' + '</div>' + '</div>'
      });
    }
  }], [{
    key: 'selector',
    get: function get() {
      return '.molecule-lb-836:not(.appearance-theme-2)';
    }
  }]);

  function Molecule836() {
    var _ref;

    _classCallCheck(this, Molecule836);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    var _this = _possibleConstructorReturn(this, (_ref = Molecule836.__proto__ || Object.getPrototypeOf(Molecule836)).call.apply(_ref, [this].concat(args)));

    _this._init();
    return _this;
  }

  _createClass(Molecule836, [{
    key: '_init',
    value: function _init() {

      this._prepareMobile();
      this._collectContents();
      this._wrapLinks();
      this.isMobile = false;
      this._initListeners();
      this._wrapSticky();
      this._initStickableBehaviour();
      this._clickOutsideCb = this._clickOutsideCb.bind(this);
      this._checkMobile();
      this.processContentsBackground();

      var self = this;
      this.tabHashes = {};

      this.links.each(function (index, link) {
        if (link.hash) {
          self.tabHashes[link.hash] = true;
        }
      });

      if (!this._openTabByHash()) {
        this.links.first().trigger('click', { initialClick: true });
        this.linksMobile.first().trigger('click', { initialClick: true });
      }
    }

    //here detecting tabs sections with content

  }, {
    key: '_collectContents',
    value: function _collectContents() {
      this.container = this.el.find(this.options.linksContainerSelector);
      this.links = this.container.find(this.options.linksSelector);
      this.containerMobile = this.el.find(this.options.linksContainerMobile);
      this.linksMobile = this.containerMobile.find(this.options.linksSelector);
      this.contents = this.el.parents(this.options.contentSelector).nextAll(this.options.contentSelector).slice(0, this.links.length).addClass(this.options.contentClass);
      this.el.closest(this.options.rowSelector).addClass(this.options.rowClass);
    }
  }, {
    key: '_wrapLinks',
    value: function _wrapLinks() {
      this.links.each(function (index, el) {
        $(el).wrapInner("<span></span>");
      });
    }
  }, {
    key: '_openTabByHash',
    value: function _openTabByHash(hash, forceScroll) {
      var tab = this._findTabByHash(hash),
          self = this;

      if (!tab.length) {
        return false;
      }

      if (!tab.parent().hasClass(this.options.activeTabClass)) {
        tab.each(function (i, $el) {
          self._updateActiveTab($el);
          self._updateActiveContent($el);
        });
      }

      var target = $(this.el),
          stickyNavBefore = self._getPrevStickyNavHeight() || 0,
          topNumber = target.offset().top - stickyNavBefore;
      if (!target.hasClass('no-initial-scroll') || forceScroll) {
        window.setTimeout(function () {
          GLOBALS.ScrollTo.navigateTo(topNumber, 1000);
        }, 200);
      }

      return true;
    }
  }, {
    key: '_findTabByHash',
    value: function _findTabByHash(hash) {
      hash = hash || window.location.hash;
      return $($.grep(this.tabTriggers, function (item) {
        return hash && item.hash === hash;
      }));
    }
  }, {
    key: '_prepareMobile',
    value: function _prepareMobile() {
      var tpl = _.template(this.options.mobileTpl),
          items = this.el.find('ul').clone(true).html(),
          firstTab = this.el.find('li:first-child a'),
          dropTrigger = '<a class="drop-trigger" href="javascript: void(0)"><span>' + firstTab.text() + '</span></a>';

      this.el.append(tpl({ items: items, dropTrigger: dropTrigger }));
    }
  }, {
    key: '_checkMobile',
    value: function _checkMobile() {
      if (window.innerWidth > 960) {
        this.isMobile = false;
      } else {
        this.isMobile = true;
      }
    }
  }, {
    key: '_initListeners',
    value: function _initListeners() {
      var _this2 = this;

      this.dropTrigger = this.el.find('.mobile-tabs-container > a');
      this.drop = this.el.find('.drop');
      this.mobileNav = this.el.find('.mobile-tabs');
      this.tabTriggers = this.el.find('a:not(.drop-trigger)');
      var self = this,
          $htmlOrBody = $('html, body');

      // Any link on a page may be targeting tabs
      $('body').on('click', 'a', function (e) {
        var link = e.currentTarget;
        if (self.tabTriggers.index(link) == -1) {
          if (link.hash && link.hash in self.tabHashes) {
            self._openTabByHash(link.hash, true);
            window.location.hash = $(link).attr('href');
            e.preventDefault();
          }
        }
      });

      this.dropTrigger.on('click', function (el) {
        if (_this2.mobileNav.hasClass(_this2.options.activeMobile)) {
          _this2._collapseDrop();
        } else {
          _this2._expandDrop();
        }
      });

      this.tabTriggers.on('click', function (e, params) {
        var clicked = $(e.currentTarget);
        _this2._collapseDrop();
        _this2._updateActiveTab(clicked);
        _this2._updateActiveContent(clicked);

        if (!params || !params.initialClick) {
          if (clicked.attr('href').length > 1) {
            window.location.hash = clicked.attr('href');
          }
          var target = $(_this2.el),
              stickyNavBefore = self._getPrevStickyNavHeight() || 0,
              topNumber = target.offset().top - stickyNavBefore;
          window.setTimeout(function () {
            GLOBALS.ScrollTo.navigateTo(topNumber, 1000);
          }, 0);
        }

        //to force slider recalculation
        $(window).trigger("resize");
      });

      $(window).on("load resize", function () {
        self._checkMobile();
      });

      $htmlOrBody.on(['scroll', 'mousedown', 'wheel', 'DOMMouseScroll', 'mousewheel', 'keyup', 'touchmove'].join(' '), function stopTabsAnimations() {
        $htmlOrBody.stop();
      });
    }
  }, {
    key: '_updateActiveTab',
    value: function _updateActiveTab(el) {
      var activeTab = $(el),
          index = this.isMobile ? this.linksMobile.index(activeTab) : this.links.index(activeTab);

      if (index != -1) {
        this.tabTriggers.parents('li').removeClass(this.options.activeTabClass);
        $(this.links.parent('li').eq(index)).addClass(this.options.activeTabClass);
        $(this.linksMobile.parent('li').eq(index)).addClass(this.options.activeTabClass);
        this.dropTrigger.find('span').text(activeTab.text());
      }
    }
  }, {
    key: '_updateActiveContent',
    value: function _updateActiveContent(el) {
      var clickedLink = $(el),
          index = this.isMobile ? this.linksMobile.index(clickedLink) : this.links.index(clickedLink);

      if (index != -1) {
        this.contents.removeClass(this.options.activeClass).eq(index).addClass(this.options.activeClass);
      }
    }
  }, {
    key: '_expandDrop',
    value: function _expandDrop() {
      $('body').on('click', this._clickOutsideCb);
      this.mobileNav.addClass(this.options.activeMobile);
      this.drop.slideDown(200);
    }
  }, {
    key: '_collapseDrop',
    value: function _collapseDrop() {
      $('body').off('click', this._clickOutsideCb);
      this.mobileNav.removeClass(this.options.activeMobile);
      this.drop.slideUp(200);
    }
  }, {
    key: '_wrapSticky',
    value: function _wrapSticky() {
      $(this.el).wrapInner("<div class='sticky-wrapper'></div>");
    }
  }, {
    key: '_initStickableBehaviour',
    value: function _initStickableBehaviour() {
      if (!this.el.hasClass('non-stickable')) {
        new _stickable.Stickable(this.el.find(this.options.stickyWrapperSelector));
      }
    }
  }, {
    key: '_clickOutsideCb',
    value: function _clickOutsideCb(e) {
      if (!$(e.target).closest('.molecule-lb-836').length) {
        this._collapseDrop();
      }
    }
  }, {
    key: 'processContentsBackground',
    value: function processContentsBackground() {
      var self = this;
      this.contents.each(function () {
        var $tabContent = $(this),
            sectionInSlider = $tabContent.find(self.options.sliderSelector),
            className = '';

        if (sectionInSlider.length && sectionInSlider[0].className.indexOf('bkg-color-') > -1) {
          sectionInSlider.each(function () {
            var classes = this.className.split(' ');
            for (var i = 0; i < classes.length; i++) {
              if (classes[i].indexOf('bkg-color-') > -1) {
                className = classes[i];
              }
            }
          });
          $tabContent.find('.section').addClass(className);
        }
      });
    }
  }, {
    key: '_getPrevStickyNavHeight',
    value: function _getPrevStickyNavHeight() {
      var prevParent = this.el.parents(this.options.contentSelector).prevAll('.flex-content').not('.tab-content'),
          prevParentNear = prevParent.children().find(".molecule-lb-835, .molecule-lb-836, .molecule-lb-830").not(".non-stickable"),
          maxHeight = 0;

      if (prevParentNear.length > 0) {
        maxHeight = $(prevParentNear.eq(0)).height();
      }
      return maxHeight;
    }
  }]);

  return Molecule836;
}(_baseComponent.BaseComponent);

},{"../../common/scripts/stickable.js":7,"../../components/base-component.js":11}]},{},[22])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJhcHBcXGNvbW1vblxcc2NyaXB0c1xcY29tbW9uLmpzIiwiYXBwXFxjb21tb25cXHNjcmlwdHNcXGV2ZW50LWJ1cy5qcyIsImFwcFxcY29tbW9uXFxzY3JpcHRzXFxtZXRyaWNzLmpzIiwiYXBwXFxjb21tb25cXHNjcmlwdHNcXHBhZ2UtY29uZmlnLmpzIiwiYXBwXFxjb21tb25cXHNjcmlwdHNcXHNjcm9sbC10by5qcyIsImFwcFxcY29tbW9uXFxzY3JpcHRzXFxzbGlkZXItaGVscGVyLmpzIiwiYXBwXFxjb21tb25cXHNjcmlwdHNcXHN0aWNrYWJsZS5qcyIsImFwcFxcY29tbW9uXFxzY3JpcHRzXFx1dGlscy5qcyIsImFwcFxcY29tcG9uZW50c1xcYmFubmVyc1xcYmFubmVycy5qcyIsImFwcFxcY29tcG9uZW50c1xcYmFubmVyc1xcY3VzdG9tLWhlaWdodC5qcyIsImFwcFxcY29tcG9uZW50c1xcYmFzZS1jb21wb25lbnQuanMiLCJhcHBcXGNvbXBvbmVudHNcXGNvbW1vblxcY3VzdG9tLWZvbnQuanMiLCJhcHBcXGNvbXBvbmVudHNcXGluZGV4LmpzIiwiYXBwXFxjb21wb25lbnRzXFxvdmVybGF5c1xcb3ZlcmxheVxcb3ZlcmxheS5qcyIsImFwcFxcY29tcG9uZW50c1xcb3ZlcmxheXNcXG92ZXJsYXlzLmpzIiwiYXBwXFxjb21wb25lbnRzXFxvdmVybGF5c1xcdmlkZW8tb3ZlcmxheVxcdmlkZW8tb3ZlcmxheS5qcyIsImFwcFxcY29tcG9uZW50c1xcc2xpZGVyc1xcbGItc2xpZGVyLmpzIiwiYXBwXFxjb21wb25lbnRzXFxzbGlkZXJzXFxzbGlkZXJzLmpzIiwiYXBwXFxjb21wb25lbnRzXFx2aWRlb1xcYnJpZ2h0Y292ZS1wbGF5ZXJcXGJyaWdodGNvdmUtcGxheWVyLmpzIiwiYXBwXFxjb21wb25lbnRzXFx2aWRlb1xceW91dHViZS1wbGF5ZXJcXHlvdXR1YmUtcGxheWVyLmpzIiwiYXBwXFxncmlkXFxzY3JpcHRzXFxncmlkLmpzIiwiYXBwXFxsb29rYm9vay5qcyIsImFwcFxcbW9sZWN1bGVzXFw5OTlcXGNvbGxhcHNpYmxlLXJvd1xcY29sbGFwc2libGUtcm93LmpzIiwiYXBwXFxtb2xlY3VsZXNcXDk5OVxcaW5saW5lLXZpZGVvXFxpbmxpbmUtdmlkZW8uanMiLCJhcHBcXG1vbGVjdWxlc1xcOTk5XFxzY3JvbGwtdG8tbmV4dFxcc2Nyb2xsLXRvLW5leHQuanMiLCJhcHBcXG1vbGVjdWxlc1xcOTk5XFxzY3JvbGwtdG8tdG9wXFxzY3JvbGwtdG8tdG9wLmpzIiwiYXBwXFxtb2xlY3VsZXNcXGhlbHBlcnMuanMiLCJhcHBcXG1vbGVjdWxlc1xcaW5kZXguanMiLCJhcHBcXG1vbGVjdWxlc1xcbGItMjA5XFxtb2xlY3VsZS1sYi0yMDkuanMiLCJhcHBcXG1vbGVjdWxlc1xcbGItMzAwXFxtb2xlY3VsZS1sYi0zMDAuanMiLCJhcHBcXG1vbGVjdWxlc1xcbGItMzAyXFxtb2xlY3VsZS1sYi0zMDIuanMiLCJhcHBcXG1vbGVjdWxlc1xcbGItNDA2XFxtb2xlY3VsZS1sYi00MDYuanMiLCJhcHBcXG1vbGVjdWxlc1xcbGItNDEzXFxtb2xlY3VsZS1sYi00MTMuanMiLCJhcHBcXG1vbGVjdWxlc1xcbGItNTAxXFxtb2xlY3VsZS1sYi01MDEuanMiLCJhcHBcXG1vbGVjdWxlc1xcbGItNTEwXFxtb2xlY3VsZS1sYi01MTAuanMiLCJhcHBcXG1vbGVjdWxlc1xcbGItNzAxXFxtb2xlY3VsZS1sYi03MDEuanMiLCJhcHBcXG1vbGVjdWxlc1xcbGItNzAyXFxtb2xlY3VsZS1sYi03MDIuanMiLCJhcHBcXG1vbGVjdWxlc1xcbGItODIzXFxtb2xlY3VsZS1sYi04MjMuanMiLCJhcHBcXG1vbGVjdWxlc1xcbGItODI3XFxtb2xlY3VsZS1sYi04MjctaW1hZ2UtZ2FsbGVyeS5qcyIsImFwcFxcbW9sZWN1bGVzXFxsYi04MjdcXG1vbGVjdWxlLWxiLTgyNy12aWRlby1nYWxsZXJ5LmpzIiwiYXBwXFxtb2xlY3VsZXNcXGxiLTgyN1xcbW9sZWN1bGUtbGItODI3LmpzIiwiYXBwXFxtb2xlY3VsZXNcXGxiLTgzMFxcbW9sZWN1bGUtbGItODMwLmpzIiwiYXBwXFxtb2xlY3VsZXNcXGxiLTgzNVxcbW9sZWN1bGUtbGItODM1LmpzIiwiYXBwXFxtb2xlY3VsZXNcXGxiLTgzNlxcbW9sZWN1bGUtbGItODM2LXRoZW1lLTIuanMiLCJhcHBcXG1vbGVjdWxlc1xcbGItODM2XFxtb2xlY3VsZS1sYi04MzYuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUNBQTs7Ozs7Ozs7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7Ozs7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7Ozs7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7Ozs7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7Ozs7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7Ozs7Ozs7O0FDTk8sSUFBSSw4QkFBVztBQUNwQixJQURvQixjQUNqQixTQURpQixFQUNOLE9BRE0sRUFDRztBQUFBOztBQUNyQixNQUFFLElBQUYsRUFBUSxFQUFSLENBQVcsU0FBWCxFQUFzQixPQUF0QjtBQUNBLFdBQU8sWUFBTTtBQUNYLFlBQUssR0FBTCxDQUFTLFNBQVQsRUFBb0IsT0FBcEI7QUFDRCxLQUZEO0FBR0QsR0FObUI7QUFRcEIsS0FSb0IsZUFRaEIsU0FSZ0IsRUFRTCxPQVJLLEVBUUk7QUFDdEIsTUFBRSxJQUFGLEVBQVEsR0FBUixDQUFZLFNBQVosRUFBdUIsT0FBdkI7QUFDRCxHQVZtQjtBQVlwQixTQVpvQixtQkFZWixTQVpZLEVBWVE7QUFBQSxzQ0FBTixJQUFNO0FBQU4sVUFBTTtBQUFBOztBQUMxQixNQUFFLElBQUYsRUFBUSxjQUFSLENBQXVCLFNBQXZCLFlBQXNDLElBQXRDO0FBQ0Q7QUFkbUIsQ0FBZjs7O0FDQVA7Ozs7Ozs7Ozs7O0FBQ0E7Ozs7Ozs7O0lBRWEsTyxXQUFBLE87Ozs7O3dCQUNJO0FBQ2IsYUFBTyxFQUFFLE1BQUYsQ0FBUyxFQUFULG1HQUE2QjtBQUNsQyxxQkFBaUIsT0FEaUI7QUFFbEMscUJBQW1CLEtBRmU7QUFHbEMsbUJBQWlCLFVBSGlCO0FBSWxDLHVCQUFnQixDQUNkLEVBQUUsTUFBUSxJQUFWLEVBRGMsRUFFZCxFQUFFLE1BQU8sUUFBVCxFQUZjLEVBR2QsRUFBRSxNQUFPLGNBQVQsRUFIYyxDQUprQjtBQVNsQyx3QkFBaUIsQ0FDZixFQUFFLE1BQU8sWUFBVCxFQURlLEVBRWYsRUFBRSxNQUFPLE9BQVQsRUFGZTtBQVRpQixPQUE3QixDQUFQO0FBY0Q7OztBQUVELHFCQUFxQjtBQUFBOztBQUFBOztBQUFBLHNDQUFOLElBQU07QUFBTixVQUFNO0FBQUE7O0FBQUEsNklBQ1YsSUFEVTs7QUFFbkIsTUFBRTtBQUFBLGFBQU0sTUFBSyxLQUFMLEVBQU47QUFBQSxLQUFGO0FBRm1CO0FBR3BCOzs7OzRCQUVPO0FBQUE7O0FBQ04sVUFBSSxPQUFZLEVBQUUsTUFBRixDQUFoQjtBQUFBLFVBQ0UsWUFBWSxFQURkO0FBQUEsVUFFRSxXQUFXLDhDQUNMLCtFQURLLEdBRUwsaUVBRkssR0FHTCw2REFISyxHQUlMLG1FQUpLLEdBS0wsd0VBTEssR0FNTCw0QkFSUjtBQVNBLFdBQUssWUFBTCxHQUFvQixTQUFTLGVBQVQsQ0FBeUIsSUFBekIsQ0FBOEIsS0FBOUIsQ0FBb0MsR0FBcEMsRUFBeUMsQ0FBekMsQ0FBcEIsQ0FWTSxDQVUwRDtBQUNoRSxXQUFLLFdBQUwsR0FBb0IsU0FBUyxlQUFULENBQXlCLElBQXpCLENBQThCLEtBQTlCLENBQW9DLEdBQXBDLEVBQXlDLENBQXpDLENBQXBCLENBWE0sQ0FXMEQ7QUFDaEUsV0FBSyxVQUFMLEdBQWtCLEVBQWxCO0FBQ0EsV0FBSyxLQUFMLEdBQWEsRUFBYjtBQUNBLFdBQUssS0FBTCxHQUFhLElBQWI7O0FBRUEsV0FBSSxJQUFJLElBQUksQ0FBWixFQUFlLElBQUksS0FBSyxPQUFMLENBQWEsYUFBYixDQUEyQixNQUE5QyxFQUFzRCxHQUF0RCxFQUEwRDtBQUN4RCxvQkFBWSxLQUFLLE1BQUwsQ0FBWSxhQUFZLEtBQUssT0FBTCxDQUFhLGFBQWIsQ0FBMkIsQ0FBM0IsRUFBOEIsSUFBMUMsR0FBZ0QsSUFBNUQsRUFBa0UsSUFBbEUsQ0FBdUUsU0FBdkUsQ0FBWjtBQUNBLFlBQUksU0FBSixFQUFlO0FBQ2IsY0FBSSxLQUFLLE9BQUwsQ0FBYSxhQUFiLENBQTJCLENBQTNCLEVBQThCLElBQTlCLElBQXNDLGNBQTFDLEVBQTBEO0FBQ3hELGdCQUFJLFVBQVUsTUFBVixHQUFtQixFQUF2QixFQUEyQjtBQUN6QiwwQkFBWSxVQUFVLE1BQVYsQ0FBaUIsQ0FBakIsRUFBbUIsRUFBbkIsQ0FBWjtBQUNEO0FBQ0Y7QUFDRCxlQUFLLFVBQUwsQ0FBZ0IsSUFBaEIsQ0FBcUIsU0FBckI7QUFDRDs7QUFFRCxZQUFJLEtBQUssT0FBTCxDQUFhLGFBQWIsQ0FBMkIsQ0FBM0IsRUFBOEIsSUFBOUIsSUFBc0MsUUFBMUMsRUFBb0Q7QUFDbEQsZUFBSyxVQUFMLENBQWdCLElBQWhCLENBQXFCLEtBQUssV0FBMUI7QUFDQSxlQUFLLFVBQUwsQ0FBZ0IsSUFBaEIsQ0FBcUIsS0FBSyxZQUExQjtBQUNEO0FBRUY7QUFDRCxRQUFFLFFBQUYsRUFBWSxFQUFaLENBQWUsT0FBZixFQUF3QixRQUF4QixFQUFrQyxVQUFDLENBQUQsRUFBTztBQUN2QyxlQUFLLGdCQUFMLENBQXNCLENBQXRCO0FBQ0QsT0FGRDtBQUdBLFFBQUUsUUFBRixFQUFZLEVBQVosQ0FBZSxzQkFBZixFQUF1QywwQkFBdkMsRUFBbUUsVUFBQyxDQUFELEVBQU87QUFDeEUsZUFBSyxnQkFBTCxDQUFzQixDQUF0QjtBQUNELE9BRkQ7O0FBSUEsUUFBRSxRQUFGLEVBQVksRUFBWixDQUFlLFlBQWYsRUFBNkIsVUFBVSxDQUFWLEVBQWE7QUFDeEMsY0FBTSxVQUFOLENBQWlCLEVBQUUsS0FBbkI7QUFDRCxPQUZEO0FBR0Q7OztxQ0FFZ0IsQyxFQUFHO0FBQUE7O0FBQ2xCLFVBQUksQ0FBQyxLQUFLLEtBQVYsRUFBZ0I7QUFDZCxhQUFLLEtBQUwsR0FBYSxXQUFXLFlBQU07QUFDNUIsdUJBQWEsT0FBSyxLQUFsQjtBQUNBLGlCQUFLLEtBQUwsR0FBYSxJQUFiO0FBQ0QsU0FIWSxFQUdWLEdBSFUsQ0FBYjtBQUlBLFlBQUksRUFBRSxLQUFGLEtBQVksQ0FBaEIsRUFBa0I7QUFDaEIsZUFBSyxVQUFMLENBQWlCLEVBQUUsRUFBRSxhQUFKLENBQWpCO0FBQ0Q7QUFDRjtBQUNGOzs7K0JBRVUsYyxFQUFnQjtBQUN6QixVQUFJLG9CQUFvQixFQUF4QjtBQUFBLFVBQ0UsY0FBb0IsRUFEdEI7QUFBQSxVQUVFLGNBQW9CLEVBRnRCO0FBQUEsVUFHRSxRQUFRLEtBQUssT0FBTCxDQUFhLGNBSHZCOztBQUtBLDBCQUFvQixLQUFLLG9CQUFMLENBQTBCLGNBQTFCLEVBQTBDLEtBQTFDLENBQXBCO0FBQ0Esb0JBQWUsS0FBSyxPQUFMLENBQWEsV0FBYixHQUEyQixHQUEzQixHQUFpQyxLQUFLLFVBQUwsQ0FBZ0IsSUFBaEIsQ0FBcUIsR0FBckIsQ0FBakMsR0FBNkQsR0FBN0QsR0FBbUUsaUJBQWxGO0FBQ0Esb0JBQWMsZUFBZSxJQUFmLENBQW9CLHdCQUFwQixLQUFpRCxFQUEvRDs7QUFFQTtBQUNBLFVBQUksZUFBZSxJQUFmLENBQW9CLHNCQUFwQixDQUFKLEVBQWlEO0FBQy9DO0FBQ0Q7O0FBRUQ7QUFDQSxVQUFJLGVBQWUsUUFBZixDQUF3QixvQkFBeEIsS0FBaUQsZUFBZSxRQUFmLENBQXdCLHNCQUF4QixDQUFyRCxFQUFzRztBQUNwRyxzQkFBYyxNQUFkO0FBQ0Q7O0FBRUQsVUFBSyxXQUFMLEVBQW1CO0FBQ2pCLFlBQUk7QUFDRixrQkFBUSxHQUFSLENBQVksa0JBQWtCLEtBQUssT0FBTCxDQUFhLFNBQS9CLEdBQTJDLFVBQTNDLEdBQXdELFdBQXhELEdBQXNFLFVBQXRFLEdBQW1GLFdBQS9GO0FBQ0EsdUJBQWEsS0FBSyxPQUFMLENBQWEsU0FBMUIsRUFBcUMsRUFBRSxNQUFRLFdBQVYsRUFBdUIsTUFBTyxXQUE5QixFQUFyQztBQUNELFNBSEQsQ0FHRSxPQUFPLEtBQVAsRUFBYztBQUNkLGtCQUFRLEdBQVIsQ0FBWSxNQUFNLE9BQWxCO0FBQ0Q7QUFDRixPQVBELE1BT087QUFDTCxnQkFBUSxHQUFSLENBQVksMEZBQVo7QUFDRDs7QUFFRCxhQUFPLElBQVA7QUFDRDs7O3lDQUVvQixNLEVBQVEsSyxFQUFPOztBQUVsQyxVQUFJLGFBQWEsRUFBakI7QUFBQSxVQUNFLFFBQVEsRUFEVjs7QUFHQSxVQUFJLE9BQU8sTUFBUCxLQUFrQixDQUF0QixFQUF5QjtBQUN2QixlQUFPLEtBQVA7QUFDRDs7QUFFRCxXQUFLLElBQUksSUFBSSxDQUFiLEVBQWdCLElBQUksTUFBTSxNQUExQixFQUFrQyxHQUFsQyxFQUF1QztBQUNyQyxZQUFJLFlBQVksRUFBaEI7O0FBRUEsWUFBSSxNQUFNLENBQU4sRUFBUyxJQUFULElBQWlCLFlBQXJCLEVBQW1DO0FBQ2pDLGNBQUksT0FBTyxPQUFQLENBQWUsMENBQWYsRUFBMkQsTUFBL0QsRUFBdUU7QUFDckUsd0JBQVksT0FBTyxPQUFQLENBQWUsMENBQWYsRUFBMkQsSUFBM0QsQ0FBZ0UseUJBQWhFLENBQVo7QUFDRCxXQUZELE1BRU87QUFDTCxnQkFBSSxPQUFPLElBQVAsQ0FBWSx5QkFBWixNQUEyQyxXQUEvQyxFQUE0RDtBQUMxRCwwQkFBWSxPQUFPLElBQVAsQ0FBWSx5QkFBWixDQUFaO0FBQ0QsYUFGRCxNQUVPO0FBQ0wsMEJBQVksRUFBWjtBQUNEO0FBQ0Y7QUFDRjs7QUFFRCxZQUFJLE1BQU0sQ0FBTixFQUFTLElBQVQsSUFBaUIsT0FBckIsRUFBOEI7O0FBRTVCLGNBQUksT0FBTyxPQUFQLENBQWUsaUJBQWYsRUFBa0MsTUFBdEMsRUFBOEM7QUFDNUMsd0JBQVksT0FBTyxPQUFQLENBQWUsaUJBQWYsRUFBa0MsSUFBbEMsQ0FBdUMsa0JBQWtCLE1BQU0sQ0FBTixFQUFTLElBQWxFLENBQVo7QUFDRCxXQUZELE1BRU8sSUFBSSxPQUFPLE9BQVAsQ0FBZSxpQkFBZixFQUFrQyxNQUF0QyxFQUE4QztBQUNuRCx3QkFBWSxPQUFPLE9BQVAsQ0FBZSxpQkFBZixFQUFrQyxJQUFsQyxDQUF1QyxrQkFBa0IsTUFBTSxDQUFOLEVBQVMsSUFBbEUsQ0FBWjtBQUNEOztBQUVELGNBQUksY0FBYyxPQUFPLElBQVAsQ0FBWSxrQkFBaUIsTUFBTSxDQUFOLEVBQVMsSUFBdEMsQ0FBbEI7O0FBRUEsY0FBSSxXQUFKLEVBQWlCO0FBQ2YsZ0JBQUksT0FBTyxRQUFQLENBQWdCLG9CQUFoQixLQUF5QyxPQUFPLFFBQVAsQ0FBZ0Isc0JBQWhCLENBQTdDLEVBQXNGO0FBQ3BGLGtCQUFJLFlBQVksTUFBWixHQUFxQixFQUF6QixFQUE2QjtBQUMzQiw4QkFBYyxZQUFZLE1BQVosQ0FBbUIsQ0FBbkIsRUFBcUIsRUFBckIsQ0FBZDtBQUNEO0FBQ0Y7QUFDRCxnQkFBSSxTQUFKLEVBQWU7QUFDYiwwQkFBWSxZQUFZLEdBQVosR0FBa0IsV0FBOUI7QUFDRCxhQUZELE1BRU87QUFDTCwwQkFBWSxXQUFaO0FBQ0Q7QUFDRjtBQUVGOztBQUVELFlBQUksU0FBSixFQUFlO0FBQ2IscUJBQVcsSUFBWCxDQUFnQixTQUFoQjtBQUNEO0FBRUY7O0FBRUQsVUFBSSxXQUFXLE1BQWYsRUFBdUI7QUFDckIsZ0JBQVEsV0FBVyxJQUFYLENBQWdCLEdBQWhCLENBQVI7QUFDRDs7QUFFRCxhQUFPLEtBQVA7QUFDRDs7Ozs7OztBQ2pMSDs7Ozs7Ozs7Ozs7O0lBRWEsVSxXQUFBLFU7QUFDWCx3QkFBYztBQUFBOztBQUNaLFNBQUssSUFBTCxHQUFZLEVBQVo7O0FBRUE7QUFDQyxLQUFDLE9BQU8sRUFBVCxLQUFpQixPQUFPLEVBQVAsR0FBWSxFQUE3Qjs7QUFFQTtBQUNBLFdBQU8sRUFBUCxDQUFVLFVBQVYsR0FBdUIsSUFBdkI7QUFDRDs7Ozt3QkFFRyxHLEVBQUs7QUFDUCxhQUFPLE9BQU8sU0FBUCxDQUFpQixjQUFqQixDQUFnQyxJQUFoQyxDQUFxQyxLQUFLLElBQTFDLEVBQWdELEdBQWhELENBQVA7QUFDRDs7O3dCQUVHLEcsRUFBSyxLLEVBQU87QUFDZCxVQUFJLFVBQVUsU0FBVixJQUF1QixVQUFVLEtBQUssSUFBTCxDQUFVLEdBQVYsQ0FBckMsRUFDRSxPQUFPLEtBQVA7O0FBRUYsVUFBSSxLQUFLLEdBQUwsQ0FBUyxHQUFULEtBQWlCLFFBQU8sS0FBSyxHQUFMLENBQVMsR0FBVCxDQUFQLE1BQXlCLFFBQTFDLElBQXNELFFBQU8sS0FBUCx5Q0FBTyxLQUFQLE9BQWlCLFFBQTNFLEVBQXFGO0FBQ25GLGFBQUksSUFBSSxJQUFSLElBQWdCLEtBQWhCLEVBQXNCO0FBQ3BCLGVBQUssSUFBTCxDQUFVLEdBQVYsRUFBZSxJQUFmLElBQXVCLE1BQU0sSUFBTixDQUF2QjtBQUNEO0FBQ0YsT0FKRCxNQUlPO0FBQ0wsYUFBSyxJQUFMLENBQVUsR0FBVixJQUFpQixLQUFqQjtBQUNEOztBQUVELGFBQU8sS0FBSyxJQUFMLENBQVUsR0FBVixDQUFQO0FBQ0Q7Ozt3QkFFRyxHLEVBQUs7QUFDUCxhQUFPLEtBQUssSUFBTCxDQUFVLEdBQVYsQ0FBUDtBQUNEOzs7eUJBRUksRyxFQUFLO0FBQ1IsWUFBTSxPQUFPLElBQWI7O0FBRUEsVUFBSSxRQUFRLElBQVosRUFBaUI7QUFDZixlQUFPLEtBQUssSUFBWjtBQUNBLGFBQUssSUFBTCxHQUFZLEVBQVo7QUFDRCxPQUhELE1BR08sSUFBSSxLQUFLLEdBQUwsQ0FBUyxHQUFULENBQUosRUFBbUI7QUFDeEIsZUFBTyxLQUFLLElBQUwsQ0FBVSxHQUFWLENBQVA7QUFDRDtBQUNGOzs7Ozs7O0FDN0NIOzs7Ozs7Ozs7O0lBRWEsUSxXQUFBLFE7QUFFWCxzQkFBcUI7QUFBQTs7QUFDbkIsU0FBSyxrQkFBTDtBQUNBLFNBQUssb0JBQUw7QUFDQSxTQUFLLFNBQUwsR0FBaUIsS0FBakI7QUFDRDs7Ozt5Q0FFb0I7QUFBQTs7QUFDbkI7QUFDQSxRQUFFLFlBQU07QUFDTjtBQUNBLFVBQUUsb0JBQUYsRUFBd0IsSUFBeEIsQ0FBNkIsVUFBQyxLQUFELEVBQVEsRUFBUixFQUFlO0FBQzFDLGNBQUksTUFBTSxFQUFFLEVBQUYsQ0FBVjtBQUFBLGNBQ0UsS0FBSyxJQUFJLElBQUosQ0FBUyxJQUFULENBRFA7O0FBR0EsY0FBSSxFQUFKLEVBQVE7QUFDTixnQkFBSSxPQUFKLENBQVksVUFBWixFQUF3QixJQUF4QixDQUE2QixhQUE3QixFQUE0QyxFQUE1QztBQUNBLGdCQUFJLE1BQUo7QUFDRDtBQUNGLFNBUkQ7QUFTQTtBQUNBO0FBQ0EsVUFBRSxxREFBRixFQUF5RCxJQUF6RCxDQUE4RCxVQUFDLEtBQUQsRUFBUSxFQUFSLEVBQWU7QUFDM0UsY0FBSSxNQUFNLEVBQUUsRUFBRixDQUFWO0FBQUEsY0FDRSxTQUFTLElBQUksSUFBSixDQUFTLGFBQVQsQ0FEWDs7QUFHQSxjQUFJLFVBQUosQ0FBZSxhQUFmLEVBQThCLE9BQTlCLENBQXNDLFVBQXRDLEVBQWtELElBQWxELENBQXVELGFBQXZELEVBQXNFLE1BQXRFO0FBQ0QsU0FMRDtBQU1BLFVBQUUsTUFBRixFQUFVLE9BQVYsQ0FBa0Isa0JBQWxCO0FBQ0EsZUFBTyxPQUFQLENBQWUsZUFBZixHQUFpQyxJQUFqQztBQUNBLGNBQUssZ0JBQUw7QUFDRCxPQXRCRDtBQXVCRDs7OzJDQUVzQjtBQUNyQixhQUFPLGdCQUFQLEdBQTJCLFlBQVU7QUFDbkMsZUFBUSxPQUFPLHFCQUFQLElBQ0EsT0FBTywyQkFEUCxJQUVBLE9BQU8sd0JBRlAsSUFHQSxVQUFVLFFBQVYsRUFBb0I7QUFDbEIsaUJBQU8sVUFBUCxDQUFrQixRQUFsQixFQUE0QixPQUFPLEVBQW5DO0FBQ0QsU0FMVDtBQU1ELE9BUHlCLEVBQTFCO0FBUUQ7Ozs2QkFFUSxJLEVBQU0sRSxFQUFJLFEsRUFBVSxFLEVBQUksTSxFQUFRO0FBQ3ZDLFVBQUksWUFBSjtBQUFBLFVBQ0ksaUJBREo7QUFBQSxVQUVJLFlBRko7QUFBQSxVQUdJLHlCQUhKO0FBQUEsVUFJSSxlQUpKO0FBS0EsZUFBUyxVQUFVLENBQW5CO0FBQ0EsYUFBTyxLQUFLLEVBQUwsR0FBVSxLQUFLLE9BQUwsQ0FBYSxHQUFiLEVBQWtCLEVBQWxCLENBQWpCO0FBQ0EsaUJBQVcsTUFBTSxFQUFFLHNCQUFzQixJQUF0QixHQUE2QixJQUEvQixDQUFqQjs7QUFFQSxZQUFNLFNBQVMsTUFBVCxHQUFrQixRQUFsQixHQUE2QixFQUFFLE1BQU0sSUFBUixDQUFuQzs7QUFFQSxVQUFJLElBQUksTUFBUixFQUFnQjtBQUNkLGNBQU0sS0FBSyxLQUFMLENBQVcsSUFBSSxNQUFKLEdBQWEsR0FBYixHQUFtQixDQUE5QixDQUFOO0FBQ0EsMkJBQW1CLEVBQUUsa0JBQUYsRUFBc0IsV0FBdEIsRUFBbkI7QUFDQSxpQkFBUyxDQUFDLEdBQUQsR0FBTyxDQUFDLGdCQUFSLEdBQTJCLE1BQXBDO0FBQ0EsYUFBSyxRQUFMLENBQWMsTUFBZCxFQUFzQixRQUF0QixFQUFnQyxFQUFoQztBQUNBLGVBQU8sSUFBUDtBQUNEO0FBQ0QsYUFBTyxLQUFQO0FBQ0Q7OztxQ0FFZ0IsTSxFQUFRLFEsRUFBVSxFLEVBQUksZ0IsRUFBa0I7QUFDdkQsVUFBSSxZQUFZLEVBQUUsTUFBRixFQUFVLFNBQVYsRUFBaEI7QUFBQSxVQUNFLFNBQVMsWUFBWSxNQUR2Qjs7QUFHQSwyQkFBcUIsVUFBVSxFQUFFLGtCQUFGLEVBQXNCLFdBQXRCLEVBQS9CO0FBQ0EsV0FBSyxRQUFMLENBQWMsTUFBZCxFQUFzQixRQUF0QixFQUFnQyxFQUFoQztBQUNEOzs7c0NBRWlCLEksRUFBTSxFLEVBQUksUSxFQUFVLEUsRUFBSSxNLEVBQVE7QUFDaEQsVUFBSSxZQUFKO0FBQUEsVUFDRSxpQkFERjtBQUFBLFVBRUUsWUFGRjtBQUFBLFVBR0UsZUFIRjtBQUlBLGVBQVMsVUFBVSxDQUFuQjtBQUNBLGFBQU8sS0FBSyxFQUFMLEdBQVUsS0FBSyxPQUFMLENBQWEsR0FBYixFQUFrQixFQUFsQixDQUFqQjtBQUNBLGlCQUFXLE1BQU0sRUFBRSxzQkFBc0IsSUFBdEIsR0FBNkIsSUFBL0IsQ0FBakI7O0FBRUEsWUFBTSxTQUFTLE1BQVQsR0FBa0IsUUFBbEIsR0FBNkIsRUFBRSxNQUFNLElBQVIsQ0FBbkM7O0FBRUEsVUFBSSxJQUFJLE1BQVIsRUFBZ0I7QUFDZCxjQUFNLEtBQUssS0FBTCxDQUFXLElBQUksTUFBSixHQUFhLEdBQWIsR0FBbUIsQ0FBOUIsQ0FBTjtBQUNBLGlCQUFTLENBQUMsR0FBRCxHQUFPLE1BQWhCO0FBQ0EsYUFBSyxRQUFMLENBQWMsTUFBZCxFQUFzQixRQUF0QixFQUFnQyxFQUFoQztBQUNBLGVBQU8sSUFBUDtBQUNEO0FBQ0QsYUFBTyxLQUFQO0FBQ0Q7OzsrQkFFVSxNLEVBQVEsUSxFQUFVLEUsRUFBSTtBQUMvQixXQUFLLFFBQUwsQ0FBYyxNQUFkLEVBQXNCLFFBQXRCLEVBQWdDLEVBQWhDO0FBQ0Q7Ozs2QkFFUSxNLEVBQTRCO0FBQUEsVUFBcEIsUUFBb0IsdUVBQVQsR0FBUztBQUFBLFVBQUosRUFBSTs7QUFDbkMsV0FBSyxTQUFMLEdBQWlCLElBQWpCOztBQUVBLFVBQUksUUFBUSxLQUFSLENBQWMsTUFBZCxDQUFxQixRQUF6QixFQUFtQztBQUNqQyxhQUFLLGNBQUwsQ0FBb0IsTUFBcEIsRUFBNEIsUUFBNUIsRUFBc0MsZ0JBQXRDLEVBQXdELEVBQXhEO0FBQ0QsT0FGRCxNQUVPO0FBQ0wsYUFBSyxlQUFMLENBQXFCLE1BQXJCLEVBQTZCLFFBQTdCLEVBQXVDLGdCQUF2QyxFQUF5RCxFQUF6RDtBQUNEO0FBQ0Y7OztvQ0FFZSxNLEVBQVEsUSxFQUFVLE0sRUFBUSxFLEVBQUk7QUFBQTs7QUFDNUMsUUFBRSxZQUFGLEVBQWdCLElBQWhCLEdBQXVCLFVBQXZCLEdBQW9DLE9BQXBDLENBQTRDO0FBQzFDLG1CQUFXLFNBQVM7QUFEc0IsT0FBNUMsRUFFRyxRQUZILEVBRWEsTUFGYixFQUVxQixZQUFNO0FBQ3hCLGVBQU8sRUFBUCxLQUFjLFVBQWYsSUFBOEIsSUFBOUI7QUFDQSxlQUFLLFNBQUwsR0FBaUIsS0FBakI7QUFDRCxPQUxEO0FBTUQ7OzttQ0FFYyxNLEVBQVEsUSxFQUFVLE0sRUFBUSxFLEVBQUk7QUFDM0MsV0FBSyxlQUFMLENBQXFCLE1BQXJCLEVBQTZCLFFBQTdCLEVBQXVDLE1BQXZDLEVBQStDLFlBQVc7QUFDeEQsYUFBSyxTQUFMLEdBQWlCLEtBQWpCO0FBQ0EsY0FBTSxJQUFOO0FBQ0QsT0FIOEMsQ0FHN0MsSUFINkMsQ0FHeEMsSUFId0MsQ0FBL0M7QUFJRDs7O29DQUVlLFMsRUFBVyxRLEVBQVUsTSxFQUFRLEUsRUFBSTtBQUMvQyxVQUFJLFVBQVUsT0FBTyxPQUFQLElBQWtCLE9BQU8sV0FBdkM7QUFBQSxVQUNJLFdBQVcsYUFBYSxDQUQ1QjtBQUFBLFVBRUksY0FBYyxDQUZsQjtBQUFBLFVBR0ksT0FBTyxXQUFXLElBSHRCO0FBQUEsVUFJSSxrQkFBa0I7QUFDaEIsd0JBQWdCLHdCQUFTLEdBQVQsRUFBYztBQUM1QixjQUFJLENBQUMsT0FBSyxHQUFOLElBQWEsQ0FBakIsRUFBb0IsT0FBTyxNQUFJLEtBQUssR0FBTCxDQUFTLEdBQVQsRUFBYSxDQUFiLENBQVg7QUFDcEIsaUJBQU8sT0FBTyxLQUFLLEdBQUwsQ0FBVSxNQUFJLENBQWQsRUFBaUIsQ0FBakIsSUFBc0IsQ0FBN0IsQ0FBUDtBQUNEO0FBSmUsT0FKdEI7O0FBV0EsZUFBUyxVQUFVLGdCQUFuQjs7QUFFQSxlQUFTLElBQVQsR0FBZ0I7QUFDZCx1QkFBZSxJQUFJLEVBQW5COztBQUVBLFlBQUksSUFBSSxjQUFjLElBQXRCO0FBQUEsWUFDSSxJQUFJLGdCQUFnQixNQUFoQixFQUF3QixDQUF4QixDQURSOztBQUdBLFlBQUksSUFBSSxDQUFSLEVBQVc7QUFDVCwyQkFBaUIsSUFBakI7QUFDQSxpQkFBTyxRQUFQLENBQWdCLENBQWhCLEVBQW1CLFVBQVcsQ0FBQyxXQUFXLE9BQVosSUFBdUIsQ0FBckQ7QUFDRCxTQUhELE1BR087QUFDTCxpQkFBTyxRQUFQLENBQWdCLENBQWhCLEVBQW1CLFFBQW5CO0FBQ0EsZ0JBQU0sSUFBTjtBQUNEO0FBQ0Y7QUFDRDtBQUNEOzs7dUNBRWtCO0FBQUE7O0FBQ2pCLFVBQUksWUFBWSxPQUFPLFFBQVAsQ0FBZ0IsSUFBaEM7QUFDQSxVQUFLLFNBQUwsRUFBaUI7QUFDYixvQkFBWSxhQUFhLFVBQVUsT0FBVixDQUFrQixLQUFsQixFQUF3QixHQUF4QixDQUF6QjtBQUNBLFVBQUUsTUFBRixFQUFVLEVBQVYsQ0FBYSxNQUFiLEVBQXFCO0FBQUEsaUJBQU0sT0FBSyxRQUFMLENBQWUsU0FBZixDQUFOO0FBQUEsU0FBckI7QUFDSDtBQUNGOzs7Ozs7O0FDcktIOzs7Ozs7Ozs7O0lBRWEsWSxXQUFBLFk7Ozs7Ozs7OztBQUVYOzs7Ozs7O29DQU9nQixNLEVBQVEsTyxFQUFTO0FBQy9CLFVBQUksU0FBUyxPQUFPLElBQVAsQ0FBWSxjQUFaLEVBQTRCLEdBQTVCLENBQWdDLFlBQWhDLEVBQThDLENBQTlDLENBQWI7QUFBQSxVQUNJLFlBQVksT0FBTyxJQUFQLENBQVksb0JBQW9CLFFBQVEsaUJBQXhDLEVBQTJELEtBQTNELEVBRGhCOztBQUdBLGVBQVMsTUFBVCxHQUFrQjtBQUNoQixZQUFJLGNBQWMsT0FBTyxLQUFQLEdBQWUsTUFBZixFQUFsQjtBQUFBLFlBQ0ksa0JBQWtCLFVBQVUsTUFBVixFQUR0Qjs7QUFHQSwyQkFBbUIsT0FBTyxHQUFQLENBQVcsS0FBWCxFQUFrQixDQUFDLGtCQUFrQixXQUFuQixJQUFrQyxDQUFwRCxDQUFuQjtBQUNEO0FBQ0Q7O0FBRUEsYUFBTyxFQUFQLENBQVUsYUFBVixFQUF5QixFQUFFLFFBQUYsQ0FBVztBQUFBLGVBQU0sUUFBTjtBQUFBLE9BQVgsRUFBMkIsRUFBM0IsQ0FBekI7QUFDRDs7QUFHRDs7Ozs7Ozs7d0NBS29CLE0sRUFBUSxnQixFQUFrQixnQixFQUFrQjtBQUM5RCxVQUFJLENBQUMsZ0JBQUQsSUFBcUIsQ0FBQyxnQkFBMUIsRUFBNEM7QUFDNUMsZUFBUyxNQUFULEdBQWtCO0FBQ2hCLFlBQUksU0FBUyxpQkFBaUIsTUFBakIsRUFBYjs7QUFFQSx5QkFBaUIsTUFBakIsQ0FBd0IsTUFBeEI7QUFDRDtBQUNEOztBQUVBLGFBQU8sRUFBUCxDQUFVLGFBQVYsRUFBeUIsRUFBRSxRQUFGLENBQVc7QUFBQSxlQUFNLFFBQU47QUFBQSxPQUFYLEVBQTJCLEVBQTNCLENBQXpCO0FBQ0Q7Ozs7Ozs7QUMxQ0g7Ozs7Ozs7Ozs7SUFFYSxTLFdBQUEsUzs7O3dCQUNHO0FBQ1osYUFBTztBQUNMLGNBQU0sU0FERCxFQUNZO0FBQ2pCLHFCQUFhLFFBRlI7QUFHTCxtQkFBVyxHQUhOO0FBSUwsc0JBQWM7QUFKVCxPQUFQO0FBTUQ7OztBQUVELHFCQUFZLFdBQVosRUFBeUIsWUFBekIsRUFBdUMsR0FBdkMsRUFBNEM7QUFBQTs7QUFDMUMsU0FBSyxJQUFMLEdBQVksRUFBRSxNQUFGLENBQVMsRUFBVCxFQUFhLEtBQUssT0FBbEIsRUFBMkIsR0FBM0IsQ0FBWjtBQUNBLFNBQUssRUFBTCxHQUFVLFdBQVY7QUFDQSxTQUFLLFlBQUwsR0FBb0IsZ0JBQWdCLFlBQVksT0FBWixDQUFvQixVQUFwQixDQUFwQztBQUNBLFNBQUssS0FBTDtBQUNEOzs7OzRCQUVPO0FBQUE7O0FBQ04sV0FBSyxTQUFMLEdBQWlCO0FBQUEsZUFBTSxNQUFLLFVBQUwsR0FBa0IsTUFBSyxJQUFMLENBQVUsSUFBNUIsR0FBTjtBQUFBLE9BQWpCO0FBQ0EsV0FBSyxTQUFMOztBQUVBLFFBQUUsTUFBRixFQUFVLEVBQVYsQ0FBYSxRQUFiLEVBQXVCLEtBQUssU0FBNUI7QUFDQSxXQUFLLFFBQUwsR0FBZ0I7QUFBQSxlQUFNLEVBQUUsTUFBRixFQUFVLEdBQVYsQ0FBYyxRQUFkLEVBQXdCLE1BQUssU0FBN0IsQ0FBTjtBQUFBLE9BQWhCO0FBQ0Q7OztpQ0FFWTtBQUFBOztBQUNYLGFBQU87QUFDTCxtQkFBVyxtQkFBTTtBQUNmLGNBQUksTUFBTSxPQUFLLFlBQUwsQ0FBa0IsTUFBbEIsR0FBMkIsR0FBckM7QUFDQSxpQkFBSyxFQUFMLENBQVMsRUFBRSxNQUFGLEVBQVUsU0FBVixLQUF3QixDQUF6QixHQUE4QixHQUE5QixHQUFvQyxVQUFwQyxHQUFpRCxhQUF6RCxFQUF3RSxPQUFLLElBQUwsQ0FBVSxXQUFsRjtBQUNELFNBSkk7QUFLTCxvQkFBWSxvQkFBTTtBQUNoQixjQUFJLGFBQWEsT0FBSyxZQUFMLENBQWtCLENBQWxCLEVBQXFCLHFCQUFyQixFQUFqQjtBQUFBLGNBQ0UsVUFBVSxFQUFFLE1BQUYsRUFBVSxNQUFWLEVBRFo7QUFFQSxpQkFBSyxFQUFMLENBQVUsV0FBVyxHQUFYLEdBQWlCLE9BQUssSUFBTCxDQUFVLFNBQTVCLEdBQXlDLE9BQXpDLElBQXFELFdBQVcsTUFBWCxHQUFvQixPQUFLLElBQUwsQ0FBVSxZQUEvQixHQUErQyxPQUFwRyxHQUErRyxVQUEvRyxHQUE0SCxhQUFwSSxFQUFtSixPQUFLLElBQUwsQ0FBVSxXQUE3SjtBQUNELFNBVEk7QUFVTCxrQkFBVSxrQkFBTTtBQUNkLGNBQUksWUFBWSxFQUFFLE1BQUYsRUFBVSxTQUFWLEVBQWhCO0FBQ0EsaUJBQUssRUFBTCxDQUFRLFlBQVksT0FBSyxJQUFMLENBQVUsU0FBdEIsR0FBa0MsVUFBbEMsR0FBK0MsYUFBdkQsRUFBc0UsT0FBSyxJQUFMLENBQVUsV0FBaEY7QUFDRDtBQWJJLE9BQVA7QUFlRDs7OzhCQUVTO0FBQ1IsV0FBSyxFQUFMLENBQVEsV0FBUixDQUFvQixLQUFLLElBQUwsQ0FBVSxXQUE5QjtBQUNBLFdBQUssUUFBTDtBQUNEOzs7Ozs7O0FDaERIOzs7Ozs7Ozs7O0lBRWEsSyxXQUFBLEs7QUFFWCxtQkFBcUI7QUFBQTs7QUFDbkIsU0FBSyxLQUFMO0FBQ0Q7Ozs7NEJBRU87QUFDTixXQUFLLE1BQUwsR0FBYztBQUNaLGtCQUFVLGtCQUFrQixNQUFsQixJQUE0QixVQUFVLGdCQUF0QyxJQUEwRCxLQUR4RDtBQUVaLHFCQUFhLHVCQUFXO0FBQ3RCLGlCQUFPLE9BQU8sVUFBUCxDQUFrQiwwQkFBbEIsRUFBOEMsT0FBckQ7QUFDRDtBQUpXLE9BQWQ7QUFNQSxVQUFJLEtBQUssTUFBTCxDQUFZLFFBQWhCLEVBQTBCO0FBQ3hCLFVBQUU7QUFBQSxpQkFBTSxFQUFFLE1BQUYsRUFBVSxRQUFWLENBQW1CLFdBQW5CLENBQU47QUFBQSxTQUFGO0FBQ0Q7O0FBRUQ7QUFDQSxhQUFPLGNBQVAsQ0FBc0IsaUJBQWlCLFNBQXZDLEVBQWtELFdBQWxELEVBQStEO0FBQzdELGFBQUssZUFBVTtBQUNiLGlCQUFPLENBQUMsRUFBRSxLQUFLLFdBQUwsR0FBbUIsSUFBbkIsSUFBMkIsQ0FBQyxLQUFLLE1BQWpDLElBQTJDLENBQUMsS0FBSyxLQUFqRCxJQUEwRCxLQUFLLFVBQUwsR0FBa0IsQ0FBOUUsQ0FBUjtBQUNEO0FBSDRELE9BQS9EO0FBS0EsV0FBSyxVQUFMO0FBQ0Q7OztnQ0FFVyxJLEVBQU07QUFDaEIsVUFBSSxRQUFRLEVBQVo7QUFBQSxVQUNFLFFBQVEsQ0FBQyxLQUFLLENBQUwsTUFBWSxHQUFaLEdBQWtCLEtBQUssTUFBTCxDQUFZLENBQVosQ0FBbEIsR0FBbUMsSUFBcEMsRUFBMEMsS0FBMUMsQ0FBZ0QsR0FBaEQsQ0FEVjs7QUFHQSxXQUFLLElBQUksSUFBSSxDQUFiLEVBQWdCLElBQUksTUFBTSxNQUExQixFQUFrQyxHQUFsQyxFQUF1QztBQUNyQyxZQUFJLE9BQU8sTUFBTSxDQUFOLEVBQVMsS0FBVCxDQUFlLEdBQWYsQ0FBWDtBQUNBLGNBQU0sbUJBQW1CLEtBQUssQ0FBTCxDQUFuQixDQUFOLElBQXFDLG1CQUFtQixLQUFLLENBQUwsS0FBVyxFQUE5QixDQUFyQztBQUNEO0FBQ0QsYUFBTyxLQUFQO0FBQ0Q7OztpQ0FFWTtBQUFBOztBQUNYLFVBQUksUUFBUSxFQUFaO0FBQ0EsV0FBSyxLQUFMLEdBQWE7QUFDWCxhQUFLLGFBQUMsSUFBRCxFQUFPLEtBQVAsRUFBaUI7QUFDcEIsZ0JBQU0sSUFBTixJQUFjLENBQUMsS0FBRCxDQUFkO0FBQ0QsU0FIVTtBQUlYLGNBQU0sY0FBQyxJQUFELEVBQU8sS0FBUCxFQUFpQjtBQUNyQixjQUFJLE1BQU0sSUFBTixDQUFKLEVBQWlCO0FBQ2Ysa0JBQU0sSUFBTixFQUFZLElBQVosQ0FBaUIsS0FBakI7QUFDRCxXQUZELE1BRU87QUFDTCxrQkFBSyxLQUFMLENBQVcsR0FBWCxDQUFlLElBQWYsRUFBcUIsS0FBckI7QUFDRDtBQUNGLFNBVlU7QUFXWCxhQUFLLGFBQUMsSUFBRCxFQUFVO0FBQ2IsaUJBQU8sTUFBTSxJQUFOLEtBQWUsRUFBdEI7QUFDRCxTQWJVO0FBY1gsZ0JBQVEsZ0JBQUMsSUFBRCxFQUFVO0FBQ2hCLGlCQUFPLE1BQU0sSUFBTixDQUFQO0FBQ0Q7QUFoQlUsT0FBYjtBQWtCRDs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDM0RIOzs7Ozs7OztJQUVhLE8sV0FBQSxPOzs7Ozt3QkFJSTtBQUNiLGFBQU8sRUFBRSxNQUFGLENBQVMsRUFBVCxtR0FBNkI7QUFDbEMseUJBQWlCO0FBRGlCLE9BQTdCLENBQVA7QUFHRDs7O3dCQU5xQjtBQUFFLGFBQU8sK0ZBQVA7QUFBeUc7OztBQVFqSSxxQkFBcUI7QUFBQTs7QUFBQTs7QUFBQSxzQ0FBTixJQUFNO0FBQU4sVUFBTTtBQUFBOztBQUFBLDZJQUNWLElBRFU7O0FBRW5CLFVBQUssS0FBTDtBQUZtQjtBQUdwQjs7Ozs0QkFFTztBQUNOLFdBQUssV0FBTDtBQUNEOzs7a0NBRWE7QUFDWixVQUFJLFdBQVcsS0FBSyxrQkFBTCxFQUFmO0FBQUEsVUFDRSxjQUFjLE9BQU8sVUFBUCxDQUFrQiw0QkFBNEIsUUFBNUIsR0FBdUMsS0FBekQsQ0FEaEI7QUFBQSxVQUVFLE1BQU0sS0FBSyxFQUFMLENBQVEsR0FBUixDQUFZLGtCQUFaLEVBQWdDLE9BQWhDLENBQXdDLHdCQUF4QyxFQUFrRSxJQUFsRSxDQUZSO0FBR0EsV0FBSyxFQUFMLENBQVEsSUFBUixDQUFhLHlCQUFiLEVBQXdDLEdBQXhDO0FBQ0EsV0FBSyxrQkFBTCxDQUF3QixXQUF4QjtBQUNBLGtCQUFZLFdBQVosQ0FBd0IsS0FBSyxrQkFBTCxDQUF3QixJQUF4QixDQUE2QixJQUE3QixDQUF4QjtBQUNEOzs7dUNBRWtCLEcsRUFBSztBQUN0QixVQUFJLFFBQVEsSUFBSSxPQUFKLEdBQWMsMkJBQWQsR0FBNEMseUJBQXhEO0FBQ0EsV0FBSyxFQUFMLENBQVEsR0FBUixDQUFZLGtCQUFaLEVBQWdDLFVBQVMsS0FBSyxFQUFMLENBQVEsSUFBUixDQUFhLEtBQWIsQ0FBVCxHQUErQixJQUEvRDtBQUNEOzs7eUNBRW9CO0FBQ25CLFVBQUksU0FBUyxrQ0FBa0MsSUFBbEMsQ0FBdUMsS0FBSyxFQUFMLENBQVEsSUFBUixDQUFhLE9BQWIsQ0FBdkMsQ0FBYjtBQUNBLFVBQUksTUFBSixFQUFZO0FBQ1YsZUFBTyxPQUFPLENBQVAsQ0FBUDtBQUNEO0FBQ0QsYUFBTyxLQUFLLE9BQUwsQ0FBYSxlQUFwQjtBQUNEOzs7Ozs7Ozs7Ozs7Ozs7O0FDekNIOzs7Ozs7OztJQUVhLFksV0FBQSxZOzs7Ozt3QkFFVztBQUFFLGFBQU8sNkNBQVA7QUFBdUQ7OztBQUUvRSwwQkFBcUI7QUFBQTs7QUFBQTs7QUFBQSxzQ0FBTixJQUFNO0FBQU4sVUFBTTtBQUFBOztBQUFBLHVKQUNWLElBRFU7O0FBRW5CLFVBQUssS0FBTDtBQUZtQjtBQUdwQjs7Ozs0QkFFTztBQUNOLFdBQUssaUJBQUw7QUFDRDs7O3dDQUVtQjtBQUNsQjtBQUNBLFVBQU0sUUFBUSxLQUFLLEVBQUwsQ0FBUSxJQUFSLENBQWEsT0FBYixDQUFkO0FBQ0EsZUFBUyxLQUFLLEVBQUwsQ0FBUSxJQUFSLENBQWEsT0FBYixFQUFzQixNQUFNLE9BQU4sQ0FBYyxVQUFkLEVBQTBCLFlBQTFCLENBQXRCLENBQVQ7QUFDRDs7Ozs7Ozs7Ozs7Ozs7OztBQ25CSDs7OztJQUVhLGEsV0FBQSxhOzs7d0JBQ0k7QUFDYixhQUFPLEVBQVA7QUFDRDs7O0FBRUQseUJBQVksT0FBWixFQUFxQixPQUFyQixFQUE4QjtBQUFBOztBQUM1QixTQUFLLEVBQUwsR0FBVSxFQUFFLE9BQUYsQ0FBVjtBQUNBLFNBQUssT0FBTCxHQUFlLEVBQUUsTUFBRixDQUFTLEVBQVQsRUFBYSxLQUFLLFFBQWxCLEVBQTRCLE9BQTVCLENBQWY7O0FBRUEsUUFBSSxLQUFLLFVBQVQsRUFBcUI7QUFDbkIsV0FBSyxFQUFMLENBQVEsUUFBUixDQUFpQixLQUFLLFVBQXRCO0FBQ0Q7QUFDRjs7Ozt1QkFFRSxTLEVBQVcsTyxFQUFTO0FBQUE7O0FBQ3JCLFdBQUssRUFBTCxDQUFRLEVBQVIsQ0FBVyxTQUFYLEVBQXNCLE9BQXRCO0FBQ0E7QUFDQSxhQUFPO0FBQUEsZUFBTSxNQUFLLEdBQUwsQ0FBUyxTQUFULEVBQW9CLE9BQXBCLENBQU47QUFBQSxPQUFQO0FBQ0Q7Ozt3QkFFRyxTLEVBQVcsTyxFQUFTO0FBQ3RCLFdBQUssRUFBTCxDQUFRLEdBQVIsQ0FBWSxTQUFaLEVBQXVCLE9BQXZCO0FBQ0Q7Ozs0QkFFTyxTLEVBQW9CO0FBQUEsd0NBQU4sSUFBTTtBQUFOLFlBQU07QUFBQTs7QUFDMUIsV0FBSyxFQUFMLENBQVEsT0FBUixDQUFnQixTQUFoQixZQUErQixJQUEvQjtBQUNEOzs7OEJBRVMsUyxFQUFvQjtBQUFBLHlDQUFOLElBQU07QUFBTixZQUFNO0FBQUE7O0FBQzVCLFdBQUssT0FBTCxjQUFhLFNBQWIsU0FBMkIsSUFBM0I7QUFDQSx5QkFBUyxPQUFULDRCQUFpQixTQUFqQixFQUE0QixJQUE1QixTQUFxQyxJQUFyQztBQUNEOzs7NkJBRVEsRSxFQUFJLEssRUFBMEI7QUFBQSxVQUFuQixNQUFtQix1RUFBWixVQUFZOztBQUNyQyxVQUFJLFVBQVUsRUFBRSxRQUFGLENBQVcsRUFBWCxFQUFlLGFBQWYsQ0FBZDtBQUNBLFFBQUUsTUFBRixFQUFVLEVBQVYsQ0FBYSxRQUFiLEVBQXVCLE9BQXZCO0FBQ0EsYUFBTztBQUFBLGVBQU0sRUFBRSxNQUFGLEVBQVUsR0FBVixDQUFjLFFBQWQsRUFBd0IsT0FBeEIsQ0FBTjtBQUFBLE9BQVA7QUFDRDs7Ozs7OztBQ3ZDSDs7Ozs7Ozs7O0FBRUE7Ozs7Ozs7O0lBRWEsVSxXQUFBLFU7Ozs7O3dCQUlJO0FBQ2IsYUFBTztBQUNMLGdCQUFRO0FBQ04sbUJBQVMsZ0NBREg7QUFFTixrQkFBUSx1REFGRjtBQUdOLGtCQUFRO0FBSEY7QUFESCxPQUFQO0FBT0Q7Ozt3QkFWc0I7QUFBRSxhQUFPLHNDQUFQO0FBQWdEOzs7QUFZekUsd0JBQXFCO0FBQUE7O0FBQUE7O0FBQUEsc0NBQU4sSUFBTTtBQUFOLFVBQU07QUFBQTs7QUFBQSxtSkFDVixJQURVOztBQUVuQixVQUFLLEtBQUw7QUFGbUI7QUFHcEI7Ozs7NEJBRU87QUFBQTs7QUFDTixXQUFLLHFCQUFMO0FBQ0EsV0FBSyxnQkFBTDtBQUNBLFdBQUssSUFBSSxDQUFULElBQWMsS0FBSyxPQUFMLENBQWEsTUFBM0IsRUFBbUM7QUFDakMsWUFBSSxRQUFRLE9BQU8sVUFBUCxDQUFrQixLQUFLLE9BQUwsQ0FBYSxNQUFiLENBQW9CLENBQXBCLENBQWxCLENBQVo7QUFDQSxjQUFNLFdBQU4sQ0FBa0IsVUFBQyxLQUFEO0FBQUEsaUJBQVcsT0FBSyxNQUFMLENBQVksS0FBWixDQUFYO0FBQUEsU0FBbEI7QUFDQSxhQUFLLE1BQUwsQ0FBWSxLQUFaO0FBQ0Q7QUFDRjs7OzRDQUV1QjtBQUN0QixVQUFJLFFBQVEsS0FBSyxFQUFMLENBQVEsSUFBUixDQUFhLE9BQWIsQ0FBWjtBQUNBLFVBQUksS0FBSixFQUFXO0FBQ1QsWUFBSSxVQUFVLE1BQU0sT0FBTixDQUFjLGlCQUFkLEVBQWlDLEVBQWpDLENBQWQ7QUFBQSxZQUNFLFNBQVMsTUFBTSxLQUFOLENBQVkscUJBQVosQ0FEWDtBQUFBLFlBRUUsU0FBUyxNQUFNLEtBQU4sQ0FBWSxxQkFBWixDQUZYOztBQUlBLGlCQUFTLFNBQVMsT0FBTyxDQUFQLENBQVQsR0FBcUIsRUFBOUI7QUFDQSxpQkFBUyxTQUFTLE9BQU8sQ0FBUCxDQUFULEdBQXFCLEVBQTlCO0FBQ0EsbUJBQVcsS0FBSyxFQUFMLENBQVEsSUFBUixDQUFhLG9CQUFiLEVBQW1DLE9BQW5DLENBQVg7QUFDQSxrQkFBVSxLQUFLLEVBQUwsQ0FBUSxJQUFSLENBQWEsbUJBQWIsRUFBa0MsTUFBbEMsQ0FBVjtBQUNBLGtCQUFVLEtBQUssRUFBTCxDQUFRLElBQVIsQ0FBYSxtQkFBYixFQUFrQyxNQUFsQyxDQUFWO0FBQ0Q7QUFDRjs7O3VDQUVrQjtBQUNqQixPQUFDLEtBQUssRUFBTCxDQUFRLElBQVIsQ0FBYSxvQkFBYixDQUFELElBQXVDLEtBQUssRUFBTCxDQUFRLElBQVIsQ0FBYSxvQkFBYixFQUFtQyxFQUFuQyxDQUF2QztBQUNBLFVBQUksQ0FBQyxLQUFLLEVBQUwsQ0FBUSxJQUFSLENBQWEsbUJBQWIsQ0FBRCxJQUFzQyxLQUFLLEVBQUwsQ0FBUSxJQUFSLENBQWEsbUJBQWIsQ0FBMUMsRUFBNkU7QUFDM0UsYUFBSyxFQUFMLENBQVEsSUFBUixDQUFhLG1CQUFiLEVBQWtDLEtBQUssRUFBTCxDQUFRLElBQVIsQ0FBYSxtQkFBYixDQUFsQztBQUNEO0FBQ0Y7OzsyQkFFTSxLLEVBQU87QUFDWixVQUFJLE1BQU0sT0FBVixFQUFtQjtBQUNqQixnQkFBUSxNQUFNLEtBQWQ7QUFDRSxlQUFLLEtBQUssT0FBTCxDQUFhLE1BQWIsQ0FBb0IsT0FBekI7QUFBa0M7QUFDaEMsbUJBQUssUUFBTCxDQUFjLFNBQWQ7QUFDQTtBQUNEO0FBQ0QsZUFBSyxLQUFLLE9BQUwsQ0FBYSxNQUFiLENBQW9CLE1BQXpCO0FBQWlDO0FBQy9CLG1CQUFLLFFBQUwsQ0FBYyxRQUFkO0FBQ0E7QUFDRDtBQUNELGVBQUssS0FBSyxPQUFMLENBQWEsTUFBYixDQUFvQixNQUF6QjtBQUFpQztBQUMvQixtQkFBSyxRQUFMLENBQWMsUUFBZDtBQUNBO0FBQ0Q7QUFaSDtBQWNEO0FBQ0Y7Ozs2QkFFUSxNLEVBQVE7QUFDZixVQUFJLFFBQVEsS0FBSyxFQUFMLENBQVEsSUFBUixDQUFhLFdBQVcsTUFBeEIsS0FBbUMsS0FBSyxFQUFMLENBQVEsSUFBUixDQUFhLGVBQWIsQ0FBL0M7QUFDQSxXQUFLLEVBQUwsQ0FBUSxJQUFSLENBQWEsT0FBYixFQUFzQixLQUF0QjtBQUNEOzs7Ozs7O0FDN0VIOztBQUVBOzs7Ozs7Ozs7O0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOzs7O0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOzs7O0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOzs7O0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOzs7O0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOzs7O0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOzs7Ozs7Ozs7Ozs7OztBQ1ZBOzs7Ozs7OztJQUdhLE8sV0FBQSxPOzs7Ozt3QkFLSTtBQUNiLGFBQU8sRUFBRSxNQUFGLENBQVMsRUFBVCxtR0FBNkI7QUFDbEMsNkJBQXFCLGVBRGE7QUFFbEMsNkJBQXFCLHNCQUZhO0FBR2xDLHFCQUFhLFFBSHFCO0FBSWxDLHVCQUFlO0FBSm1CLE9BQTdCLENBQVA7QUFNRDs7O3dCQVZ1QjtBQUFFLGFBQU8sZ0JBQVA7QUFBMEI7Ozt3QkFDM0I7QUFBRSxhQUFPLGdCQUFQO0FBQTBCOzs7QUFXckQscUJBQXFCO0FBQUE7O0FBQUE7O0FBQUEsc0NBQU4sSUFBTTtBQUFOLFVBQU07QUFBQTs7QUFBQSw2SUFDVixJQURVOztBQUduQixVQUFLLFdBQUwsR0FBbUIsTUFBSyxFQUFMLENBQVEsSUFBUixDQUFhLE1BQUssT0FBTCxDQUFhLG1CQUExQixDQUFuQjtBQUNBLFVBQUssT0FBTCxHQUFlLEVBQUUsWUFBWSxNQUFLLEVBQUwsQ0FBUSxJQUFSLENBQWEsSUFBYixDQUFaLEdBQWlDLElBQW5DLENBQWY7QUFDQSxVQUFLLEVBQUwsQ0FBUSxJQUFSLENBQWEsaUJBQWlCLE1BQUssT0FBTCxDQUFhLG1CQUE5QixHQUFvRCxVQUFqRSxFQUE2RSxNQUE3RSxHQUFzRixRQUF0RixDQUErRixTQUFTLElBQXhHO0FBQ0EsVUFBSyxjQUFMO0FBQ0EsVUFBSyxhQUFMLEdBQXFCLE1BQUssRUFBTCxDQUFRLElBQVIsQ0FBYSwyQkFBYixDQUFyQjtBQVBtQjtBQVFwQjs7OztxQ0FFZ0I7QUFBQTs7QUFDZixXQUFLLE9BQUwsQ0FBYSxFQUFiLENBQWdCLE9BQWhCLEVBQXlCLFVBQUMsQ0FBRDtBQUFBLGVBQU8sT0FBSyxJQUFMLENBQVUsQ0FBVixDQUFQO0FBQUEsT0FBekI7QUFDQSxXQUFLLFdBQUwsQ0FBaUIsRUFBakIsQ0FBb0IsT0FBcEIsRUFBNkI7QUFBQSxlQUFNLE9BQUssS0FBTCxFQUFOO0FBQUEsT0FBN0I7QUFDQSxXQUFLLEVBQUwsQ0FBUSxNQUFSLEdBQWlCLEVBQWpCLENBQW9CLE9BQXBCLEVBQTZCLFVBQUMsQ0FBRCxFQUFPO0FBQ2xDLFVBQUUsRUFBRSxNQUFKLEVBQVksUUFBWixDQUFxQixpQkFBckIsS0FBMkMsT0FBSyxLQUFMLEVBQTNDO0FBQ0QsT0FGRDtBQUdEOzs7eUJBRUksQyxFQUFHO0FBQUE7O0FBQ04sV0FBSyxhQUFMLENBQW1CLE9BQW5CLENBQTJCLG9CQUEzQjtBQUNBLFdBQUssT0FBTDtBQUNBLFdBQUssYUFBTCxDQUFtQixPQUFuQixDQUEyQixnQkFBM0I7O0FBRUE7QUFDQSxRQUFFLFFBQUYsRUFBWSxHQUFaLENBQWdCLGVBQWhCLEVBQWlDLFVBQUMsQ0FBRCxFQUFPO0FBQ3JDLFVBQUUsT0FBRixJQUFhLEVBQWQsSUFBcUIsT0FBSyxLQUFMLEVBQXJCO0FBQ0QsT0FGRDtBQUdBLFFBQUUsY0FBRjtBQUNEOzs7OEJBRVM7QUFDUixXQUFLLGlCQUFMOztBQUVBLFdBQUssRUFBTCxDQUFRLFFBQVIsQ0FBaUIsS0FBSyxPQUFMLENBQWEsV0FBOUI7QUFDQSxXQUFLLEVBQUwsQ0FBUSxNQUFSLEdBQWlCLFFBQWpCLENBQTBCLEtBQUssT0FBTCxDQUFhLFdBQXZDO0FBQ0Q7Ozs0QkFFTztBQUNOLFFBQUUsUUFBRixFQUFZLEdBQVosQ0FBZ0IsZUFBaEI7QUFDQSxXQUFLLFFBQUw7QUFDRDs7OytCQUVVO0FBQ1QsV0FBSyxFQUFMLENBQVEsV0FBUixDQUFvQixLQUFLLE9BQUwsQ0FBYSxXQUFqQztBQUNBLFdBQUssRUFBTCxDQUFRLE1BQVIsR0FBaUIsV0FBakIsQ0FBNkIsS0FBSyxPQUFMLENBQWEsV0FBMUM7QUFDQSxXQUFLLGFBQUwsQ0FBbUIsT0FBbkIsQ0FBMkIsZ0JBQTNCO0FBQ0EsV0FBSyxpQkFBTDtBQUNEOzs7d0NBRW1CO0FBQ2xCLFFBQUUsU0FBUyxJQUFYLEVBQWlCLFFBQWpCLENBQTBCLEtBQUssT0FBTCxDQUFhLGFBQXZDO0FBQ0Q7Ozt3Q0FFbUI7QUFDbEIsUUFBRSxTQUFTLElBQVgsRUFBaUIsV0FBakIsQ0FBNkIsS0FBSyxPQUFMLENBQWEsYUFBMUM7QUFDRDs7Ozs7OztBQ3hFSDs7Ozs7Ozs7O0FBRUE7O0FBQ0E7O0FBQ0E7Ozs7SUFFYSxlLFdBQUEsZTs7O3dCQUdHO0FBQ1osYUFBTztBQUNMLDZCQUFxQixLQUNuQixzRkFEbUIsR0FFakIsMENBRmlCLEdBR25CO0FBSkcsT0FBUDtBQU1EOzs7d0JBQ2tCO0FBQ2pCLGFBQU87QUFDTCw4QkFBc0Isa0JBRGpCO0FBRUwsNkJBQXFCO0FBRmhCLE9BQVA7QUFJRDs7O3dCQWRxQjtBQUFFLGFBQU8sa0NBQVA7QUFBNEM7OztBQWVwRSwyQkFBWSxFQUFaLEVBQWdCO0FBQUE7O0FBQ2QsU0FBSyxLQUFMLENBQVcsRUFBWDtBQUNEOzs7OzBCQUVLLEUsRUFBSTtBQUNSLFdBQUssRUFBTCxHQUFVLEVBQUUsRUFBRixDQUFWO0FBQ0E7QUFDQSxVQUFJLEtBQUssRUFBTCxDQUFRLElBQVIsQ0FBYSxLQUFLLFlBQUwsQ0FBa0Isb0JBQS9CLEVBQXFELE1BQXJELElBQStELENBQUMsS0FBSyxFQUFMLENBQVEsUUFBUixDQUFpQixLQUFLLFlBQUwsQ0FBa0IsbUJBQW5DLENBQXBFLEVBQTZIO0FBQzNILHVDQUFpQixFQUFqQjtBQUNBO0FBQ0Q7QUFDRDtBQUNBLFVBQUksS0FBSyxFQUFMLENBQVEsUUFBUixDQUFpQixLQUFLLFlBQUwsQ0FBa0IsbUJBQW5DLENBQUosRUFBNkQ7QUFDM0QsYUFBSyxlQUFMLE1BQTBCLHFCQUFZLEtBQUssRUFBTCxDQUFRLENBQVIsQ0FBWixDQUExQjtBQUNBO0FBQ0Q7O0FBRUQ7QUFDQSwyQkFBWSxFQUFaO0FBQ0Q7OztzQ0FFaUI7QUFDaEIsV0FBSyxvQkFBTDtBQUNBLFVBQUksVUFBVSxLQUFLLEVBQUwsQ0FBUSxJQUFSLENBQWEsMkJBQWIsRUFBMEMsSUFBMUMsQ0FBK0MseUJBQS9DLENBQWQ7QUFBQSxVQUNJLFVBQVUsRUFBRSxjQUFjLE9BQWQsR0FBd0IsSUFBMUIsQ0FEZDtBQUVBLFVBQUksV0FBVyxRQUFRLE1BQXZCLEVBQStCO0FBQzdCLGFBQUssRUFBTCxHQUFVLEtBQUssRUFBTCxDQUFRLFdBQVIsQ0FBb0IsS0FBSyxZQUFMLENBQWtCLG1CQUF0QyxFQUNELElBREMsQ0FDSSwrQkFBK0IsS0FBSyxZQUFMLENBQWtCLG1CQUFqRCxHQUF1RSxVQUQzRSxFQUVELE1BRkMsR0FHRCxJQUhDLENBR0ksSUFISixFQUdVLE9BSFYsRUFJRCxNQUpDLENBSU0sS0FBSyxPQUFMLENBQWEsbUJBSm5CLENBQVY7QUFLQSxnQkFBUSxJQUFSLENBQWEsS0FBYixFQUFvQixPQUFwQjtBQUNEO0FBQ0QsYUFBTyxLQUFLLEVBQVo7QUFDRDs7QUFFRDs7OzsyQ0FDdUI7QUFDckIsV0FBSyxFQUFMLENBQVEsSUFBUixDQUFhLFFBQWIsRUFBdUIsTUFBdkI7QUFDRDs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDOURIOztBQUNBOztBQUNBOzs7Ozs7OztJQUdhLFksV0FBQSxZOzs7Ozs7QUFDWDt3QkFDaUI7QUFBRSxhQUFPLGVBQVA7QUFBeUI7Ozt3QkFFN0I7QUFDYixhQUFPLEVBQUUsTUFBRixDQUFTLEVBQVQsNkdBQTZCO0FBQ2xDLDhCQUFzQix5QkFEWTtBQUVsQyxpQ0FBeUIsb0JBRlM7QUFHbEMsZ0NBQXdCO0FBSFUsT0FBN0IsQ0FBUDtBQUtEOzs7QUFFRCwwQkFBcUI7QUFBQTs7QUFBQTs7QUFBQSxzQ0FBTixJQUFNO0FBQU4sVUFBTTtBQUFBOztBQUFBLHVKQUNWLElBRFU7O0FBRW5CLFVBQUssUUFBTCxHQUFnQixNQUFLLG1CQUFMLEVBQWhCO0FBRm1CO0FBR3BCOzs7O3lDQUVvQjtBQUNuQixVQUFJLHNCQUFzQixLQUFLLEVBQUwsQ0FBUSxJQUFSLENBQWEsS0FBSyxPQUFMLENBQWEsb0JBQTFCLENBQTFCO0FBQ0EsVUFBSSx5QkFBeUIsS0FBSyxFQUFMLENBQVEsSUFBUixDQUFhLEtBQUssT0FBTCxDQUFhLHVCQUExQixDQUE3QjtBQUNBLFVBQUksb0JBQW9CLE1BQXhCLEVBQWdDO0FBQzlCLGVBQU8saUNBQWtCLG1CQUFsQixDQUFQO0FBQ0QsT0FGRCxNQUVPLElBQUksdUJBQXVCLE1BQTNCLEVBQW1DO0FBQ3hDLGVBQU8sdUNBQXFCLHNCQUFyQixDQUFQO0FBQ0Q7O0FBRUQsY0FBUSxJQUFSLENBQWEsNENBQWI7QUFDQSxhQUFPLEVBQVA7QUFDRDs7OzBDQUVxQjtBQUNwQixVQUFJLENBQUMsS0FBSyxhQUFWLEVBQXlCO0FBQ3ZCLGFBQUssT0FBTCxHQUFlLEVBQUUsUUFBRixFQUFZLEVBQUMsU0FBUyxTQUFWLEVBQVosQ0FBZjtBQUNBLGFBQUssRUFBTCxDQUFRLElBQVIsQ0FBYSxLQUFLLE9BQUwsQ0FBYSxzQkFBMUIsRUFBa0QsTUFBbEQsQ0FBeUQsS0FBSyxPQUE5RDtBQUNEO0FBQ0Y7Ozs4QkFFUztBQUFBOztBQUNSLFVBQUksQ0FBQyxLQUFLLFlBQVYsRUFBd0I7QUFDdEIsYUFBSyxZQUFMLEdBQW9CLEtBQUssa0JBQUwsRUFBcEI7QUFDQSxhQUFLLFlBQUwsQ0FBa0IsSUFBbEIsQ0FBdUI7QUFBQSxpQkFBTSxPQUFLLFlBQUwsRUFBTjtBQUFBLFNBQXZCO0FBQ0E7QUFDQSxZQUFJLFFBQVEsS0FBUixDQUFjLE1BQWQsQ0FBcUIsUUFBekIsRUFBbUM7QUFDakM7QUFDQSxpQkFBTyxLQUFQO0FBQ0Q7QUFDRjtBQUNELFdBQUssV0FBTDtBQUNBO0FBQ0Q7OzsrQkFFVTtBQUNULFdBQUssV0FBTDtBQUNBO0FBQ0Q7OztrQ0FFYTtBQUNaLFdBQUssWUFBTCxDQUFrQixJQUFsQjtBQUNEOzs7a0NBRWE7QUFDWixXQUFLLFlBQUwsQ0FBa0IsS0FBbEI7QUFDRDs7O21DQUVjO0FBQ2IsV0FBSyxPQUFMLENBQWEsT0FBYjtBQUNEOzs7Ozs7Ozs7Ozs7Ozs7OztBQ3ZFSDtJQUNhLFEsV0FBQSxROzs7d0JBRUk7QUFDYixhQUFPO0FBQ0wscUJBQWEsbUJBRFI7QUFFTCx1QkFBZSxXQUZWO0FBR0wsc0JBQWMsQ0FIVDtBQUlMLHVCQUFlLENBSlY7QUFLTCxxQkFBYSxFQUxSO0FBTUwseUJBQWlCLElBTlo7QUFPTCxpQ0FBeUIsSUFQcEI7QUFRTCxpQkFBUyxtQkFBVyxDQUFFO0FBUmpCLE9BQVA7QUFVRDs7O0FBQ0Qsb0JBQVksRUFBWixFQUFnQixPQUFoQixFQUF5QjtBQUFBOztBQUN2QixTQUFLLENBQUwsR0FBUyxFQUFFLE1BQUYsQ0FBUyxFQUFULEVBQWEsS0FBSyxRQUFsQixFQUE0QixPQUE1QixDQUFUO0FBQ0EsU0FBSyxLQUFMLENBQVcsRUFBWDtBQUNEOzs7OzBCQUVLLEUsRUFBSTtBQUNSLFdBQUssVUFBTCxDQUFnQixFQUFoQjtBQUNBLFdBQUssU0FBTDtBQUNBLFdBQUssY0FBTCxDQUFvQixLQUFLLENBQUwsQ0FBTyxZQUEzQjtBQUNBLFdBQUssY0FBTDtBQUNEOzs7K0JBRVUsRSxFQUFJO0FBQ2IsV0FBSyxDQUFMLEdBQVM7QUFDUCxZQUFJLEVBREc7QUFFUCxnQkFBUSxFQUFFLEVBQUYsRUFBTSxRQUFOLEVBRkQ7QUFHUCxzQkFBYyxLQUFLLENBQUwsQ0FBTyxZQUhkO0FBSVAsbUJBQVcsQ0FKSjtBQUtQLHFCQUFhLEdBQUcsUUFBSCxHQUFjLE1BTHBCO0FBTVAsb0JBQVksQ0FOTDtBQU9QLG9CQUFZLENBUEw7QUFRUCx1QkFBZSxLQUFLLENBQUwsQ0FBTyxhQVJmO0FBU1AsMEJBQWtCO0FBVFgsT0FBVDtBQVdBLFdBQUssQ0FBTCxDQUFPLEVBQVAsQ0FBVSxRQUFWLENBQW1CLEtBQUssQ0FBTCxDQUFPLFdBQVAsR0FBcUIsR0FBckIsR0FBMkIsS0FBSyxDQUFMLENBQU8sV0FBckQ7QUFDQSxXQUFLLGlCQUFMO0FBQ0EsV0FBSyxZQUFMO0FBQ0Q7Ozt3Q0FFbUI7QUFDbEIsV0FBSyxDQUFMLENBQU8sSUFBUCxHQUFjLEVBQUUsU0FBRixFQUFhLEVBQUMsT0FBTyxnQkFBUixFQUFiLENBQWQ7QUFDQSxXQUFLLENBQUwsQ0FBTyxLQUFQLEdBQWUsRUFBRSxTQUFGLEVBQWEsRUFBQyxPQUFPLGlCQUFSLEVBQWIsQ0FBZjtBQUNBLFdBQUssQ0FBTCxDQUFPLElBQVAsQ0FBWSxNQUFaLENBQW1CLEtBQUssQ0FBTCxDQUFPLEtBQVAsQ0FBYSxNQUFiLENBQW9CLEtBQUssQ0FBTCxDQUFPLE1BQTNCLENBQW5CO0FBQ0EsV0FBSyxDQUFMLENBQU8sRUFBUCxDQUFVLE1BQVYsQ0FBaUIsS0FBSyxDQUFMLENBQU8sSUFBUCxDQUFZLE1BQVosQ0FBbUIsS0FBSyxDQUFMLENBQU8sS0FBMUIsQ0FBakI7QUFDRDs7O3FDQUVnQjtBQUNmLGFBQU8sS0FBSyxDQUFMLENBQU8sVUFBUCxHQUFvQixLQUFLLENBQUwsQ0FBTyxXQUFsQztBQUNEOzs7cUNBRWdCO0FBQ2YsVUFBSSxDQUFDLEtBQUssQ0FBTCxDQUFPLGFBQVosRUFBMkI7QUFDekIsWUFBSSxVQUFVLElBQUksTUFBSixDQUFXLGFBQVgsQ0FBZDtBQUFBLFlBQ0ksYUFBYSxLQUFLLENBQUwsQ0FBTyxNQUFQLENBQWMsS0FBZCxHQUFzQixJQUF0QixDQUEyQixPQUEzQixDQURqQjtBQUFBLFlBRUksT0FBTyxRQUFRLElBQVIsQ0FBYSxVQUFiLENBRlg7O0FBSUEsYUFBSyxDQUFMLENBQU8sYUFBUCxHQUF3QixRQUFRLEtBQUssQ0FBTCxDQUFULEdBQW9CLEtBQU0sQ0FBQyxLQUFLLENBQUwsQ0FBM0IsR0FBc0MsQ0FBN0Q7QUFDRDs7QUFFRCxhQUFPLEtBQUssQ0FBTCxDQUFPLEVBQVAsQ0FBVSxLQUFWLEtBQW9CLEtBQUssQ0FBTCxDQUFPLGFBQWxDO0FBQ0Q7OzttQ0FFYztBQUNiLFdBQUssQ0FBTCxDQUFPLFNBQVAsR0FBbUIsRUFBRSxZQUFGLEVBQWdCLEVBQUMsT0FBTyxNQUFSLEVBQWdCLE1BQU0sTUFBdEIsRUFBaEIsQ0FBbkI7QUFDQSxXQUFLLENBQUwsQ0FBTyxTQUFQLEdBQW1CLEVBQUUsWUFBRixFQUFnQixFQUFDLE9BQU8sTUFBUixFQUFnQixNQUFNLE1BQXRCLEVBQWhCLENBQW5CO0FBQ0EsV0FBSyxDQUFMLENBQU8sRUFBUCxDQUFVLE1BQVYsQ0FBaUIsS0FBSyxDQUFMLENBQU8sU0FBeEIsRUFBbUMsTUFBbkMsQ0FBMEMsS0FBSyxDQUFMLENBQU8sU0FBakQ7O0FBRUE7QUFDQSxXQUFLLENBQUwsQ0FBTyxlQUFQLElBQTBCLFFBQVEsWUFBUixDQUFxQixlQUFyQixDQUFxQyxLQUFLLENBQUwsQ0FBTyxFQUE1QyxFQUFnRDtBQUN4RSxnQkFBUSxLQUFLLENBQUwsQ0FBTyxTQUFQLENBQWlCLEdBQWpCLENBQXFCLEtBQUssQ0FBTCxDQUFPLFNBQTVCLENBRGdFO0FBRXhFLG1CQUFXLEtBQUssQ0FBTCxDQUFPLE1BQVAsQ0FBYyxLQUFkLEdBQXNCLElBQXRCLENBQTJCLEtBQUssQ0FBTCxDQUFPLGVBQWxDO0FBRjZELE9BQWhELENBQTFCO0FBSUQ7OztnQ0FFVztBQUNWLFdBQUssWUFBTDtBQUNBLFdBQUssVUFBTDtBQUNEOzs7bUNBRWM7QUFDYixXQUFLLENBQUwsQ0FBTyxVQUFQLEdBQW9CLEtBQUssY0FBTCxFQUFwQjtBQUNBLFdBQUssQ0FBTCxDQUFPLFVBQVAsR0FBb0IsS0FBSyxjQUFMLEVBQXBCO0FBQ0EsV0FBSyxDQUFMLENBQU8sWUFBUCxHQUFzQixLQUFLLENBQUwsQ0FBTyxVQUFQLEdBQW9CLEtBQUssQ0FBTCxDQUFPLEVBQVAsQ0FBVSxLQUFWLEVBQTFDO0FBQ0EsV0FBSyxDQUFMLENBQU8sWUFBUCxHQUF1QixLQUFLLENBQUwsQ0FBTyxZQUFQLEdBQXNCLENBQXZCLEdBQTRCLENBQTVCLEdBQWdDLEtBQUssQ0FBTCxDQUFPLFlBQTdEO0FBQ0Q7OztpQ0FFWTtBQUNYLFdBQUssQ0FBTCxDQUFPLE1BQVAsQ0FBYyxLQUFkLENBQW9CLEtBQUssQ0FBTCxDQUFPLFVBQTNCO0FBQ0EsV0FBSyxDQUFMLENBQU8sS0FBUCxDQUFhLEtBQWIsQ0FBbUIsS0FBSyxDQUFMLENBQU8sVUFBMUI7QUFDQSxXQUFLLE9BQUwsQ0FBYSxLQUFLLENBQUwsQ0FBTyxZQUFwQjs7QUFFQTtBQUNBLFVBQUksS0FBSyxDQUFMLENBQU8sdUJBQVgsRUFBb0M7QUFDbEMsYUFBSyxDQUFMLENBQU8sRUFBUCxDQUFVLEtBQUssQ0FBTCxDQUFPLEVBQVAsQ0FBVSxLQUFWLE1BQXFCLEtBQUssQ0FBTCxDQUFPLFVBQTVCLEdBQXlDLFVBQXpDLEdBQXNELGFBQWhFLEVBQStFLEtBQUssQ0FBTCxDQUFPLGFBQXRGO0FBQ0Q7O0FBRUQsV0FBSyxDQUFMLENBQU8sRUFBUCxDQUFVLE9BQVYsQ0FBa0IsYUFBbEI7QUFDRDs7O3FDQUVnQjtBQUFBOztBQUNmLFFBQUUsTUFBRixFQUFVLEVBQVYsQ0FBYSwrQkFBYixFQUE4QyxFQUFFLFFBQUYsQ0FBVztBQUFBLGVBQU0sTUFBSyxTQUFMLEVBQU47QUFBQSxPQUFYLEVBQW1DLEVBQW5DLENBQTlDO0FBQ0EsV0FBSyxDQUFMLENBQU8sU0FBUCxDQUFrQixFQUFsQixDQUFxQixPQUFyQixFQUE4QjtBQUFBLGVBQU0sTUFBSyxTQUFMLEVBQU47QUFBQSxPQUE5QjtBQUNBLFdBQUssQ0FBTCxDQUFPLFNBQVAsQ0FBa0IsRUFBbEIsQ0FBcUIsT0FBckIsRUFBOEI7QUFBQSxlQUFNLE1BQUssU0FBTCxFQUFOO0FBQUEsT0FBOUI7QUFDQSxXQUFLLENBQUwsQ0FBTyxNQUFQLENBQWMsRUFBZCxDQUFpQixPQUFqQixFQUEwQixVQUFDLENBQUQ7QUFBQSxlQUFPLE1BQUssZ0JBQUwsQ0FBc0IsQ0FBdEIsQ0FBUDtBQUFBLE9BQTFCO0FBQ0EsV0FBSyxpQkFBTDtBQUNEOzs7cUNBRWdCLEMsRUFBRztBQUNsQixXQUFLLE9BQUwsQ0FBYSxFQUFFLEVBQUUsYUFBSixFQUFtQixLQUFuQixFQUFiO0FBQ0Q7OztnQ0FFVztBQUNWLFdBQUssQ0FBTCxDQUFPLFNBQVAsR0FBbUIsS0FBSyxDQUFMLENBQU8sWUFBUCxHQUFzQixDQUF6QztBQUNDLFdBQUssQ0FBTCxDQUFPLFNBQVAsSUFBb0IsS0FBSyxDQUFMLENBQU8sV0FBNUIsS0FBNkMsS0FBSyxDQUFMLENBQU8sU0FBUCxHQUFtQixDQUFoRTs7QUFFQSxXQUFLLE9BQUw7QUFDRDs7O2dDQUVXO0FBQ1YsV0FBSyxDQUFMLENBQU8sU0FBUCxHQUFtQixLQUFLLENBQUwsQ0FBTyxZQUFQLEdBQXNCLENBQXpDO0FBQ0MsV0FBSyxDQUFMLENBQU8sU0FBUCxHQUFtQixDQUFwQixLQUEyQixLQUFLLENBQUwsQ0FBTyxTQUFQLEdBQW1CLEtBQUssQ0FBTCxDQUFPLFdBQVAsR0FBcUIsQ0FBbkU7O0FBRUEsV0FBSyxPQUFMO0FBQ0Q7Ozs0QkFFTyxLLEVBQU87QUFDYixVQUFJLE1BQU8sVUFBVSxTQUFYLEdBQXdCLEtBQXhCLEdBQWdDLEtBQUssQ0FBTCxDQUFPLFNBQWpEO0FBQUEsVUFDSSxRQUFRLEtBQUssU0FBTCxDQUFlLEdBQWYsQ0FEWjs7QUFHQTtBQUNBLFdBQUssQ0FBTCxDQUFPLGdCQUFQLEdBQTBCLEtBQTFCOztBQUVBO0FBQ0EsV0FBSyxNQUFMLENBQVksS0FBWjs7QUFFQTtBQUNBLFdBQUssQ0FBTCxDQUFPLFNBQVAsR0FBbUIsQ0FBbkI7O0FBRUE7QUFDQSxXQUFLLENBQUwsQ0FBTyxZQUFQLEdBQXNCLEdBQXRCOztBQUVBO0FBQ0EsV0FBSyxjQUFMLENBQW9CLEdBQXBCO0FBQ0Q7OzsyQkFFTSxFLEVBQUksWSxFQUFjLGdCLEVBQWtCO0FBQ3pDLFVBQUksV0FBVyxlQUFlLEtBQUssd0JBQUwsQ0FBOEIsRUFBOUIsQ0FBZixHQUFtRCxFQUFsRTtBQUNBLFdBQUssQ0FBTCxDQUFPLEtBQVAsQ0FBYSxHQUFiLENBQWlCLFdBQWpCLG1CQUE0QyxRQUE1QztBQUNEOzs7NkNBRXdCLEUsRUFBSTtBQUMzQixhQUFRLENBQUMsQ0FBRixHQUFPLEtBQUssQ0FBTCxDQUFPLFVBQWQsR0FBMkIsS0FBSyxLQUFMLENBQVcsS0FBSyxHQUFMLENBQVMsRUFBVCxJQUFlLEtBQUssQ0FBTCxDQUFPLFVBQWpDLENBQWxDO0FBQ0Q7Ozs4QkFFUyxHLEVBQUs7QUFDYixVQUFJLFFBQVEsS0FBSyxZQUFMLENBQWtCLEdBQWxCLENBQVo7O0FBRUEsVUFBSSxRQUFRLENBQVosRUFBZSxPQUFPLENBQVA7QUFDZixVQUFJLFNBQVMsS0FBSyxDQUFMLENBQU8sWUFBcEIsRUFBa0MsT0FBTyxLQUFLLENBQUwsQ0FBTyxZQUFQLEdBQXVCLENBQUMsQ0FBL0I7O0FBRWxDLGFBQU8sUUFBUyxDQUFDLENBQWpCO0FBQ0Q7OztpQ0FFWSxLLEVBQU87QUFDbEIsYUFBTyxRQUFRLEtBQUssQ0FBTCxDQUFPLFVBQXRCO0FBQ0Q7OzttQ0FFYyxLLEVBQU87QUFDcEIsVUFBSSxJQUFJLFNBQVMsS0FBSyxDQUFMLENBQU8sWUFBeEI7QUFDQSxXQUFLLENBQUwsQ0FBTyxNQUFQLENBQWMsV0FBZCxDQUEwQixRQUExQixFQUFvQyxFQUFwQyxDQUF1QyxDQUF2QyxFQUEwQyxRQUExQyxDQUFtRCxRQUFuRDs7QUFFQTtBQUNBLFdBQUssQ0FBTCxDQUFPLE9BQVAsQ0FBZSxDQUFmO0FBQ0Q7Ozt3Q0FFbUI7QUFBQTs7QUFDbEIsVUFBSSxZQUFZLENBQWhCO0FBQUEsVUFDSSxRQUFRO0FBQ04sa0JBQVUsS0FESjtBQUVOLHdCQUFnQixDQUZWO0FBR04seUJBQWlCLENBSFg7QUFJTixxQkFBYSxDQUpQO0FBS04sb0JBQVksQ0FMTjtBQU1OLGtCQUFVLENBTko7QUFPTixpQkFBUztBQVBILE9BRFo7O0FBV0EsV0FBSyxDQUFMLENBQU8sRUFBUCxDQUFVLEVBQVYsQ0FBYSxZQUFiLEVBQTJCLFVBQUMsQ0FBRCxFQUFPO0FBQ2hDLGNBQU0sUUFBTixHQUFpQixLQUFqQjtBQUNBLG9CQUFZLE1BQU0sY0FBTixHQUF1QixPQUFLLG9CQUFMLEVBQW5DO0FBQ0EsY0FBTSxlQUFOLEdBQXdCLEVBQUUsYUFBRixDQUFnQixjQUFoQixDQUErQixDQUEvQixFQUFrQyxPQUExRDtBQUNELE9BSkQ7O0FBTUEsV0FBSyxDQUFMLENBQU8sRUFBUCxDQUFVLEVBQVYsQ0FBYSxXQUFiLEVBQTBCLFVBQUMsQ0FBRCxFQUFPO0FBQy9CLGVBQUssQ0FBTCxDQUFPLEVBQVAsQ0FBVSxXQUFWLENBQXNCLFNBQXRCO0FBQ0EsY0FBTSxRQUFOLEdBQWlCLElBQWpCOztBQUVBLGNBQU0sV0FBTixHQUFvQixNQUFNLFFBQTFCO0FBQ0EsY0FBTSxVQUFOLEdBQW1CLE1BQU0sT0FBekI7QUFDQSxjQUFNLFFBQU4sR0FBaUIsRUFBRSxTQUFuQjtBQUNBLGNBQU0sT0FBTixHQUFnQixFQUFFLGFBQUYsQ0FBZ0IsY0FBaEIsQ0FBK0IsQ0FBL0IsRUFBa0MsT0FBbEQ7QUFDQSxvQkFBWSxNQUFNLGNBQU4sR0FBdUIsTUFBTSxPQUE3QixHQUF1QyxNQUFNLGVBQXpEOztBQUVBO0FBQ0EsWUFBSSxZQUFZLENBQWhCLEVBQW1CO0FBQ2pCLGlCQUFLLE1BQUwsQ0FBWSxZQUFVLENBQXRCO0FBQ0QsU0FGRCxNQUVPLElBQUksS0FBSyxHQUFMLENBQVMsU0FBVCxJQUFzQixPQUFLLENBQUwsQ0FBTyxZQUFqQyxFQUErQztBQUNwRCxpQkFBSyxNQUFMLENBQVksQ0FBQyxPQUFLLENBQUwsQ0FBTyxZQUFSLEdBQXdCLENBQUMsT0FBSyxDQUFMLENBQU8sWUFBUCxHQUFzQixTQUF2QixJQUFvQyxDQUF4RTtBQUNELFNBRk0sTUFFQTtBQUNMLGlCQUFLLE1BQUwsQ0FBWSxTQUFaO0FBQ0Q7QUFDRixPQWxCRDs7QUFvQkEsV0FBSyxDQUFMLENBQU8sRUFBUCxDQUFVLEVBQVYsQ0FBYSxVQUFiLEVBQXlCLFlBQU07QUFDN0IsWUFBSSxNQUFNLFFBQVYsRUFBb0I7QUFDbEIsY0FBSSxPQUFPLE1BQU0sUUFBTixHQUFpQixNQUFNLFdBQWxDO0FBQUEsY0FDSSxXQUFXLE1BQU0sT0FBTixHQUFnQixNQUFNLFVBRHJDO0FBQUEsY0FFSSxhQUFhLFdBQVcsQ0FBWCxHQUFlLENBQUMsQ0FBaEIsR0FBb0IsQ0FGckM7QUFBQSxjQUdJLFFBQVEsS0FBSyxHQUFMLENBQVMsV0FBVyxJQUFYLEdBQWtCLEVBQTNCLENBSFo7QUFBQSxjQUlJLEtBQUssUUFBUSxDQUFSLEdBQWEsQ0FBQyxRQUFRLElBQVIsR0FBZSxJQUFoQixJQUF3QixFQUF4QixHQUE2QixVQUExQyxHQUF5RCxRQUFRLEVBQVIsR0FBYSxVQUovRTs7QUFNQSxpQkFBSyxDQUFMLENBQU8sRUFBUCxDQUFVLFFBQVYsQ0FBbUIsU0FBbkI7O0FBRUEsY0FBSSxVQUFVLFlBQVksRUFBMUI7O0FBRUE7QUFDQSxjQUFJLFlBQVksQ0FBWixJQUFpQixVQUFVLENBQS9CLEVBQWtDO0FBQ2hDLHNCQUFVLENBQVY7QUFDRDs7QUFFRDtBQUNBLGNBQUksWUFBWSxDQUFDLE9BQUssQ0FBTCxDQUFPLFlBQXBCLElBQW9DLFVBQVUsQ0FBQyxPQUFLLENBQUwsQ0FBTyxZQUExRCxFQUF3RTtBQUN0RSxzQkFBVSxDQUFDLE9BQUssQ0FBTCxDQUFPLFlBQWxCO0FBQ0Q7O0FBRUQsaUJBQUssTUFBTCxDQUFZLE9BQVosRUFBcUIsSUFBckIsRUFBMkIsU0FBM0I7QUFDRDtBQUNGLE9BeEJEO0FBeUJEOzs7MkNBRXNCO0FBQ3JCLFVBQUksVUFBVSxJQUFJLE1BQUosQ0FBVyxzQkFBWCxDQUFkO0FBQUEsVUFDSSxRQUFRLEtBQUssQ0FBTCxDQUFPLEtBQVAsQ0FBYSxJQUFiLENBQWtCLE9BQWxCLENBRFo7QUFBQSxVQUVJLE9BQU8sUUFBUSxJQUFSLENBQWEsS0FBYixDQUZYOztBQUlBLFVBQUksSUFBSixFQUFVLE9BQU8sQ0FBQyxLQUFLLENBQUwsQ0FBUjs7QUFFVixhQUFPLENBQVA7QUFDRDs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDN1BIOzs7Ozs7OztBQUVBOzs7SUFHYSxjLFdBQUEsYzs7O3dCQUlPO0FBQ2hCLGFBQU87QUFDTCxpQkFBUyxXQURKO0FBRUwsa0JBQVU7QUFGTCxPQUFQO0FBSUQ7Ozt3QkFQcUI7QUFBRSxhQUFPLDhCQUFQO0FBQXdDOzs7QUFRaEUsMEJBQVksRUFBWixFQUFnQjtBQUFBOztBQUNkLFNBQUssS0FBTCxDQUFXLEVBQVg7QUFDRDs7OzswQkFFSyxFLEVBQUk7QUFDUixVQUFJLEVBQUUsRUFBRixFQUFNLFFBQU4sQ0FBZSxLQUFLLFdBQUwsQ0FBaUIsT0FBaEMsQ0FBSixFQUE4QztBQUM1QyxZQUFJLGFBQUosQ0FBa0IsRUFBbEI7QUFDRDtBQUNELFVBQUksRUFBRSxFQUFGLEVBQU0sUUFBTixDQUFlLEtBQUssV0FBTCxDQUFpQixRQUFoQyxDQUFKLEVBQStDO0FBQzdDLFlBQUksY0FBSixDQUFtQixFQUFFLEVBQUYsRUFBTSxJQUFOLENBQVcsT0FBWCxDQUFuQjtBQUNEO0FBQ0Y7Ozs7OztBQUlIOzs7OztJQUdNLGE7Ozs7O3dCQUVXO0FBQ2IsYUFBTyxFQUFFLE1BQUYsQ0FBUyxFQUFULCtHQUE2QjtBQUNsQyxrQkFBVSxJQUR3QjtBQUVsQyxrQkFBVSxJQUZ3QjtBQUdsQyxtQkFBVyxLQUh1QjtBQUlsQyxpQkFBUyxNQUp5QjtBQUtsQyxvQkFBWSxPQUxzQjtBQU1sQywyQkFBbUIsZUFOZTtBQU9sQyx3QkFBZ0IsWUFQa0I7QUFRbEMseUJBQWlCLG1CQVJpQjtBQVNsQyxpQ0FBeUIsc0JBVFM7QUFVbEMsaUNBQXlCLDJCQVZTO0FBV2xDLHNCQUFjO0FBWG9CLE9BQTdCLENBQVA7QUFhRDs7O0FBRUQsMkJBQXFCO0FBQUE7O0FBQUE7O0FBQUEsc0NBQU4sSUFBTTtBQUFOLFVBQU07QUFBQTs7QUFBQSx5SkFDVixJQURVOztBQUVuQixVQUFLLEtBQUw7QUFGbUI7QUFHcEI7Ozs7NEJBRU87QUFDTixXQUFLLFNBQUwsR0FBaUIsSUFBakI7QUFDQSxXQUFLLGNBQUw7QUFDQSxXQUFLLE1BQUw7QUFDQSxXQUFLLFdBQUw7QUFDQSxXQUFLLFFBQUw7QUFDRDs7QUFFRDs7OztxQ0FDaUI7QUFDZixXQUFLLE9BQUwsR0FBZSxFQUFFLE1BQUYsQ0FBUyxFQUFULEVBQWEsS0FBSyxPQUFsQixFQUEyQixLQUFLLEVBQUwsQ0FBUSxJQUFSLEVBQTNCLENBQWY7O0FBRUE7QUFDQSxXQUFLLEVBQUwsQ0FBUSxPQUFSLENBQWdCLHVCQUFoQixFQUF5QyxNQUF6QyxLQUFvRCxLQUFLLE9BQUwsQ0FBYSxTQUFiLEdBQXlCLE1BQTdFO0FBQ0Q7OztrQ0FFYTtBQUFBOztBQUNaLFVBQUksS0FBSyxFQUFMLENBQVEsT0FBUixDQUFnQixLQUFLLE9BQUwsQ0FBYSx1QkFBN0IsRUFBc0QsTUFBdEQsSUFBZ0UsS0FBSyxFQUFMLENBQVEsSUFBUixDQUFhLEtBQUssT0FBTCxDQUFhLHVCQUExQixFQUFtRCxNQUF2SCxFQUErSDtBQUM3SCxZQUFJLFFBQVEsS0FBSyxFQUFMLENBQVEsS0FBUixDQUFjLFVBQWQsQ0FBWjtBQUNBLGFBQUssT0FBTCxDQUFhLFlBQWIsR0FBNEIsTUFBTSxVQUFsQztBQUNBLGFBQUssU0FBTCxHQUFpQixFQUFFLFVBQUYsRUFBYyxFQUFDLE9BQU8sWUFBUixFQUFkLENBQWpCO0FBQ0EsYUFBSyxFQUFMLENBQVEsRUFBUixDQUFXLG9CQUFYLEVBQWlDLFVBQUMsQ0FBRCxFQUFJLEtBQUosRUFBVyxZQUFYO0FBQUEsaUJBQTRCLE9BQUssY0FBTCxDQUFvQixZQUFwQixDQUE1QjtBQUFBLFNBQWpDLEVBQ1EsTUFEUixDQUNlLEtBQUssU0FEcEI7O0FBR0EsYUFBSyxjQUFMLENBQW9CLE1BQU0sWUFBMUI7QUFDRDtBQUNGOzs7bUNBRWMsWSxFQUFjO0FBQzNCLFdBQUssU0FBTCxDQUFlLElBQWYsQ0FBdUIsZUFBZSxDQUF0QyxTQUEyQyxLQUFLLE9BQUwsQ0FBYSxZQUF4RDtBQUNEOzs7NkJBRVE7QUFDUCxXQUFLLEVBQUwsQ0FDRyxFQURILENBQ00sTUFETixFQUNjLEtBQUssWUFEbkIsRUFFRyxFQUZILENBRU0sY0FGTixFQUVzQixLQUFLLGFBRjNCLEVBR0csRUFISCxDQUdNLGFBSE4sRUFHcUIsS0FBSyxZQUgxQixFQUlHLEtBSkgsQ0FJUztBQUNMLGNBQU0sS0FBSyxPQUFMLENBQWEsT0FBYixLQUF5QixNQUF6QixJQUFtQyxLQUFLLE9BQUwsQ0FBYSxPQUFiLEtBQXlCLFFBRDdEO0FBRUwsZ0JBQVEsS0FBSyxPQUFMLENBQWEsT0FBYixLQUF5QixNQUF6QixJQUFtQyxLQUFLLE9BQUwsQ0FBYSxPQUFiLEtBQXlCLE9BQTVELElBQXVFLEtBQUssRUFBTCxDQUFRLElBQVIsQ0FBYSxnQkFBYixNQUFtQyxNQUY3RztBQUdMLGtCQUFVLEtBQUssT0FBTCxDQUFhLFNBSGxCO0FBSUwsbUJBQVcsS0FKTjtBQUtMLHVCQUFlLEtBQUssT0FBTCxDQUFhLFFBTHZCO0FBTUwsa0JBQVUsSUFOTDtBQU9MLHdCQUFnQjtBQVBYLE9BSlQ7QUFhRDs7O2tDQUVhLEMsRUFBRyxLLEVBQU87QUFDdEIsWUFBTSxPQUFOLENBQWMsRUFBZCxDQUFpQixNQUFNLFlBQXZCLEVBQXFDLElBQXJDLENBQTBDLGlCQUExQyxFQUE2RCxPQUE3RCxDQUFxRSxvQkFBckU7QUFDRDs7O2lDQUVZLEMsRUFBRyxLLEVBQU87QUFDckIsWUFBTSxPQUFOLENBQWMsRUFBZCxDQUFpQixNQUFNLFlBQXZCLEVBQXFDLElBQXJDLENBQTBDLGlCQUExQyxFQUE2RCxPQUE3RCxDQUFxRSxtQkFBckU7QUFDRDs7OytCQUVVO0FBQUE7O0FBQ1QsV0FBSyxFQUFMLENBQVEsSUFBUixDQUFhLHNCQUFiLEVBQXFDLE1BQXJDLElBQStDLEtBQUssRUFBTCxDQUFRLFFBQVIsQ0FBaUIsS0FBSyxPQUFMLENBQWEsY0FBOUIsQ0FBL0M7QUFDQSxXQUFLLEVBQUwsQ0FBUSxJQUFSLENBQWEsNEJBQWIsRUFBMkMsTUFBM0MsSUFBcUQsS0FBSyxFQUFMLENBQVEsUUFBUixDQUFpQixLQUFLLE9BQUwsQ0FBYSxlQUE5QixDQUFyRDtBQUNBLFdBQUssRUFBTCxDQUFRLElBQVIsQ0FBYSwrQkFBYixFQUE4QyxNQUE5QyxJQUF3RCxLQUFLLEVBQUwsQ0FBUSxRQUFSLENBQWlCLHNCQUFqQixDQUF4RDtBQUNBLFdBQUssRUFBTCxDQUFRLFFBQVIsQ0FBaUIsS0FBSyxPQUFMLENBQWEsVUFBOUI7O0FBRUE7QUFDQSxVQUFJLENBQUMsS0FBSyxFQUFMLENBQVEsT0FBUixDQUFnQixxQkFBaEIsRUFBdUMsTUFBeEMsSUFBa0QsQ0FBQyxLQUFLLE9BQUwsQ0FBYSxTQUFwRSxFQUErRTtBQUM3RSxhQUFLLEVBQUwsQ0FBUSxRQUFSLENBQWlCLEtBQUssT0FBTCxDQUFhLGlCQUE5QjtBQUNEOztBQUVEOztBQUVBLFVBQUksS0FBSyxFQUFMLENBQVEsT0FBUixDQUFnQiw0QkFBaEIsRUFBOEMsTUFBOUMsSUFBd0QsS0FBSyxPQUFMLENBQWEsT0FBYixLQUF5QixPQUFqRixJQUE0RixLQUFLLEVBQUwsQ0FBUSxJQUFSLENBQWEsNEJBQWIsRUFBMkMsTUFBM0ksRUFBbUo7QUFDakosYUFBSyxFQUFMLENBQVEsSUFBUixDQUFhLGNBQWIsRUFBNkIsUUFBN0IsQ0FBc0MsS0FBSyxPQUFMLENBQWEsdUJBQW5EO0FBQ0Q7O0FBRUQsUUFBRSxNQUFGLEVBQVUsRUFBVixDQUFhLFFBQWIsRUFBdUI7QUFBQSxlQUFNLEVBQUUsUUFBRixDQUFXO0FBQUEsaUJBQU0sT0FBSyxFQUFMLENBQVEsS0FBUixDQUFjLGFBQWQsQ0FBTjtBQUFBLFNBQVgsRUFBK0MsR0FBL0MsQ0FBTjtBQUFBLE9BQXZCO0FBQ0Q7Ozs7OztBQUlIOzs7Ozs7SUFJTSxjOzs7Ozt3QkFDVztBQUNiLGFBQU87QUFDTCw4QkFBc0IsbUJBRGpCO0FBRUwsNEJBQW9CLGdCQUZmO0FBR0wsNkJBQXFCLEdBSGhCO0FBSUwsb0JBQVksT0FKUDtBQUtMLGlDQUF5QixTQUxwQjtBQU1MLDhCQUFzQiwwQkFOakI7QUFPTCwyQkFBbUIsZUFQZDtBQVFMLGVBQU87QUFDTCxtQkFBUyxVQURKO0FBRUwsa0JBQVE7QUFGSDtBQVJGLE9BQVA7QUFhRDs7O0FBRUQsNEJBQXFCO0FBQUE7O0FBQUE7O0FBQUEsdUNBQU4sSUFBTTtBQUFOLFVBQU07QUFBQTs7QUFBQSw4SkFDVixJQURVOztBQUVuQixXQUFLLEtBQUw7QUFGbUI7QUFHcEI7Ozs7NEJBRU87QUFDTixXQUFLLElBQUwsR0FBWTtBQUNWLGNBQU0sSUFESTtBQUVWLG1CQUFXLEtBRkQ7QUFHVixrQkFBVSxDQUFDLEtBQUssRUFBTCxDQUFRLE9BQVIsQ0FBZ0IsS0FBSyxPQUFMLENBQWEsdUJBQTdCLEVBQXNELE1BSHZEO0FBSVYsc0JBQWMsS0FBSyxnQkFBTCxFQUpKO0FBS1Ysd0JBQWdCLEdBTE47QUFNVixlQUFPO0FBTkcsT0FBWjtBQVFBLFdBQUssZ0JBQUwsR0FBd0I7QUFDdEIsb0JBQVksSUFEVTtBQUV0Qix1QkFBZSxLQUZPO0FBR3RCLHNCQUFjLENBSFE7QUFJdEIsb0JBQVksQ0FBQztBQUNULHNCQUFZLElBREg7QUFFVCxvQkFBVTtBQUNSLDJCQUFlO0FBRFA7QUFGRCxTQUFELEVBTVY7QUFDRSxzQkFBWSxHQURkO0FBRUUsb0JBQVU7QUFDUiwyQkFBZTtBQURQO0FBRlosU0FOVTtBQUpVLE9BQXhCO0FBa0JBLFdBQUssT0FBTCxDQUFhLElBQWIsR0FBb0IsS0FBSyxjQUFMLEVBQXBCO0FBQ0EsV0FBSyxvQkFBTDtBQUNBLFdBQUssUUFBTDtBQUVEOztBQUVEOzs7O3VDQUNtQjtBQUNqQixVQUFJLGVBQWUsQ0FBbkI7QUFBQSxVQUNFLDBCQUEwQixLQUFLLEVBQUwsQ0FBUSxPQUFSLENBQWdCLEtBQUssT0FBTCxDQUFhLG9CQUE3QixDQUQ1Qjs7QUFHQSxVQUFJLHdCQUF3QixNQUE1QixFQUFvQztBQUNsQyx1QkFBZSx5QkFBeUIsSUFBekIsQ0FBOEIsd0JBQXdCLElBQXhCLENBQTZCLE9BQTdCLENBQTlCLEVBQXFFLENBQXJFLENBQWY7QUFDQSx1QkFBZSxDQUFDLFlBQUQsR0FBZ0IsQ0FBL0I7QUFDRDtBQUNELGFBQU8sWUFBUDtBQUNEOzs7cUNBRWdCO0FBQ2YsYUFBTyxLQUFLLEVBQUwsQ0FBUSxNQUFSLEdBQWlCLFFBQWpCLENBQTBCLEtBQUssT0FBTCxDQUFhLGtCQUF2QyxJQUE2RCxLQUFLLE9BQUwsQ0FBYSxLQUFiLENBQW1CLE1BQWhGLEdBQXlGLEtBQUssT0FBTCxDQUFhLEtBQWIsQ0FBbUIsT0FBbkg7QUFDRDs7OzJDQUVzQjtBQUNyQjtBQUNBLFVBQUksS0FBSyxFQUFMLENBQVEsTUFBUixHQUFpQixRQUFqQixDQUEwQixLQUFLLE9BQUwsQ0FBYSxrQkFBdkMsQ0FBSixFQUFnRTs7QUFFOUQ7QUFDQSxZQUFJLEtBQUssRUFBTCxDQUFRLE1BQVIsR0FBaUIsUUFBakIsQ0FBMEIsWUFBMUIsS0FBMkMsRUFBRSxhQUFGLEVBQWlCLE1BQWhFLEVBQXdFO0FBQ3RFLGVBQUssRUFBTCxDQUFRLFFBQVIsQ0FBaUIsWUFBakIsRUFBK0IsTUFBL0IsR0FBd0MsV0FBeEMsQ0FBb0QsWUFBcEQ7QUFDQSxlQUFLLGdCQUFMLEdBQXdCLEVBQUUsTUFBRixDQUFTLEVBQVQsRUFBYSxLQUFLLGdCQUFsQixFQUFvQztBQUMxRCxzQkFBVSxhQURnRDtBQUUxRCxrQkFBTTtBQUZvRCxXQUFwQyxDQUF4QjtBQUlEO0FBQ0QsWUFBSSxLQUFLLEVBQUwsQ0FBUSxNQUFSLEdBQWlCLFFBQWpCLENBQTBCLFlBQTFCLEtBQTJDLEVBQUUsYUFBRixFQUFpQixNQUFoRSxFQUF3RTtBQUN0RSxlQUFLLEVBQUwsQ0FBUSxRQUFSLENBQWlCLFlBQWpCLEVBQStCLE1BQS9CLEdBQXdDLFdBQXhDLENBQW9ELFlBQXBEO0FBQ0EsZUFBSyxnQkFBTCxHQUF3QixFQUFFLE1BQUYsQ0FBUyxFQUFULEVBQWEsS0FBSyxnQkFBbEIsRUFBb0M7QUFDMUQsc0JBQVUsYUFEZ0Q7QUFFMUQsb0JBQVEsS0FGa0Q7QUFHMUQsbUJBQU87QUFIbUQsV0FBcEMsQ0FBeEI7QUFLRDtBQUNEOztBQUVBLGFBQUssSUFBTCxHQUFZLEVBQUUsTUFBRixDQUFTLEVBQVQsRUFBYSxLQUFLLElBQWxCLEVBQXdCLEtBQUssZ0JBQTdCLENBQVo7QUFDQSxhQUFLLE1BQUw7QUFDQTtBQUNEO0FBQ0QsVUFBSSxDQUFDLEtBQUssY0FBTCxFQUFELElBQTBCLEtBQUssb0JBQUwsRUFBOUIsRUFBMkQ7QUFDekQsYUFBSyxNQUFMO0FBQ0E7QUFDRDtBQUNELFVBQUksS0FBSyxjQUFMLE1BQXlCLENBQUMsS0FBSyxvQkFBTCxFQUE5QixFQUEyRDtBQUN6RCxhQUFLLFFBQUw7QUFDRDtBQUNGOzs7cUNBRWdCO0FBQ2YsYUFBTyxLQUFLLEVBQUwsQ0FBUSxRQUFSLENBQWlCLEtBQUssT0FBTCxDQUFhLG9CQUE5QixDQUFQO0FBQ0Q7OzsyQ0FFc0I7QUFDckIsYUFBTyxPQUFPLFVBQVAsR0FBb0IsS0FBSyxPQUFMLENBQWEsbUJBQXhDO0FBQ0Q7Ozs2QkFFUTtBQUFBOztBQUNQLFdBQUssRUFBTCxDQUFRLEtBQVIsQ0FBYyxLQUFLLElBQW5CO0FBQ0EsV0FBSyxJQUFMLENBQVUsWUFBVixJQUEwQixXQUFXO0FBQUEsZUFBTSxPQUFLLEVBQUwsQ0FBUSxLQUFSLENBQWMsV0FBZCxFQUEyQixPQUFLLElBQUwsQ0FBVSxZQUFyQyxDQUFOO0FBQUEsT0FBWCxDQUExQjtBQUNEOzs7K0JBRVU7QUFDVCxXQUFLLEVBQUwsQ0FBUSxLQUFSLENBQWMsU0FBZDtBQUNEOzs7K0JBRVU7QUFBQTs7QUFDVCxXQUFLLEVBQUwsQ0FBUSxRQUFSLENBQWlCLEtBQUssT0FBTCxDQUFhLGlCQUFiLEdBQWlDLEdBQWpDLEdBQXVDLEtBQUssT0FBTCxDQUFhLFVBQXJFO0FBQ0EsVUFBSSxLQUFLLE9BQUwsQ0FBYSxJQUFiLEtBQXNCLEtBQUssT0FBTCxDQUFhLEtBQWIsQ0FBbUIsT0FBN0MsRUFBc0Q7QUFDcEQsVUFBRSxNQUFGLEVBQVUsRUFBVixDQUFhLFFBQWIsRUFBdUIsRUFBRSxRQUFGLENBQVc7QUFBQSxpQkFBTSxPQUFLLG9CQUFMLEVBQU47QUFBQSxTQUFYLEVBQThDLEdBQTlDLENBQXZCO0FBQ0Q7QUFDRjs7Ozs7Ozs7Ozs7Ozs7OztBQzFRSDs7Ozs7Ozs7QUFFQSxJQUFJLGNBQWMsRUFBRSxRQUFGLEVBQWxCOztJQUVhLGdCLFdBQUEsZ0I7Ozs7O3dCQUVPO0FBQ2hCLGFBQU87QUFDTCxzQkFBYSxTQURSO0FBRUwsb0JBQVk7QUFGUCxPQUFQO0FBSUQ7OztBQUNELDRCQUFZLE9BQVosRUFBcUI7QUFBQTs7QUFBQSxvSUFDYixPQURhOztBQUVuQixVQUFLLGtCQUFMLEdBQTBCLEVBQUUsUUFBRixFQUExQjtBQUNBLFVBQUssa0JBQUwsR0FBMEIsRUFBRSxRQUFGLEVBQTFCO0FBQ0EsVUFBSyxTQUFMLEdBQWlCLElBQWpCO0FBQ0EsVUFBSyxTQUFMLEdBQWlCLEtBQWpCO0FBQ0EsVUFBSyxjQUFMLEdBQXNCLEVBQUUsUUFBRixDQUFXLFFBQVgsQ0FBdEI7QUFObUI7QUFPcEI7Ozs7eUJBRUksRSxFQUFJO0FBQUE7O0FBQ1AsV0FBSyxlQUFMO0FBQ0EsV0FBSyxrQkFBTDs7QUFFQSxhQUFPLEtBQUssZUFBTCxHQUNKLElBREksQ0FDQztBQUFBLGVBQU0sT0FBSyxlQUFMLEVBQU47QUFBQSxPQURELEVBRUosSUFGSSxDQUVDLFVBQUMsTUFBRCxFQUFZO0FBQ2hCLGVBQUssU0FBTCxHQUFpQixNQUFqQjtBQUNBLGVBQUssa0JBQUwsQ0FBd0IsT0FBeEI7QUFDQSxlQUFLLFlBQUw7QUFDQSxlQUFLLFNBQUwsQ0FBZSxFQUFmLENBQWtCLE1BQWxCLEVBQTBCLFlBQU07QUFDOUIsaUJBQUssa0JBQUwsQ0FBd0IsT0FBeEI7QUFDRCxTQUZEO0FBR0EsY0FBTSxJQUFOOztBQUVBLGVBQU8sT0FBSyxrQkFBTCxDQUF3QixPQUF4QixFQUFQO0FBQ0QsT0FaSSxDQUFQO0FBYUQ7Ozt5Q0FFb0I7QUFDbkIsVUFBSSxjQUFjLEVBQUUsTUFBRixDQUFTLEVBQVQsRUFBYSxLQUFLLFdBQWxCLEVBQStCLEtBQUssVUFBTCxFQUEvQixDQUFsQjtBQUFBLFVBQ0ksV0FBVyxFQUFFLFdBQUYsRUFBZSxFQUFDLE1BQU0sS0FBSyxjQUFaLEVBQTRCLFNBQVMsVUFBckMsRUFBZixDQURmOztBQUdBLFdBQUssSUFBSSxDQUFULElBQWMsV0FBZCxFQUEyQjtBQUN6QixpQkFBUyxJQUFULENBQWMsQ0FBZCxFQUFpQixZQUFZLENBQVosQ0FBakI7QUFDRDtBQUNELFdBQUssRUFBTCxDQUFRLFdBQVIsQ0FBb0IsUUFBcEI7O0FBRUEsV0FBSyxFQUFMLEdBQVUsUUFBVjtBQUNBLFdBQUssU0FBTCxHQUFpQixZQUFZLGNBQVosQ0FBakI7QUFDQSxXQUFLLFFBQUwsR0FBZ0IsWUFBWSxhQUFaLENBQWhCO0FBQ0Q7OztzQ0FFaUI7QUFBQTs7QUFDaEIsV0FBSyxTQUFMLEdBQWlCLEtBQUssRUFBTCxDQUFRLElBQVIsQ0FBYSxlQUFiLE1BQWtDLE1BQW5EO0FBQ0EsV0FBSyxTQUFMLElBQWtCLEtBQUssa0JBQUwsQ0FBd0IsSUFBeEIsQ0FBNkIsWUFBTTtBQUNuRCxlQUFLLGFBQUw7QUFDRCxPQUZpQixDQUFsQjtBQUdEOzs7aUNBRVk7QUFBQTs7QUFDWDtBQUNBLFVBQUksS0FBSyxFQUFMLENBQVEsSUFBUixDQUFhLFlBQWIsQ0FBSixFQUFnQztBQUM5QixlQUFPLFFBQVEsS0FBUixDQUFjLFdBQWQsQ0FBMEIsS0FBSyxFQUFMLENBQVEsSUFBUixDQUFhLFlBQWIsQ0FBMUIsQ0FBUDtBQUNEO0FBQ0QsVUFBSSxpQkFBaUIsQ0FBQztBQUNoQixjQUFNLFNBRFU7QUFFaEIsYUFBSztBQUZXLE9BQUQsRUFHZDtBQUNELGNBQU0sUUFETDtBQUVELGFBQUs7QUFGSixPQUhjLEVBTWQ7QUFDRCxjQUFNLFVBREw7QUFFRCxhQUFLO0FBRkosT0FOYyxDQUFyQjtBQUFBLFVBVUksU0FBUyxFQVZiOztBQVlBLHFCQUFlLE9BQWYsQ0FBdUIsVUFBQyxLQUFELEVBQVEsS0FBUixFQUFrQjtBQUN2QyxlQUFPLFVBQVUsTUFBTSxJQUF2QixJQUErQixPQUFLLEVBQUwsQ0FBUSxJQUFSLENBQWEsVUFBVSxNQUFNLEdBQTdCLENBQS9CO0FBQ0QsT0FGRDtBQUdBLGFBQU8sTUFBUDtBQUNEOzs7c0NBRWlCO0FBQ2hCLFVBQUksWUFBWSxFQUFFLFFBQUYsRUFBaEI7QUFBQSxVQUNJLFNBQVMsS0FBSyxvQkFBTCxFQURiOztBQUdBLFFBQUUsU0FBRixDQUFZLE1BQVosRUFBb0IsWUFBTTtBQUN4QixrQkFBVSxPQUFWO0FBQ0QsT0FGRDtBQUdBLGFBQU8sVUFBVSxPQUFWLEVBQVA7QUFDRDs7O29DQUVlLE8sRUFBUyxPLEVBQVM7QUFDaEMsVUFBSSxjQUFjLEVBQUUsUUFBRixFQUFsQjs7QUFFQSxVQUFJLE9BQU8sT0FBUCxJQUFrQixXQUF0QixFQUFtQztBQUNqQyxnQkFBUSxLQUFLLGNBQWIsRUFBNkIsS0FBN0IsQ0FBbUMsWUFBVztBQUM1QztBQUNBLHNCQUFZLE9BQVosQ0FBb0IsSUFBcEI7QUFDRCxTQUhEO0FBSUQ7O0FBRUQsYUFBTyxZQUFZLE9BQVosRUFBUDtBQUNEOzs7MkNBRXNCO0FBQ3JCLGFBQU8sOEJBQThCLEtBQUssU0FBbkMsR0FBK0MsR0FBL0MsR0FBcUQsS0FBSyxRQUExRCxHQUFxRSx1QkFBNUU7QUFDRDs7O29DQUVlO0FBQ2QsVUFBSSxTQUFTLEtBQUssU0FBTCxDQUFlLFVBQWYsRUFBYjtBQUNBLFdBQUssSUFBSSxJQUFJLENBQWIsRUFBZ0IsSUFBSSxPQUFPLE1BQTNCLEVBQW1DLEdBQW5DLEVBQXdDO0FBQ3RDLFlBQUksUUFBUSxPQUFPLENBQVAsQ0FBWjtBQUNBLFlBQUksTUFBTSxJQUFOLEtBQWUsVUFBZixJQUE2QixLQUFLLHFCQUFMLENBQTJCLE1BQU0sUUFBakMsQ0FBakMsRUFBNkU7QUFDM0UsZ0JBQU0sSUFBTixHQUFhLFNBQWI7QUFDRDtBQUNGO0FBQ0Y7OzswQ0FFcUIsUSxFQUFVO0FBQzlCLFVBQUksUUFBSixFQUFjO0FBQ1osWUFBSSxZQUFZLFNBQVMsV0FBVCxFQUFoQjtBQUFBLFlBQ0ksbUJBQW1CLFVBQVUsS0FBVixDQUFnQixHQUFoQixDQUR2QjtBQUFBLFlBRUksV0FBVyxFQUFFLE1BQUYsRUFBVSxJQUFWLENBQWUsTUFBZixJQUF5QixFQUFFLE1BQUYsRUFBVSxJQUFWLENBQWUsTUFBZixFQUF1QixXQUF2QixFQUF6QixHQUFnRSxPQUYvRTs7QUFJQSxZQUFJLGlCQUFpQixNQUFqQixHQUEwQixDQUE5QixFQUFpQztBQUMvQixpQkFBTyxjQUFjLFFBQXJCO0FBQ0Q7QUFDRCxlQUFPLGlCQUFpQixDQUFqQixNQUF3QixTQUFTLEtBQVQsQ0FBZSxHQUFmLEVBQW9CLENBQXBCLENBQS9CO0FBQ0Q7QUFDRjs7OzBDQUVxQjtBQUNwQixVQUFJLE9BQU8sRUFBRSxNQUFGLEVBQVUsSUFBVixDQUFlLE1BQWYsS0FBMEIsSUFBckM7QUFDQSxhQUFPLEtBQUssS0FBTCxDQUFXLEdBQVgsRUFBZ0IsQ0FBaEIsQ0FBUDtBQUNEOzs7eUJBRUksRSxFQUFJO0FBQUE7O0FBQ1AsV0FBSyxrQkFBTCxDQUF3QixJQUF4QixDQUE2QixZQUFNO0FBQ2pDLGVBQUssU0FBTCxDQUFlLElBQWY7QUFDQSxjQUFNLElBQU47QUFDRCxPQUhEO0FBSUQ7Ozs0QkFFTztBQUFBOztBQUNOLFdBQUssa0JBQUwsQ0FBd0IsSUFBeEIsQ0FBNkIsWUFBTTtBQUNqQyxlQUFLLFNBQUwsQ0FBZSxLQUFmO0FBQ0QsT0FGRDtBQUdEOzs7MkJBRU07QUFBQTs7QUFDTCxXQUFLLGtCQUFMLENBQXdCLElBQXhCLENBQTZCLFlBQU07QUFDakMsZUFBSyxTQUFMLENBQWUsS0FBZjtBQUNBLGVBQUssU0FBTCxDQUFlLFdBQWYsQ0FBMkIsSUFBM0I7QUFDRCxPQUhEO0FBSUQ7OzttQ0FFYztBQUFBOztBQUNiLFVBQUksU0FBUyxDQUFDLFNBQUQsRUFBWSxPQUFaLEVBQXFCLE9BQXJCLENBQWI7QUFBQSxVQUNJLGFBQWEsRUFEakI7QUFBQSxVQUVJLCtCQUZKO0FBQUEsVUFHSSxpQ0FISjtBQUFBLFVBSUksZUFBZSxJQUpuQjs7QUFNQTtBQUNBLFdBQUssU0FBTCxDQUFlLEVBQWYsQ0FBa0IsZ0JBQWxCLEVBQW9DLFlBQU07QUFDeEMsaUNBQXlCLEtBQUssS0FBTCxDQUFXLE9BQUssU0FBTCxDQUFlLFFBQWYsTUFBNkIsQ0FBeEMsQ0FBekI7QUFDQSxxQkFBYSxPQUFLLFNBQUwsQ0FBZSxTQUFmLEdBQTJCLE9BQUssU0FBTCxDQUFlLFNBQWYsQ0FBeUIsSUFBcEQsR0FBMkQsRUFBeEU7QUFDRCxPQUhEOztBQUtBO0FBQ0EsYUFBTyxPQUFQLENBQWUsVUFBQyxLQUFELEVBQVc7QUFDeEIsZUFBSyxTQUFMLENBQWUsRUFBZixDQUFrQixLQUFsQixFQUF5QixVQUFDLEtBQUQ7QUFBQSxpQkFBVyxZQUFZLElBQVosU0FBdUIsS0FBdkIsQ0FBWDtBQUFBLFNBQXpCO0FBQ0QsT0FGRDs7QUFJQSxlQUFTLFdBQVQsQ0FBcUIsS0FBckIsRUFBNEI7QUFDMUI7QUFDQSxZQUFJLHNCQUFKLEVBQTRCO0FBQzFCLHFDQUEyQixLQUFLLEtBQUwsQ0FBVyxLQUFLLFNBQUwsQ0FBZSxXQUFmLE1BQWdDLENBQTNDLENBQTNCO0FBQ0Esa0JBQVEsTUFBTSxJQUFkO0FBQ0UsaUJBQUssU0FBTDtBQUFnQjtBQUNkO0FBQ0Esb0JBQUksWUFBSixFQUFrQjtBQUNoQixpQ0FBZSxLQUFmO0FBQ0EsdUJBQUssTUFBTCxFQUFhLFVBQWIsRUFBeUIsc0JBQXpCO0FBQ0Q7QUFDRCxxQkFBSyxNQUFMLEVBQWEsVUFBYixFQUF5Qix3QkFBekI7QUFDQTtBQUNEO0FBQ0QsaUJBQUssT0FBTDtBQUFjO0FBQ1o7QUFDQyw0Q0FBNEIsc0JBQTdCLElBQXdELEtBQUssTUFBTCxFQUFhLFVBQWIsRUFBeUIsd0JBQXpCLENBQXhEO0FBQ0E7QUFDRDtBQUNELGlCQUFLLE9BQUw7QUFBYztBQUNaOztBQUVBLCtCQUFlLElBQWY7QUFDQSxxQkFBSyxPQUFMLEVBQWMsVUFBZCxFQUEwQix3QkFBMUI7QUFDQTtBQUNEO0FBckJIO0FBdUJEO0FBQ0Y7O0FBRUQsZUFBUyxJQUFULENBQWMsU0FBZCxFQUF5QixLQUF6QixFQUFnQyxRQUFoQyxFQUEwQztBQUN4QyxnQkFBUSxHQUFSLENBQVksU0FBWixFQUF1QixLQUF2QixFQUE4QixRQUE5QjtBQUNBLFlBQUk7QUFDRiw0QkFBa0IsU0FBbEIsRUFBNkIsS0FBN0IsRUFBb0MsUUFBcEM7QUFDRCxTQUZELENBRUUsT0FBTSxDQUFOLEVBQVM7QUFDVCxrQkFBUSxHQUFSLENBQVksMkJBQTJCLEVBQUUsT0FBekM7QUFDRDtBQUNGO0FBQ0Y7Ozs7Ozs7Ozs7Ozs7Ozs7QUN2Tkg7Ozs7Ozs7O0FBRUEsSUFBTSx1QkFBdUIsb0NBQTdCOztBQUVBLElBQUksbUJBQW1CLEVBQUUsUUFBRixFQUF2Qjs7SUFFYSxhLFdBQUEsYTs7Ozs7d0JBTVk7QUFDckIsYUFBTztBQUNMLG9CQUFZO0FBQ1YsZUFBSztBQURLO0FBRFAsT0FBUDtBQUtEOzs7d0JBRW9CO0FBQ25CLGFBQU87QUFDTCxpQkFBVTtBQURMLE9BQVA7QUFHRDs7O3dCQUVpQjtBQUNoQixhQUFPO0FBQ0wseUJBQWlCLENBQUMsQ0FEYjtBQUVMLG1CQUFXLENBQUMsQ0FGUDtBQUdMLGVBQU8sQ0FIRjtBQUlMLGlCQUFTLENBSko7QUFLTCxnQkFBUSxDQUxIO0FBTUwsbUJBQVcsQ0FOTjtBQU9MLGNBQU07QUFQRCxPQUFQO0FBU0Q7Ozt3QkE1QnVCO0FBQUUsYUFBTyxxQkFBUDtBQUErQjs7O3dCQUNoQztBQUFFLGFBQU8sc0JBQVA7QUFBZ0M7Ozt3QkFDbkM7QUFBRSxhQUFPLHFCQUFQO0FBQStCOzs7QUE0QnpELHlCQUFZLE9BQVosRUFBcUI7QUFBQTs7QUFBQSw4SEFDYixPQURhOztBQUVuQixVQUFLLE1BQUwsR0FBYyxFQUFFLE1BQUYsQ0FBUyxFQUFULEVBQWEsTUFBSyxXQUFsQixDQUFkO0FBQ0EsVUFBSyxrQkFBTCxHQUEwQixFQUFFLFFBQUYsRUFBMUI7QUFDQSxVQUFLLGNBQUwsR0FBc0IsSUFBdEI7O0FBRUE7QUFDQSxVQUFLLE9BQUwsR0FBZSxNQUFLLGdCQUFMLENBQXNCLE1BQUssRUFBM0IsRUFBK0IsTUFBSyxjQUFwQyxDQUFmO0FBQ0EsVUFBSyxPQUFMLEdBQWUsRUFBRSxNQUFGLENBQVMsRUFBVCxFQUFhLE1BQUssT0FBbEIsRUFBMkIsTUFBSyxnQkFBaEMsQ0FBZjtBQVJtQjtBQVNwQjs7OztxQ0FFZ0IsTyxFQUFTLFUsRUFBWTtBQUNwQyxhQUFPLElBQVAsQ0FBWSxVQUFaLEVBQXdCLE9BQXhCLENBQWdDLFVBQUMsSUFBRCxFQUFVO0FBQ3hDLG1CQUFXLElBQVgsSUFBbUIsUUFBUSxJQUFSLENBQWEsV0FBVyxJQUFYLENBQWIsQ0FBbkI7QUFDRCxPQUZEO0FBR0EsYUFBTyxVQUFQO0FBQ0Q7Ozt5QkFFSSxFLEVBQUk7QUFBQTs7QUFDUCxVQUFNLGVBQWUsS0FBSyxFQUFMLENBQVEsQ0FBUixDQUFyQjs7QUFFQSxhQUFPLGtCQUNKLElBREksQ0FDQztBQUFBLGVBQU0sZ0JBQWdCLFlBQWhCLEVBQThCLE9BQUssT0FBbkMsQ0FBTjtBQUFBLE9BREQsRUFFSixJQUZJLENBRUMsVUFBQyxNQUFELEVBQVk7QUFDaEIsZUFBSyxjQUFMLEdBQXNCLE1BQXRCO0FBQ0EsZUFBSyxjQUFMLENBQW9CLGdCQUFwQixDQUFxQyxlQUFyQyxFQUFzRCxVQUFDLElBQUQ7QUFBQSxpQkFBVSxPQUFLLG9CQUFMLENBQTBCLElBQTFCLENBQVY7QUFBQSxTQUF0RDtBQUNBLGVBQUssa0JBQUwsQ0FBd0IsT0FBeEI7QUFDQSxjQUFNLElBQU47QUFDQSxlQUFPLE9BQUssa0JBQUwsQ0FBd0IsT0FBeEIsRUFBUDtBQUNELE9BUkksQ0FBUDtBQVNEOzs7eUNBRW9CLEssRUFBTztBQUMxQixVQUFJLHlCQUF5QixLQUFLLEtBQUwsQ0FBWSxLQUFLLGNBQUwsQ0FBb0IsV0FBcEIsS0FBb0MsSUFBckMsSUFBOEMsQ0FBekQsQ0FBN0I7QUFBQSxVQUNJLDJCQUEyQixLQUFLLEtBQUwsQ0FBVyxLQUFLLGNBQUwsQ0FBb0IsY0FBcEIsTUFBd0MsQ0FBbkQsQ0FEL0I7QUFBQSxVQUVJLGFBQWEsS0FBSyxjQUFMLENBQW9CLFlBQXBCLEdBQW1DLEtBRnBEO0FBR0EsVUFBSTtBQUNBLFlBQUksS0FBSyxNQUFMLENBQVksZUFBWixJQUErQixLQUFLLE1BQUwsQ0FBWSxTQUEvQyxFQUEwRDtBQUN0RCxrQkFBUSxHQUFSLENBQVksb0RBQVosRUFBa0UsU0FBUyxHQUFULEdBQWUsVUFBZixHQUE0QixHQUE1QixHQUFrQyxzQkFBcEc7QUFDQSw0QkFBa0IsTUFBbEIsRUFBMEIsVUFBMUIsRUFBc0Msc0JBQXRDO0FBQ0g7QUFDRCxZQUFJLE1BQU0sSUFBTixJQUFjLEtBQUssTUFBTCxDQUFZLE9BQTlCLEVBQXVDO0FBQ25DLGtCQUFRLEdBQVIsQ0FBWSxrREFBWixFQUFnRSxTQUFTLEdBQVQsR0FBZSxVQUFmLEdBQTRCLEdBQTVCLEdBQWtDLHdCQUFsRztBQUNBLDRCQUFrQixNQUFsQixFQUEwQixVQUExQixFQUFzQyx3QkFBdEM7QUFDSCxTQUhELE1BR08sSUFBSSxNQUFNLElBQU4sSUFBYyxLQUFLLE1BQUwsQ0FBWSxNQUE5QixFQUFzQztBQUN6QyxrQkFBUSxHQUFSLENBQVksaURBQVosRUFBK0QsU0FBUyxHQUFULEdBQWUsVUFBZixHQUE0QixHQUE1QixHQUFrQyx3QkFBakc7QUFDQSw0QkFBa0IsTUFBbEIsRUFBMEIsVUFBMUIsRUFBc0Msd0JBQXRDO0FBQ0gsU0FITSxNQUdBLElBQUksTUFBTSxJQUFOLElBQWMsS0FBSyxNQUFMLENBQVksS0FBOUIsRUFBcUM7QUFDeEMsa0JBQVEsR0FBUixDQUFZLGdEQUFaLEVBQThELFVBQVUsR0FBVixHQUFnQixVQUE5RTtBQUNBLDRCQUFrQixPQUFsQixFQUEyQixVQUEzQjtBQUNIO0FBQ0osT0FmRCxDQWVFLE9BQU0sQ0FBTixFQUFTO0FBQ1AsZ0JBQVEsR0FBUixDQUFZLEVBQUUsT0FBZDtBQUNIO0FBQ0QsVUFBSSxNQUFNLElBQU4sSUFBYyxLQUFLLE1BQUwsQ0FBWSxLQUE5QixFQUFxQztBQUNuQyxhQUFLLE1BQUwsQ0FBWSxlQUFaLEdBQThCLEtBQUssTUFBTCxDQUFZLFNBQTFDO0FBQ0QsT0FGRCxNQUVRO0FBQ04sYUFBSyxNQUFMLENBQVksZUFBWixHQUE4QixNQUFNLElBQXBDO0FBQ0Q7QUFDRjs7O3lCQUVJLEUsRUFBSTtBQUFBOztBQUNQLFdBQUssa0JBQUwsQ0FBd0IsSUFBeEIsQ0FBNkIsWUFBTTtBQUNqQyxlQUFLLGNBQUwsQ0FBb0IsU0FBcEI7QUFDQSxlQUFLLFNBQUwsQ0FBZSxjQUFjLFVBQTdCO0FBQ0EsY0FBTSxJQUFOO0FBQ0QsT0FKRDtBQUtEOzs7NEJBRU87QUFBQTs7QUFDTixXQUFLLGtCQUFMLENBQXdCLElBQXhCLENBQTZCLFlBQU07QUFDakMsZUFBSyxjQUFMLENBQW9CLFVBQXBCO0FBQ0EsZUFBSyxTQUFMLENBQWUsY0FBYyxXQUE3QjtBQUNELE9BSEQ7QUFJRDs7OzJCQUVNO0FBQUE7O0FBQ0wsV0FBSyxrQkFBTCxDQUF3QixJQUF4QixDQUE2QixZQUFNO0FBQ2pDLGVBQUssY0FBTCxDQUFvQixTQUFwQjtBQUNBLGVBQUssU0FBTCxDQUFlLGNBQWMsVUFBN0I7QUFDRCxPQUhEO0FBSUQ7Ozs7OztBQUdILFNBQVMsZUFBVCxHQUEyQjtBQUN6QixNQUFJLENBQUMsT0FBTyxFQUFaLEVBQWdCO0FBQ2QsV0FBTyx1QkFBUCxHQUFpQztBQUFBLGFBQU0saUJBQWlCLE9BQWpCLEVBQU47QUFBQSxLQUFqQztBQUNBLE1BQUUsU0FBRixDQUFZLG9CQUFaO0FBQ0QsR0FIRCxNQUdPO0FBQ0wsUUFBSSxDQUFDLE9BQU8sRUFBUCxDQUFVLE1BQWYsRUFBdUI7QUFDckIsYUFBTyx1QkFBUCxHQUFpQztBQUFBLGVBQU0saUJBQWlCLE9BQWpCLEVBQU47QUFBQSxPQUFqQztBQUNELEtBRkQsTUFFTztBQUNMLHVCQUFpQixPQUFqQjtBQUNEO0FBQ0Y7QUFDRCxTQUFPLGlCQUFpQixPQUFqQixFQUFQO0FBQ0Q7O0FBRUQsU0FBUyxlQUFULENBQXlCLE9BQXpCLEVBQWtDLE9BQWxDLEVBQTJDO0FBQ3pDLE1BQUksY0FBYyxFQUFFLFFBQUYsRUFBbEI7O0FBRUEsVUFBUSxNQUFSLEdBQWlCLFFBQVEsTUFBUixJQUFrQixFQUFuQztBQUNBLFVBQVEsTUFBUixDQUFlLE9BQWYsR0FBeUIsVUFBQyxLQUFELEVBQVc7QUFDbEMsZ0JBQVksT0FBWixDQUFvQixNQUFNLE1BQTFCO0FBQ0QsR0FGRDtBQUdBLFVBQVEsTUFBUixDQUFlLE9BQWYsR0FBeUI7QUFBQSxXQUFNLFlBQVksTUFBWixFQUFOO0FBQUEsR0FBekI7QUFDQSxVQUFRLEdBQVIsQ0FBWSxPQUFaO0FBQ0EsTUFBSSxPQUFPLEVBQVAsQ0FBVSxNQUFkLENBQXFCLE9BQXJCLEVBQThCLE9BQTlCOztBQUVBLFNBQU8sWUFBWSxPQUFaLEVBQVA7QUFDRDs7Ozs7Ozs7Ozs7OztJQ3BKWSxJLFdBQUEsSTs7O3dCQUVLO0FBQ2QsYUFBTztBQUNMLG9CQUFZLGNBRFA7QUFFTCwyQkFBbUI7QUFGZCxPQUFQO0FBSUQ7OztBQUVELGtCQUFxQjtBQUFBOztBQUNuQixTQUFLLEtBQUw7QUFDRDs7Ozs0QkFFTztBQUFBOztBQUNOLFFBQUUsS0FBSyxTQUFMLENBQWUsVUFBakIsRUFBNkIsSUFBN0IsQ0FBa0MsVUFBQyxLQUFELEVBQVEsRUFBUixFQUFlO0FBQy9DLFlBQUksUUFBUSxFQUFFLEVBQUYsRUFBTSxJQUFOLENBQVcsMEJBQVgsQ0FBWjtBQUNBLFlBQUksVUFBVSxFQUFFLEVBQUYsQ0FBZDtBQUNBLGNBQUssaUJBQUwsQ0FBdUIsS0FBdkIsRUFBOEIsT0FBOUI7QUFDQSxjQUFLLHdCQUFMLENBQThCLEtBQTlCO0FBQ0EsVUFBRSxNQUFGLEVBQVUsRUFBVixDQUFhLGFBQWIsRUFBNEIsRUFBRSxRQUFGLENBQVc7QUFBQSxpQkFBTSxNQUFLLGlCQUFMLENBQXVCLEtBQXZCLEVBQThCLE9BQTlCLENBQU47QUFBQSxTQUFYLEVBQXlELEVBQXpELENBQTVCO0FBQ0QsT0FORDtBQU9BLFFBQUUsS0FBSyxTQUFMLENBQWUsaUJBQWpCLEVBQW9DLElBQXBDLENBQXlDLFVBQUMsS0FBRCxFQUFRLEVBQVIsRUFBZTtBQUN0RCxZQUFJLFdBQVcsRUFBRSxFQUFGLEVBQU0sSUFBTixDQUFXLFFBQVgsQ0FBZjtBQUFBLFlBQ0UsUUFBUSxTQUFTLElBQVQsQ0FBYyxtQkFBZCxDQURWO0FBQUEsWUFFRSxXQUFXLE1BQU0sTUFBTixDQUFhLFVBQUMsS0FBRCxFQUFRLEVBQVI7QUFBQSxpQkFBZSxFQUFFLEVBQUYsRUFBTSxRQUFOLENBQWUsUUFBZixDQUFmO0FBQUEsU0FBYixDQUZiOztBQUlBLGlCQUFTLE1BQVQsSUFBbUIsRUFBRSxFQUFGLEVBQU0sUUFBTixDQUFlLHdCQUF3QixTQUFTLEtBQVQsRUFBdkMsQ0FBbkI7QUFDQSxjQUFNLElBQU4sQ0FBVyxVQUFDLEtBQUQsRUFBUSxFQUFSO0FBQUEsaUJBQWUsRUFBRSxFQUFGLEVBQU0sSUFBTixDQUFXLFlBQVgsRUFBeUIsRUFBRSxFQUFGLEVBQU0sS0FBTixFQUF6QixDQUFmO0FBQUEsU0FBWDtBQUNBLGlCQUFTLEVBQVQsQ0FBWSxTQUFaLEVBQXVCLFlBQU07QUFDM0IsZ0JBQ0csSUFESCxDQUNRLFVBQUMsQ0FBRCxFQUFJLENBQUo7QUFBQSxtQkFBVSxDQUFDLEVBQUUsQ0FBRixFQUFLLElBQUwsQ0FBVSxZQUFWLENBQUQsR0FBMkIsQ0FBQyxFQUFFLENBQUYsRUFBSyxJQUFMLENBQVUsWUFBVixDQUF0QztBQUFBLFdBRFIsRUFFRyxRQUZILENBRVksUUFGWjtBQUdELFNBSkQ7QUFLRCxPQVpEO0FBYUQ7OztzQ0FFaUIsSyxFQUFPLE8sRUFBUzs7QUFFaEM7O0FBRUEsVUFBSSxPQUFPLFVBQVAsR0FBb0IsR0FBcEIsSUFBMkIsUUFBUSxRQUFSLENBQWlCLGlCQUFqQixDQUEvQixFQUFvRTtBQUNsRSxlQUFPLEtBQVA7QUFDRDs7QUFFRCxVQUFJLFNBQVMsRUFBRSxPQUFGLENBQVUsS0FBVixFQUFpQixVQUFDLEVBQUQ7QUFBQSxlQUFRLEVBQUUsRUFBRixFQUFNLE1BQU4sR0FBZSxHQUF2QjtBQUFBLE9BQWpCLENBQWI7QUFDQSxVQUFJLE9BQU8sSUFBUCxDQUFZLE1BQVosRUFBb0IsTUFBcEIsSUFBOEIsTUFBTSxNQUF4QyxFQUFnRDtBQUM5QyxhQUFLLElBQUksQ0FBVCxJQUFjLE1BQWQsRUFBc0I7QUFDcEIsMEJBQWdCLEVBQUUsT0FBTyxDQUFQLENBQUYsQ0FBaEI7QUFDRDtBQUNGLE9BSkQsTUFJTztBQUNMLGNBQU0sTUFBTixDQUFhLE1BQWI7QUFDRDs7QUFHRCxlQUFTLGVBQVQsQ0FBeUIsS0FBekIsRUFBZ0M7QUFDOUIsWUFBSSxNQUFNLENBQVY7QUFDQSxjQUFNLE1BQU4sQ0FBYSxNQUFiO0FBQ0EsY0FBTSxNQUFNLEVBQU4sQ0FBUyxDQUFULEVBQVksTUFBWixFQUFOO0FBQ0EsYUFBSyxJQUFJLEtBQUksQ0FBYixFQUFnQixLQUFJLE1BQU0sTUFBMUIsRUFBa0MsSUFBbEMsRUFBdUM7QUFDckMsZ0JBQU0sTUFBTSxFQUFOLENBQVMsRUFBVCxFQUFZLE1BQVosS0FBdUIsR0FBdkIsR0FBNkIsTUFBTSxFQUFOLENBQVMsRUFBVCxFQUFZLE1BQVosRUFBN0IsR0FBb0QsR0FBMUQ7QUFDRDtBQUNELGNBQU0sTUFBTixDQUFhLEdBQWI7QUFDRDtBQUNGOzs7NkNBRXdCLEssRUFBTztBQUM5QixZQUFNLElBQU4sQ0FBVyxVQUFDLEtBQUQsRUFBUSxFQUFSLEVBQWU7QUFDeEIsWUFBSSxnQkFBZ0IsRUFBRSxFQUFGLEVBQU0sSUFBTixDQUFXLHNCQUFYLENBQXBCO0FBQ0EsWUFBSSxjQUFjLFFBQWQsQ0FBdUIsVUFBdkIsQ0FBSixFQUF3QztBQUN0QyxZQUFFLEVBQUYsRUFBTSxRQUFOLENBQWUsVUFBZjtBQUNEO0FBQ0QsWUFBSSxjQUFjLFFBQWQsQ0FBdUIsVUFBdkIsQ0FBSixFQUF3QztBQUN0QyxZQUFFLEVBQUYsRUFBTSxRQUFOLENBQWUsVUFBZjtBQUNEO0FBQ0YsT0FSRDtBQVNEOzs7Ozs7O0FDM0VIOztBQUVBOztBQUNBOztJQUFZLGE7O0FBQ1o7O0lBQVksYzs7QUFDWjs7SUFBWSxNOzs7O0FBR1o7QUFDQSxPQUFPLE9BQVAsR0FBaUIsRUFBakI7QUFDQSxLQUFLLElBQUksVUFBVCxJQUF1QixNQUF2QixFQUErQjtBQUM3QixVQUFRLFVBQVIsSUFBc0IsSUFBSSxPQUFPLFVBQVAsQ0FBSixFQUF0QjtBQUNEOztBQUdELEVBQUUsWUFBTTtBQUNOO0FBQ0EsT0FBSyxJQUFJLFlBQVQsSUFBeUIsYUFBekIsRUFBd0M7QUFDdEMsS0FBQyxVQUFDLFFBQUQsRUFBYztBQUNiLFFBQUUsU0FBUyxRQUFYLEVBQXFCLElBQXJCLENBQTBCLFVBQUMsQ0FBRCxFQUFJLEdBQUosRUFBWTtBQUNwQyxZQUFJLFFBQUosQ0FBYSxHQUFiO0FBQ0QsT0FGRDtBQUdELEtBSkQsRUFJRyxjQUFjLFlBQWQsQ0FKSDtBQUtEOztBQUVELElBQUUsTUFBRixFQUFVLE9BQVYsQ0FBa0IscUJBQWxCOztBQUVBO0FBQ0EsT0FBSyxJQUFJLGFBQVQsSUFBMEIsY0FBMUIsRUFBMEM7QUFDeEMsS0FBQyxVQUFDLFNBQUQsRUFBZTtBQUNkLFVBQUksVUFBVSxRQUFkLEVBQXdCO0FBQ3RCLFVBQUUsVUFBVSxRQUFaLEVBQXNCLElBQXRCLENBQTJCLFVBQUMsQ0FBRCxFQUFJLEdBQUo7QUFBQSxpQkFBWSx1QkFBUyxZQUFNO0FBQ3BELGdCQUFJLFNBQUosQ0FBYyxHQUFkO0FBQ0QsV0FGc0MsQ0FBWjtBQUFBLFNBQTNCO0FBR0QsT0FKRCxNQUlPO0FBQ0wsWUFBSSxTQUFKO0FBQ0Q7QUFFRixLQVRELEVBU0csZUFBZSxhQUFmLENBVEg7QUFVRDtBQUNGLENBekJEOzs7Ozs7Ozs7Ozs7OztBQ2ZBOztBQUNBOzs7Ozs7OztJQUdhLGUsV0FBQSxlOzs7Ozt3QkFJSTtBQUNiLGFBQU8sRUFBRSxNQUFGLENBQVMsRUFBVCxtSEFBNkI7QUFDbEMsMkJBQW1CLGdDQURlO0FBRWxDLHlCQUFpQixLQUFLLEVBQUwsQ0FBUSxJQUFSLENBQWEscUNBQWIsRUFBb0QsTUFBcEQsR0FBNkQsS0FBSyxFQUFMLENBQVEsSUFBUixDQUFhLHFDQUFiLENBQTdELEdBQW1ILG9PQUZsRztBQUdsQywyQkFBbUIsb0JBSGU7QUFJbEMseUJBQWlCO0FBSmlCLE9BQTdCLENBQVA7QUFNRDs7O3dCQVRzQjtBQUFFLGFBQU8sZ0NBQVA7QUFBMEM7OztBQVduRSw2QkFBcUI7QUFBQTs7QUFBQTs7QUFBQSxzQ0FBTixJQUFNO0FBQU4sVUFBTTtBQUFBOztBQUFBLDZKQUNWLElBRFU7O0FBRW5CLFVBQUssS0FBTDtBQUZtQjtBQUdwQjs7Ozs0QkFFTztBQUNOLFdBQUssT0FBTCxHQUFlLEtBQUssRUFBTCxDQUFRLElBQVIsQ0FBYSxJQUFiLENBQWY7QUFDQSxXQUFLLGNBQUwsR0FBc0IsS0FBSyxPQUFMLENBQWEsT0FBYixDQUFxQiwyQkFBckIsRUFDYSxRQURiLENBQ3NCLGlDQUR0QixDQUF0Qjs7QUFHQSxXQUFLLGNBQUwsQ0FBb0IsT0FBcEIsQ0FBNEIsTUFBNUIsRUFBb0MsR0FBcEMsQ0FBd0MsYUFBeEMsRUFBdUQsR0FBdkQ7QUFDQSxXQUFLLHNCQUFMO0FBQ0EsV0FBSyxvQkFBTDtBQUNBLFdBQUssY0FBTDtBQUNEOzs7NkNBRXdCO0FBQ3ZCLFVBQUksZ0JBQWdCLEtBQUssRUFBTCxDQUFRLE9BQVIsQ0FBZ0IsTUFBaEIsRUFBd0IsSUFBeEIsRUFBcEI7QUFBQSxVQUNJLGFBQWEsS0FBSyxFQUFMLENBQVEsT0FBUixDQUFnQixpQkFBaEIsRUFBbUMsUUFBbkMsQ0FBNEMsS0FBSyxPQUFMLENBQWEsaUJBQXpELElBQThFLEtBQUssT0FBTCxDQUFhLGVBQTNGLEdBQTZHLEVBRDlIOztBQUdBO0FBQ0E7QUFDQSxVQUFJLGNBQWMsSUFBZCxDQUFtQixNQUFuQixFQUEyQixNQUEvQixFQUF1QztBQUNyQyxhQUFLLEdBQUwsR0FBVyxjQUFjLElBQWQsQ0FBbUIsTUFBbkIsQ0FBWDtBQUNELE9BRkQsTUFFTztBQUNMLGFBQUssR0FBTCxHQUFXLGNBQWMsTUFBZCxHQUF1QixJQUF2QixDQUE0QixVQUE1QixDQUFYO0FBQ0Q7QUFDRCxXQUFLLEdBQUwsQ0FBUyxRQUFULENBQWtCLCtCQUErQixVQUFqRDtBQUNEOzs7MkNBRXNCO0FBQ3JCLFdBQUssR0FBTCxDQUFTLE1BQVQsQ0FBZ0IsS0FBSyxPQUFMLENBQWEsZUFBN0I7QUFDQSxXQUFLLFlBQUwsR0FBb0IsS0FBSyxHQUFMLENBQVMsSUFBVCxDQUFjLEtBQUssT0FBTCxDQUFhLGlCQUEzQixDQUFwQjtBQUNEOzs7cUNBRWdCO0FBQUE7O0FBRWYsV0FBSyxPQUFMLENBQWEsRUFBYixDQUFnQixPQUFoQixFQUF5QixZQUFNO0FBQzdCLGVBQUssVUFBTDtBQUNELE9BRkQ7O0FBSUEsV0FBSyxZQUFMLENBQWtCLEVBQWxCLENBQXFCLE9BQXJCLEVBQThCLFlBQU07QUFDbEMsZUFBSyxZQUFMLENBQWtCLFdBQWxCLENBQThCLGFBQTlCO0FBQ0EsZUFBSyxZQUFMO0FBQ0QsT0FIRDtBQUlEOzs7aUNBRVk7QUFBQTs7QUFDWCxXQUFLLG9CQUFMLEdBQTRCLEtBQUssY0FBTCxDQUFvQixXQUFwQixDQUFnQyxJQUFoQyxDQUE1Qjs7QUFFQSxXQUFLLEdBQUwsQ0FBUyxFQUFULENBQVksbUNBQVosRUFBaUQsVUFBQyxDQUFELEVBQU87QUFDckQsWUFBSSxFQUFFLGFBQUYsQ0FBZ0IsWUFBaEIsSUFBZ0MsWUFBaEMsSUFBZ0QsRUFBRSxFQUFFLE1BQUosRUFBWSxFQUFaLENBQWUsT0FBSyxHQUFwQixDQUFwRCxFQUE4RTtBQUM3RSxpQkFBSyxHQUFMLENBQVMsR0FBVCxDQUFhLG1DQUFiO0FBQ0EsaUJBQUssR0FBTCxDQUFTLEdBQVQsQ0FBYSxZQUFiLEVBQTJCLE1BQTNCO0FBQ0E7QUFDSCxPQUxEOztBQU9BLFdBQUssR0FBTCxDQUFTLEdBQVQsQ0FBYSxZQUFiLEVBQTJCLEtBQUssR0FBTCxDQUFTLENBQVQsRUFBWSxZQUF2Qzs7QUFFQSxXQUFLLGNBQUwsQ0FBb0IsRUFBcEIsQ0FBdUIsbUNBQXZCLEVBQTRELFVBQUMsQ0FBRCxFQUFPO0FBQ2pFLFlBQUksRUFBRSxhQUFGLENBQWdCLFlBQWhCLElBQWdDLFlBQWhDLElBQWdELEVBQUUsRUFBRSxNQUFKLEVBQVksUUFBWixDQUFxQixpQ0FBckIsQ0FBcEQsRUFBNkc7QUFDM0csaUJBQUssY0FBTCxDQUFvQixHQUFwQixDQUF3QixtQ0FBeEI7QUFDQSxpQkFBSyxjQUFMLENBQW9CLFFBQXBCLENBQTZCLFVBQTdCO0FBQ0EsaUJBQUssb0JBQUwsR0FBNEIseUJBQWMsT0FBSyxZQUFuQixFQUFpQyxJQUFqQyxFQUF1QyxFQUFDLE1BQU0sVUFBUCxFQUF2QyxDQUE1QjtBQUNBLGlCQUFLLFlBQUwsQ0FBa0IsUUFBbEIsQ0FBMkIsYUFBM0I7QUFDRDtBQUNGLE9BUEQ7O0FBU0EsV0FBSyxjQUFMLENBQW9CLFFBQXBCLENBQTZCLFVBQTdCLEVBQXlDLEdBQXpDLENBQTZDLEVBQUMsY0FBYyxDQUFDLEtBQUssb0JBQU4sR0FBNkIsSUFBNUMsRUFBN0M7QUFDQSxjQUFRLFFBQVIsQ0FBaUIsUUFBakIsQ0FBMEIsSUFBMUIsRUFBZ0MsSUFBaEMsRUFBc0MsSUFBdEMsRUFBNEMsS0FBSyxHQUFqRCxFQUFzRCxDQUFDLEtBQUssb0JBQTVEO0FBQ0Q7OzttQ0FFYztBQUFBOztBQUNiLGlCQUFXO0FBQUEsZUFBTSxPQUFLLG9CQUFMLENBQTBCLE9BQTFCLEVBQU47QUFBQSxPQUFYLEVBQXNELEdBQXREO0FBQ0EsVUFBSSxpQkFBaUIsS0FBSyxHQUFMLENBQVMsTUFBVCxHQUFrQixHQUFsQixHQUF3QixFQUFFLE1BQUYsRUFBVSxTQUFWLEVBQXhCLEdBQWdELEtBQUssb0JBQXJELEdBQTRFLEdBQWpHOztBQUVBLFdBQUssY0FBTCxDQUFvQixFQUFwQixDQUF1QixtQ0FBdkIsRUFBNEQsVUFBQyxDQUFELEVBQU87QUFDakUsWUFBSSxFQUFFLGFBQUYsQ0FBZ0IsWUFBaEIsSUFBZ0MsWUFBaEMsSUFBZ0QsRUFBRSxFQUFFLE1BQUosRUFBWSxRQUFaLENBQXFCLGlDQUFyQixDQUFwRCxFQUE2RztBQUMzRyxpQkFBSyxjQUFMLENBQW9CLEdBQXBCLENBQXdCLG1DQUF4QjtBQUNBLGlCQUFLLGNBQUwsQ0FBb0IsV0FBcEIsQ0FBZ0MsbUJBQWhDO0FBQ0Q7QUFDRixPQUxEO0FBTUEsV0FBSyxHQUFMLENBQVMsR0FBVCxDQUFhLFlBQWIsRUFBMkIsS0FBSyxHQUFMLENBQVMsTUFBVCxFQUEzQjtBQUNBLGlCQUFXLFlBQU07QUFDZixlQUFLLEdBQUwsQ0FBUyxHQUFULENBQWEsWUFBYixFQUEyQixDQUEzQjtBQUNBLGVBQUssY0FBTCxDQUFvQixHQUFwQixDQUF3QixZQUF4QixFQUFzQyxDQUF0QztBQUNELE9BSEQ7O0FBS0EsY0FBUSxRQUFSLENBQWlCLGdCQUFqQixDQUFrQyxjQUFsQyxFQUFrRCxJQUFsRCxFQUF3RCxJQUF4RCxFQUE4RCxJQUE5RDtBQUNEOzs7Ozs7Ozs7Ozs7Ozs7O0FDMUdIOzs7Ozs7OztJQUVhLFcsV0FBQSxXOzs7Ozt3QkFJSTtBQUNiLGFBQU87QUFDTCxtQkFBVyxHQUROO0FBRUwsY0FBTSxLQUZEO0FBR0wsc0JBQWMsR0FIVDtBQUlMLGlDQUF5Qix1QkFKcEI7QUFLTCxnQ0FBd0IsaUJBTG5CO0FBTUwsNEJBQW9CLGVBTmY7QUFPTCxxQkFBYTtBQVBSLE9BQVA7QUFTRDs7O3dCQVpzQjtBQUFFLGFBQU8sNkJBQVA7QUFBdUM7OztBQWFoRSx5QkFBcUI7QUFBQTs7QUFBQTs7QUFBQSxzQ0FBTixJQUFNO0FBQU4sVUFBTTtBQUFBOztBQUFBLHFKQUNWLElBRFU7O0FBRW5CLFVBQUssTUFBTCxHQUFjLFFBQVEsS0FBUixDQUFjLE1BQWQsQ0FBcUIsUUFBbkM7QUFDQSxVQUFLLEtBQUw7QUFIbUI7QUFJcEI7Ozs7NEJBRU87QUFBQTs7QUFDTixXQUFLLEtBQUwsR0FBYSxLQUFLLEVBQUwsQ0FBUSxJQUFSLENBQWEsT0FBYixFQUFzQixNQUF0QixJQUFnQyxLQUFLLEVBQUwsQ0FBUSxJQUFSLENBQWEsT0FBYixFQUFzQixDQUF0QixDQUE3QztBQUNBLFVBQUksS0FBSyxLQUFULEVBQWdCO0FBQ2QsYUFBSyxZQUFMLEdBQW9CLEtBQUssRUFBTCxDQUFRLE9BQVIsQ0FBZ0IsaUJBQWhCLENBQXBCO0FBQ0EsYUFBSyxVQUFMLEdBQWtCLEtBQUssRUFBTCxDQUFRLElBQVIsQ0FBYSxLQUFLLE9BQUwsQ0FBYSxrQkFBMUIsQ0FBbEI7O0FBRUE7O0FBRUEsWUFBSSxLQUFLLE1BQVQsRUFBaUI7QUFDZixlQUFLLFVBQUwsQ0FBZ0IsS0FBaEIsQ0FBc0IsS0FBSyxPQUFMLENBQWEsV0FBbkM7QUFDQSxlQUFLLFFBQUwsR0FBZ0IsS0FBSyxFQUFMLENBQVEsSUFBUixDQUFhLFFBQWIsQ0FBaEI7QUFDRCxTQUhELE1BR087QUFDTCxlQUFLLGNBQUwsR0FBc0IsS0FBSyxFQUFMLENBQVEsUUFBUixDQUFpQixLQUFLLE9BQUwsQ0FBYSx1QkFBOUIsQ0FBdEI7QUFDQSxlQUFLLGNBQUwsR0FBc0IsS0FBSyxFQUFMLENBQVEsUUFBUixDQUFpQixLQUFLLE9BQUwsQ0FBYSxzQkFBOUIsQ0FBdEI7QUFDRDs7QUFFRDtBQUNBLFlBQUksQ0FBQyxLQUFLLE1BQU4sSUFBZ0IsS0FBSyxjQUF6QixFQUF5QztBQUN2QyxlQUFLLGFBQUwsR0FBcUIsSUFBckI7O0FBRUEsZUFBSyxjQUFMLEdBQXNCLEtBQUssRUFBTCxDQUFRLE9BQVIsQ0FBZ0IsZUFBaEIsQ0FBdEI7QUFDQSxlQUFLLGNBQUwsR0FBc0IsS0FBSyxjQUFMLENBQW9CLE1BQXBCLEdBQTZCLEtBQUssY0FBbEMsR0FBbUQsS0FBSyxFQUE5RTtBQUNBLGVBQUssY0FBTCxDQUFvQixlQUFwQixDQUFvQztBQUNsQyxvQkFBUSxJQUQwQjtBQUVsQyxvQkFBUSxHQUYwQjtBQUdsQyw4QkFBa0IsMEJBQUMsR0FBRCxFQUFNLENBQU4sRUFBWTtBQUM1QixrQkFBSSxNQUFNLEtBQVYsRUFBaUI7QUFDZixvQkFBSSxPQUFLLGFBQVQsRUFBd0I7QUFDdEIseUJBQUssVUFBTDtBQUNBLHlCQUFLLGFBQUwsR0FBcUIsS0FBckI7QUFDRDtBQUNGLGVBTEQsTUFLTztBQUNMLHVCQUFLLFdBQUw7QUFDQSx1QkFBSyxhQUFMLEdBQXFCLElBQXJCO0FBQ0Q7QUFDRjtBQWJpQyxXQUFwQztBQWVEO0FBQ0Q7QUFDQSxZQUFJLENBQUMsS0FBSyxNQUFWLEVBQWtCO0FBQ2hCLGVBQUssWUFBTCxDQUFrQixFQUFsQixDQUFxQixtQkFBckIsRUFBMEM7QUFBQSxtQkFBTSxXQUFXO0FBQUEscUJBQU0sT0FBSyxVQUFMLEVBQU47QUFBQSxhQUFYLEVBQW9DLEdBQXBDLENBQU47QUFBQSxXQUExQztBQUNBO0FBQ0EsZUFBSyxZQUFMLENBQWtCLEVBQWxCLENBQXFCLG9CQUFyQixFQUEyQztBQUFBLG1CQUFNLFdBQVc7QUFBQSxxQkFBTSxPQUFLLFdBQUwsRUFBTjtBQUFBLGFBQVgsRUFBcUMsR0FBckMsQ0FBTjtBQUFBLFdBQTNDO0FBQ0QsU0FKRCxNQUlPO0FBQ0wsZUFBSyxZQUFMLENBQWtCLEVBQWxCLENBQXFCLG9CQUFyQixFQUEyQztBQUFBLG1CQUFNLFdBQVcsWUFBTTtBQUNoRSxrQkFBSSxPQUFLLEtBQUwsQ0FBVyxTQUFmLEVBQTBCO0FBQ3hCLHVCQUFLLGFBQUw7QUFDRDtBQUNELHFCQUFLLFdBQUw7QUFDRCxhQUxnRCxFQUs5QyxHQUw4QyxDQUFOO0FBQUEsV0FBM0M7QUFNRDs7QUFFRCxhQUFLLGdCQUFMO0FBQ0Q7QUFDRjs7O2lDQUVZO0FBQUE7O0FBQ1gsVUFBSSxDQUFDLEtBQUssTUFBTixJQUFnQixLQUFLLGNBQXpCLEVBQXlDO0FBQ3ZDLFlBQUksS0FBSyxjQUFMLENBQW9CLFFBQXBCLENBQTZCLFNBQTdCLENBQUosRUFBNkM7QUFDM0MsY0FBSSxDQUFDLEtBQUssS0FBTCxDQUFXLFNBQWhCLEVBQTJCO0FBQ3pCLGdCQUFJLEtBQUssRUFBTCxDQUFRLE9BQVIsQ0FBZ0IsZUFBaEIsRUFBaUMsTUFBckMsRUFBNkM7QUFDM0Msa0JBQUksS0FBSyxFQUFMLENBQVEsT0FBUixDQUFnQixlQUFoQixFQUFpQyxNQUFyQyxFQUE2QztBQUMzQywyQkFBVztBQUFBLHlCQUFNLE9BQUssS0FBTCxFQUFOO0FBQUEsaUJBQVgsRUFBK0IsRUFBL0I7QUFDRDtBQUNGLGFBSkQsTUFJTztBQUNMLHlCQUFXO0FBQUEsdUJBQU0sT0FBSyxLQUFMLEVBQU47QUFBQSxlQUFYLEVBQStCLEVBQS9CO0FBQ0Q7QUFFRjtBQUNGO0FBQ0YsT0FiRCxNQWFPO0FBQ0wsWUFBSSxDQUFDLEtBQUssS0FBTCxDQUFXLFNBQWhCLEVBQTJCO0FBQ3pCLGVBQUssS0FBTDtBQUNEO0FBQ0Y7QUFDRjs7OzRCQUVPO0FBQ04sV0FBSyxlQUFMO0FBQ0EsV0FBSyxhQUFMO0FBQ0EsV0FBSyxLQUFMLENBQVcsSUFBWDtBQUNEOzs7a0NBRWE7QUFDWixVQUFJLEtBQUssS0FBTCxDQUFXLFNBQWYsRUFBMEI7QUFDeEIsYUFBSyxLQUFMLENBQVcsS0FBWDtBQUNEO0FBQ0Y7Ozt1Q0FFa0I7QUFBQTs7QUFFakIsVUFBSSxDQUFDLEtBQUssTUFBVixFQUFrQjtBQUNoQixVQUFFLEtBQUssS0FBUCxFQUNHLEVBREgsQ0FDTSxnQkFETixFQUN3QixZQUFNO0FBQzFCLGlCQUFLLEtBQUwsQ0FBVyxXQUFYLEdBQXlCLElBQXpCO0FBQ0QsU0FISCxFQUlHLEVBSkgsQ0FJTSxPQUpOLEVBSWUsWUFBTTtBQUNqQixjQUFJLENBQUMsT0FBSyxjQUFWLEVBQTBCO0FBQ3hCLG1CQUFLLGVBQUw7QUFDRCxXQUZELE1BRU87QUFDTCxtQkFBSyxVQUFMO0FBQ0Q7QUFDRixTQVZIO0FBV0QsT0FaRCxNQVlPO0FBQ0wsYUFBSyxhQUFMO0FBQ0EsVUFBRSxLQUFLLEtBQVAsRUFBYyxFQUFkLENBQWlCLE9BQWpCLEVBQTBCO0FBQUEsaUJBQU0sT0FBSyxlQUFMLEVBQU47QUFBQSxTQUExQjtBQUNEOztBQUVELFdBQUssVUFBTCxJQUFtQixLQUFLLFVBQUwsQ0FBZ0IsRUFBaEIsQ0FBbUIsT0FBbkIsRUFBNEIsVUFBQyxDQUFELEVBQU87QUFDcEQsWUFBSSxDQUFDLE9BQUssS0FBTCxDQUFXLFNBQWhCLEVBQTJCO0FBQ3pCLGlCQUFLLFVBQUw7QUFDRDtBQUNGLE9BSmtCLENBQW5COztBQU1BLFdBQUssUUFBTCxJQUFpQixLQUFLLFFBQUwsQ0FBYyxFQUFkLENBQWlCLE9BQWpCLEVBQTBCLFVBQUMsQ0FBRCxFQUFPO0FBQ2hELGVBQUssVUFBTDtBQUNELE9BRmdCLENBQWpCO0FBR0Q7OztvQ0FFZTtBQUNkLFdBQUssUUFBTCxJQUFpQixLQUFLLFFBQUwsQ0FBYyxRQUFkLENBQXVCLFNBQXZCLENBQWpCO0FBQ0Q7OztvQ0FFZTtBQUNkLFdBQUssUUFBTCxJQUFpQixLQUFLLFFBQUwsQ0FBYyxXQUFkLENBQTBCLFNBQTFCLENBQWpCO0FBQ0Q7OztzQ0FFaUI7QUFDaEIsV0FBSyxVQUFMLElBQW1CLEtBQUssVUFBTCxDQUFnQixRQUFoQixDQUF5QixTQUF6QixDQUFuQjtBQUNEOzs7c0NBRWlCO0FBQ2hCLFdBQUssVUFBTCxJQUFtQixLQUFLLFVBQUwsQ0FBZ0IsV0FBaEIsQ0FBNEIsU0FBNUIsQ0FBbkI7QUFDRDs7Ozs7Ozs7Ozs7Ozs7OztBQzVKSDs7Ozs7Ozs7SUFFYSxZLFdBQUEsWTs7Ozs7d0JBTUk7QUFDYixhQUFPO0FBQ0wsd0JBQWdCLElBRFg7QUFFTCxrQ0FBMEI7QUFGckIsT0FBUDtBQUlEOzs7d0JBVHFCO0FBQ3BCLGFBQU8sZ0NBQVA7QUFDRDs7O0FBU0QsMEJBQXFCO0FBQUE7O0FBQUE7O0FBQUEsc0NBQU4sSUFBTTtBQUFOLFVBQU07QUFBQTs7QUFBQSx1SkFDVixJQURVOztBQUVuQixVQUFLLEtBQUw7QUFGbUI7QUFHcEI7Ozs7NEJBRU87QUFDTixXQUFLLFFBQUwsR0FBZ0IsS0FBSyxFQUFMLENBQVEsT0FBUixDQUFnQixVQUFoQixDQUFoQjtBQUNBLFdBQUssUUFBTCxHQUFnQixLQUFLLEVBQUwsQ0FBUSxPQUFSLENBQWdCLGVBQWhCLENBQWhCO0FBQ0EsVUFBSSxLQUFLLFFBQUwsQ0FBYyxNQUFsQixFQUEwQjtBQUN4QixhQUFLLFFBQUwsQ0FBYyxRQUFkLENBQXVCLEtBQUssT0FBTCxDQUFhLHdCQUFwQztBQUNBLGFBQUssZUFBTDtBQUNEOztBQUVELFdBQUssY0FBTDtBQUNEOzs7c0NBRWlCO0FBQ2hCLFVBQUksS0FBSyxRQUFMLENBQWMsUUFBZCxDQUF1QixpQkFBdkIsQ0FBSixFQUE4QztBQUM1QyxhQUFLLFFBQUwsQ0FBYyxRQUFkLENBQXVCLGtCQUF2QjtBQUNEO0FBQ0QsVUFBSSxLQUFLLFFBQUwsQ0FBYyxRQUFkLENBQXVCLGlCQUF2QixDQUFKLEVBQThDO0FBQzVDLGFBQUssUUFBTCxDQUFjLFFBQWQsQ0FBdUIsa0JBQXZCO0FBQ0Q7QUFDRCxVQUFJLEtBQUssUUFBTCxDQUFjLFFBQWQsQ0FBdUIsZ0JBQXZCLENBQUosRUFBNkM7QUFDM0MsYUFBSyxRQUFMLENBQWMsUUFBZCxDQUF1QixpQkFBdkI7QUFDRDtBQUNELFVBQUksS0FBSyxRQUFMLENBQWMsUUFBZCxDQUF1QixnQkFBdkIsQ0FBSixFQUE2QztBQUMzQyxhQUFLLFFBQUwsQ0FBYyxRQUFkLENBQXVCLGlCQUF2QjtBQUNEO0FBQ0Y7OztxQ0FFZ0I7QUFBQTs7QUFDZixXQUFLLEVBQUwsQ0FBUSxFQUFSLENBQVcsT0FBWCxFQUFvQixVQUFDLENBQUQsRUFBTztBQUN6QixnQkFBUSxRQUFSLENBQWlCLGlCQUFqQixDQUFtQyxFQUFFLEVBQUUsYUFBSixFQUFtQixJQUFuQixDQUF3QixNQUF4QixDQUFuQyxFQUFvRSxJQUFwRSxFQUEwRSxPQUFLLE9BQUwsQ0FBYSxjQUF2RjtBQUNBLFVBQUUsY0FBRjtBQUNELE9BSEQ7QUFJRDs7Ozs7Ozs7Ozs7Ozs7OztBQ25ESDs7QUFDQTs7Ozs7Ozs7SUFFYSxXLFdBQUEsVzs7Ozs7d0JBSUk7QUFDYixhQUFPO0FBQ0wsbUJBQVcsR0FETjtBQUVMLHdCQUFnQjtBQUZYLE9BQVA7QUFJRDs7O3dCQVBzQjtBQUFFLGFBQU8sK0JBQVA7QUFBeUM7OztBQVFsRSx5QkFBcUI7QUFBQTs7QUFBQTs7QUFBQSxzQ0FBTixJQUFNO0FBQU4sVUFBTTtBQUFBOztBQUFBLHFKQUNWLElBRFU7O0FBRW5CLFVBQUssS0FBTDtBQUZtQjtBQUdwQjs7Ozs0QkFFTztBQUNOLCtCQUFjLEtBQUssRUFBbkIsRUFBdUIsSUFBdkIsRUFBNkIsRUFBQyxNQUFNLFFBQVAsRUFBaUIsV0FBVyxDQUFDLEtBQUssRUFBTCxDQUFRLElBQVIsQ0FBYSxrQkFBYixDQUFELElBQXFDLEtBQUssT0FBTCxDQUFhLFNBQTlFLEVBQTdCO0FBQ0EsV0FBSyxjQUFMO0FBQ0Q7OztxQ0FFZ0I7QUFBQTs7QUFDZixXQUFLLEVBQUwsQ0FBUSxFQUFSLENBQVcsT0FBWCxFQUFvQjtBQUFBLGVBQU0sUUFBUSxRQUFSLENBQWlCLFVBQWpCLENBQTRCLENBQTVCLEVBQStCLENBQUMsT0FBSyxFQUFMLENBQVEsSUFBUixDQUFhLHNCQUFiLENBQUQsSUFBeUMsT0FBSyxPQUFMLENBQWEsY0FBckYsQ0FBTjtBQUFBLE9BQXBCO0FBQ0Q7Ozs7Ozs7Ozs7OztRQ3pCYSxRLEdBQUEsUTtBQUFULFNBQVMsUUFBVCxDQUFrQixFQUFsQixFQUFzQjtBQUMzQixTQUFPLFVBQVAsQ0FBa0IsRUFBbEIsRUFBc0IsQ0FBdEI7QUFDRDs7O0FDRkQ7QUFDQTs7Ozs7Ozs7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7Ozs7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7Ozs7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7Ozs7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7Ozs7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7Ozs7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7Ozs7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7Ozs7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7Ozs7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7Ozs7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7Ozs7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7Ozs7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7Ozs7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7Ozs7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7Ozs7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7Ozs7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7Ozs7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7Ozs7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7Ozs7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7Ozs7Ozs7Ozs7Ozs7O0FDcEJBOztBQUNBOzs7Ozs7OztJQUdhLFcsV0FBQSxXOzs7Ozt3QkFJSTtBQUNiLGFBQU8sRUFBRSxNQUFGLENBQVMsRUFBVCwyR0FBNkI7QUFDbEMseUJBQWlCLFVBRGlCO0FBRWxDLHlCQUFpQixvQ0FGaUI7QUFHbEMsK0JBQXVCLGtDQUhXO0FBSWxDLHFCQUFhO0FBSnFCLE9BQTdCLENBQVA7QUFNRDs7O3dCQVRzQjtBQUFFLGFBQU8sa0JBQVA7QUFBNEI7OztBQVdyRCx5QkFBcUI7QUFBQTs7QUFBQTs7QUFBQSxzQ0FBTixJQUFNO0FBQU4sVUFBTTtBQUFBOztBQUFBLHFKQUNWLElBRFU7O0FBRW5CLFVBQUssS0FBTDtBQUZtQjtBQUdwQjs7Ozs0QkFFTztBQUFBOztBQUVOLFdBQUssUUFBTCxHQUFnQixLQUFLLEVBQUwsQ0FBUSxPQUFSLENBQWdCLEtBQUssT0FBTCxDQUFhLGVBQTdCLENBQWhCOztBQUVBLDZCQUFTLFlBQU07QUFDYixlQUFLLGdCQUFMO0FBQ0EsZUFBSyxRQUFMLENBQWMsUUFBZCxDQUF1QixPQUFLLE9BQUwsQ0FBYSxxQkFBcEM7QUFDRCxPQUhEO0FBSUQ7Ozt1Q0FFa0I7QUFDZixVQUFJLFdBQVcsS0FBSyxlQUFMLEVBQWY7QUFBQSxVQUNJLFVBQVUsS0FBSyxFQUFMLENBQVEsSUFBUixDQUFhLEtBQUssT0FBTCxDQUFhLGVBQTFCLENBRGQ7O0FBR0EsY0FBUSxHQUFSLENBQVksV0FBWixFQUF5QixRQUF6QjtBQUNIOzs7c0NBRWlCO0FBQ2QsVUFBSSxTQUFTLHFCQUFxQixJQUFyQixDQUEwQixLQUFLLFFBQUwsQ0FBYyxJQUFkLENBQW1CLE9BQW5CLENBQTFCLENBQWI7QUFDQSxVQUFJLE1BQUosRUFBWTtBQUNSLGVBQU8sQ0FBQyxPQUFPLENBQVAsQ0FBUjtBQUNIO0FBQ0QsYUFBTyxLQUFLLE9BQUwsQ0FBYSxXQUFwQjtBQUNIOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUM3Q0g7Ozs7Ozs7O0lBR2EsVyxXQUFBLFc7Ozs7O3dCQUlJO0FBQ2IsYUFBTyxFQUFFLE1BQUYsQ0FBUyxFQUFULDJHQUE2QixFQUE3QixDQUFQO0FBQ0Q7Ozt3QkFKc0I7QUFBRSxhQUFPLGtCQUFQO0FBQTRCOzs7QUFNckQseUJBQXFCO0FBQUE7O0FBQUE7O0FBQUEsc0NBQU4sSUFBTTtBQUFOLFVBQU07QUFBQTs7QUFBQSxxSkFDVixJQURVOztBQUVuQixVQUFLLEtBQUw7QUFGbUI7QUFHcEI7Ozs7NEJBRU87QUFDTixVQUFJLEtBQUssRUFBTCxDQUFRLElBQVIsQ0FBYSxVQUFiLEVBQXlCLE1BQXpCLEdBQWtDLENBQXRDLEVBQXlDO0FBQ3ZDLGFBQUssRUFBTCxDQUFRLElBQVIsQ0FBYSxVQUFiLEVBQXlCLElBQXpCLENBQThCLGlDQUE5QjtBQUNEO0FBQ0Y7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3BCSDs7Ozs7Ozs7SUFHYSxXLFdBQUEsVzs7Ozs7d0JBSUk7QUFDYixhQUFPLEVBQUUsTUFBRixDQUFTLEVBQVQsMkdBQTZCO0FBQ2xDLHFCQUFhLE1BRHFCO0FBRWxDLDBCQUFrQixLQUZnQjtBQUdsQyx1QkFBZSxLQUhtQjtBQUlsQyw2QkFBcUIsZUFKYTtBQUtsQyxnQ0FBd0Isa0JBTFU7O0FBT2xDLHFCQUFhLFFBUHFCO0FBUWxDLHFCQUFhLGtCQVJxQjtBQVNsQyxzQkFBYyxVQVRvQjtBQVVsQyw0QkFBb0I7QUFWYyxPQUE3QixDQUFQO0FBWUQ7Ozt3QkFmc0I7QUFBRSxhQUFPLGtCQUFQO0FBQTRCOzs7QUFpQnJELHlCQUFxQjtBQUFBOztBQUFBOztBQUFBLHNDQUFOLElBQU07QUFBTixVQUFNO0FBQUE7O0FBQUEscUpBQ1YsSUFEVTs7QUFFbkIsVUFBSyxLQUFMO0FBRm1CO0FBR3BCOzs7OzRCQUVPO0FBQ04sVUFBSSxRQUFRLEtBQUssRUFBTCxDQUFRLElBQVIsQ0FBYSxLQUFLLE9BQUwsQ0FBYSxhQUExQixDQUFaO0FBQ0EsVUFBSSxpQkFBaUIsS0FBSyxFQUFMLENBQVEsSUFBUixDQUFhLEtBQUssT0FBTCxDQUFhLHNCQUExQixDQUFyQjtBQUNBLFVBQUksb0JBQW9CLGVBQWUsSUFBZixDQUFvQixLQUFLLE9BQUwsQ0FBYSxnQkFBakMsRUFBbUQsTUFBM0U7O0FBRUEsVUFBSSxNQUFNLE1BQVYsRUFBa0I7QUFDaEIsYUFBSyxFQUFMLENBQVEsSUFBUixDQUFhLEtBQUssT0FBTCxDQUFhLG1CQUExQixFQUErQyxHQUEvQyxDQUFtRDtBQUNqRCx3Q0FBNEIsTUFBTSxJQUFOLENBQVcsS0FBWCxDQUE1QjtBQURpRCxTQUFuRDs7QUFJQTtBQUNBLFlBQUksaUJBQUosRUFBdUI7QUFDckIseUJBQWUsSUFBZixDQUFvQiwwQkFBcEIsRUFBZ0QsSUFBaEQsQ0FBcUQsaUNBQXJEO0FBQ0EsY0FBSSxjQUFjLGVBQ2IsS0FEYSxDQUNQLElBRE8sRUFDRCxJQURDLEVBRWIsUUFGYSxDQUVELEtBQUssT0FBTCxDQUFhLFdBRlosU0FFMkIsS0FBSyxPQUFMLENBQWEsV0FGeEMsQ0FBbEI7O0FBSUEsc0JBQVksSUFBWixDQUFpQixnQkFBakIsRUFBbUMsTUFBbkM7QUFDQSxlQUFLLEVBQUwsQ0FBUSxNQUFSLENBQWUsV0FBZjtBQUNEO0FBQ0YsT0FmRCxNQWVPO0FBQ0wsYUFBSyxFQUFMLENBQVEsUUFBUixDQUFpQixLQUFLLE9BQUwsQ0FBYSxZQUE5QjtBQUNEO0FBQ0Q7QUFDQSxVQUFJLGdCQUFnQixLQUFLLEVBQUwsQ0FBUSxJQUFSLENBQWEsT0FBYixFQUFzQixLQUF0QixDQUE0QiwyQ0FBNUIsQ0FBcEI7QUFDQSxVQUFJLENBQUMsYUFBRCxJQUFtQixpQkFBaUIsY0FBYyxNQUFkLEdBQXVCLENBQS9ELEVBQW1FO0FBQ2pFLGFBQUssRUFBTCxDQUFRLE9BQVIsQ0FBZ0IsS0FBSyxPQUFMLENBQWEsV0FBN0IsRUFBMEMsUUFBMUMsQ0FBbUQsS0FBSyxPQUFMLENBQWEsa0JBQWhFO0FBQ0Q7QUFDRjs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDdkRIOzs7Ozs7OztJQUVhLFcsV0FBQSxXOzs7Ozt3QkFJSTtBQUNiLGFBQU8sRUFBRSxNQUFGLENBQVMsRUFBVCwyR0FBNkI7QUFDbEMsd0JBQWdCLGtCQURrQjtBQUVsQyxzQkFBYyxnQkFGb0I7QUFHbEMsK0JBQXVCLHFCQUhXO0FBSWxDLDZCQUFxQixvQkFKYTtBQUtsQyx5QkFBaUIsa0JBTGlCO0FBTWxDLGlDQUF5QjtBQU5TLE9BQTdCLENBQVA7QUFRRDs7O3dCQVhzQjtBQUFFLGFBQU8sa0JBQVA7QUFBNEI7OztBQWFyRCx5QkFBcUI7QUFBQTs7QUFBQTs7QUFBQSxzQ0FBTixJQUFNO0FBQU4sVUFBTTtBQUFBOztBQUFBLHFKQUNWLElBRFU7O0FBRW5CLFVBQUssS0FBTDtBQUZtQjtBQUdwQjs7Ozs0QkFFTztBQUNOLFdBQUssZ0JBQUw7QUFDQSxXQUFLLGNBQUw7QUFDQSxXQUFLLFlBQUwsR0FBb0IsSUFBcEI7QUFDRDs7O3VDQUVrQjtBQUNqQixVQUFNLGNBQWMsRUFBRSxTQUFGLEVBQWEsRUFBQyxPQUFPLFdBQVIsRUFBYixDQUFwQjtBQUNBLFdBQUssRUFBTCxDQUFRLElBQVIsQ0FBYSxLQUFLLE9BQUwsQ0FBYSxlQUExQixFQUEyQyxNQUEzQyxDQUFrRCxXQUFsRDtBQUNBLFVBQUksS0FBSyxFQUFMLENBQVEsUUFBUixDQUFpQixLQUFLLE9BQUwsQ0FBYSxxQkFBOUIsQ0FBSixFQUEwRDtBQUN4RCxhQUFLLEVBQUwsQ0FBUSxNQUFSLEdBQWlCLFFBQWpCLENBQTBCLGFBQTFCO0FBQ0Q7QUFDRCxVQUFJLEtBQUssRUFBTCxDQUFRLFFBQVIsQ0FBaUIsS0FBSyxPQUFMLENBQWEsbUJBQTlCLENBQUosRUFBd0Q7QUFDdEQsYUFBSyxFQUFMLENBQVEsTUFBUixHQUFpQixRQUFqQixDQUEwQixZQUExQjtBQUNEO0FBQ0Y7OztxQ0FFZ0I7QUFDZixVQUFJLE1BQU0sRUFBRSxRQUFGLENBQVcsS0FBSyxPQUFMLENBQWEsU0FBeEIsQ0FBVjtBQUFBLFVBQ0ksUUFBUSxLQUFLLEVBQUwsQ0FBUSxJQUFSLENBQWEsSUFBYixFQUFtQixLQUFuQixHQUEyQixJQUEzQixFQURaO0FBQUEsVUFFSSxNQUFNLEtBQUssRUFBTCxDQUFRLElBQVIsQ0FBYSxjQUFiLEVBQTZCLEtBQTdCLEdBQXFDLElBQXJDLEVBRlY7QUFBQSxVQUdJLGNBQWMsS0FBSyxFQUFMLENBQVEsSUFBUixDQUFhLGtCQUFiLENBSGxCO0FBQUEsVUFJSSxjQUFjLHdEQUF3RCxZQUFZLElBQVosRUFBeEQsR0FBNkUsTUFKL0Y7O0FBTUEsV0FBSyxFQUFMLENBQVEsTUFBUixDQUFlLElBQUksRUFBQyxZQUFELEVBQVEsUUFBUixFQUFhLHdCQUFiLEVBQUosQ0FBZjtBQUNEOzs7cUNBRWdCO0FBQUE7O0FBQ2YsV0FBSyxFQUFMLENBQVEsSUFBUixDQUFhLEtBQUssT0FBTCxDQUFhLHVCQUExQixFQUFtRCxFQUFuRCxDQUFzRCxPQUF0RCxFQUErRCxVQUFDLENBQUQsRUFBTztBQUNwRSxVQUFFLE9BQUssT0FBTCxDQUFhLGNBQWYsRUFBK0IsR0FBL0IsQ0FBbUMsT0FBSyxFQUF4QyxFQUE0QyxXQUE1QyxDQUF3RCxRQUF4RDtBQUNBLFlBQUksT0FBSyxFQUFMLENBQVEsUUFBUixDQUFpQixRQUFqQixDQUFKLEVBQWdDO0FBQzlCLGlCQUFLLEVBQUwsQ0FBUSxXQUFSLENBQW9CLFFBQXBCO0FBQ0QsU0FGRCxNQUVPO0FBQ0wsaUJBQUssRUFBTCxDQUFRLFFBQVIsQ0FBaUIsUUFBakI7QUFDQSxpQkFBSyxxQkFBTDtBQUNEO0FBQ0YsT0FSRDtBQVNBLFdBQUssRUFBTCxDQUFRLEVBQVIsQ0FBVyxVQUFYLEVBQXVCLFlBQU07QUFDM0IsZUFBSyxZQUFMLEdBQW9CLFdBQVcsWUFBTTtBQUNuQyxpQkFBSyxFQUFMLENBQVEsV0FBUixDQUFvQixRQUFwQjtBQUNELFNBRm1CLEVBRWpCLEdBRmlCLENBQXBCO0FBR0QsT0FKRDtBQUtBLFdBQUssRUFBTCxDQUFRLElBQVIsQ0FBYSxLQUFLLE9BQUwsQ0FBYyxZQUEzQixFQUF5QyxFQUF6QyxDQUE0QyxPQUE1QyxFQUFxRCxHQUFyRCxFQUEwRCxZQUFNO0FBQzlELGVBQUssRUFBTCxDQUFRLE9BQVIsQ0FBZ0IsVUFBaEI7QUFDRCxPQUZEO0FBR0Q7Ozs0Q0FDdUI7QUFBQTs7QUFDdEIsUUFBRSxNQUFGLEVBQVUsR0FBVixDQUFjLFVBQWQsRUFBMEIsRUFBMUIsQ0FBNkIsVUFBN0IsRUFBeUMsVUFBQyxDQUFELEVBQU87QUFDOUMsWUFBSSxDQUFDLEVBQUUsRUFBRSxNQUFKLEVBQVksT0FBWixDQUFvQixPQUFLLE9BQUwsQ0FBYSxjQUFqQyxFQUFpRCxNQUF0RCxFQUE4RDtBQUM1RCxZQUFFLE9BQUssT0FBTCxDQUFhLGNBQWYsRUFBK0IsV0FBL0IsQ0FBMkMsUUFBM0M7QUFDQSxZQUFFLE1BQUYsRUFBVSxHQUFWLENBQWMsVUFBZDtBQUNBLHVCQUFhLE9BQUssWUFBbEI7QUFDRDtBQUNGLE9BTkQ7QUFPRDs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDNUVIOzs7Ozs7OztJQUVhLFcsV0FBQSxXOzs7Ozt3QkFJSTtBQUNiLGFBQU8sRUFBRSxNQUFGLENBQVMsRUFBVCwyR0FBNkI7QUFDbEMsbUNBQTJCO0FBRE8sT0FBN0IsQ0FBUDtBQUdEOzs7d0JBTnNCO0FBQUUsYUFBTyxrQkFBUDtBQUE0Qjs7O0FBUXJELHlCQUFxQjtBQUFBOztBQUFBOztBQUFBLHNDQUFOLElBQU07QUFBTixVQUFNO0FBQUE7O0FBQUEscUpBQ1YsSUFEVTs7QUFFbkIsVUFBSyxLQUFMO0FBRm1CO0FBR3BCOzs7OzRCQUVPO0FBQ04sVUFBSSxLQUFLLEVBQUwsQ0FBUSxRQUFSLENBQWlCLEtBQUssT0FBTCxDQUFhLHlCQUE5QixDQUFKLEVBQThEO0FBQzVELGFBQUsscUJBQUw7QUFDRCxPQUZELE1BRU87QUFDTCxhQUFLLFVBQUw7QUFDRDtBQUNGOzs7NENBRXVCO0FBQ3RCLFVBQUksTUFBTSxLQUFLLEVBQUwsQ0FBUSxJQUFSLENBQWEsS0FBYixDQUFWO0FBQUEsVUFDRSxTQUFTLENBQUMsS0FBSyxFQUFMLENBQVEsSUFBUixDQUFhLEtBQWIsRUFBb0IsSUFBcEIsQ0FBeUIsUUFBekIsQ0FEWjtBQUFBLFVBRUUsYUFBYSxTQUFTLElBQUksSUFBSixDQUFTLEtBQVQsQ0FBVCxHQUEyQixHQUYxQztBQUdBLFVBQUksTUFBSixHQUFhLEdBQWIsQ0FBaUI7QUFDZiw0QkFBb0IsVUFETDtBQUVmLGdCQUFRO0FBRk8sT0FBakI7O0FBS0EsV0FBSyxlQUFMO0FBQ0Q7OztpQ0FFWTtBQUNYLFdBQUssRUFBTCxDQUFRLElBQVIsQ0FBYSxLQUFiLEVBQW9CLElBQXBCLENBQXlCLFVBQUMsQ0FBRCxFQUFJLEVBQUo7QUFBQSxlQUFXLEVBQUUsRUFBRixFQUFNLElBQU4sQ0FBVyxTQUFYLENBQVg7QUFBQSxPQUF6QjtBQUNEOzs7c0NBRWlCO0FBQ2hCLFdBQUssRUFBTCxDQUFRLFFBQVIsQ0FBaUIsYUFBakI7QUFDRDs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDM0NIOzs7Ozs7OztJQUdhLFcsV0FBQSxXOzs7Ozt3QkFJSTtBQUNiLGFBQU8sRUFBRSxNQUFGLENBQVMsRUFBVCwyR0FBNkI7QUFDbEMsZ0NBQXdCO0FBRFUsT0FBN0IsQ0FBUDtBQUdEOzs7d0JBTnFCO0FBQUUsYUFBTyxrQkFBUDtBQUE0Qjs7O0FBUXBELHlCQUFxQjtBQUFBOztBQUFBOztBQUFBLHNDQUFOLElBQU07QUFBTixVQUFNO0FBQUE7O0FBQUEscUpBQ1YsSUFEVTs7QUFFbkIsVUFBSyxLQUFMO0FBRm1CO0FBR3BCOzs7OzBCQUVLLEUsRUFBSTtBQUNSLFVBQUksS0FBSyxFQUFMLENBQVEsUUFBUixDQUFpQixLQUFLLE9BQUwsQ0FBYSxzQkFBOUIsQ0FBSixFQUEyRDtBQUN6RCxhQUFLLEVBQUwsQ0FBUSxJQUFSLENBQWEsK0JBQWIsRUFBOEMsT0FBOUMsQ0FBc0Qsb0NBQXREO0FBQ0Q7QUFDRjs7Ozs7Ozs7Ozs7Ozs7OztBQ3RCSDs7Ozs7Ozs7SUFHYSxXLFdBQUEsVzs7Ozs7d0JBRVk7QUFBRSxhQUFPLGtCQUFQO0FBQTRCOzs7QUFFckQseUJBQXFCO0FBQUE7O0FBQUE7O0FBQUEsc0NBQU4sSUFBTTtBQUFOLFVBQU07QUFBQTs7QUFBQSxxSkFDVixJQURVOztBQUVuQixVQUFLLEtBQUw7QUFGbUI7QUFHcEI7Ozs7NEJBRU87QUFDTixXQUFLLHdCQUFMO0FBQ0EsV0FBSyxXQUFMO0FBQ0EsV0FBSyxjQUFMO0FBQ0Q7OzsrQ0FFMEI7QUFDekIsVUFBSSxFQUFFLElBQUYsQ0FBTyxLQUFLLEVBQUwsQ0FBUSxJQUFSLENBQWEsY0FBYixFQUE2QixJQUE3QixFQUFQLENBQUosRUFBaUQ7QUFDL0MsYUFBSyxFQUFMLENBQVEsSUFBUixDQUFhLGNBQWIsRUFBNkIsSUFBN0IsQ0FBa0Msb0NBQWxDO0FBQ0EsYUFBSyxFQUFMLENBQVEsUUFBUixDQUFpQixXQUFqQjtBQUNELE9BSEQsTUFHTztBQUNMLFlBQUksQ0FBQyxLQUFLLEVBQUwsQ0FBUSxJQUFSLENBQWEsVUFBYixFQUF5QixNQUE5QixFQUFzQztBQUNwQyxlQUFLLEVBQUwsQ0FBUSxNQUFSLEdBQWlCLFFBQWpCLENBQTBCLFFBQTFCO0FBQ0Q7QUFDRjtBQUNGOzs7a0NBRWE7QUFDWixVQUFJLE1BQU0sS0FBSyxFQUFMLENBQVEsSUFBUixDQUFhLEtBQWIsRUFBb0IsSUFBcEIsQ0FBeUIsS0FBekIsQ0FBVjtBQUFBLFVBQ0ksYUFBYSxLQUFLLEVBQUwsQ0FBUSxJQUFSLENBQWEsY0FBYixDQURqQjs7QUFHQSxpQkFBVyxHQUFYLENBQWUsa0JBQWYsRUFBbUMsU0FBUyxHQUFULEdBQWUsR0FBbEQ7QUFDQSxXQUFLLEVBQUwsQ0FBUSxRQUFSLENBQWlCLGFBQWpCO0FBQ0Q7OztxQ0FFZ0I7QUFBQTs7QUFDZixVQUFNLFNBQVMsS0FBSyxFQUFMLENBQVEsT0FBUixDQUFnQix5QkFBaEIsQ0FBZjtBQUNBLFdBQUssRUFBTCxDQUFRLElBQVIsQ0FBYSwwQkFBYixFQUF5QyxFQUF6QyxDQUE0QyxPQUE1QyxFQUFxRCxZQUFNO0FBQ3pELGVBQU8sSUFBUCxDQUFZLGtCQUFaLEVBQWdDLEdBQWhDLENBQW9DLE9BQUssRUFBekMsRUFBNkMsSUFBN0MsQ0FBa0QsVUFBQyxLQUFELEVBQVEsRUFBUjtBQUFBLGlCQUFlLE9BQUssZ0JBQUwsQ0FBc0IsRUFBRSxFQUFGLENBQXRCLENBQWY7QUFBQSxTQUFsRDtBQUNBLFlBQUksT0FBSyxFQUFMLENBQVEsUUFBUixDQUFpQixRQUFqQixDQUFKLEVBQWdDO0FBQzlCLGlCQUFLLGdCQUFMLENBQXNCLE9BQUssRUFBM0I7QUFDRCxTQUZELE1BRU87QUFDTCxpQkFBSyxnQkFBTCxDQUFzQixPQUFLLEVBQTNCO0FBQ0Q7QUFDRixPQVBEO0FBUUQ7OztxQ0FFZ0IsRyxFQUFLO0FBQ3BCLFVBQUksSUFBSSxRQUFKLENBQWEsUUFBYixDQUFKLEVBQTRCO0FBQzFCLFlBQUksSUFBSixDQUFTLGNBQVQsRUFBeUIsSUFBekIsR0FBZ0MsVUFBaEMsR0FBNkMsT0FBN0MsQ0FBcUQsRUFBQyxTQUFTLENBQVYsRUFBckQsRUFBbUUsR0FBbkU7QUFDQSxZQUFJLFdBQUosQ0FBZ0IsUUFBaEIsRUFBMEIsSUFBMUIsQ0FBK0Isc0JBQS9CLEVBQXVELE9BQXZELENBQStELEdBQS9EO0FBQ0EsYUFBSyxjQUFMLENBQW9CLEdBQXBCO0FBQ0Q7QUFDRjs7O3FDQUVnQixHLEVBQUs7QUFDcEIsVUFBSSxRQUFKLENBQWEsUUFBYjtBQUNBLFVBQUksSUFBSixDQUFTLHNCQUFULEVBQWlDLFNBQWpDLENBQTJDLEdBQTNDLEVBQWdELFlBQU07QUFDcEQsWUFBSSxJQUFKLENBQVMsY0FBVCxFQUF5QixJQUF6QixHQUFnQyxVQUFoQyxHQUE2QyxPQUE3QyxDQUFxRCxFQUFDLFdBQVcsQ0FBWixFQUFyRDtBQUNELE9BRkQ7QUFHQSxXQUFLLGVBQUwsQ0FBcUIsR0FBckI7QUFDRDs7O21DQUVjLEcsRUFBSztBQUNsQixpQkFBVztBQUFBLGVBQU0sSUFBSSxJQUFKLENBQVMsaUJBQVQsRUFBNEIsVUFBNUIsQ0FBdUMsc0JBQXZDLENBQU47QUFBQSxPQUFYO0FBQ0Q7OztvQ0FFZSxHLEVBQUs7QUFDbkIsaUJBQVc7QUFBQSxlQUFNLElBQUksSUFBSixDQUFTLGlCQUFULEVBQTRCLElBQTVCLENBQWlDLHNCQUFqQyxFQUF5RCxVQUF6RCxDQUFOO0FBQUEsT0FBWDtBQUNEOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUN2RUg7Ozs7Ozs7O0lBR2EsVyxXQUFBLFc7Ozs7O3dCQUlJO0FBQ2IsYUFBTyxFQUFFLE1BQUYsQ0FBUyxFQUFULDJHQUE2QjtBQUNsQyx5QkFBaUIsVUFEaUI7QUFFbEMsNkJBQXFCLGVBRmE7QUFHbEMsMEJBQWtCO0FBSGdCLE9BQTdCLENBQVA7QUFLRDs7O3dCQVJzQjtBQUFFLGFBQU8sa0JBQVA7QUFBNEI7OztBQVVyRCx5QkFBcUI7QUFBQTs7QUFBQTs7QUFBQSxzQ0FBTixJQUFNO0FBQU4sVUFBTTtBQUFBOztBQUFBLHFKQUNWLElBRFU7O0FBRW5CLFVBQUssS0FBTDtBQUZtQjtBQUdwQjs7Ozs0QkFFTztBQUNOLFdBQUssZUFBTDtBQUNBLFdBQUssTUFBTDtBQUNBLFdBQUssTUFBTDtBQUNEOzs7c0NBRWlCO0FBQ2hCLFdBQUssUUFBTCxHQUFnQixLQUFLLEVBQUwsQ0FBUSxJQUFSLENBQWEsS0FBSyxPQUFMLENBQWEsZ0JBQTFCLENBQWhCO0FBQ0EsV0FBSyxPQUFMLEdBQWUsRUFBRSxNQUFGLENBQVMsRUFBVCxFQUFhLEtBQUssT0FBbEIsRUFBMkIsS0FBSyxRQUFMLENBQWMsSUFBZCxFQUEzQixDQUFmO0FBQ0Q7Ozs2QkFFUTtBQUNQLFdBQUssT0FBTCxDQUFhLEVBQWIsR0FBa0IsRUFBbEI7QUFDQSxVQUFJLEtBQUssT0FBTCxDQUFhLEtBQWpCLEVBQXdCO0FBQ3RCLGFBQUssT0FBTCxDQUFhLEVBQWIsSUFBbUIsaUJBQWlCLEtBQUssT0FBTCxDQUFhLEtBQTlCLEdBQXNDLEdBQXpEO0FBQ0EsWUFBSSxLQUFLLE9BQUwsQ0FBYSxHQUFqQixFQUFzQjtBQUNwQixjQUFJLFlBQWEsS0FBSyxPQUFMLENBQWEsU0FBYixLQUEyQixVQUE1QixHQUEwQyxRQUExQyxHQUFxRCxPQUFyRTtBQUNBLGVBQUssT0FBTCxDQUFhLEVBQWIsSUFBbUIsb0NBQW9DLFNBQXBDLEdBQWdELElBQWhELEdBQXVELEtBQUssT0FBTCxDQUFhLEtBQXBFLEdBQTRFLE1BQTVFLEdBQXFGLEtBQUssT0FBTCxDQUFhLEdBQWxHLEdBQXdHLFNBQTNIO0FBQ0Q7QUFDRjtBQUNGOzs7NkJBRVE7QUFDUCxVQUFNLFVBQVcsS0FBSyxPQUFMLENBQWEsSUFBYixLQUFzQixlQUF2QixHQUEwQyxhQUExQyxHQUEwRCxTQUExRTtBQUNBLFVBQU0sWUFBWSxLQUFLLEVBQUwsQ0FBUSxPQUFSLENBQWdCLEtBQUssT0FBTCxDQUFhLFVBQVUsVUFBdkIsQ0FBaEIsQ0FBbEI7QUFDQSxXQUFLLE9BQUwsQ0FBYSxFQUFiLElBQW1CLFVBQVUsSUFBVixDQUFlLE9BQWYsRUFBd0IsS0FBSyxPQUFMLENBQWEsRUFBckMsQ0FBbkI7QUFDRDs7Ozs7Ozs7Ozs7Ozs7OztBQzlDSDs7Ozs7Ozs7SUFHYSxXLFdBQUEsVzs7Ozs7d0JBRVk7QUFBRSxhQUFPLGtCQUFQO0FBQTRCOzs7QUFFckQseUJBQXFCO0FBQUE7O0FBQUE7O0FBQUEsc0NBQU4sSUFBTTtBQUFOLFVBQU07QUFBQTs7QUFBQSxxSkFDVixJQURVOztBQUVuQixVQUFLLEtBQUw7QUFGbUI7QUFHcEI7Ozs7NEJBRU87QUFBQTs7QUFDTixVQUFJLENBQUMsUUFBUSxLQUFSLENBQWMsTUFBZCxDQUFxQixRQUExQixFQUFvQztBQUNsQyxhQUFLLFlBQUwsR0FBb0IsS0FBSyxFQUFMLENBQVEsSUFBUixDQUFhLHdCQUFiLENBQXBCO0FBQ0EsYUFBSyxNQUFMLEdBQWMsS0FBSyxZQUFMLENBQWtCLElBQWxCLENBQXVCLGVBQXZCLEVBQXdDLElBQXhDLEVBQWQ7QUFDQSxhQUFLLEVBQUwsQ0FBUSxPQUFSLENBQWdCLFVBQWhCLEVBQTRCLE9BQTVCLENBQW9DLEtBQUssWUFBekMsRUFBdUQsUUFBdkQsQ0FBZ0UsdUJBQWhFO0FBQ0EsbUJBQVc7QUFBQSxpQkFBTSxPQUFLLFlBQUwsRUFBTjtBQUFBLFNBQVg7QUFDRCxPQUxELE1BS087QUFDTCxhQUFLLEVBQUwsQ0FBUSxNQUFSO0FBQ0Q7QUFDRjs7O21DQUVjO0FBQUE7O0FBQ2IsV0FBSyxLQUFMLEdBQWEsU0FBUyxhQUFULENBQXVCLE9BQXZCLENBQWI7QUFDQSxXQUFLLE1BQUwsQ0FBWSxHQUFaLElBQW1CLEtBQUssVUFBTCxDQUFnQixLQUFLLE1BQUwsQ0FBWSxHQUE1QixFQUFpQyxXQUFqQyxDQUFuQjtBQUNBLFdBQUssTUFBTCxDQUFZLElBQVosSUFBb0IsS0FBSyxVQUFMLENBQWdCLEtBQUssTUFBTCxDQUFZLElBQTVCLEVBQWtDLFlBQWxDLENBQXBCO0FBQ0EsV0FBSyxLQUFMLENBQVcsWUFBWCxDQUF3QixVQUF4QixFQUFvQyxVQUFwQztBQUNBLFdBQUssS0FBTCxDQUFXLFlBQVgsQ0FBd0IsT0FBeEIsRUFBaUMsT0FBakM7QUFDQSxXQUFLLEtBQUwsQ0FBVyxZQUFYLENBQXdCLE1BQXhCLEVBQWdDLE1BQWhDO0FBQ0EsV0FBSyxZQUFMLENBQWtCLE1BQWxCLENBQXlCLEtBQUssS0FBOUI7QUFDQSxXQUFLLEtBQUwsQ0FBVyxnQkFBWCxDQUE0QixZQUE1QixFQUEwQyxZQUFNO0FBQzlDLGVBQUssRUFBTCxDQUFRLE9BQVIsQ0FBZ0IsVUFBaEIsRUFBNEIsUUFBNUIsQ0FBcUMsY0FBckM7QUFDQSxlQUFLLGdCQUFMO0FBQ0QsT0FIRDtBQUlEOzs7dUNBRWtCO0FBQUE7O0FBQ2pCLFdBQUssS0FBTCxHQUFhLEtBQUssS0FBTCxDQUFXLFVBQVgsR0FBd0IsS0FBSyxLQUFMLENBQVcsV0FBaEQ7QUFDQSxRQUFFLE1BQUYsRUFBVSxFQUFWLENBQWEsUUFBYixFQUF1QixFQUFFLFFBQUYsQ0FBVztBQUFBLGVBQU0sT0FBSyxlQUFMLEVBQU47QUFBQSxPQUFYLEVBQXlDLEdBQXpDLENBQXZCO0FBQ0EsV0FBSyxlQUFMO0FBQ0Q7OztzQ0FFaUI7QUFDaEIsVUFBSSxXQUFXLEtBQUssS0FBTCxHQUFhLEtBQUssWUFBTCxDQUFrQixNQUFsQixFQUE1QjtBQUNBLFFBQUUsS0FBSyxLQUFQLEVBQWMsR0FBZCxDQUFrQixXQUFsQixFQUErQixRQUEvQjtBQUNEOzs7K0JBRVUsRyxFQUFLLEksRUFBTTtBQUNwQixVQUFNLFNBQVMsU0FBUyxhQUFULENBQXVCLFFBQXZCLENBQWY7QUFDQSxhQUFPLEdBQVAsR0FBYSxHQUFiO0FBQ0EsYUFBTyxJQUFQLEdBQWMsSUFBZDtBQUNBLFdBQUssS0FBTCxDQUFXLFdBQVgsQ0FBdUIsTUFBdkI7QUFDRDs7O2lDQUVZO0FBQ1gsV0FBSyxLQUFMLENBQVcsSUFBWDtBQUNEOzs7Ozs7Ozs7Ozs7Ozs7O0FDekRIOzs7Ozs7OztJQUdhLFcsV0FBQSxXOzs7Ozt3QkFFWTtBQUFFLGFBQU8saUNBQVA7QUFBMkM7O0FBRXBFOzs7Ozs7OztBQU1BLHlCQUFxQjtBQUFBOztBQUFBOztBQUFBLHNDQUFOLElBQU07QUFBTixVQUFNO0FBQUE7O0FBQUEscUpBQ1YsSUFEVTs7QUFFbkIsVUFBSyxLQUFMO0FBRm1CO0FBR3BCOzs7OzRCQUVPO0FBQ04sVUFBSSxjQUFjLEtBQUssRUFBTCxDQUFRLElBQVIsQ0FBYSxnQ0FBYixFQUErQyxNQUFqRTtBQUNBLFdBQUssRUFBTCxDQUFRLFFBQVIsQ0FBaUIsYUFBYSxXQUE5QjtBQUNBOzs7Ozs7O0FBT0Q7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzVCSDs7QUFDQTs7Ozs7Ozs7SUFFYSxZLFdBQUEsWTs7Ozs7OztBQUVYO3dCQUNpQjtBQUFFLGFBQU8sZUFBUDtBQUF3Qjs7O3dCQUU1QjtBQUNiLGFBQU8sRUFBRSxNQUFGLENBQVMsRUFBVCw2R0FBNkI7QUFDbEMsaUNBQXlCLHNCQURTO0FBRWxDLDhCQUFzQiwwQkFGWTtBQUdsQyxzQkFBYztBQUhvQixPQUE3QixDQUFQO0FBS0Q7OztBQUVELDBCQUFxQjtBQUFBOztBQUFBOztBQUFBLHNDQUFOLElBQU07QUFBTixVQUFNO0FBQUE7O0FBQUEsdUpBQ1YsSUFEVTs7QUFFbkIsVUFBSyxLQUFMO0FBRm1CO0FBR3BCOzs7OzRCQUVPO0FBQ047QUFDQSxVQUFJLEtBQUssRUFBTCxDQUFRLE9BQVIsQ0FBZ0Isa0JBQWhCLEVBQW9DLE1BQXhDLEVBQWdEO0FBQzlDLGFBQUssU0FBTDtBQUNELE9BRkQsTUFFTztBQUNMLGFBQUssWUFBTDtBQUNEO0FBQ0Y7OztnQ0FFVztBQUFBOztBQUNWLFdBQUssRUFBTCxDQUFRLEdBQVIsQ0FBWSxnQkFBWixFQUE4QjtBQUFBLGVBQU0sT0FBSyxZQUFMLEVBQU47QUFBQSxPQUE5QjtBQUNBO0FBQ0EsV0FBSyxFQUFMLENBQVEsR0FBUixDQUFZLG9CQUFaLEVBQWtDO0FBQUEsZUFBTSxPQUFLLEVBQUwsQ0FBUSxPQUFSLENBQWdCLGtCQUFoQixFQUFvQyxJQUFwQyxDQUF5QyxXQUF6QyxFQUFzRCxRQUF0RCxDQUErRCxtQkFBL0QsQ0FBTjtBQUFBLE9BQWxDO0FBQ0Q7OzttQ0FFYztBQUNiLFdBQUssVUFBTCxHQUFrQixJQUFsQjtBQUNBLFdBQUssU0FBTCxHQUFpQixJQUFqQjtBQUNBLFdBQUssYUFBTCxHQUFxQixJQUFyQjtBQUNBLFdBQUssYUFBTCxHQUFxQixLQUFLLEVBQUwsQ0FBUSxJQUFSLENBQWEsa0JBQWIsQ0FBckI7QUFDQSxXQUFLLGNBQUwsR0FBc0IsS0FBSyxFQUFMLENBQVEsSUFBUixDQUFhLGtCQUFiLENBQXRCO0FBQ0EsV0FBSyxhQUFMLEdBQXFCLEtBQUssRUFBTCxDQUFRLElBQVIsQ0FBYSxlQUFiLENBQXJCO0FBQ0EsV0FBSyxZQUFMLEdBQW9CLEtBQUssV0FBTCxFQUFwQjtBQUNBLFdBQUssVUFBTDtBQUNBLFdBQUssYUFBTDtBQUNBLFdBQUssY0FBTDtBQUNBLFdBQUssWUFBTDtBQUNBLFdBQUssYUFBTDtBQUNBLFdBQUssWUFBTDtBQUNBLFdBQUssWUFBTDtBQUNBLFdBQUssV0FBTDtBQUNBLFdBQUssT0FBTDtBQUNEOzs7b0NBRWU7QUFBQTs7QUFFZCxXQUFLLGFBQUwsQ0FBbUIsSUFBbkIsQ0FBd0IsT0FBeEIsRUFBaUMsRUFBakMsQ0FBb0MsWUFBcEMsRUFBa0QsVUFBQyxDQUFELEVBQU87QUFDdkQsZUFBSyxTQUFMLENBQWUsRUFBRSxFQUFFLGFBQUosQ0FBZjtBQUNELE9BRkQ7O0FBSUEsV0FBSyxhQUFMLENBQW1CLElBQW5CLENBQXdCLE9BQXhCLEVBQWlDLEVBQWpDLENBQW9DLE9BQXBDLEVBQTZDLFVBQUMsQ0FBRCxFQUFPO0FBQ2xELFlBQUksS0FBSyxFQUFFLEVBQUUsYUFBSixDQUFUOztBQUVBLGVBQUssYUFBTCxDQUFtQixJQUFuQixDQUF3QixPQUF4QixFQUFpQyxXQUFqQyxDQUE2QyxRQUE3QztBQUNBLFdBQUcsUUFBSCxDQUFZLFFBQVo7QUFDQSxnQkFBUSxHQUFSLENBQVksRUFBWjtBQUNBLGVBQUssYUFBTCxDQUFtQixLQUFuQixDQUF5QixXQUF6QixFQUFzQyxHQUFHLEtBQUgsRUFBdEM7QUFDRCxPQVBEOztBQVNBLFdBQUssYUFBTCxDQUFtQixFQUFuQixDQUFzQixhQUF0QixFQUFxQyxVQUFDLENBQUQsRUFBSSxLQUFKLEVBQVcsWUFBWDtBQUFBLGVBQTRCLE9BQUssVUFBTCxDQUFnQixNQUFNLFlBQXRCLENBQTVCO0FBQUEsT0FBckM7QUFDQSxXQUFLLGFBQUwsQ0FBbUIsRUFBbkIsQ0FBc0IsY0FBdEIsRUFBc0MsVUFBQyxDQUFELEVBQUksS0FBSixFQUFXLFlBQVgsRUFBeUIsU0FBekI7QUFBQSxlQUF1QyxPQUFLLFVBQUwsQ0FBZ0IsU0FBaEIsQ0FBdkM7QUFBQSxPQUF0QztBQUNEOzs7bUNBRWM7QUFDYixVQUFJLEtBQUssRUFBTCxDQUFRLE9BQVIsQ0FBZ0Isa0JBQWhCLEVBQW9DLE1BQXhDLEVBQWdEO0FBQzlDLGFBQUssYUFBTCxHQUFxQixFQUFFLFNBQUYsRUFBYSxFQUFDLE9BQU8sY0FBUixFQUFiLENBQXJCO0FBQ0EsYUFBSyxFQUFMLENBQVEsSUFBUixDQUFhLGtCQUFiLEVBQWlDLE9BQWpDLENBQXlDLEtBQUssYUFBOUM7QUFDRDtBQUVGOzs7OEJBRVMsRyxFQUFLO0FBQ2IsVUFBSSxnQkFBZ0IsS0FBSyxFQUFMLENBQVEsSUFBUixDQUFhLGtCQUFiLENBQXBCOztBQUVFLFVBQUksY0FBYyxLQUFkLEtBQXdCLGNBQWMsR0FBZCxDQUFrQixDQUFsQixFQUFxQixXQUFqRCxFQUE4RDtBQUM1RCxZQUFJLFlBQVksS0FBSyxLQUFMLENBQVcsSUFBSSxRQUFKLEdBQWUsSUFBMUIsQ0FBaEI7QUFBQSxZQUNJLFVBQVUsWUFBWSxTQUFTLEtBQUssYUFBTCxDQUFtQixHQUFuQixDQUF1QixjQUF2QixDQUFULENBRDFCOztBQUdBLHNCQUFjLElBQWQsQ0FBbUIsSUFBbkIsRUFBeUIsSUFBekIsRUFBK0IsS0FBL0IsQ0FBcUMsR0FBckMsRUFBMEMsT0FBMUMsQ0FBa0Q7QUFDaEQsc0JBQVk7QUFEb0MsU0FBbEQsRUFFRyxHQUZIO0FBR0Q7QUFDSjs7O2tDQUVhO0FBQ1osVUFBSSxLQUFKO0FBQ0EsVUFBSSxPQUFPLFVBQVAsR0FBb0IsR0FBeEIsRUFBNkI7QUFDM0IsZ0JBQVEsU0FBUjtBQUNELE9BRkQsTUFFTztBQUNMLGdCQUFRLFFBQVI7QUFDRDtBQUNELGFBQU8sS0FBUDtBQUNEOzs7dUNBRWtCO0FBQ2pCLFVBQUksUUFBUSxLQUFLLFdBQUwsRUFBWjtBQUNBLFVBQUksU0FBUyxLQUFLLFlBQWxCLEVBQWdDO0FBQzlCO0FBQ0QsT0FGRCxNQUVPO0FBQ0wsYUFBSyxZQUFMLEdBQW9CLEtBQXBCO0FBQ0EsYUFBSyxhQUFMO0FBQ0Q7QUFDRjs7O2lDQUVZO0FBQUE7O0FBQ1gsV0FBSyxjQUFMLENBQW9CLElBQXBCLENBQXlCLFVBQUMsS0FBRCxFQUFRLEVBQVIsRUFBZTtBQUN0QyxZQUFJLE9BQU8sRUFBRSxFQUFGLENBQVg7QUFBQSxZQUNFLFlBQVksS0FBSyxJQUFMLENBQVUsS0FBVixDQURkO0FBQUEsWUFFRSxVQUFVLEVBRlo7O0FBSUEsa0JBQVUsSUFBVixDQUFlLFVBQVMsS0FBVCxFQUFnQixFQUFoQixFQUFvQjtBQUNqQyxjQUFJLE9BQU8sRUFBRSxFQUFGLENBQVg7QUFDQSxjQUFJLEtBQUssUUFBTCxDQUFjLFNBQWQsQ0FBSixFQUE4QjtBQUM1QixvQkFBUSxVQUFSLEdBQXFCLEtBQUssSUFBTCxDQUFVLEtBQVYsQ0FBckI7QUFDRCxXQUZELE1BRU8sSUFBSSxLQUFLLFFBQUwsQ0FBYyxRQUFkLENBQUosRUFBNkI7QUFDbEMsb0JBQVEsU0FBUixHQUFvQixLQUFLLElBQUwsQ0FBVSxLQUFWLENBQXBCO0FBQ0Q7QUFDRCxrQkFBUSxHQUFSLEdBQWMsS0FBSyxJQUFMLENBQVUsS0FBVixLQUFvQixFQUFsQztBQUNBLGVBQUssTUFBTDtBQUNELFNBVEQ7O0FBV0EsYUFBSyxNQUFMLENBQVksT0FBSyxZQUFMLENBQWtCLE9BQWxCLENBQVo7QUFDRCxPQWpCRDtBQWtCRDs7O29DQUVlO0FBQ2QsV0FBSyxjQUFMLENBQW9CLElBQXBCLENBQXlCLEtBQXpCLEVBQWdDLElBQWhDLENBQXFDLFVBQUMsS0FBRCxFQUFRLEtBQVIsRUFBa0I7QUFDckQsWUFBSSxZQUFZLEVBQUUsS0FBRixDQUFoQjtBQUFBLFlBQ0UsYUFBYSxVQUFVLElBQVYsQ0FBZSxhQUFmLENBRGY7QUFBQSxZQUVFLFlBQVksVUFBVSxJQUFWLENBQWUsWUFBZixDQUZkO0FBQUEsWUFHRSxNQUFNLEVBSFI7QUFJQSxZQUFJLE9BQU8sVUFBUCxHQUFvQixHQUF4QixFQUE2QjtBQUMzQixnQkFBTSxVQUFOO0FBQ0QsU0FGRCxNQUVPO0FBQ0wsZ0JBQU0sWUFBWSxTQUFaLEdBQXdCLFVBQTlCO0FBQ0Q7O0FBRUQsa0JBQVUsSUFBVixDQUFlLEtBQWYsRUFBc0IsR0FBdEI7QUFDRCxPQVpEO0FBY0Q7Ozt3Q0FFNEM7QUFBQSxVQUE5QixVQUE4QixTQUE5QixVQUE4QjtBQUFBLFVBQWxCLFNBQWtCLFNBQWxCLFNBQWtCO0FBQUEsVUFBUCxHQUFPLFNBQVAsR0FBTzs7QUFDM0MsVUFBSSxRQUFRLEVBQUUsU0FBRixFQUFhO0FBQ3ZCLGFBQUssR0FEa0I7QUFFdkIsNEJBQW9CLFVBRkc7QUFHdkIsMkJBQW1CLFNBSEk7QUFJdkIsZUFBTyxZQUFZLGdCQUFaLEdBQStCO0FBSmYsT0FBYixDQUFaO0FBTUEsYUFBTyxLQUFQO0FBQ0Q7OztxQ0FFZ0I7QUFDZixVQUFJLFFBQVEsRUFBRSxTQUFGLEVBQWE7QUFDdkIsZUFBTztBQURnQixPQUFiLENBQVo7QUFHQSxXQUFLLGFBQUwsQ0FBbUIsT0FBbkIsQ0FBMkIscUJBQTNCLEVBQWtELFNBQWxELENBQTRELEtBQTVEO0FBQ0Q7OzttQ0FFYztBQUFBOztBQUNiLFdBQUssYUFBTCxDQUNDLEVBREQsQ0FDSSxNQURKLEVBQ1ksVUFBQyxDQUFELEVBQUksS0FBSjtBQUFBLGVBQWMsT0FBSyxPQUFMLENBQWEsQ0FBYixFQUFnQixLQUFoQixDQUFkO0FBQUEsT0FEWixFQUVDLEtBRkQsQ0FFTztBQUNMLG1CQUFXLEtBRE47QUFFTCxrQkFBVSxLQUZMO0FBR0wsd0JBQWdCLEVBSFg7QUFJTCxzQkFBYyxDQUpUO0FBS0wsY0FBTTtBQUxELE9BRlA7QUFTQSxXQUFLLEVBQUwsQ0FBUSxJQUFSLENBQWEsY0FBYixFQUE2QixRQUE3QixDQUFzQyxLQUFLLE9BQUwsQ0FBYSx1QkFBbkQ7QUFDRDs7OzRCQUVPLEMsRUFBRyxLLEVBQU87QUFDaEIsVUFBRyxLQUFLLGFBQVIsRUFBdUI7QUFDckIsZ0JBQVEsWUFBUixDQUFxQixtQkFBckIsQ0FBeUMsS0FBSyxhQUE5QyxFQUE2RCxLQUFLLGFBQUwsQ0FBbUIsSUFBbkIsQ0FBd0IsaUNBQXhCLENBQTdELEVBQXlILEtBQUssYUFBOUg7QUFDRDtBQUNGOzs7a0NBRWE7QUFBQTs7QUFDWixVQUFJLEtBQUssRUFBTCxDQUFRLFFBQVIsQ0FBaUIsS0FBSyxPQUFMLENBQWEsb0JBQTlCLENBQUosRUFBeUQ7QUFDdkQsWUFBSSxRQUFRLEtBQUssYUFBTCxDQUFtQixLQUFuQixDQUF5QixVQUF6QixDQUFaO0FBQ0EsYUFBSyxPQUFMLENBQWEsWUFBYixHQUE0QixNQUFNLFVBQWxDO0FBQ0EsYUFBSyxTQUFMLEdBQWlCLEVBQUUsVUFBRixFQUFjLEVBQUMsT0FBTyxZQUFSLEVBQWQsQ0FBakI7QUFDQSxhQUFLLGFBQUwsQ0FBbUIsRUFBbkIsQ0FBc0Isb0JBQXRCLEVBQTRDLFVBQUMsQ0FBRCxFQUFJLEtBQUosRUFBVyxZQUFYO0FBQUEsaUJBQTRCLE9BQUssY0FBTCxDQUFvQixZQUFwQixDQUE1QjtBQUFBLFNBQTVDLEVBQ1EsTUFEUixDQUNlLEtBQUssU0FEcEI7O0FBR0EsYUFBSyxjQUFMLENBQW9CLE1BQU0sWUFBMUI7QUFDRDtBQUNGOzs7bUNBRWUsWSxFQUFjO0FBQzVCLFdBQUssU0FBTCxDQUFlLElBQWYsQ0FBdUIsZUFBZSxDQUF0QyxTQUEyQyxLQUFLLE9BQUwsQ0FBYSxZQUF4RDtBQUNEOzs7K0JBRVUsSyxFQUFPO0FBQUE7O0FBQ2hCLFdBQUssYUFBTCxDQUFtQixJQUFuQixDQUF3QixPQUF4QixFQUFpQyxXQUFqQyxDQUE2QyxRQUE3QztBQUNBLFVBQUksWUFBWSxFQUFFLEtBQUssYUFBTCxDQUFtQixJQUFuQixDQUF3QixPQUF4QixFQUFpQyxFQUFqQyxDQUFvQyxLQUFwQyxDQUFGLEVBQThDLFFBQTlDLENBQXVELFFBQXZELENBQWhCOztBQUVBLFdBQUssU0FBTCxDQUFlLFNBQWY7QUFDQSxpQkFBVztBQUFBLGVBQU0sT0FBSyxnQkFBTCxDQUFzQixTQUF0QixDQUFOO0FBQUEsT0FBWCxFQUFtRCxHQUFuRDtBQUNEOzs7cUNBRWdCLFMsRUFBVztBQUMxQixVQUFJLFNBQVMsWUFBWSxTQUFaLEdBQXdCLEtBQUssYUFBTCxDQUFtQixJQUFuQixDQUF3QixjQUF4QixDQUFyQztBQUFBLFVBQ0UsVUFBVSxPQUFPLFFBQVAsR0FBa0IsSUFBbEIsR0FBeUIsQ0FBQyxLQUFLLGFBQUwsQ0FBbUIsVUFBbkIsS0FBa0MsS0FBSyxhQUFMLENBQW1CLEtBQW5CLEVBQW5DLElBQWlFLENBRHRHO0FBRUEsV0FBSyxVQUFMLENBQWdCLEdBQWhCLENBQW9CLEVBQUMsT0FBTyxPQUFPLEtBQVAsRUFBUixFQUFwQixFQUE2QyxHQUE3QyxDQUFpRCxFQUFDLGVBQWUsT0FBaEIsRUFBakQ7QUFDRDs7O21DQUVjO0FBQ2IsV0FBSyxVQUFMLEdBQWtCLEVBQUUsUUFBRixFQUFZLEVBQUMsT0FBTyxnQkFBUixFQUFaLENBQWxCO0FBQ0EsVUFBSSxrQkFBa0IsRUFBRSxTQUFGLEVBQWEsRUFBQyxPQUFPLGtCQUFSLEVBQWIsRUFBMEMsTUFBMUMsQ0FBaUQsS0FBSyxVQUF0RCxDQUF0QjtBQUNBLFdBQUssYUFBTCxDQUFtQixNQUFuQixDQUEwQixlQUExQjtBQUNEOzs7OEJBRVM7QUFBQTs7QUFDUixXQUFLLGFBQUwsQ0FBbUIsS0FBbkIsQ0FBeUIsYUFBekI7O0FBRUEsUUFBRSxNQUFGLEVBQVUsRUFBVixDQUFhLFFBQWIsRUFBdUIsRUFBRSxRQUFGLENBQVcsWUFBTTtBQUN0QyxlQUFLLGdCQUFMO0FBQ0EsZUFBSyxhQUFMLENBQW1CLEtBQW5CLENBQXlCLGFBQXpCO0FBQ0EsZUFBSyxnQkFBTDtBQUNELE9BSnNCLEVBSXBCLEdBSm9CLENBQXZCO0FBS0Q7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3pPSDs7QUFDQTs7QUFDQTs7QUFDQTs7Ozs7Ozs7OztJQUVhLFksV0FBQSxZOzs7Ozs7O0FBRVg7d0JBQ2lCO0FBQUUsYUFBTyxlQUFQO0FBQXdCOzs7d0JBRTVCO0FBQ2IsYUFBTyxFQUFFLE1BQUYsQ0FBUyxFQUFULDZHQUE2QjtBQUNsQyxpQ0FBeUIsc0JBRFM7QUFFbEMscUJBQWE7QUFBQSxpQkFBTSxvSkFBTjtBQUFBO0FBRnFCLE9BQTdCLENBQVA7QUFJRDs7O0FBRUQsMEJBQXFCO0FBQUE7O0FBQUE7O0FBQUEsc0NBQU4sSUFBTTtBQUFOLFVBQU07QUFBQTs7QUFBQSx1SkFDVixJQURVOztBQUVuQixVQUFLLFFBQUwsR0FBZ0IsRUFBaEI7QUFDQSxVQUFLLFFBQUwsR0FBZ0IsRUFBaEI7QUFDQSxVQUFLLGFBQUwsR0FBcUIsSUFBckI7QUFDQSxVQUFLLEtBQUw7QUFMbUI7QUFNcEI7Ozs7NEJBRU87QUFDTjtBQUNBLFVBQUksS0FBSyxFQUFMLENBQVEsT0FBUixDQUFnQixrQkFBaEIsRUFBb0MsTUFBeEMsRUFBZ0Q7QUFDOUMsYUFBSyxTQUFMO0FBQ0QsT0FGRCxNQUVPO0FBQ0wsYUFBSyxZQUFMO0FBQ0Q7QUFDRjs7O2dDQUVXO0FBQUE7O0FBQ1YsV0FBSyxFQUFMLENBQVEsR0FBUixDQUFZLGdCQUFaLEVBQThCO0FBQUEsZUFBTSxPQUFLLFlBQUwsRUFBTjtBQUFBLE9BQTlCO0FBQ0E7QUFDQSxXQUFLLEVBQUwsQ0FBUSxHQUFSLENBQVksb0JBQVosRUFBa0M7QUFBQSxlQUFNLE9BQUssRUFBTCxDQUFRLE9BQVIsQ0FBZ0Isa0JBQWhCLEVBQW9DLElBQXBDLENBQXlDLFdBQXpDLEVBQXNELFFBQXRELENBQStELG1CQUEvRCxDQUFOO0FBQUEsT0FBbEM7QUFDQSxXQUFLLEVBQUwsQ0FBUSxFQUFSLENBQVcsZ0JBQVgsRUFBNkI7QUFBQSxlQUFNLE9BQUssZUFBTCxFQUFOO0FBQUEsT0FBN0I7QUFDRDs7O21DQUVjO0FBQ2IsV0FBSyxhQUFMLEdBQXFCLEtBQUssRUFBTCxDQUFRLElBQVIsQ0FBYSxxQkFBYixDQUFyQjtBQUNBLFdBQUssWUFBTDtBQUNBLFdBQUssZUFBTDtBQUNBLFdBQUssYUFBTDtBQUNEOzs7bUNBRWM7QUFDYixXQUFLLGFBQUwsR0FBcUIsRUFBRSxTQUFGLEVBQWEsRUFBQyxPQUFPLGNBQVIsRUFBYixDQUFyQjtBQUNBLFdBQUssRUFBTCxDQUFRLElBQVIsQ0FBYSxrQkFBYixFQUFpQyxPQUFqQyxDQUF5QyxLQUFLLGFBQTlDO0FBQ0Q7OztzQ0FFaUI7QUFDaEIsV0FBSyxhQUFMLEdBQXFCLElBQUksaUJBQUosQ0FBc0IsS0FBSyxhQUEzQixDQUFyQjtBQUNEOzs7b0NBRWU7QUFBQTs7QUFDZCxXQUFLLFdBQUwsR0FBbUIsS0FBSyxFQUFMLENBQVEsSUFBUixDQUFhLGVBQWIsQ0FBbkI7QUFDQSxXQUFLLFlBQUwsR0FBb0IsS0FBSyxFQUFMLENBQVEsSUFBUixDQUFhLGtCQUFiLEVBQWlDLFFBQWpDLENBQTBDLFNBQTFDLENBQXBCOztBQUVBLFdBQUssV0FBTCxDQUNHLEVBREgsQ0FDTSxNQUROLEVBQ2MsVUFBQyxDQUFELEVBQUksS0FBSjtBQUFBLGVBQWMsT0FBSyxPQUFMLENBQWEsQ0FBYixFQUFnQixLQUFoQixDQUFkO0FBQUEsT0FEZCxFQUVHLEVBRkgsQ0FFTSxjQUZOLEVBRXNCO0FBQUEsZUFBYSxPQUFLLGFBQUwseUJBQWI7QUFBQSxPQUZ0QixFQUdHLEVBSEgsQ0FHTSxhQUhOLEVBR3FCLFVBQUMsQ0FBRCxFQUFJLEtBQUo7QUFBQSxlQUFjLE9BQUssWUFBTCxDQUFrQixDQUFsQixFQUFxQixLQUFyQixDQUFkO0FBQUEsT0FIckIsRUFJRyxLQUpILENBSVM7QUFDTCxtQkFBVyxLQUROO0FBRUwsa0JBQVUsSUFGTDtBQUdMLHdCQUFnQixHQUhYO0FBSUwsZUFBTyxLQUpGO0FBS0wsY0FBTTtBQUxELE9BSlQ7O0FBWUEsVUFBSSxrQkFBa0IsS0FBSyxXQUFMLENBQWlCLEtBQWpCLENBQXVCLFVBQXZCLENBQXRCOztBQUVBO0FBQ0EsV0FBSyxlQUFMLEdBQXVCLHVCQUFhLEtBQUssWUFBbEIsRUFBZ0M7QUFDckQsdUJBQWUsQ0FEc0M7QUFFckQsaUJBQVMsbUJBQU0sQ0FBRSxDQUZvQyxFQUVuQztBQUNsQixxQkFBYSxjQUh3QztBQUlyRCx5QkFBaUI7QUFKb0MsT0FBaEMsQ0FBdkI7QUFPRDs7O3NDQUVpQjtBQUNoQjtBQUNBLFdBQUssYUFBTCxDQUFtQixRQUFuQixDQUE0QixJQUE1Qjs7QUFFQTtBQUNBLFdBQUssV0FBTCxDQUFpQixLQUFqQixDQUF1QixXQUF2QixFQUFvQyxDQUFwQyxFQUF1QyxJQUF2QztBQUNEOzs7NEJBRU8sQyxFQUFHLEssRUFBTztBQUFBOztBQUNoQjtBQUNBLGlCQUFXO0FBQUEsZUFBTSxNQUFNLFdBQU4sRUFBTjtBQUFBLE9BQVgsRUFBc0MsR0FBdEM7O0FBRUE7QUFDQSxjQUFRLFlBQVIsQ0FBcUIsZUFBckIsQ0FBcUMsS0FBSyxXQUExQyxFQUF1RCxFQUFDLG1CQUFtQixrQkFBcEIsRUFBdkQ7O0FBRUE7QUFDQSxjQUFRLFlBQVIsQ0FBcUIsbUJBQXJCLENBQXlDLEtBQUssV0FBOUMsRUFBMkQsS0FBSyxXQUFMLENBQWlCLElBQWpCLENBQXNCLGlDQUF0QixDQUEzRCxFQUFxSCxLQUFLLGFBQTFIOztBQUVBO0FBQ0EsV0FBSyxXQUFMLENBQWlCLElBQWpCLENBQXNCLGNBQXRCLEVBQXNDLFFBQXRDLENBQStDLEtBQUssT0FBTCxDQUFhLHVCQUFiLEdBQXVDLGNBQXRGOztBQUVBO0FBQ0EsV0FBSyxXQUFMLENBQWlCLE1BQWpCLG9EQUF5RSxNQUFNLFVBQS9FO0FBQ0EsV0FBSyxRQUFMLEdBQWdCLEtBQUssV0FBTCxDQUFpQixJQUFqQixDQUFzQixhQUF0QixDQUFoQjs7QUFFQTtBQUNBLFdBQUssV0FBTCxDQUFpQixFQUFqQixDQUFvQixhQUFwQixFQUFtQyxVQUFDLElBQUQ7QUFBQSxlQUFVLE9BQUssdUJBQUwsRUFBVjtBQUFBLE9BQW5DO0FBQ0Q7O0FBRUQ7Ozs7d0NBQ29CLEMsRUFBRyxLLEVBQU87QUFDNUIsY0FBUSxZQUFSLENBQXFCLGVBQXJCLENBQXFDLEtBQUssWUFBMUMsRUFBd0QsRUFBQyxtQkFBbUIsU0FBcEIsRUFBeEQ7QUFDQSxXQUFLLFlBQUwsQ0FBa0IsSUFBbEIsQ0FBdUIsY0FBdkIsRUFBdUMsUUFBdkMsQ0FBZ0QsS0FBSyxPQUFMLENBQWEsdUJBQWIsR0FBdUMsY0FBdkY7QUFDRDs7O2tDQUVhLEMsRUFBRyxLLEVBQU8sUyxFQUFXLFMsRUFBVztBQUM1QyxXQUFLLGFBQUwsQ0FBbUIsUUFBbkI7QUFDQSxXQUFLLGVBQUwsQ0FBcUIsT0FBckIsQ0FBNkIsU0FBN0I7QUFDQSxXQUFLLGNBQUwsQ0FBb0IsTUFBTSxVQUExQixFQUFzQyxTQUF0QztBQUNEOzs7aUNBRVksQyxFQUFHLEssRUFBTztBQUNyQixXQUFLLGFBQUwsQ0FBbUIsV0FBbkIsQ0FBK0IsTUFBTSxZQUFyQztBQUNEOzs7bUNBRWMsVSxFQUFZLFksRUFBYztBQUN2QyxXQUFLLFFBQUwsQ0FBYyxJQUFkLENBQXNCLGVBQWUsQ0FBckMsU0FBMEMsVUFBMUM7QUFDRDs7OzhDQUV5QjtBQUN4QixVQUFJLFNBQVMsS0FBSyxXQUFMLENBQWlCLElBQWpCLENBQXNCLGlDQUF0QixFQUF5RCxLQUF6RCxHQUFpRSxNQUFqRSxFQUFiOztBQUVBLGdCQUFVLEtBQUssUUFBTCxDQUFjLEdBQWQsQ0FBa0IsS0FBbEIsRUFBeUIsTUFBekIsQ0FBVjtBQUNEOzs7Ozs7SUFJRyxpQjtBQUNKLDZCQUFZLEtBQVosRUFBbUI7QUFBQTs7QUFDakIsU0FBSyxLQUFMLENBQVcsS0FBWDtBQUNEOzs7OzBCQUVLLEssRUFBTztBQUNYLFdBQUssTUFBTCxHQUFjLEtBQWQ7QUFDQSxXQUFLLE9BQUwsR0FBZSxFQUFmO0FBQ0EsV0FBSyxTQUFMLENBQWUsQ0FBZjtBQUNEOzs7OEJBRVMsSyxFQUFPO0FBQUE7O0FBQ2YsVUFBSSxLQUFLLEtBQUssTUFBTCxDQUFZLEVBQVosQ0FBZSxLQUFmLENBQVQ7QUFDQSxVQUFJLEdBQUcsTUFBUCxFQUFlO0FBQ2IsYUFBSyxPQUFMLENBQWEsSUFBYixDQUFrQixJQUFJLFNBQUosQ0FBYyxFQUFkLENBQWxCO0FBQ0EsbUJBQVc7QUFBQSxpQkFBTSxPQUFLLFNBQUwsQ0FBZSxFQUFFLEtBQWpCLENBQU47QUFBQSxTQUFYLEVBQTBDLEdBQTFDO0FBQ0Q7QUFDRjs7OzZCQUVRLFUsRUFBWTtBQUNuQixXQUFLLE9BQUwsQ0FBYSxPQUFiLENBQXFCLFVBQUMsS0FBRDtBQUFBLGVBQVcsTUFBTSxVQUFOLENBQWlCLFVBQWpCLENBQVg7QUFBQSxPQUFyQjtBQUNEOzs7Z0NBRVcsWSxFQUFjO0FBQ3hCLFdBQUssT0FBTCxDQUFhLE9BQWIsQ0FBcUIsVUFBQyxLQUFELEVBQVEsQ0FBUjtBQUFBLGVBQWUsTUFBTSxZQUFQLElBQXdCLE1BQU0sVUFBTixFQUF0QztBQUFBLE9BQXJCO0FBQ0Q7Ozs7OztJQUlHLFM7Ozt3QkFDVTtBQUNaLGFBQU87QUFDTCw4QkFBc0IseUJBRGpCO0FBRUwsaUNBQXlCLG9CQUZwQjtBQUdMLHFCQUFhLE9BQU8sVUFBUCxDQUFrQiwrQkFBbEIsQ0FIUjtBQUlMLHFCQUFhO0FBQUEsaUJBQU0sb0pBQU47QUFBQTtBQUpSLE9BQVA7QUFNRDs7O0FBRUQscUJBQVksRUFBWixFQUFnQjtBQUFBOztBQUNkLFNBQUssS0FBTCxDQUFXLEVBQVg7QUFDRDs7OzswQkFFSyxFLEVBQUk7QUFDUixXQUFLLEVBQUwsR0FBVSxFQUFWO0FBQ0EsV0FBSyxPQUFMLEdBQWUsSUFBZjtBQUNBLFdBQUssT0FBTCxHQUFlLElBQWY7QUFDQSxXQUFLLFdBQUw7QUFDQSxXQUFLLFdBQUw7QUFDQSxXQUFLLGNBQUw7QUFDRDs7O2tDQUVhO0FBQ1osVUFBSSxZQUFZLEtBQUssRUFBTCxDQUFRLElBQVIsQ0FBYSxVQUFiLEVBQXlCLElBQXpCLENBQThCLFVBQTlCLENBQWhCO0FBQUEsVUFDSSxXQUFXLEtBQUssRUFBTCxDQUFRLElBQVIsQ0FBYSxTQUFiLEVBQXdCLElBQXhCLENBQTZCLFVBQTdCLEtBQTRDLFNBRDNEO0FBQUEsVUFFSSxXQUFXLEVBQUUsU0FBRixFQUFhO0FBQ3RCLGlCQUFTLEtBRGE7QUFFdEIsaUJBQVMsS0FBSyxXQUFMLENBQWlCLFNBQWpCLEVBQTRCLFFBQTVCO0FBRmEsT0FBYixDQUZmOztBQU9BLFdBQUssT0FBTCxHQUFlLEtBQUssRUFBTCxDQUFRLElBQVIsQ0FBYSxrQkFBYixFQUNRLE1BRFIsQ0FDZSxRQURmLEVBRVEsTUFGUixDQUVlLEtBQUssT0FBTCxDQUFhLFdBQWIsRUFGZixFQUdRLElBSFIsQ0FHYTtBQUNKLGFBQUssQ0FBQyxTQUFELEVBQVksUUFBWjtBQURELE9BSGIsQ0FBZjtBQU1EOzs7dUNBRWtCO0FBQ2pCLFdBQUssT0FBTCxDQUFhLElBQWIsQ0FBa0IsTUFBbEIsRUFBMEIsSUFBMUIsQ0FBK0IsT0FBL0IsRUFBd0MsS0FBSyxXQUFMLGdDQUFvQixLQUFLLE9BQUwsQ0FBYSxJQUFiLEdBQW9CLEdBQXhDLEVBQXhDO0FBQ0Q7OztnQ0FFVyxFLEVBQUk7QUFDZCxXQUFLLE9BQUwsR0FBZSxLQUFLLGtCQUFMLEVBQWY7QUFDQSxXQUFLLE9BQUwsQ0FBYSxJQUFiO0FBQ0Q7OztxQ0FFZ0I7QUFBQTs7QUFDZixXQUFLLEVBQUwsQ0FBUSxJQUFSLENBQWEsT0FBYixFQUFzQixFQUF0QixDQUF5QixPQUF6QixFQUFrQztBQUFBLGVBQU0sT0FBSyxTQUFMLEVBQU47QUFBQSxPQUFsQztBQUNBLFdBQUssT0FBTCxDQUFhLFdBQWIsQ0FBeUIsV0FBekIsQ0FBcUM7QUFBQSxlQUFNLE9BQUssZ0JBQUwsRUFBTjtBQUFBLE9BQXJDO0FBQ0Q7Ozt5Q0FFb0I7QUFDbkIsVUFBSSxzQkFBc0IsS0FBSyxFQUFMLENBQVEsSUFBUixDQUFhLEtBQUssT0FBTCxDQUFhLG9CQUExQixDQUExQjtBQUFBLFVBQ0kseUJBQXlCLEtBQUssRUFBTCxDQUFRLElBQVIsQ0FBYSxLQUFLLE9BQUwsQ0FBYSx1QkFBMUIsQ0FEN0I7O0FBR0EsVUFBSSxvQkFBb0IsTUFBeEIsRUFBZ0M7QUFDOUIsZUFBTyxpQ0FBa0IsbUJBQWxCLENBQVA7QUFDRCxPQUZELE1BRU8sSUFBSSx1QkFBdUIsTUFBM0IsRUFBbUM7QUFDeEMsZUFBTyx1Q0FBcUIsc0JBQXJCLENBQVA7QUFDRDtBQUNELGFBQU8sRUFBUDtBQUNEOzs7Z0NBRVcsYSxFQUFlLFksRUFBYztBQUN2QyxVQUFJLFNBQVMsS0FBSyxPQUFMLENBQWEsV0FBYixDQUF5QixPQUF6QixHQUFtQyxZQUFuQyxHQUFrRCxhQUEvRDtBQUNBLGtDQUEwQixNQUExQjtBQUNEOzs7a0NBRWE7QUFDWixXQUFLLE9BQUwsQ0FBYSxPQUFiLENBQXFCLEdBQXJCO0FBQ0Q7OztpQ0FFWTtBQUNYLFdBQUssT0FBTCxDQUFhLE1BQWIsQ0FBb0IsR0FBcEI7QUFDRDs7O2dDQUVXO0FBQ1YsV0FBSyxPQUFMLENBQWEsSUFBYjtBQUNBLFdBQUssV0FBTDtBQUNEOztBQUVEOzs7Ozs7OytCQUlXLFUsRUFBWTtBQUNyQixVQUFJLEtBQUssT0FBVCxFQUFrQjtBQUNoQixZQUFJLENBQUMsVUFBTCxFQUFpQjtBQUNmLGVBQUssT0FBTCxDQUFhLEtBQWI7QUFDRCxTQUZELE1BRU87QUFDTCxlQUFLLE9BQUwsQ0FBYSxJQUFiO0FBQ0EsZUFBSyxVQUFMO0FBQ0Q7QUFDRjtBQUVGOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUM1UUg7O0FBQ0E7O0FBQ0E7Ozs7Ozs7O0lBR2Esa0IsV0FBQSxrQjs7Ozs7d0JBSUk7QUFDYixhQUFPLEVBQUUsTUFBRixDQUFTLEVBQVQseUhBQTZCO0FBQ2xDLCtCQUF1QjtBQURXLE9BQTdCLENBQVA7QUFHRDs7O3dCQU5zQjtBQUFFLGFBQU8sa0JBQVA7QUFBNEI7OztBQVFyRCxnQ0FBcUI7QUFBQTs7QUFBQTs7QUFBQSxzQ0FBTixJQUFNO0FBQU4sVUFBTTtBQUFBOztBQUFBLG1LQUNWLElBRFU7O0FBRW5CLFVBQUssS0FBTDtBQUZtQjtBQUdwQjs7Ozs0QkFFTztBQUNOLFVBQUksS0FBSyxFQUFMLENBQVEsSUFBUixDQUFhLEtBQUssT0FBTCxDQUFhLHFCQUExQixFQUFpRCxNQUFyRCxFQUE2RDtBQUMzRCxvREFBaUIsS0FBSyxFQUF0QjtBQUNELE9BRkQsTUFFTztBQUNMLG9EQUFpQixLQUFLLEVBQXRCO0FBQ0Q7QUFDRjs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDMUJIOztBQUNBOzs7Ozs7OztJQUdhLGUsV0FBQSxlOzs7Ozt3QkFJSTtBQUNiLGFBQU8sRUFBRSxNQUFGLENBQVMsRUFBVCxtSEFBNkI7QUFDbEMsbUNBQTJCLGVBRE87QUFFbEMsMEJBQWtCLEtBRmdCO0FBR2xDLHFCQUFhLE9BQU8sVUFBUCxDQUFrQiwrQkFBbEI7QUFIcUIsT0FBN0IsQ0FBUDtBQUtEOzs7d0JBUnFCO0FBQUUsYUFBTyxrQkFBUDtBQUE0Qjs7O0FBVXBELDZCQUFxQjtBQUFBOztBQUFBOztBQUFBLHNDQUFOLElBQU07QUFBTixVQUFNO0FBQUE7O0FBQUEsNkpBQ1YsSUFEVTs7QUFFbkIsVUFBSyxLQUFMO0FBRm1CO0FBR3BCOzs7OzRCQUVPO0FBQ04sV0FBSyxHQUFMLEdBQVcsS0FBSyxFQUFMLENBQVEsSUFBUixDQUFhLEtBQWIsQ0FBWDs7QUFFQSxVQUFJLEtBQUssV0FBTCxFQUFKLEVBQXdCO0FBQ3RCLGFBQUssZUFBTDtBQUNBLGFBQUssV0FBTCxHQUFtQixLQUFLLEVBQUwsQ0FBUSxJQUFSLENBQWEsVUFBYixDQUFuQjtBQUNBLGFBQUssY0FBTDtBQUNELE9BSkQsTUFJTztBQUNMLGFBQUssRUFBTCxDQUFRLFFBQVIsQ0FBaUIsWUFBakI7QUFDRDs7QUFFRCxVQUFJLENBQUMsS0FBSyxFQUFMLENBQVEsUUFBUixDQUFpQixLQUFLLE9BQUwsQ0FBYSx5QkFBOUIsQ0FBTCxFQUErRDtBQUM3RCxhQUFLLHVCQUFMO0FBQ0Q7QUFDRjs7O2tDQUVhO0FBQ1osYUFBTyxDQUFDLENBQUMsS0FBSyxFQUFMLENBQVEsSUFBUixDQUFhLFdBQWIsRUFBMEIsTUFBbkM7QUFDRDs7O3NDQUVpQjtBQUNoQixVQUFJLGNBQWMsRUFBbEI7QUFDQSxVQUFJLFVBQVUsRUFBRSxTQUFGLEVBQWEsRUFBRSxPQUFPLGFBQVQsRUFBYixDQUFkOztBQUVBLFdBQUssZUFBTCxHQUF1QixFQUFFLFNBQUYsRUFBYSxFQUFFLE9BQU8sa0JBQVQsRUFBYixDQUF2QjtBQUNBLFdBQUssRUFBTCxDQUFRLElBQVIsQ0FBYSxzQ0FBYixFQUFxRCxJQUFyRCxDQUEwRCxVQUFDLENBQUQsRUFBSSxJQUFKLEVBQWE7QUFDckUsWUFBSSxLQUFLLEVBQUUsSUFBRixDQUFUO0FBQUEsWUFDRSxLQUFLLEdBQUcsSUFBSCxDQUFRLElBQVIsQ0FEUDtBQUVBLHVCQUFlLGNBQWMsRUFBZCxHQUFtQixnQ0FBbkIsR0FBc0QsR0FBRyxJQUFILEVBQXRELEdBQWtFLFFBQWpGO0FBQ0QsT0FKRDtBQUtBLFdBQUssZUFBTCxDQUFxQixJQUFyQixDQUEwQixXQUExQjtBQUNBLGNBQVEsTUFBUixDQUFlLEtBQUssRUFBTCxDQUFRLElBQVIsQ0FBYSxTQUFiLENBQWY7QUFDQSxjQUFRLElBQVIsQ0FBYSxTQUFiLEVBQXdCLElBQXhCLENBQTZCLGlDQUE3QjtBQUNBLGNBQVEsTUFBUixDQUFlLEtBQUssZUFBcEI7QUFDQSxXQUFLLEdBQUwsQ0FBUyxNQUFULENBQWdCLE9BQWhCOztBQUVBLFdBQUssSUFBTCxHQUFZLE9BQVo7QUFDQSxXQUFLLGVBQUwsR0FBdUIsUUFBUSxJQUFSLENBQWEsZ0NBQWIsQ0FBdkI7QUFDQSxXQUFLLFdBQUwsR0FBbUIsUUFBUSxJQUFSLENBQWEsMEJBQWIsQ0FBbkI7QUFDQSxXQUFLLFdBQUwsR0FBbUIsUUFBUSxJQUFSLENBQWEsNEJBQWIsQ0FBbkI7QUFDQSxXQUFLLGVBQUwsR0FBdUIsS0FBSyxFQUFMLENBQVEsSUFBUixDQUFhLFNBQWIsQ0FBdkI7QUFDRDs7O3FDQUVnQjtBQUFBOztBQUNmLFdBQUssRUFBTCxDQUFRLElBQVIsQ0FBYSx5QkFBYixFQUF3QyxFQUF4QyxDQUEyQyxPQUEzQyxFQUFvRCxVQUFDLENBQUQsRUFBTztBQUN6RCxZQUFJLEtBQUssRUFBRSxFQUFFLGFBQUosQ0FBVDtBQUFBLFlBQ0UsVUFBVSxPQUFLLFdBQUwsRUFEWjtBQUFBLFlBRUUsUUFBUSxFQUFFLEVBQUUsYUFBSixFQUFtQixJQUFuQixDQUF3QixLQUF4QixJQUFpQyxPQUYzQztBQUFBLFlBR0UsT0FBTyxFQUFFLE1BQU0sS0FBUixDQUhUOztBQUtBLFlBQUksR0FBRyxRQUFILENBQVksUUFBWixDQUFKLEVBQTJCO0FBQ3pCLGlCQUFLLGVBQUwsQ0FBcUIsV0FBckIsQ0FBaUMsUUFBakM7QUFDQSxpQkFBSyxXQUFMLENBQWlCLE9BQWpCO0FBQ0EsaUJBQUssV0FBTCxDQUFpQixXQUFqQixDQUE2QixRQUE3QjtBQUNBLGlCQUFLLGVBQUwsQ0FBcUIsV0FBckIsQ0FBaUMsUUFBakM7QUFDRCxTQUxELE1BS087QUFDTDtBQUNBLGNBQUksT0FBSixFQUFhO0FBQ1gsbUJBQUssZUFBTCxDQUFxQixXQUFyQixDQUFpQyxRQUFqQztBQUNBLG1CQUFLLGVBQUwsQ0FBcUIsUUFBckIsQ0FBOEIsUUFBOUI7QUFDQSxpQkFBSyxRQUFMLENBQWMsUUFBZDtBQUNELFdBSkQsTUFJTztBQUNMLG1CQUFLLFdBQUwsQ0FBaUIsT0FBakI7QUFDQSxpQkFBSyxTQUFMO0FBQ0Q7QUFDRCxpQkFBSyxXQUFMLENBQWlCLFdBQWpCLENBQTZCLFFBQTdCO0FBQ0EsYUFBRyxRQUFILENBQVksUUFBWjtBQUNEO0FBRUYsT0F6QkQ7QUEwQkEsV0FBSyxXQUFMLENBQWlCLEVBQWpCLENBQW9CLE9BQXBCLEVBQTZCLFVBQUMsQ0FBRCxFQUFPO0FBQ2xDLFlBQUksS0FBSyxFQUFFLEVBQUUsYUFBSixDQUFUO0FBQ0EsVUFBRSxjQUFGO0FBQ0EsWUFBSSxHQUFHLFFBQUgsQ0FBWSxRQUFaLENBQUosRUFBMkI7QUFDekIsaUJBQUssU0FBTCxDQUFlLElBQWY7QUFDRCxTQUZELE1BRU87QUFDTCxhQUFHLFFBQUgsQ0FBWSxRQUFaO0FBQ0EsaUJBQUssSUFBTCxDQUFVLFNBQVYsQ0FBb0IsWUFBTTtBQUN4QixtQkFBSyxJQUFMLENBQVUsR0FBVixDQUFjLFNBQWQsRUFBeUIsTUFBekI7QUFDRCxXQUZEO0FBR0Q7QUFDRixPQVhEOztBQWFBLFFBQUUsTUFBRixFQUFVLEVBQVYsQ0FBYSxPQUFiLEVBQXNCLFVBQUMsQ0FBRCxFQUFPO0FBQzNCLGVBQUssZUFBTCxDQUFxQixDQUFyQjtBQUNELE9BRkQ7QUFHQSxXQUFLLE9BQUwsQ0FBYSxXQUFiLENBQXlCLFdBQXpCLENBQXFDLFVBQUMsQ0FBRDtBQUFBLGVBQU8sT0FBSyxTQUFMLENBQWUsQ0FBZixDQUFQO0FBQUEsT0FBckM7QUFDRDs7O2tDQUVhO0FBQ1osVUFBSSxVQUFVLE9BQU8sVUFBUCxDQUFrQiw0QkFBNEIsS0FBSyxPQUFMLENBQWEsZ0JBQXpDLEdBQTRELEtBQTlFLEVBQXFGLE9BQXJGLEdBQStGLFVBQS9GLEdBQTRHLEVBQTFIO0FBQ0EsYUFBTyxPQUFQO0FBQ0Q7Ozs4Q0FFeUI7QUFDeEIsV0FBSyxXQUFMLEdBQW1CLEtBQUssRUFBTCxDQUFRLElBQVIsQ0FBYSxLQUFiLENBQW5CO0FBQ0EsK0JBQWMsS0FBSyxXQUFuQixFQUFnQyxLQUFLLFdBQUwsQ0FBaUIsTUFBakIsRUFBaEM7QUFDRDs7O2dDQUVXO0FBQUE7O0FBQ1YsV0FBSyxXQUFMLENBQWlCLE9BQWpCO0FBQ0EsV0FBSyxJQUFMLENBQVUsT0FBVixDQUFrQixZQUFNO0FBQ3RCLGVBQUssV0FBTCxDQUFpQixXQUFqQixDQUE2QixRQUE3QjtBQUNBLGVBQUssZUFBTCxDQUFxQixXQUFyQixDQUFpQyxRQUFqQztBQUNBLGVBQUssZUFBTCxDQUFxQixXQUFyQixDQUFpQyxRQUFqQztBQUNELE9BSkQ7O0FBTUEsVUFBSSxVQUFVLE1BQWQsRUFBc0I7QUFDcEIsYUFBSyxXQUFMLENBQWlCLFdBQWpCLENBQTZCLFFBQTdCO0FBQ0Q7QUFDRjs7O29DQUVlLEMsRUFBRztBQUNqQixVQUFJLENBQUMsRUFBRSxFQUFFLE1BQUosRUFBWSxPQUFaLENBQW9CLGtCQUFwQixFQUF3QyxNQUE3QyxFQUFxRDtBQUNuRCxhQUFLLFNBQUwsQ0FBZSxJQUFmO0FBQ0Q7QUFDRjs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDeklIOztBQUNBOzs7Ozs7OztJQUdhLFcsV0FBQSxXOzs7Ozt3QkFJSTtBQUNiLGFBQU8sRUFBRSxNQUFGLENBQVMsRUFBVCwyR0FBNkI7QUFDbEMscUJBQWEsVUFEcUI7QUFFbEMsK0JBQXVCLG9CQUZXO0FBR2xDLGtDQUEwQixzQkFIUTtBQUlsQyxrQkFBVSxjQUp3QjtBQUtsQyxtQkFDRSw2QkFDRSx1QkFERixHQUVJLG9CQUZKLEdBR0ksK0NBSEosR0FJRSxRQUpGLEdBS0Usa0JBTEYsR0FNSSxxQ0FOSixHQU9NLFlBUE4sR0FRSSxRQVJKLEdBU0UsU0FURixHQVVBO0FBaEJnQyxPQUE3QixDQUFQO0FBa0JEOzs7d0JBckJzQjtBQUFFLGFBQU8sa0JBQVA7QUFBNEI7OztBQXVCckQseUJBQXFCO0FBQUE7O0FBQUE7O0FBQUEsc0NBQU4sSUFBTTtBQUFOLFVBQU07QUFBQTs7QUFBQSxxSkFDVixJQURVOztBQUVuQixVQUFLLEtBQUw7QUFGbUI7QUFHcEI7Ozs7NEJBRU87QUFDTixVQUFJLEtBQUssRUFBTCxDQUFRLFFBQVIsQ0FBaUIsS0FBSyxPQUFMLENBQWEscUJBQTlCLENBQUosRUFBMEQ7QUFDeEQsYUFBSyxhQUFMLEdBQXFCLEtBQUssRUFBTCxDQUFRLE9BQVIsQ0FBZ0IsS0FBSyxPQUFMLENBQWEsV0FBN0IsRUFBMEMsUUFBMUMsQ0FBbUQsS0FBSyxPQUFMLENBQWEsd0JBQWhFLENBQXJCO0FBQ0EsYUFBSyxhQUFMO0FBQ0EsVUFBRSxLQUFLLEVBQVAsRUFBVyxTQUFYLENBQXFCLG1DQUFyQjtBQUNBLFlBQUksQ0FBQyxLQUFLLEVBQUwsQ0FBUSxRQUFSLENBQWlCLGVBQWpCLENBQUwsRUFBd0M7QUFDdEMsZUFBSyx1QkFBTDtBQUNBLGVBQUssaUJBQUw7QUFDRDtBQUNEO0FBQ0Q7QUFDRCxVQUFJLFNBQVMsS0FBSyxFQUFMLENBQVEsSUFBUixDQUFhLGFBQWIsRUFBNEIsTUFBekM7QUFDQSxXQUFLLEVBQUwsQ0FBUSxRQUFSLENBQWlCLFdBQVcsTUFBNUI7QUFDQSxXQUFLLEVBQUwsQ0FBUSxPQUFSLENBQWdCLEtBQUssT0FBTCxDQUFhLFdBQTdCLEVBQTBDLFFBQTFDLENBQW1ELEtBQUssT0FBTCxDQUFhLFFBQWhFO0FBQ0EsV0FBSyxrQkFBTDtBQUNBLFdBQUssaUJBQUw7QUFDQSxXQUFLLGNBQUw7QUFDQSxXQUFLLGNBQUw7QUFDQSxVQUFJLENBQUMsS0FBSyxFQUFMLENBQVEsUUFBUixDQUFpQixlQUFqQixDQUFMLEVBQXdDO0FBQ3RDLGFBQUssdUJBQUw7QUFDRDtBQUNELFdBQUssY0FBTCxHQUFzQixDQUF0QjtBQUNBLFdBQUssZUFBTCxHQUF1QixLQUFLLGVBQUwsQ0FBcUIsSUFBckIsQ0FBMEIsSUFBMUIsQ0FBdkI7QUFDRDs7O29DQUVlO0FBQ2QsVUFBSSxrQkFBa0IsT0FBTyxRQUFQLENBQWdCLElBQXRDO0FBQ0EsV0FBSyxFQUFMLENBQVEsSUFBUixDQUFhLGlCQUFiLEVBQWdDLElBQWhDLENBQXFDLFVBQUMsS0FBRCxFQUFRLEVBQVIsRUFBZTtBQUNsRCxZQUFJLEVBQUUsRUFBRixFQUFNLElBQU4sQ0FBVyxNQUFYLE1BQXVCLGVBQTNCLEVBQTRDO0FBQzFDLFlBQUUsRUFBRixFQUFNLFFBQU4sQ0FBZSxRQUFmO0FBQ0Q7QUFDRixPQUpEO0FBS0Q7Ozt5Q0FFb0I7QUFDbkIsV0FBSyxFQUFMLENBQVEsSUFBUixDQUFhLGlCQUFiLEVBQWdDLElBQWhDLENBQXFDLFVBQUMsS0FBRCxFQUFRLEVBQVIsRUFBZTtBQUNsRCxZQUFJLEVBQUUsRUFBRixFQUFNLElBQU4sQ0FBVyxNQUFYLEVBQW1CLE9BQW5CLENBQTJCLEdBQTNCLE1BQW9DLENBQXhDLEVBQTJDO0FBQ3pDLFlBQUUsRUFBRixFQUFNLFFBQU4sQ0FBZSxvQkFBZjtBQUNEO0FBQ0YsT0FKRDtBQUtEOzs7d0NBRW1CO0FBQ2xCLFVBQUksV0FBVyxLQUFLLEVBQUwsQ0FBUSxJQUFSLENBQWEsNEJBQWIsQ0FBZjtBQUFBLFVBQ0ksT0FBTyxLQUFLLEVBQUwsQ0FBUSxJQUFSLENBQWEsa0JBQWIsQ0FEWDs7QUFHQSxVQUFJLFNBQVMsTUFBVCxJQUFtQixTQUFTLElBQVQsQ0FBYyxNQUFkLEVBQXNCLE9BQXRCLENBQThCLEdBQTlCLE1BQXVDLENBQTlELEVBQWlFO0FBQy9ELGlCQUFTLFFBQVQsQ0FBa0IsZ0JBQWxCLEVBQW9DLE9BQXBDLENBQTRDLElBQTVDLEVBQWtELFFBQWxELENBQTJELFdBQTNEO0FBQ0Q7QUFDRCxVQUFJLEtBQUssTUFBVCxFQUFpQjtBQUNmLGFBQUssTUFBTCxHQUFjLFFBQWQsQ0FBdUIsV0FBdkI7QUFDRDtBQUNGOzs7cUNBRWdCO0FBQ2YsVUFBSSxNQUFNLEVBQUUsUUFBRixDQUFXLEtBQUssT0FBTCxDQUFhLFNBQXhCLENBQVY7QUFBQSxVQUNJLFFBQVEsS0FBSyxFQUFMLENBQVEsSUFBUixDQUFhLFFBQWIsRUFBdUIsS0FBdkIsR0FBK0IsSUFBL0IsRUFEWjtBQUFBLFVBRUksTUFBTSxLQUFLLEVBQUwsQ0FBUSxJQUFSLENBQWEsNEJBQWIsRUFBMkMsS0FBM0MsR0FBbUQsSUFBbkQsRUFGVjtBQUFBLFVBR0ksY0FBYyxLQUFLLEVBQUwsQ0FBUSxJQUFSLENBQWEsNkJBQWIsQ0FIbEI7QUFBQSxVQUlJLGNBQWMsd0RBQXdELFlBQVksSUFBWixFQUF4RCxHQUE2RSxNQUovRjs7QUFNQSxXQUFLLEVBQUwsQ0FBUSxNQUFSLENBQWUsSUFBSSxFQUFDLFlBQUQsRUFBUSxRQUFSLEVBQWEsd0JBQWIsRUFBSixDQUFmO0FBQ0Q7OztxQ0FFZ0I7QUFBQTs7QUFDZixXQUFLLFdBQUwsR0FBbUIsS0FBSyxFQUFMLENBQVEsSUFBUixDQUFhLGNBQWIsQ0FBbkI7QUFDQSxXQUFLLElBQUwsR0FBWSxLQUFLLEVBQUwsQ0FBUSxJQUFSLENBQWEsT0FBYixDQUFaO0FBQ0EsV0FBSyxTQUFMLEdBQWlCLEtBQUssRUFBTCxDQUFRLElBQVIsQ0FBYSxhQUFiLENBQWpCO0FBQ0EsV0FBSyxjQUFMLEdBQXNCLEtBQUssRUFBTCxDQUFRLElBQVIsQ0FBYSxzSEFBYixDQUF0QjtBQUNBLFdBQUssY0FBTCxHQUFzQixLQUFLLGdCQUFMLEVBQXRCOztBQUVBLFdBQUssV0FBTCxDQUFpQixFQUFqQixDQUFvQixPQUFwQixFQUE2QixVQUFDLEVBQUQsRUFBUTtBQUNuQyxZQUFJLE9BQUssU0FBTCxDQUFlLFFBQWYsQ0FBd0IsUUFBeEIsQ0FBSixFQUF1QztBQUNyQyxpQkFBSyxhQUFMO0FBQ0QsU0FGRCxNQUVPO0FBQ0wsaUJBQUssV0FBTDtBQUNEO0FBQ0YsT0FORDs7QUFRQSxXQUFLLGNBQUwsQ0FBb0IsRUFBcEIsQ0FBdUIsT0FBdkIsRUFBZ0MsVUFBQyxDQUFELEVBQU87QUFDckMsZUFBSyxXQUFMLENBQWlCLElBQWpCLENBQXNCLEVBQUUsRUFBRSxhQUFKLEVBQW1CLElBQW5CLEVBQXRCO0FBQ0EsZUFBSyxhQUFMO0FBQ0EsZ0JBQVEsUUFBUixDQUFpQixRQUFqQixDQUEwQixFQUFFLEVBQUUsYUFBSixFQUFtQixJQUFuQixDQUF3QixNQUF4QixDQUExQixFQUEyRCxPQUFLLG1CQUFMLENBQXlCLElBQXpCLFFBQTNELEVBQWdHLElBQWhHO0FBQ0EsVUFBRSxjQUFGO0FBQ0QsT0FMRDs7QUFPQSxRQUFFLE1BQUYsRUFBVSxFQUFWLENBQWEsUUFBYixFQUF1QixZQUFNO0FBQzNCLFlBQUksQ0FBQyxRQUFRLFFBQVIsQ0FBaUIsU0FBdEIsRUFBaUM7QUFDL0IsaUJBQUssbUJBQUw7QUFDRDtBQUNGLE9BSkQ7QUFLRDs7OzBDQUVxQjtBQUNwQixVQUFJLG1CQUFtQixLQUFLLEVBQUwsQ0FBUSxXQUFSLEVBQXZCO0FBQUEsVUFDSSxzQkFESjtBQUVBLFdBQUssSUFBSSxJQUFJLEtBQUssY0FBTCxDQUFvQixNQUFwQixHQUE2QixDQUExQyxFQUE2QyxLQUFLLENBQWxELEVBQXFELEdBQXJELEVBQTBEO0FBQ3hELFlBQUksYUFBYSxLQUFLLGNBQUwsQ0FBb0IsQ0FBcEIsRUFBdUIsQ0FBdkIsRUFBMEIscUJBQTFCLEVBQWpCOztBQUVBLFlBQUksV0FBVyxHQUFYLEdBQWlCLGdCQUFyQixFQUF1QztBQUNyQyxjQUFLLE1BQU0sS0FBSyxjQUFMLENBQW9CLE1BQXBCLEdBQTZCLENBQXhDLEVBQTRDO0FBQzFDLDRCQUFnQixLQUFLLGNBQUwsQ0FBb0IsQ0FBcEIsQ0FBaEI7QUFDQTtBQUNELFdBSEQsTUFHTztBQUNMLGdCQUFJLFdBQVcsTUFBWCxHQUFvQixnQkFBeEIsRUFBMEM7QUFDeEMsOEJBQWdCLEtBQUssY0FBTCxDQUFvQixDQUFwQixDQUFoQjtBQUNEO0FBQ0Q7QUFDRDtBQUNGO0FBQ0Y7QUFDRCxVQUFLLGFBQUwsRUFBcUI7QUFDbkIsWUFBSSxzQkFBc0IsY0FBYyxJQUFkLENBQW1CLFFBQW5CLENBQTFCO0FBQUEsWUFDSSxpQkFBaUIsS0FBSyxjQUFMLENBQW9CLE1BQXBCLENBQTJCLFVBQUMsS0FBRCxFQUFRLEVBQVIsRUFBZTtBQUN6RCxpQkFBTyxFQUFFLEVBQUYsRUFBTSxJQUFOLENBQVcsTUFBWCxNQUF1QixNQUFNLG1CQUFwQztBQUNELFNBRmdCLENBRHJCO0FBSUEsYUFBSyxjQUFMLENBQW9CLFdBQXBCLENBQWdDLFFBQWhDO0FBQ0EsYUFBSyxXQUFMLENBQWlCLElBQWpCLENBQXNCLGVBQWUsRUFBZixDQUFrQixDQUFsQixFQUFxQixJQUFyQixFQUF0QjtBQUNBLHVCQUFlLFFBQWYsQ0FBd0IsUUFBeEI7QUFDRCxPQVJELE1BUU87QUFDTCxhQUFLLGNBQUwsQ0FBb0IsV0FBcEIsQ0FBZ0MsUUFBaEM7QUFDRDtBQUNGOzs7a0NBRWE7QUFDWixRQUFFLE1BQUYsRUFBVSxFQUFWLENBQWEsT0FBYixFQUFzQixLQUFLLGVBQTNCO0FBQ0EsV0FBSyxTQUFMLENBQWUsUUFBZixDQUF3QixRQUF4QjtBQUNBLFdBQUssSUFBTCxDQUFVLFNBQVYsQ0FBb0IsR0FBcEI7QUFDRDs7O29DQUVlO0FBQ2QsUUFBRSxNQUFGLEVBQVUsR0FBVixDQUFjLE9BQWQsRUFBdUIsS0FBSyxlQUE1QjtBQUNBLFdBQUssU0FBTCxDQUFlLFdBQWYsQ0FBMkIsUUFBM0I7QUFDQSxXQUFLLElBQUwsQ0FBVSxPQUFWLENBQWtCLEdBQWxCO0FBQ0Q7Ozs4Q0FFeUI7QUFDeEIsK0JBQWMsS0FBSyxFQUFuQjtBQUNEOzs7d0NBRW1CO0FBQUE7O0FBQ2xCLFVBQU0sTUFBTSxLQUFLLGFBQUwsQ0FBbUIsSUFBbkIsQ0FBd0IsT0FBeEIsQ0FBWjtBQUNBLFVBQU0sZUFBZSxTQUFmLFlBQWUsR0FBTTtBQUN6QixZQUFJLE1BQUosQ0FBVyxPQUFLLEVBQUwsQ0FBUSxNQUFSLEVBQVg7QUFDRCxPQUZEO0FBR0EsUUFBRSxNQUFGLEVBQVUsRUFBVixDQUFhLGFBQWIsRUFBNEIsRUFBRSxRQUFGLENBQVc7QUFBQSxlQUFNLGNBQU47QUFBQSxPQUFYLEVBQWlDLEdBQWpDLENBQTVCO0FBQ0E7QUFDRDs7O29DQUVlLEMsRUFBRztBQUNqQixVQUFJLENBQUMsRUFBRSxFQUFFLE1BQUosRUFBWSxPQUFaLENBQW9CLGtCQUFwQixFQUF3QyxNQUE3QyxFQUFxRDtBQUNuRCxhQUFLLGFBQUw7QUFDRDtBQUNGOzs7dUNBRWtCO0FBQ2pCLFVBQUksV0FBVyxFQUFmO0FBQ0EsV0FBSyxFQUFMLENBQVEsSUFBUixDQUFhLG1EQUFiLEVBQWtFLElBQWxFLENBQXVFLFVBQUMsS0FBRCxFQUFRLEVBQVIsRUFBZTtBQUNwRixZQUFJLFdBQVcsRUFBRSxFQUFGLEVBQU0sSUFBTixDQUFXLE1BQVgsRUFBbUIsT0FBbkIsQ0FBMkIsR0FBM0IsRUFBZ0MsRUFBaEMsQ0FBZjtBQUFBLFlBQ0ksaUJBQWlCLEVBQUUsbUJBQW1CLFFBQW5CLEdBQThCLElBQWhDLENBRHJCO0FBRUEsdUJBQWUsTUFBZixJQUF5QixTQUFTLElBQVQsQ0FBYyxjQUFkLENBQXpCO0FBQ0QsT0FKRDtBQUtBLGFBQU8sUUFBUDtBQUNEOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNyTUg7O0FBQ0E7Ozs7Ozs7O0lBR2EsaUIsV0FBQSxpQjs7Ozs7d0JBSUk7QUFDYixhQUFPLEVBQUUsTUFBRixDQUFTLEVBQVQsdUhBQTZCO0FBQ2xDLHFCQUFhLFVBRHFCO0FBRWxDLGtCQUFVLDJCQUZ3QjtBQUdsQywrQkFBdUIsaUJBSFc7QUFJbEMsc0JBQWMsNkJBSm9CO0FBS2xDLHFCQUFhLG9CQUxxQjtBQU1sQyx3QkFBZ0IsWUFOa0I7QUFPbEMseUJBQWlCLGVBUGlCO0FBUWxDLGdDQUF3QixpQkFSVTtBQVNsQyx1QkFBZSxHQVRtQjtBQVVsQyx3QkFBZ0Isa0NBVmtCO0FBV2xDLDRCQUFvQixtQ0FDbEIsZ0NBRGtCLEdBRWxCO0FBYmdDLE9BQTdCLENBQVA7QUFlRDs7O3dCQWxCcUI7QUFBRSxhQUFPLHFDQUFQO0FBQStDOzs7QUFvQnZFLCtCQUFxQjtBQUFBOztBQUFBOztBQUFBLHNDQUFOLElBQU07QUFBTixVQUFNO0FBQUE7O0FBQUEsaUtBQ1YsSUFEVTs7QUFFbkIsVUFBSyxLQUFMO0FBRm1CO0FBR3BCOzs7OzRCQUVPOztBQUVOLFdBQUssbUJBQUw7QUFDQSxXQUFLLGlCQUFMO0FBQ0EsV0FBSyxnQkFBTDtBQUNBLFdBQUssVUFBTDtBQUNBLFdBQUssWUFBTCxHQUFvQixLQUFwQjtBQUNBLFdBQUssY0FBTDtBQUNBLFdBQUssV0FBTDtBQUNBLFdBQUssdUJBQUw7QUFDQSxXQUFLLGdCQUFMO0FBQ0EsV0FBSyx5QkFBTDtBQUNBLFVBQUksT0FBTyxJQUFYO0FBQ0EsV0FBSyxTQUFMLEdBQWlCLEVBQWpCOztBQUVBLFdBQUssS0FBTCxDQUFXLElBQVgsQ0FBZ0IsVUFBUyxLQUFULEVBQWdCLElBQWhCLEVBQXNCO0FBQ3BDLFlBQUksS0FBSyxJQUFULEVBQWU7QUFDYixlQUFLLFNBQUwsQ0FBZSxLQUFLLElBQXBCLElBQTRCLElBQTVCO0FBQ0Q7QUFDRixPQUpEOztBQU1BLFVBQUksQ0FBQyxLQUFLLGNBQUwsRUFBTCxFQUE0QjtBQUMxQixhQUFLLEtBQUwsQ0FBVyxLQUFYLEdBQW1CLE9BQW5CLENBQTJCLE9BQTNCLEVBQW9DLEVBQUUsY0FBYyxJQUFoQixFQUFwQztBQUNEO0FBQ0Y7O0FBRUQ7Ozs7dUNBQ21CO0FBQ2pCLFdBQUssU0FBTCxHQUFpQixLQUFLLEVBQUwsQ0FBUSxJQUFSLENBQWEsS0FBSyxPQUFMLENBQWEsc0JBQTFCLENBQWpCO0FBQ0EsV0FBSyxLQUFMLEdBQWEsS0FBSyxTQUFMLENBQWUsSUFBZixDQUFvQixLQUFLLE9BQUwsQ0FBYSxhQUFqQyxDQUFiO0FBQ0EsV0FBSyxRQUFMLEdBQWdCLEtBQUssRUFBTCxDQUNiLE9BRGEsQ0FDTCxLQUFLLE9BQUwsQ0FBYSxlQURSLEVBRWIsT0FGYSxDQUVMLEtBQUssT0FBTCxDQUFhLGVBRlIsRUFHYixLQUhhLENBR1AsQ0FITyxFQUdKLEtBQUssS0FBTCxDQUFXLE1BSFAsRUFJYixRQUphLENBSUosS0FBSyxPQUFMLENBQWEsWUFKVCxDQUFoQjs7QUFNQSxXQUFLLEVBQUwsQ0FBUSxPQUFSLENBQWdCLEtBQUssT0FBTCxDQUFhLFdBQTdCLEVBQTBDLFFBQTFDLENBQW1ELEtBQUssT0FBTCxDQUFhLFFBQWhFO0FBQ0Q7OztpQ0FFWTtBQUNYLFdBQUssS0FBTCxDQUFXLElBQVgsQ0FBZ0IsVUFBQyxLQUFELEVBQVEsRUFBUixFQUFlO0FBQzdCLFVBQUUsRUFBRixFQUFNLFNBQU4sQ0FBZ0IsZUFBaEI7QUFDRCxPQUZEO0FBR0Q7OzttQ0FFYyxJLEVBQU0sVyxFQUFhO0FBQ2hDLFVBQUksTUFBTSxLQUFLLGNBQUwsQ0FBb0IsSUFBcEIsQ0FBVjtBQUFBLFVBQ0UsT0FBTyxJQURUOztBQUdBLFVBQUksQ0FBQyxJQUFJLE1BQVQsRUFBaUI7QUFDZixlQUFPLEtBQVA7QUFDRDs7QUFFRCxVQUFJLENBQUMsSUFBSSxNQUFKLEdBQWEsUUFBYixDQUFzQixLQUFLLE9BQUwsQ0FBYSxjQUFuQyxDQUFMLEVBQXlEO0FBQ3ZELFlBQUksSUFBSixDQUFTLFVBQVMsQ0FBVCxFQUFZLEdBQVosRUFBaUI7QUFDeEIsZUFBSyxnQkFBTCxDQUFzQixHQUF0QjtBQUNBLGVBQUssb0JBQUwsQ0FBMEIsR0FBMUI7QUFDQSxlQUFLLGdCQUFMLENBQXNCLEdBQXRCO0FBQ0QsU0FKRDtBQUtEOztBQUVELFVBQUksU0FBUyxFQUFFLEtBQUssRUFBUCxDQUFiO0FBQUEsVUFDRSxrQkFBa0IsS0FBSyx1QkFBTCxNQUFrQyxDQUR0RDtBQUFBLFVBRUUsWUFBWSxPQUFPLE1BQVAsR0FBZ0IsR0FBaEIsR0FBc0IsZUFGcEM7QUFHQSxVQUFJLENBQUMsT0FBTyxRQUFQLENBQWdCLG1CQUFoQixDQUFELElBQXlDLFdBQTdDLEVBQTBEO0FBQ3hELGVBQU8sVUFBUCxDQUFrQixZQUFXO0FBQzNCLGtCQUFRLFFBQVIsQ0FBaUIsVUFBakIsQ0FBNEIsU0FBNUIsRUFBdUMsSUFBdkM7QUFDRCxTQUZELEVBRUcsR0FGSDtBQUdEOztBQUVELGFBQU8sSUFBUDtBQUNEOzs7bUNBRWMsSSxFQUFNO0FBQ25CLGFBQU8sUUFBUSxPQUFPLFFBQVAsQ0FBZ0IsSUFBL0I7QUFDQSxhQUFPLEVBQUUsRUFBRSxJQUFGLENBQU8sS0FBSyxXQUFaLEVBQXlCLFVBQVMsSUFBVCxFQUFlO0FBQy9DLGVBQU8sUUFBUyxLQUFLLElBQUwsS0FBYyxJQUE5QjtBQUNELE9BRlEsQ0FBRixDQUFQO0FBR0Q7OzswQ0FFcUI7QUFDcEIsVUFBSSxNQUFNLEVBQUUsUUFBRixDQUFXLEtBQUssT0FBTCxDQUFhLGtCQUF4QixDQUFWO0FBQ0EsV0FBSyxFQUFMLENBQVEsSUFBUixDQUFhLElBQWIsRUFBbUIsTUFBbkIsQ0FBMEIsR0FBMUI7QUFDRDs7O3dDQUVtQjtBQUNsQixXQUFLLEVBQUwsQ0FBUSxJQUFSLENBQWEsSUFBYixFQUFtQixJQUFuQixDQUF3QixxQ0FBeEI7QUFDQSxXQUFLLGFBQUwsR0FBcUIsS0FBSyxFQUFMLENBQVEsSUFBUixDQUFhLGtCQUFiLENBQXJCO0FBQ0Q7Ozt1Q0FFa0I7QUFDakIsVUFBSSxXQUFXLEtBQUssRUFBTCxDQUFRLElBQVIsQ0FBYSxJQUFiLENBQWY7QUFDQSxVQUFJLFNBQVMsSUFBVCxDQUFjLFdBQWQsR0FBNEIsU0FBUyxLQUFULEVBQWhDLEVBQWtEO0FBQ2hELGFBQUssWUFBTCxHQUFvQixJQUFwQjtBQUNELE9BRkQsTUFFTztBQUNMLGFBQUssWUFBTCxHQUFvQixLQUFwQjtBQUNEO0FBQ0Y7Ozs4Q0FFeUI7QUFDeEIsVUFBSSxhQUFhLEtBQUssRUFBTCxDQUFRLE9BQVIsQ0FBZ0IsS0FBSyxPQUFMLENBQWEsZUFBN0IsRUFBOEMsT0FBOUMsQ0FBc0QsZUFBdEQsRUFBdUUsR0FBdkUsQ0FBMkUsY0FBM0UsQ0FBakI7QUFBQSxVQUNFLGlCQUFpQixXQUFXLFFBQVgsR0FBc0IsSUFBdEIsQ0FBMkIsc0RBQTNCLEVBQW1GLEdBQW5GLENBQXVGLGdCQUF2RixDQURuQjtBQUFBLFVBRUUsWUFBWSxDQUZkOztBQUlBLFVBQUksZUFBZSxNQUFmLEdBQXdCLENBQTVCLEVBQStCO0FBQzdCLG9CQUFZLEVBQUUsZUFBZSxFQUFmLENBQWtCLENBQWxCLENBQUYsRUFBd0IsTUFBeEIsRUFBWjtBQUNEO0FBQ0QsYUFBTyxTQUFQO0FBQ0Q7OztxQ0FFZ0I7QUFBQTs7QUFDZixXQUFLLFdBQUwsR0FBbUIsS0FBSyxFQUFMLENBQVEsSUFBUixDQUFhLEdBQWIsQ0FBbkI7QUFDQSxVQUFJLE9BQU8sSUFBWDtBQUFBLFVBQ0UsY0FBYyxFQUFFLFlBQUYsQ0FEaEI7O0FBR0E7QUFDQSxRQUFFLE1BQUYsRUFBVSxFQUFWLENBQWEsT0FBYixFQUFzQixHQUF0QixFQUEyQixVQUFDLENBQUQsRUFBTztBQUNoQyxZQUFJLE9BQU8sRUFBRSxhQUFiO0FBQ0EsWUFBSSxLQUFLLFdBQUwsQ0FBaUIsS0FBakIsQ0FBdUIsSUFBdkIsS0FBZ0MsQ0FBQyxDQUFyQyxFQUF3QztBQUN0QyxjQUFJLEtBQUssSUFBTCxJQUFjLEtBQUssSUFBTCxJQUFhLEtBQUssU0FBcEMsRUFBZ0Q7QUFDOUMsaUJBQUssY0FBTCxDQUFvQixLQUFLLElBQXpCLEVBQStCLElBQS9CO0FBQ0EsbUJBQU8sUUFBUCxDQUFnQixJQUFoQixHQUF1QixFQUFFLElBQUYsRUFBUSxJQUFSLENBQWEsTUFBYixDQUF2QjtBQUNBLGNBQUUsY0FBRjtBQUNEO0FBQ0Y7QUFDRixPQVREOztBQVdBLFdBQUssV0FBTCxDQUFpQixFQUFqQixDQUFvQixPQUFwQixFQUE2QixVQUFDLENBQUQsRUFBSSxNQUFKLEVBQWU7QUFDMUMsWUFBSSxVQUFVLEVBQUUsRUFBRSxhQUFKLENBQWQ7QUFDQSxlQUFLLGdCQUFMLENBQXNCLE9BQXRCO0FBQ0EsZUFBSyxvQkFBTCxDQUEwQixPQUExQjtBQUNBLGVBQUssZ0JBQUwsQ0FBc0IsT0FBdEI7O0FBRUEsWUFBSSxDQUFDLE1BQUQsSUFBVyxDQUFDLE9BQU8sWUFBdkIsRUFBcUM7QUFDbkMsY0FBSSxRQUFRLElBQVIsQ0FBYSxNQUFiLEVBQXFCLE1BQXJCLEdBQThCLENBQWxDLEVBQXFDO0FBQ25DLG1CQUFPLFFBQVAsQ0FBZ0IsSUFBaEIsR0FBdUIsUUFBUSxJQUFSLENBQWEsTUFBYixDQUF2QjtBQUNEO0FBQ0QsY0FBSSxTQUFTLEVBQUUsT0FBSyxFQUFQLENBQWI7QUFBQSxjQUNFLGtCQUFrQixLQUFLLHVCQUFMLE1BQWtDLENBRHREO0FBQUEsY0FFRSxZQUFZLE9BQU8sTUFBUCxHQUFnQixHQUFoQixHQUFzQixlQUZwQztBQUdBLGlCQUFPLFVBQVAsQ0FBa0IsWUFBVztBQUMzQixvQkFBUSxRQUFSLENBQWlCLFVBQWpCLENBQTRCLFNBQTVCLEVBQXVDLElBQXZDO0FBQ0QsV0FGRCxFQUVHLENBRkg7QUFHRDs7QUFFRCxVQUFFLE1BQUYsRUFBVSxPQUFWLENBQWtCLFFBQWxCO0FBRUQsT0FwQkQ7O0FBc0JBLFdBQUssV0FBTCxDQUFpQixFQUFqQixDQUFvQixZQUFwQixFQUFrQyxVQUFDLENBQUQsRUFBSSxNQUFKLEVBQWU7QUFDL0MsWUFBSSxVQUFVLEVBQUUsRUFBRSxhQUFKLENBQWQ7O0FBRUEsWUFBSSxLQUFLLFlBQVQsRUFBdUI7QUFDckIscUJBQVcsWUFBVztBQUNwQixpQkFBSyxTQUFMLENBQWUsT0FBZjtBQUNELFdBRkQsRUFFRyxHQUZIO0FBR0Q7QUFFRixPQVREOztBQVlBLFFBQUUsTUFBRixFQUFVLEVBQVYsQ0FBYSxRQUFiLEVBQXVCLFlBQVc7QUFDaEMsWUFBSSxPQUFPLElBQVg7QUFDQSxtQkFBVyxZQUFXO0FBQ3BCLGNBQUksYUFBYSxLQUFLLEVBQUwsQ0FBUSxJQUFSLENBQWEsTUFBTSxLQUFLLE9BQUwsQ0FBYSxjQUFoQyxFQUFnRCxJQUFoRCxDQUFxRCxHQUFyRCxDQUFqQjtBQUNBLGVBQUssZ0JBQUw7QUFDQSxlQUFLLGNBQUw7QUFDQSxlQUFLLGlCQUFMLENBQXVCLFVBQXZCO0FBQ0QsU0FMRCxFQUtHLEdBTEg7QUFNRCxPQVJzQixDQVFyQixJQVJxQixDQVFoQixJQVJnQixDQUF2Qjs7QUFVQSxRQUFFLE1BQUYsRUFBVSxJQUFWLENBQWUsTUFBZixFQUF1QixZQUFXO0FBQ2hDLFlBQUksYUFBYSxLQUFLLEVBQUwsQ0FBUSxJQUFSLENBQWEsTUFBTSxLQUFLLE9BQUwsQ0FBYSxjQUFoQyxFQUFnRCxJQUFoRCxDQUFxRCxHQUFyRCxDQUFqQjs7QUFFQSxhQUFLLGdCQUFMLENBQXNCLFVBQXRCO0FBQ0EsYUFBSyxnQkFBTDtBQUNBLGFBQUssY0FBTDtBQUNBLGFBQUssaUJBQUwsQ0FBdUIsVUFBdkI7QUFDRCxPQVBzQixDQU9yQixJQVBxQixDQU9oQixJQVBnQixDQUF2Qjs7QUFTQSxrQkFBWSxFQUFaLENBQWUsQ0FDYixRQURhLEVBRWIsV0FGYSxFQUdiLE9BSGEsRUFJYixnQkFKYSxFQUtiLFlBTGEsRUFNYixPQU5hLEVBT2IsV0FQYSxFQVFiLElBUmEsQ0FRUixHQVJRLENBQWYsRUFRYSxTQUFTLGtCQUFULEdBQThCO0FBQ3pDLG9CQUFZLElBQVo7QUFDRCxPQVZEO0FBV0Q7OztxQ0FFZ0IsRSxFQUFJO0FBQ25CLFVBQUksWUFBWSxFQUFFLEVBQUYsQ0FBaEI7QUFBQSxVQUNFLFFBQVEsS0FBSyxLQUFMLENBQVcsS0FBWCxDQUFpQixTQUFqQixDQURWOztBQUdBLFVBQUksU0FBUyxDQUFDLENBQWQsRUFBaUI7QUFDZixhQUFLLFdBQUwsQ0FDRyxPQURILENBQ1csSUFEWCxFQUVHLFdBRkgsQ0FFZSxLQUFLLE9BQUwsQ0FBYSxjQUY1QjtBQUdBLFVBQUUsS0FBSyxLQUFMLENBQVcsTUFBWCxDQUFrQixJQUFsQixFQUF3QixFQUF4QixDQUEyQixLQUEzQixDQUFGLEVBQ0csUUFESCxDQUNZLEtBQUssT0FBTCxDQUFhLGNBRHpCO0FBRUQ7QUFDRjs7O3lDQUVvQixFLEVBQUk7QUFDdkIsVUFBSSxjQUFjLEVBQUUsRUFBRixDQUFsQjtBQUFBLFVBQ0UsUUFBUSxLQUFLLEtBQUwsQ0FBVyxLQUFYLENBQWlCLFdBQWpCLENBRFY7O0FBR0EsVUFBSSxTQUFTLENBQUMsQ0FBZCxFQUFpQjtBQUNmLGFBQUssUUFBTCxDQUNHLFdBREgsQ0FDZSxLQUFLLE9BQUwsQ0FBYSxXQUQ1QixFQUVHLEVBRkgsQ0FFTSxLQUZOLEVBR0csUUFISCxDQUdZLEtBQUssT0FBTCxDQUFhLFdBSHpCO0FBSUQ7QUFDRjs7O3FDQUVnQixFLEVBQUk7QUFDbkIsVUFBSSxjQUFjLEVBQUUsRUFBRixDQUFsQjtBQUFBLFVBQ0UsUUFBUSxLQUFLLEtBQUwsQ0FBVyxLQUFYLENBQWlCLFdBQWpCLENBRFY7QUFBQSxVQUVFLFdBQVcsS0FBSyxFQUFMLENBQVEsSUFBUixDQUFhLDZCQUFiLENBRmI7QUFBQSxVQUdFLFdBQVcsS0FBSyxFQUFMLENBQVEsSUFBUixDQUFhLElBQWIsQ0FIYjtBQUFBLFVBSUUsZUFBZSxTQUFTLFlBQVksTUFBWixHQUFxQixJQUFyQixHQUE0QixTQUFTLENBQVQsRUFBWSxxQkFBWixHQUFvQyxJQUFoRSxHQUF1RSxTQUFTLEdBQVQsQ0FBYSxjQUFiLEVBQTZCLE9BQTdCLENBQXFDLElBQXJDLEVBQTJDLEVBQTNDLENBQWhGLElBQWtJLElBSm5KO0FBQUEsVUFLRSxjQUFjLFNBQVMsWUFBWSxLQUFaLEVBQVQsSUFBZ0MsSUFMaEQ7O0FBT0EsVUFBSSxTQUFTLENBQUMsQ0FBZCxFQUFpQjtBQUNmLGlCQUFTLEdBQVQsQ0FBYSxPQUFiLEVBQXNCLFdBQXRCLEVBQW1DLEdBQW5DLENBQXVDLGFBQXZDLEVBQXNELFlBQXREO0FBQ0Q7QUFDRjs7OzhCQUVTLEUsRUFBSTtBQUNaLFVBQUksTUFBTSxFQUFFLEVBQUYsQ0FBVjtBQUFBLFVBQ0UsZ0JBREY7O0FBR0EsZ0JBQVUsU0FBUyxFQUFFLEdBQUYsRUFBTyxNQUFQLEdBQWdCLElBQWhCLEdBQXdCLENBQUMsS0FBSyxhQUFMLENBQW1CLEtBQW5CLEtBQTZCLElBQUksVUFBSixDQUFlLElBQWYsQ0FBOUIsSUFBc0QsQ0FBOUUsR0FBbUYsS0FBSyxhQUFMLENBQW1CLFVBQW5CLEVBQTVGLENBQVY7O0FBRUEsV0FBSyxhQUFMLENBQW1CLElBQW5CLEdBQTBCLE9BQTFCLENBQWtDO0FBQ2hDLG9CQUFZO0FBRG9CLE9BQWxDLEVBRUcsR0FGSDtBQUlEOzs7K0JBRVUsRSxFQUFJO0FBQ2IsVUFBSSxNQUFNLEVBQUUsRUFBRixDQUFWO0FBQUEsVUFDRSxZQUFZLFNBQVMsU0FBUyxJQUFULENBQWMsV0FBZCxHQUE0QixDQUE1QixHQUFnQyxJQUFJLEtBQUosS0FBYyxDQUF2RCxDQURkO0FBRUEsYUFBTyxTQUFQO0FBQ0Q7OztxQ0FFZ0I7QUFDZixVQUFJLFdBQVcsS0FBSyxFQUFMLENBQVEsSUFBUixDQUFhLElBQWIsQ0FBZjtBQUFBLFVBQ0UsY0FBYyxTQUFTLElBQVQsQ0FBYyxJQUFkLEVBQW9CLElBQXBCLEdBQTJCLEtBQTNCLEVBRGhCO0FBQUEsVUFFRSwwQkFGRjtBQUFBLFVBR0Usb0JBSEY7QUFBQSxVQUlFLG1CQUpGO0FBQUEsVUFLRSxvQkFMRjtBQUFBLFVBTUUsUUFBUSxTQUFTLElBQVQsQ0FBYyxJQUFkLENBTlY7O0FBUUEsVUFBSSxLQUFLLFlBQVQsRUFBdUI7QUFDckIsY0FBTSxJQUFOLENBQVcsVUFBUyxLQUFULEVBQWdCLEVBQWhCLEVBQW9CO0FBQzdCLGNBQUksU0FBUyxDQUFiLEVBQWdCO0FBQ2QsMEJBQWMsY0FBZDtBQUNBLHlCQUFhLGFBQWI7QUFDQSxnQ0FBb0IsS0FBSyxVQUFMLENBQWdCLEVBQWhCLENBQXBCO0FBQ0QsV0FKRCxNQUlPLElBQUksU0FBUyxXQUFiLEVBQTBCO0FBQy9CLDBCQUFjLGVBQWQ7QUFDQSx5QkFBYSxjQUFiO0FBQ0EsZ0NBQW9CLEtBQUssVUFBTCxDQUFnQixFQUFoQixDQUFwQjtBQUNELFdBSk0sTUFJQTtBQUNMO0FBQ0Q7O0FBRUQsd0JBQWMsU0FBUyxvQkFBb0IsU0FBUyxHQUFULENBQWEsV0FBYixFQUEwQixPQUExQixDQUFrQyxJQUFsQyxFQUF3QyxFQUF4QyxDQUE3QixJQUE0RSxJQUExRjtBQUNBLG1CQUFTLEdBQVQsQ0FBYSxVQUFiLEVBQXlCLFdBQXpCO0FBRUQsU0FoQlUsQ0FnQlQsSUFoQlMsQ0FnQkosSUFoQkksQ0FBWDtBQWtCRCxPQW5CRCxNQW1CTztBQUNMLFVBQUUsUUFBRixFQUFZLEdBQVosQ0FBZ0IsUUFBaEIsRUFBMEIsQ0FBMUI7QUFDRDtBQUVGOzs7c0NBRWlCLEksRUFBTTtBQUN0QixVQUFJLGFBQWEsRUFBRSxJQUFGLENBQWpCO0FBQUEsVUFDRSxPQUFPLElBRFQ7O0FBR0EsaUJBQVcsWUFBVztBQUNwQixhQUFLLFNBQUwsQ0FBZSxVQUFmO0FBQ0QsT0FGRCxFQUVHLEdBRkg7QUFHRDs7O2tDQUVhO0FBQ1osUUFBRSxLQUFLLEVBQVAsRUFBVyxTQUFYLENBQXFCLG9DQUFyQjtBQUNEOzs7OENBRXlCO0FBQ3hCLFVBQUksQ0FBQyxLQUFLLEVBQUwsQ0FBUSxRQUFSLENBQWlCLGVBQWpCLENBQUwsRUFBd0M7QUFDdEMsaUNBQWMsS0FBSyxFQUFMLENBQVEsSUFBUixDQUFhLEtBQUssT0FBTCxDQUFhLHFCQUExQixDQUFkO0FBQ0Q7QUFDRjs7O2dEQUUyQjtBQUMxQixVQUFJLE9BQU8sSUFBWDtBQUNBLFdBQUssUUFBTCxDQUFjLElBQWQsQ0FBbUIsWUFBVztBQUM1QixZQUFJLGNBQWMsRUFBRSxJQUFGLENBQWxCO0FBQUEsWUFDRSxrQkFBa0IsWUFBWSxJQUFaLENBQWlCLEtBQUssT0FBTCxDQUFhLGNBQTlCLENBRHBCO0FBQUEsWUFFRSxZQUFZLEVBRmQ7O0FBSUEsWUFBSSxnQkFBZ0IsTUFBaEIsSUFBMEIsZ0JBQWdCLENBQWhCLEVBQW1CLFNBQW5CLENBQTZCLE9BQTdCLENBQXFDLFlBQXJDLElBQXFELENBQUMsQ0FBcEYsRUFBdUY7QUFDckYsMEJBQWdCLElBQWhCLENBQXFCLFlBQVc7QUFDOUIsZ0JBQUksVUFBVSxLQUFLLFNBQUwsQ0FBZSxLQUFmLENBQXFCLEdBQXJCLENBQWQ7QUFDQSxpQkFBSyxJQUFJLElBQUksQ0FBYixFQUFnQixJQUFJLFFBQVEsTUFBNUIsRUFBb0MsR0FBcEMsRUFBeUM7QUFDdkMsa0JBQUksUUFBUSxDQUFSLEVBQVcsT0FBWCxDQUFtQixZQUFuQixJQUFtQyxDQUFDLENBQXhDLEVBQTJDO0FBQ3pDLDRCQUFZLFFBQVEsQ0FBUixDQUFaO0FBQ0Q7QUFDRjtBQUNGLFdBUEQ7QUFRQSxzQkFBWSxJQUFaLENBQWlCLFVBQWpCLEVBQTZCLFFBQTdCLENBQXNDLFNBQXRDO0FBQ0Q7QUFDRixPQWhCRDtBQWlCRDs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDaFdIOztBQUNBOzs7Ozs7OztJQUdhLFcsV0FBQSxXOzs7Ozt3QkFJSTtBQUNiLGFBQU8sRUFBRSxNQUFGLENBQVMsRUFBVCwyR0FBNkI7QUFDbEMscUJBQWEsVUFEcUI7QUFFbEMsa0JBQVUsbUJBRndCO0FBR2xDLCtCQUF1QixpQkFIVztBQUlsQyxzQkFBYyxhQUpvQjtBQUtsQyxxQkFBYSxvQkFMcUI7QUFNbEMsd0JBQWdCLFlBTmtCO0FBT2xDLHlCQUFpQixlQVBpQjtBQVFsQyxnQ0FBd0IsaUJBUlU7QUFTbEMsdUJBQWUsR0FUbUI7QUFVbEMsc0JBQWMsWUFWb0I7QUFXbEMsOEJBQXNCLDhCQVhZO0FBWWxDLHdCQUFnQixrQ0Faa0I7QUFhbEMsbUJBQVcsOEJBQ1QscUNBRFMsR0FFVCxvQkFGUyxHQUdULCtDQUhTLEdBSVQsUUFKUyxHQUtUO0FBbEJnQyxPQUE3QixDQUFQO0FBb0JEOzs7d0JBdkJxQjtBQUFFLGFBQU8sMkNBQVA7QUFBcUQ7OztBQXlCN0UseUJBQXFCO0FBQUE7O0FBQUE7O0FBQUEsc0NBQU4sSUFBTTtBQUFOLFVBQU07QUFBQTs7QUFBQSxxSkFDVixJQURVOztBQUVuQixVQUFLLEtBQUw7QUFGbUI7QUFHcEI7Ozs7NEJBRU87O0FBRU4sV0FBSyxjQUFMO0FBQ0EsV0FBSyxnQkFBTDtBQUNBLFdBQUssVUFBTDtBQUNBLFdBQUssUUFBTCxHQUFnQixLQUFoQjtBQUNBLFdBQUssY0FBTDtBQUNBLFdBQUssV0FBTDtBQUNBLFdBQUssdUJBQUw7QUFDQSxXQUFLLGVBQUwsR0FBdUIsS0FBSyxlQUFMLENBQXFCLElBQXJCLENBQTBCLElBQTFCLENBQXZCO0FBQ0EsV0FBSyxZQUFMO0FBQ0EsV0FBSyx5QkFBTDs7QUFFQSxVQUFJLE9BQU8sSUFBWDtBQUNBLFdBQUssU0FBTCxHQUFpQixFQUFqQjs7QUFFQSxXQUFLLEtBQUwsQ0FBVyxJQUFYLENBQWdCLFVBQVMsS0FBVCxFQUFnQixJQUFoQixFQUFzQjtBQUNwQyxZQUFJLEtBQUssSUFBVCxFQUFlO0FBQ2IsZUFBSyxTQUFMLENBQWUsS0FBSyxJQUFwQixJQUE0QixJQUE1QjtBQUNEO0FBQ0YsT0FKRDs7QUFNQSxVQUFJLENBQUMsS0FBSyxjQUFMLEVBQUwsRUFBNEI7QUFDMUIsYUFBSyxLQUFMLENBQVcsS0FBWCxHQUFtQixPQUFuQixDQUEyQixPQUEzQixFQUFvQyxFQUFFLGNBQWMsSUFBaEIsRUFBcEM7QUFDQSxhQUFLLFdBQUwsQ0FBaUIsS0FBakIsR0FBeUIsT0FBekIsQ0FBaUMsT0FBakMsRUFBMEMsRUFBRSxjQUFjLElBQWhCLEVBQTFDO0FBQ0Q7QUFDRjs7QUFFRDs7Ozt1Q0FDbUI7QUFDakIsV0FBSyxTQUFMLEdBQWlCLEtBQUssRUFBTCxDQUFRLElBQVIsQ0FBYSxLQUFLLE9BQUwsQ0FBYSxzQkFBMUIsQ0FBakI7QUFDQSxXQUFLLEtBQUwsR0FBYSxLQUFLLFNBQUwsQ0FBZSxJQUFmLENBQW9CLEtBQUssT0FBTCxDQUFhLGFBQWpDLENBQWI7QUFDQSxXQUFLLGVBQUwsR0FBdUIsS0FBSyxFQUFMLENBQVEsSUFBUixDQUFhLEtBQUssT0FBTCxDQUFhLG9CQUExQixDQUF2QjtBQUNBLFdBQUssV0FBTCxHQUFtQixLQUFLLGVBQUwsQ0FBcUIsSUFBckIsQ0FBMEIsS0FBSyxPQUFMLENBQWEsYUFBdkMsQ0FBbkI7QUFDQSxXQUFLLFFBQUwsR0FBZ0IsS0FBSyxFQUFMLENBQ2IsT0FEYSxDQUNMLEtBQUssT0FBTCxDQUFhLGVBRFIsRUFFYixPQUZhLENBRUwsS0FBSyxPQUFMLENBQWEsZUFGUixFQUdiLEtBSGEsQ0FHUCxDQUhPLEVBR0osS0FBSyxLQUFMLENBQVcsTUFIUCxFQUliLFFBSmEsQ0FJSixLQUFLLE9BQUwsQ0FBYSxZQUpULENBQWhCO0FBS0UsV0FBSyxFQUFMLENBQVEsT0FBUixDQUFnQixLQUFLLE9BQUwsQ0FBYSxXQUE3QixFQUEwQyxRQUExQyxDQUFtRCxLQUFLLE9BQUwsQ0FBYSxRQUFoRTtBQUNIOzs7aUNBRVk7QUFDWCxXQUFLLEtBQUwsQ0FBVyxJQUFYLENBQWdCLFVBQUMsS0FBRCxFQUFRLEVBQVIsRUFBZTtBQUM3QixVQUFFLEVBQUYsRUFBTSxTQUFOLENBQWdCLGVBQWhCO0FBQ0QsT0FGRDtBQUdEOzs7bUNBRWMsSSxFQUFNLFcsRUFBYTtBQUNoQyxVQUFJLE1BQU0sS0FBSyxjQUFMLENBQW9CLElBQXBCLENBQVY7QUFBQSxVQUNFLE9BQU8sSUFEVDs7QUFHQSxVQUFJLENBQUMsSUFBSSxNQUFULEVBQWlCO0FBQ2YsZUFBTyxLQUFQO0FBQ0Q7O0FBRUQsVUFBSSxDQUFDLElBQUksTUFBSixHQUFhLFFBQWIsQ0FBc0IsS0FBSyxPQUFMLENBQWEsY0FBbkMsQ0FBTCxFQUF5RDtBQUN2RCxZQUFJLElBQUosQ0FBUyxVQUFTLENBQVQsRUFBWSxHQUFaLEVBQWlCO0FBQ3hCLGVBQUssZ0JBQUwsQ0FBc0IsR0FBdEI7QUFDQSxlQUFLLG9CQUFMLENBQTBCLEdBQTFCO0FBQ0QsU0FIRDtBQUlEOztBQUVELFVBQUksU0FBUyxFQUFFLEtBQUssRUFBUCxDQUFiO0FBQUEsVUFDRSxrQkFBa0IsS0FBSyx1QkFBTCxNQUFrQyxDQUR0RDtBQUFBLFVBRUUsWUFBWSxPQUFPLE1BQVAsR0FBZ0IsR0FBaEIsR0FBc0IsZUFGcEM7QUFHQSxVQUFJLENBQUMsT0FBTyxRQUFQLENBQWdCLG1CQUFoQixDQUFELElBQXlDLFdBQTdDLEVBQTBEO0FBQ3hELGVBQU8sVUFBUCxDQUFrQixZQUFXO0FBQzNCLGtCQUFRLFFBQVIsQ0FBaUIsVUFBakIsQ0FBNEIsU0FBNUIsRUFBdUMsSUFBdkM7QUFDRCxTQUZELEVBRUcsR0FGSDtBQUdEOztBQUdELGFBQU8sSUFBUDtBQUNEOzs7bUNBRWMsSSxFQUFNO0FBQ25CLGFBQU8sUUFBUSxPQUFPLFFBQVAsQ0FBZ0IsSUFBL0I7QUFDQSxhQUFPLEVBQUUsRUFBRSxJQUFGLENBQU8sS0FBSyxXQUFaLEVBQXlCLFVBQVMsSUFBVCxFQUFlO0FBQy9DLGVBQU8sUUFBUyxLQUFLLElBQUwsS0FBYyxJQUE5QjtBQUNELE9BRlEsQ0FBRixDQUFQO0FBR0Q7OztxQ0FFZ0I7QUFDZixVQUFJLE1BQU0sRUFBRSxRQUFGLENBQVcsS0FBSyxPQUFMLENBQWEsU0FBeEIsQ0FBVjtBQUFBLFVBQ0UsUUFBUSxLQUFLLEVBQUwsQ0FBUSxJQUFSLENBQWEsSUFBYixFQUFtQixLQUFuQixDQUF5QixJQUF6QixFQUErQixJQUEvQixFQURWO0FBQUEsVUFFRSxXQUFXLEtBQUssRUFBTCxDQUFRLElBQVIsQ0FBYSxrQkFBYixDQUZiO0FBQUEsVUFHRSxjQUFjLDhEQUE4RCxTQUFTLElBQVQsRUFBOUQsR0FBZ0YsYUFIaEc7O0FBS0EsV0FBSyxFQUFMLENBQVEsTUFBUixDQUFlLElBQUksRUFBRSxZQUFGLEVBQVMsd0JBQVQsRUFBSixDQUFmO0FBQ0Q7OzttQ0FFYztBQUNiLFVBQUksT0FBTyxVQUFQLEdBQW9CLEdBQXhCLEVBQTZCO0FBQzNCLGFBQUssUUFBTCxHQUFnQixLQUFoQjtBQUNELE9BRkQsTUFFTztBQUNMLGFBQUssUUFBTCxHQUFnQixJQUFoQjtBQUNEO0FBQ0Y7OztxQ0FFZ0I7QUFBQTs7QUFDZixXQUFLLFdBQUwsR0FBbUIsS0FBSyxFQUFMLENBQVEsSUFBUixDQUFhLDRCQUFiLENBQW5CO0FBQ0EsV0FBSyxJQUFMLEdBQVksS0FBSyxFQUFMLENBQVEsSUFBUixDQUFhLE9BQWIsQ0FBWjtBQUNBLFdBQUssU0FBTCxHQUFpQixLQUFLLEVBQUwsQ0FBUSxJQUFSLENBQWEsY0FBYixDQUFqQjtBQUNBLFdBQUssV0FBTCxHQUFtQixLQUFLLEVBQUwsQ0FBUSxJQUFSLENBQWEsc0JBQWIsQ0FBbkI7QUFDQSxVQUFJLE9BQU8sSUFBWDtBQUFBLFVBQ0UsY0FBYyxFQUFFLFlBQUYsQ0FEaEI7O0FBR0E7QUFDQSxRQUFFLE1BQUYsRUFBVSxFQUFWLENBQWEsT0FBYixFQUFzQixHQUF0QixFQUEyQixVQUFDLENBQUQsRUFBTztBQUNoQyxZQUFJLE9BQU8sRUFBRSxhQUFiO0FBQ0EsWUFBSSxLQUFLLFdBQUwsQ0FBaUIsS0FBakIsQ0FBdUIsSUFBdkIsS0FBZ0MsQ0FBQyxDQUFyQyxFQUF3QztBQUN0QyxjQUFJLEtBQUssSUFBTCxJQUFjLEtBQUssSUFBTCxJQUFhLEtBQUssU0FBcEMsRUFBZ0Q7QUFDOUMsaUJBQUssY0FBTCxDQUFvQixLQUFLLElBQXpCLEVBQStCLElBQS9CO0FBQ0EsbUJBQU8sUUFBUCxDQUFnQixJQUFoQixHQUF1QixFQUFFLElBQUYsRUFBUSxJQUFSLENBQWEsTUFBYixDQUF2QjtBQUNBLGNBQUUsY0FBRjtBQUNEO0FBQ0Y7QUFDRixPQVREOztBQVdBLFdBQUssV0FBTCxDQUFpQixFQUFqQixDQUFvQixPQUFwQixFQUE2QixVQUFDLEVBQUQsRUFBUTtBQUNuQyxZQUFJLE9BQUssU0FBTCxDQUFlLFFBQWYsQ0FBd0IsT0FBSyxPQUFMLENBQWEsWUFBckMsQ0FBSixFQUF3RDtBQUN0RCxpQkFBSyxhQUFMO0FBQ0QsU0FGRCxNQUVPO0FBQ0wsaUJBQUssV0FBTDtBQUNEO0FBQ0YsT0FORDs7QUFRQSxXQUFLLFdBQUwsQ0FBaUIsRUFBakIsQ0FBb0IsT0FBcEIsRUFBNkIsVUFBQyxDQUFELEVBQUksTUFBSixFQUFlO0FBQzFDLFlBQUksVUFBVSxFQUFFLEVBQUUsYUFBSixDQUFkO0FBQ0EsZUFBSyxhQUFMO0FBQ0EsZUFBSyxnQkFBTCxDQUFzQixPQUF0QjtBQUNBLGVBQUssb0JBQUwsQ0FBMEIsT0FBMUI7O0FBRUEsWUFBSSxDQUFDLE1BQUQsSUFBVyxDQUFDLE9BQU8sWUFBdkIsRUFBcUM7QUFDbkMsY0FBSSxRQUFRLElBQVIsQ0FBYSxNQUFiLEVBQXFCLE1BQXJCLEdBQThCLENBQWxDLEVBQXFDO0FBQ25DLG1CQUFPLFFBQVAsQ0FBZ0IsSUFBaEIsR0FBdUIsUUFBUSxJQUFSLENBQWEsTUFBYixDQUF2QjtBQUNEO0FBQ0QsY0FBSSxTQUFTLEVBQUUsT0FBSyxFQUFQLENBQWI7QUFBQSxjQUNFLGtCQUFrQixLQUFLLHVCQUFMLE1BQWtDLENBRHREO0FBQUEsY0FFRSxZQUFZLE9BQU8sTUFBUCxHQUFnQixHQUFoQixHQUFzQixlQUZwQztBQUdBLGlCQUFPLFVBQVAsQ0FBa0IsWUFBVztBQUMzQixvQkFBUSxRQUFSLENBQWlCLFVBQWpCLENBQTRCLFNBQTVCLEVBQXVDLElBQXZDO0FBQ0QsV0FGRCxFQUVHLENBRkg7QUFHRDs7QUFFRDtBQUNBLFVBQUUsTUFBRixFQUFVLE9BQVYsQ0FBa0IsUUFBbEI7QUFFRCxPQXJCRDs7QUF1QkEsUUFBRSxNQUFGLEVBQVUsRUFBVixDQUFhLGFBQWIsRUFBNEIsWUFBVztBQUNyQyxhQUFLLFlBQUw7QUFDRCxPQUZEOztBQUlBLGtCQUFZLEVBQVosQ0FBZSxDQUNiLFFBRGEsRUFFYixXQUZhLEVBR2IsT0FIYSxFQUliLGdCQUphLEVBS2IsWUFMYSxFQU1iLE9BTmEsRUFPYixXQVBhLEVBUWIsSUFSYSxDQVFSLEdBUlEsQ0FBZixFQVFhLFNBQVMsa0JBQVQsR0FBOEI7QUFDekMsb0JBQVksSUFBWjtBQUNELE9BVkQ7QUFXRDs7O3FDQUVnQixFLEVBQUk7QUFDbkIsVUFBSSxZQUFZLEVBQUUsRUFBRixDQUFoQjtBQUFBLFVBQ0UsUUFBUSxLQUFLLFFBQUwsR0FBZ0IsS0FBSyxXQUFMLENBQWlCLEtBQWpCLENBQXVCLFNBQXZCLENBQWhCLEdBQW9ELEtBQUssS0FBTCxDQUFXLEtBQVgsQ0FBaUIsU0FBakIsQ0FEOUQ7O0FBR0EsVUFBSSxTQUFTLENBQUMsQ0FBZCxFQUFpQjtBQUNmLGFBQUssV0FBTCxDQUNHLE9BREgsQ0FDVyxJQURYLEVBRUcsV0FGSCxDQUVlLEtBQUssT0FBTCxDQUFhLGNBRjVCO0FBR0EsVUFBRSxLQUFLLEtBQUwsQ0FBVyxNQUFYLENBQWtCLElBQWxCLEVBQXdCLEVBQXhCLENBQTJCLEtBQTNCLENBQUYsRUFDRyxRQURILENBQ1ksS0FBSyxPQUFMLENBQWEsY0FEekI7QUFFQSxVQUFFLEtBQUssV0FBTCxDQUFpQixNQUFqQixDQUF3QixJQUF4QixFQUE4QixFQUE5QixDQUFpQyxLQUFqQyxDQUFGLEVBQ0csUUFESCxDQUNZLEtBQUssT0FBTCxDQUFhLGNBRHpCO0FBRUEsYUFBSyxXQUFMLENBQWlCLElBQWpCLENBQXNCLE1BQXRCLEVBQThCLElBQTlCLENBQW1DLFVBQVUsSUFBVixFQUFuQztBQUNEO0FBQ0Y7Ozt5Q0FFb0IsRSxFQUFJO0FBQ3ZCLFVBQUksY0FBYyxFQUFFLEVBQUYsQ0FBbEI7QUFBQSxVQUNFLFFBQVEsS0FBSyxRQUFMLEdBQWdCLEtBQUssV0FBTCxDQUFpQixLQUFqQixDQUF1QixXQUF2QixDQUFoQixHQUFzRCxLQUFLLEtBQUwsQ0FBVyxLQUFYLENBQWlCLFdBQWpCLENBRGhFOztBQUdBLFVBQUksU0FBUyxDQUFDLENBQWQsRUFBaUI7QUFDYixhQUFLLFFBQUwsQ0FDRyxXQURILENBQ2UsS0FBSyxPQUFMLENBQWEsV0FENUIsRUFFRyxFQUZILENBRU0sS0FGTixFQUdHLFFBSEgsQ0FHWSxLQUFLLE9BQUwsQ0FBYSxXQUh6QjtBQUlIO0FBQ0Y7OztrQ0FFYTtBQUNaLFFBQUUsTUFBRixFQUFVLEVBQVYsQ0FBYSxPQUFiLEVBQXNCLEtBQUssZUFBM0I7QUFDQSxXQUFLLFNBQUwsQ0FBZSxRQUFmLENBQXdCLEtBQUssT0FBTCxDQUFhLFlBQXJDO0FBQ0EsV0FBSyxJQUFMLENBQVUsU0FBVixDQUFvQixHQUFwQjtBQUNEOzs7b0NBRWU7QUFDZCxRQUFFLE1BQUYsRUFBVSxHQUFWLENBQWMsT0FBZCxFQUF1QixLQUFLLGVBQTVCO0FBQ0EsV0FBSyxTQUFMLENBQWUsV0FBZixDQUEyQixLQUFLLE9BQUwsQ0FBYSxZQUF4QztBQUNBLFdBQUssSUFBTCxDQUFVLE9BQVYsQ0FBa0IsR0FBbEI7QUFDRDs7O2tDQUVhO0FBQ1osUUFBRSxLQUFLLEVBQVAsRUFBVyxTQUFYLENBQXFCLG9DQUFyQjtBQUNEOzs7OENBRXlCO0FBQ3hCLFVBQUksQ0FBQyxLQUFLLEVBQUwsQ0FBUSxRQUFSLENBQWlCLGVBQWpCLENBQUwsRUFBd0M7QUFDdEMsaUNBQWMsS0FBSyxFQUFMLENBQVEsSUFBUixDQUFhLEtBQUssT0FBTCxDQUFhLHFCQUExQixDQUFkO0FBQ0Q7QUFDRjs7O29DQUVlLEMsRUFBRztBQUNqQixVQUFJLENBQUMsRUFBRSxFQUFFLE1BQUosRUFBWSxPQUFaLENBQW9CLGtCQUFwQixFQUF3QyxNQUE3QyxFQUFxRDtBQUNuRCxhQUFLLGFBQUw7QUFDRDtBQUNGOzs7Z0RBRTJCO0FBQzFCLFVBQUksT0FBTyxJQUFYO0FBQ0EsV0FBSyxRQUFMLENBQWMsSUFBZCxDQUFtQixZQUFXO0FBQzVCLFlBQUksY0FBYyxFQUFFLElBQUYsQ0FBbEI7QUFBQSxZQUNFLGtCQUFrQixZQUFZLElBQVosQ0FBaUIsS0FBSyxPQUFMLENBQWEsY0FBOUIsQ0FEcEI7QUFBQSxZQUVFLFlBQVksRUFGZDs7QUFJQSxZQUFJLGdCQUFnQixNQUFoQixJQUEwQixnQkFBZ0IsQ0FBaEIsRUFBbUIsU0FBbkIsQ0FBNkIsT0FBN0IsQ0FBcUMsWUFBckMsSUFBcUQsQ0FBQyxDQUFwRixFQUF1RjtBQUNyRiwwQkFBZ0IsSUFBaEIsQ0FBcUIsWUFBVztBQUM5QixnQkFBSSxVQUFVLEtBQUssU0FBTCxDQUFlLEtBQWYsQ0FBcUIsR0FBckIsQ0FBZDtBQUNBLGlCQUFLLElBQUksSUFBSSxDQUFiLEVBQWdCLElBQUksUUFBUSxNQUE1QixFQUFvQyxHQUFwQyxFQUF5QztBQUN2QyxrQkFBSSxRQUFRLENBQVIsRUFBVyxPQUFYLENBQW1CLFlBQW5CLElBQW1DLENBQUMsQ0FBeEMsRUFBMkM7QUFDekMsNEJBQVksUUFBUSxDQUFSLENBQVo7QUFDRDtBQUNGO0FBQ0YsV0FQRDtBQVFBLHNCQUFZLElBQVosQ0FBaUIsVUFBakIsRUFBNkIsUUFBN0IsQ0FBc0MsU0FBdEM7QUFDRDtBQUNGLE9BaEJEO0FBaUJEOzs7OENBRXlCO0FBQ3hCLFVBQUksYUFBYSxLQUFLLEVBQUwsQ0FBUSxPQUFSLENBQWdCLEtBQUssT0FBTCxDQUFhLGVBQTdCLEVBQThDLE9BQTlDLENBQXNELGVBQXRELEVBQXVFLEdBQXZFLENBQTJFLGNBQTNFLENBQWpCO0FBQUEsVUFDRSxpQkFBaUIsV0FBVyxRQUFYLEdBQXNCLElBQXRCLENBQTJCLHNEQUEzQixFQUFtRixHQUFuRixDQUF1RixnQkFBdkYsQ0FEbkI7QUFBQSxVQUVFLFlBQVksQ0FGZDs7QUFJQSxVQUFJLGVBQWUsTUFBZixHQUF3QixDQUE1QixFQUErQjtBQUM3QixvQkFBWSxFQUFFLGVBQWUsRUFBZixDQUFrQixDQUFsQixDQUFGLEVBQXdCLE1BQXhCLEVBQVo7QUFDRDtBQUNELGFBQU8sU0FBUDtBQUNEIiwiZmlsZSI6ImdlbmVyYXRlZC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzQ29udGVudCI6WyIoZnVuY3Rpb24gZSh0LG4scil7ZnVuY3Rpb24gcyhvLHUpe2lmKCFuW29dKXtpZighdFtvXSl7dmFyIGE9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtpZighdSYmYSlyZXR1cm4gYShvLCEwKTtpZihpKXJldHVybiBpKG8sITApO3ZhciBmPW5ldyBFcnJvcihcIkNhbm5vdCBmaW5kIG1vZHVsZSAnXCIrbytcIidcIik7dGhyb3cgZi5jb2RlPVwiTU9EVUxFX05PVF9GT1VORFwiLGZ9dmFyIGw9bltvXT17ZXhwb3J0czp7fX07dFtvXVswXS5jYWxsKGwuZXhwb3J0cyxmdW5jdGlvbihlKXt2YXIgbj10W29dWzFdW2VdO3JldHVybiBzKG4/bjplKX0sbCxsLmV4cG9ydHMsZSx0LG4scil9cmV0dXJuIG5bb10uZXhwb3J0c312YXIgaT10eXBlb2YgcmVxdWlyZT09XCJmdW5jdGlvblwiJiZyZXF1aXJlO2Zvcih2YXIgbz0wO288ci5sZW5ndGg7bysrKXMocltvXSk7cmV0dXJuIHN9KSIsIid1c2Ugc3RyaWN0JztcclxuXHJcbmV4cG9ydCAqIGZyb20gJy4vc2Nyb2xsLXRvJztcclxuZXhwb3J0ICogZnJvbSAnLi9wYWdlLWNvbmZpZyc7XHJcbmV4cG9ydCAqIGZyb20gJy4vdXRpbHMnO1xyXG5leHBvcnQgKiBmcm9tICcuL21ldHJpY3MnO1xyXG5leHBvcnQgKiBmcm9tICcuL3NsaWRlci1oZWxwZXInO1xyXG4iLCJleHBvcnQgbGV0IGV2ZW50QnVzID0ge1xyXG4gIG9uKGV2ZW50TmFtZSwgaGFuZGxlcikge1xyXG4gICAgJCh0aGlzKS5vbihldmVudE5hbWUsIGhhbmRsZXIpO1xyXG4gICAgcmV0dXJuICgpID0+IHtcclxuICAgICAgdGhpcy5vZmYoZXZlbnROYW1lLCBoYW5kbGVyKTtcclxuICAgIH07XHJcbiAgfSxcclxuXHJcbiAgb2ZmKGV2ZW50TmFtZSwgaGFuZGxlcikge1xyXG4gICAgJCh0aGlzKS5vZmYoZXZlbnROYW1lLCBoYW5kbGVyKTtcclxuICB9LFxyXG5cclxuICB0cmlnZ2VyKGV2ZW50TmFtZSwgLi4uZGF0YSkge1xyXG4gICAgJCh0aGlzKS50cmlnZ2VySGFuZGxlcihldmVudE5hbWUsIFsuLi5kYXRhXSk7XHJcbiAgfVxyXG59O1xyXG4iLCIndXNlIHN0cmljdCc7XHJcbmltcG9ydCB7IEJhc2VDb21wb25lbnQgfSBmcm9tICcuLi8uLi9jb21wb25lbnRzL2Jhc2UtY29tcG9uZW50JztcclxuXHJcbmV4cG9ydCBjbGFzcyBNZXRyaWNzIGV4dGVuZHMgQmFzZUNvbXBvbmVudCB7XHJcbiAgZ2V0IGRlZmF1bHRzKCkge1xyXG4gICAgcmV0dXJuICQuZXh0ZW5kKHt9LCBzdXBlci5kZWZhdWx0cywge1xyXG4gICAgICBtZXRyaWNFdmVudCAgICA6IFwiY2xpY2tcIixcclxuICAgICAgdHJhY2tQcmVmaXggICAgICA6IFwiMi4wXCIsXHJcbiAgICAgIHRyYWNrTmFtZSAgICAgIDogXCJuZXcubGlua1wiLFxyXG4gICAgICBtZXRhVG9Db2xsZWN0IDogW1xyXG4gICAgICAgIHsgbmFtZSAgOiBcImJ1XCJ9LFxyXG4gICAgICAgIHsgbmFtZSA6IFwic3ViX2J1XCJ9LFxyXG4gICAgICAgIHsgbmFtZSA6IFwic2ltcGxlX3RpdGxlXCJ9XHJcbiAgICAgIF0sXHJcbiAgICAgIGF0dHJzVG9Db2xsZWN0IDogW1xyXG4gICAgICAgIHsgbmFtZSA6IFwiaWRlbnRpZmllclwiIH0sXHJcbiAgICAgICAgeyBuYW1lIDogXCJ0aXRsZVwiIH1cclxuICAgICAgXVxyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBjb25zdHJ1Y3RvciguLi5hcmdzKSB7XHJcbiAgICBzdXBlciguLi5hcmdzKTtcclxuICAgICQoKCkgPT4gdGhpcy5faW5pdCgpKTtcclxuICB9XHJcblxyXG4gIF9pbml0KCkge1xyXG4gICAgbGV0IG1ldGEgICAgICA9ICQoXCJtZXRhXCIpLFxyXG4gICAgICBtZXRhVmFsdWUgPSAnJyxcclxuICAgICAgc2VsZWN0b3IgPSAnLmZsZXgyLW1vbGVjdWxlW2RhdGEtbWV0cmljcy1pZGVudGlmaWVyXSAnICtcclxuICAgICAgICAgICAgJ2FbaHJlZl06bm90KFtocmVmKj1cInNsaWRlc2hhcmVcIl0sIFtocmVmPVwiI1wiXSwgW2hyZWYqPVwiamF2YXNjcmlwdDp2b2lkKDApXCJdKSwgJyArXHJcbiAgICAgICAgICAgICcuZmxleDItbW9sZWN1bGVbZGF0YS1tZXRyaWNzLWlkZW50aWZpZXJdIGEuanNfb3ZlcmxheV90cmlnZ2VyLCAnICtcclxuICAgICAgICAgICAgJy5mbGV4Mi1tb2xlY3VsZVtkYXRhLW1ldHJpY3MtaWRlbnRpZmllcl0gYS5hY3Rpb24tdHJpZ2dlciwgJyArXHJcbiAgICAgICAgICAgICcuZmxleDItbW9sZWN1bGVbZGF0YS1tZXRyaWNzLWlkZW50aWZpZXJdIGEuaWZyYW1lLXBvcHVwLXRyaWdnZXIsICcgK1xyXG4gICAgICAgICAgICAnLmpzX21lbnVfaG9sZGVyIGFbaHJlZl06bm90KFtocmVmPVwiI1wiXSwgW2hyZWYqPVwiamF2YXNjcmlwdDp2b2lkKDApXCJdKSwnICtcclxuICAgICAgICAgICAgJ2FbZGF0YS1tZXRyaWNzLWlkZW50aWZpZXJdJztcclxuICAgIHRoaXMubGFuZ3VhZ2VDb2RlID0gZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50Lmxhbmcuc3BsaXQoXCItXCIpWzBdOy8vamF8fHVzXHJcbiAgICB0aGlzLmNvdW50cnlDb2RlID0gIGRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5sYW5nLnNwbGl0KFwiLVwiKVsxXTsvL2pwfHxlblxyXG4gICAgdGhpcy5tZXRhVmFsdWVzID0gW107XHJcbiAgICB0aGlzLmxpbmtzID0gW107XHJcbiAgICB0aGlzLnRpbWVyID0gbnVsbDtcclxuXHJcbiAgICBmb3IodmFyIHEgPSAwOyBxIDwgdGhpcy5vcHRpb25zLm1ldGFUb0NvbGxlY3QubGVuZ3RoOyBxKyspe1xyXG4gICAgICBtZXRhVmFsdWUgPSBtZXRhLmZpbHRlcihcIipbbmFtZT0nXCIrIHRoaXMub3B0aW9ucy5tZXRhVG9Db2xsZWN0W3FdLm5hbWUgK1wiJ11cIikuYXR0cihcImNvbnRlbnRcIik7XHJcbiAgICAgIGlmIChtZXRhVmFsdWUpIHtcclxuICAgICAgICBpZiAodGhpcy5vcHRpb25zLm1ldGFUb0NvbGxlY3RbcV0ubmFtZSA9PSAnc2ltcGxlX3RpdGxlJykge1xyXG4gICAgICAgICAgaWYgKG1ldGFWYWx1ZS5sZW5ndGggPiA0MCkge1xyXG4gICAgICAgICAgICBtZXRhVmFsdWUgPSBtZXRhVmFsdWUuc3Vic3RyKDAsNDApO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLm1ldGFWYWx1ZXMucHVzaChtZXRhVmFsdWUpO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBpZiAodGhpcy5vcHRpb25zLm1ldGFUb0NvbGxlY3RbcV0ubmFtZSA9PSAnc3ViX2J1Jykge1xyXG4gICAgICAgIHRoaXMubWV0YVZhbHVlcy5wdXNoKHRoaXMuY291bnRyeUNvZGUpO1xyXG4gICAgICAgIHRoaXMubWV0YVZhbHVlcy5wdXNoKHRoaXMubGFuZ3VhZ2VDb2RlKTtcclxuICAgICAgfVxyXG5cclxuICAgIH1cclxuICAgICQoZG9jdW1lbnQpLm9uKCdjbGljaycsIHNlbGVjdG9yLCAoZSkgPT4ge1xyXG4gICAgICB0aGlzLmRlbGF5ZWRUcmFja0NhbGwoZSk7XHJcbiAgICB9KTtcclxuICAgICQoZG9jdW1lbnQpLm9uKCdtb3VzZWRvd24gdG91Y2hzdGFydCcsICcubWV0YWxvY2F0b3ItYnV5LXdyYXBwZXInLCAoZSkgPT4ge1xyXG4gICAgICB0aGlzLmRlbGF5ZWRUcmFja0NhbGwoZSk7XHJcbiAgICB9KTtcclxuXHJcbiAgICAkKGRvY3VtZW50KS5vbignc2VuZE1ldHJpYycsIGZ1bmN0aW9uIChlKSB7XHJcbiAgICAgIF9zZWxmLnNlbmRNZXRyaWMoZS4kbGluayk7ICAgICAgICAgICAgICAgICAgICBcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgZGVsYXllZFRyYWNrQ2FsbChlKSB7XHJcbiAgICBpZiAoIXRoaXMudGltZXIpe1xyXG4gICAgICB0aGlzLnRpbWVyID0gc2V0VGltZW91dCgoKSA9PiB7XHJcbiAgICAgICAgY2xlYXJUaW1lb3V0KHRoaXMudGltZXIpO1xyXG4gICAgICAgIHRoaXMudGltZXIgPSBudWxsO1xyXG4gICAgICB9LCAxMDApO1xyXG4gICAgICBpZiAoZS53aGljaCAhPT0gMyl7XHJcbiAgICAgICAgdGhpcy5zZW5kTWV0cmljKCAkKGUuY3VycmVudFRhcmdldCkgKTsgICAgICAgICAgICAgICAgICAgICAgICAgICAgXHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcblxyXG4gIHNlbmRNZXRyaWMoZWxlbWVudFRvVHJhY2spIHtcclxuICAgIGxldCBtZXRyaWNUYXJnZXRQcm9wcyA9IFtdLFxyXG4gICAgICBtZXRyaWNzVHlwZSAgICAgICA9ICcnLFxyXG4gICAgICBtZXRyaWNWYWx1ZSAgICAgICA9IFwiXCIsXHJcbiAgICAgIHByb3BzID0gdGhpcy5vcHRpb25zLmF0dHJzVG9Db2xsZWN0O1xyXG5cclxuICAgIG1ldHJpY1RhcmdldFByb3BzID0gdGhpcy5jb2xsZWN0VHJhY2tpbmdWYWx1ZShlbGVtZW50VG9UcmFjaywgcHJvcHMpO1xyXG4gICAgbWV0cmljVmFsdWUgPSAodGhpcy5vcHRpb25zLnRyYWNrUHJlZml4ICsgXCIvXCIgKyB0aGlzLm1ldGFWYWx1ZXMuam9pbihcIi9cIikgKyBcIi9cIiArIG1ldHJpY1RhcmdldFByb3BzKTtcclxuICAgIG1ldHJpY3NUeXBlID0gZWxlbWVudFRvVHJhY2suYXR0cignZGF0YS1tZXRyaWNzLWxpbmstdHlwZScpIHx8ICcnO1xyXG5cclxuICAgIC8vaGFuZGxlIGNvbGxhcHNlL2V4cGFuZCBsaW5rcywgaWdub3JlIHNlbmRpbmcgbWV0cmljcyBmb3IgZXhwYW5kZWQgc3RhdGVcclxuICAgIGlmIChlbGVtZW50VG9UcmFjay5hdHRyKCdkYXRhLWRpc2FibGUtbWV0cmljcycpKSB7XHJcbiAgICAgIHJldHVybjtcclxuICAgIH1cclxuXHJcbiAgICAvL3ZpZGVvcyAmIHBvcHVwc1xyXG4gICAgaWYgKGVsZW1lbnRUb1RyYWNrLmhhc0NsYXNzKCdqc19vdmVybGF5X3RyaWdnZXInKSB8fCBlbGVtZW50VG9UcmFjay5oYXNDbGFzcygnaWZyYW1lLXBvcHVwLXRyaWdnZXInKSkge1xyXG4gICAgICBtZXRyaWNzVHlwZSA9ICdsaW5rJztcclxuICAgIH1cclxuXHJcbiAgICBpZiAoIG1ldHJpY3NUeXBlICkge1xyXG4gICAgICB0cnkge1xyXG4gICAgICAgIGNvbnNvbGUubG9nKCd0cmFja01ldHJpY3M6JyArIHRoaXMub3B0aW9ucy50cmFja05hbWUgKyAnLCBuYW1lOiAnICsgbWV0cmljVmFsdWUgKyAnLCB0eXBlOiAnICsgbWV0cmljc1R5cGUpO1xyXG4gICAgICAgIHRyYWNrTWV0cmljcyh0aGlzLm9wdGlvbnMudHJhY2tOYW1lLCB7IG5hbWUgOiAgbWV0cmljVmFsdWUsIHR5cGUgOiBtZXRyaWNzVHlwZSB9KTtcclxuICAgICAgfSBjYXRjaCAoZXhjcHQpIHtcclxuICAgICAgICBjb25zb2xlLmxvZyhleGNwdC5tZXNzYWdlKTtcclxuICAgICAgfTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIGNvbnNvbGUubG9nKCd0cmFja01ldHJpY3MgaGFzIE5PVCBiZWV0IGNhbGxlZCBjYXVzZSBkYXRhLW1ldHJpY3MtbGluay10eXBlIHBhcmFtIGlzIG5vdCBzZXQgdG8gYSBsaW5rJyk7XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIHRydWU7XHJcbiAgfVxyXG5cclxuICBjb2xsZWN0VHJhY2tpbmdWYWx1ZSh0YXJnZXQsIHByb3BzKSB7XHJcblxyXG4gICAgbGV0IHZhbHVlQXJyYXkgPSBbXSxcclxuICAgICAgdmFsdWUgPSBcIlwiO1xyXG5cclxuICAgIGlmICh0YXJnZXQubGVuZ3RoID09PSAwKSB7XHJcbiAgICAgIHJldHVybiB2YWx1ZTtcclxuICAgIH1cclxuXHJcbiAgICBmb3IgKGxldCBpID0gMDsgaSA8IHByb3BzLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgIGxldCBtZXRyaWNDbnQgPSAnJztcclxuXHJcbiAgICAgIGlmIChwcm9wc1tpXS5uYW1lID09ICdpZGVudGlmaWVyJykge1xyXG4gICAgICAgIGlmICh0YXJnZXQucGFyZW50cygnLmZsZXgyLW1vbGVjdWxlW2RhdGEtbWV0cmljcy1pZGVudGlmaWVyXScpLmxlbmd0aCkge1xyXG4gICAgICAgICAgbWV0cmljQ250ID0gdGFyZ2V0LnBhcmVudHMoJy5mbGV4Mi1tb2xlY3VsZVtkYXRhLW1ldHJpY3MtaWRlbnRpZmllcl0nKS5hdHRyKCdkYXRhLW1ldHJpY3MtaWRlbnRpZmllcicpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICBpZiAodGFyZ2V0LmF0dHIoJ2RhdGEtbWV0cmljcy1pZGVudGlmaWVyJykgIT09ICd1bmRlZmluZWQnKSB7XHJcbiAgICAgICAgICAgIG1ldHJpY0NudCA9IHRhcmdldC5hdHRyKCdkYXRhLW1ldHJpY3MtaWRlbnRpZmllcicpO1xyXG4gICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgbWV0cmljQ250ID0gJyc7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcblxyXG4gICAgICBpZiAocHJvcHNbaV0ubmFtZSA9PSAndGl0bGUnKSB7XHJcblxyXG4gICAgICAgIGlmICh0YXJnZXQucGFyZW50cygnLmZsZXgyLW1vbGVjdWxlJykubGVuZ3RoKSB7XHJcbiAgICAgICAgICBtZXRyaWNDbnQgPSB0YXJnZXQucGFyZW50cygnLmZsZXgyLW1vbGVjdWxlJykuYXR0cihcImRhdGEtbWV0cmljcy1cIiArIHByb3BzW2ldLm5hbWUpXHJcbiAgICAgICAgfSBlbHNlIGlmICh0YXJnZXQucGFyZW50cygnLmpzX21lbnVfaG9sZGVyJykubGVuZ3RoKSB7XHJcbiAgICAgICAgICBtZXRyaWNDbnQgPSB0YXJnZXQucGFyZW50cygnLmpzX21lbnVfaG9sZGVyJykuYXR0cihcImRhdGEtbWV0cmljcy1cIiArIHByb3BzW2ldLm5hbWUpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgbGV0IHRhcmdldFRpdGxlID0gdGFyZ2V0LmF0dHIoXCJkYXRhLW1ldHJpY3MtXCIrIHByb3BzW2ldLm5hbWUpO1xyXG5cclxuICAgICAgICBpZiAodGFyZ2V0VGl0bGUpIHtcclxuICAgICAgICAgIGlmICh0YXJnZXQuaGFzQ2xhc3MoJ2pzX292ZXJsYXlfdHJpZ2dlcicpIHx8IHRhcmdldC5oYXNDbGFzcygnaWZyYW1lLXBvcHVwLXRyaWdnZXInKSkge1xyXG4gICAgICAgICAgICBpZiAodGFyZ2V0VGl0bGUubGVuZ3RoID4gMjApIHtcclxuICAgICAgICAgICAgICB0YXJnZXRUaXRsZSA9IHRhcmdldFRpdGxlLnN1YnN0cigwLDIwKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgaWYgKG1ldHJpY0NudCkge1xyXG4gICAgICAgICAgICBtZXRyaWNDbnQgPSBtZXRyaWNDbnQgKyAnLycgKyB0YXJnZXRUaXRsZTtcclxuICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIG1ldHJpY0NudCA9IHRhcmdldFRpdGxlO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGlmIChtZXRyaWNDbnQpIHtcclxuICAgICAgICB2YWx1ZUFycmF5LnB1c2gobWV0cmljQ250KTtcclxuICAgICAgfVxyXG5cclxuICAgIH1cclxuXHJcbiAgICBpZiAodmFsdWVBcnJheS5sZW5ndGgpIHtcclxuICAgICAgdmFsdWUgPSB2YWx1ZUFycmF5LmpvaW4oJy8nKTtcclxuICAgIH1cclxuXHJcbiAgICByZXR1cm4gdmFsdWU7XHJcbiAgfVxyXG59XHJcbiIsIid1c2Ugc3RyaWN0JztcclxuXHJcbmV4cG9ydCBjbGFzcyBQYWdlQ29uZmlnIHtcclxuICBjb25zdHJ1Y3RvcigpIHtcclxuICAgIHRoaXMuZGF0YSA9IHt9O1xyXG5cclxuICAgIC8vQWRkaW5nIGdsb2JhbCBIUCBpZiBpdCBpcyBub3QgZGVmaW5lZC5cclxuICAgICghd2luZG93LkhQKSAmJiAod2luZG93LkhQID0ge30pO1xyXG5cclxuICAgIC8vQWRkaW5nIHBhZ2VDb25maWcsIGNhdXNlIG9uIHNvbWUgcGFnZXMgdGhlcmUgaXMgaW5saW5lIHVzYWdlIG9mIEhQLnBhZ2VDb25maWcgLT4gZXJyb3JzIGlmIG5vdCBkZWZpbmVkXHJcbiAgICB3aW5kb3cuSFAucGFnZUNvbmZpZyA9IHRoaXM7XHJcbiAgfVxyXG5cclxuICBoYXMoa2V5KSB7XHJcbiAgICByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKHRoaXMuZGF0YSwga2V5KTtcclxuICB9XHJcblxyXG4gIHNldChrZXksIHZhbHVlKSB7XHJcbiAgICBpZiAodmFsdWUgPT09IHVuZGVmaW5lZCB8fCB2YWx1ZSA9PT0gdGhpcy5kYXRhW2tleV0pXHJcbiAgICAgIHJldHVybiBmYWxzZTtcclxuXHJcbiAgICBpZiAodGhpcy5oYXMoa2V5KSAmJiB0eXBlb2YgdGhpcy5nZXQoa2V5KSA9PT0gXCJvYmplY3RcIiAmJiB0eXBlb2YgdmFsdWUgPT09IFwib2JqZWN0XCIpIHtcclxuICAgICAgZm9yKGxldCBwcm9wIGluIHZhbHVlKXtcclxuICAgICAgICB0aGlzLmRhdGFba2V5XVtwcm9wXSA9IHZhbHVlW3Byb3BdO1xyXG4gICAgICB9XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLmRhdGFba2V5XSA9IHZhbHVlO1xyXG4gICAgfVxyXG5cclxuICAgIHJldHVybiB0aGlzLmRhdGFba2V5XTtcclxuICB9XHJcblxyXG4gIGdldChrZXkpIHtcclxuICAgIHJldHVybiB0aGlzLmRhdGFba2V5XTtcclxuICB9XHJcblxyXG4gIGRyb3Aoa2V5KSB7XHJcbiAgICBrZXkgPSBrZXkgfHwgbnVsbDtcclxuXHJcbiAgICBpZiAoa2V5ID09PSBudWxsKXtcclxuICAgICAgZGVsZXRlIHRoaXMuZGF0YTtcclxuICAgICAgdGhpcy5kYXRhID0ge307XHJcbiAgICB9IGVsc2UgaWYgKHRoaXMuaGFzKGtleSkpIHtcclxuICAgICAgZGVsZXRlIHRoaXMuZGF0YVtrZXldO1xyXG4gICAgfVxyXG4gIH1cclxufSIsIid1c2Ugc3RyaWN0JztcclxuXHJcbmV4cG9ydCBjbGFzcyBTY3JvbGxUbyB7XHJcblxyXG4gIGNvbnN0cnVjdG9yKC4uLmFyZ3MpIHtcclxuICAgIHRoaXMucmVzb2x2ZUFuY2hvcnNSb3dzKCk7XHJcbiAgICB0aGlzLl9pbml0TW9iaWxlU2Nyb2xsaW5nKCk7XHJcbiAgICB0aGlzLmFuaW1hdGluZyA9IGZhbHNlO1xyXG4gIH1cclxuXHJcbiAgcmVzb2x2ZUFuY2hvcnNSb3dzKCkge1xyXG4gICAgLy9lZGl0b3JzIGNhbiBub3QgcHV0IGFuIGFuY2hvciB0byBhIFJPVy4gXHJcbiAgICAkKCgpID0+IHtcclxuICAgICAgLy9pZiBhbmNob3IgaXMgc2V0IHZpYSBjdXN0b20gYmxvY2sgd2l0aCBpZCAtPiBzZXQgaXQgdG8gY2xvc2VzdCBST1dcclxuICAgICAgJCgnLnJvdy1hbmNob3ItaGVscGVyJykuZWFjaCgoaW5kZXgsIGVsKSA9PiB7XHJcbiAgICAgICAgbGV0ICRlbCA9ICQoZWwpLFxyXG4gICAgICAgICAgaWQgPSAkZWwuYXR0cignaWQnKTtcclxuXHJcbiAgICAgICAgaWYgKGlkKSB7XHJcbiAgICAgICAgICAkZWwuY2xvc2VzdCgnLnNlY3Rpb24nKS5hdHRyKCdkYXRhLWFuY2hvcicsIGlkKTtcclxuICAgICAgICAgICRlbC5yZW1vdmUoKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gICAgICAvL2lmIGFuY2hvciBpcyBzZXQgdmlhIG1vZHVsZSBkYXRhLWFuY2hvciB3aXRoIG5vIFwiYW5jaG9yLXRvLW1vZHVsZVwiIGNsYXNzbmFtZVxyXG4gICAgICAvLyAtPiBzZXQgaXQgdG8gY2xvc2VzdCBST1dcclxuICAgICAgJCgnLmZsZXgyLW1vbGVjdWxlOm5vdCguYW5jaG9yLXRvLW1vZHVsZSlbZGF0YS1hbmNob3JdJykuZWFjaCgoaW5kZXgsIGVsKSA9PiB7XHJcbiAgICAgICAgbGV0ICRlbCA9ICQoZWwpLFxyXG4gICAgICAgICAgYW5jaG9yID0gJGVsLmF0dHIoJ2RhdGEtYW5jaG9yJyk7XHJcbiAgICAgICAgICBcclxuICAgICAgICAkZWwucmVtb3ZlQXR0cignZGF0YS1hbmNob3InKS5jbG9zZXN0KCcuc2VjdGlvbicpLmF0dHIoJ2RhdGEtYW5jaG9yJywgYW5jaG9yKTtcclxuICAgICAgfSk7XHJcbiAgICAgICQod2luZG93KS50cmlnZ2VyKCdhbmNob3JzLXJlc29sdmVkJyk7XHJcbiAgICAgIHdpbmRvdy5HTE9CQUxTLmFuY2hvcnNSZXNvbHZlZCA9IHRydWU7XHJcbiAgICAgIHRoaXMuY2hlY2tBbmNob3JJblVybCgpO1xyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBfaW5pdE1vYmlsZVNjcm9sbGluZygpIHtcclxuICAgIHdpbmRvdy5yZXF1ZXN0QW5pbUZyYW1lID0gKGZ1bmN0aW9uKCl7XHJcbiAgICAgIHJldHVybiAgd2luZG93LnJlcXVlc3RBbmltYXRpb25GcmFtZSAgICAgICB8fFxyXG4gICAgICAgICAgICAgIHdpbmRvdy53ZWJraXRSZXF1ZXN0QW5pbWF0aW9uRnJhbWUgfHxcclxuICAgICAgICAgICAgICB3aW5kb3cubW96UmVxdWVzdEFuaW1hdGlvbkZyYW1lICAgIHx8XHJcbiAgICAgICAgICAgICAgZnVuY3Rpb24oIGNhbGxiYWNrICl7XHJcbiAgICAgICAgICAgICAgICB3aW5kb3cuc2V0VGltZW91dChjYWxsYmFjaywgMTAwMCAvIDYwKTtcclxuICAgICAgICAgICAgICB9O1xyXG4gICAgfSkoKTtcclxuICB9XHJcblxyXG4gIG5hdmlnYXRlKGhhc2gsIGNiLCBkdXJhdGlvbiwgZWwsIG9mZnNldCkgeyAgXHJcbiAgICBsZXQgJGVsLCBcclxuICAgICAgICBhbmNob3JFbCwgXHJcbiAgICAgICAgdG9wLCBcclxuICAgICAgICB1cGRhdGVkTmF2SGVpZ2h0LCBcclxuICAgICAgICBuZXdUb3A7XHJcbiAgICBvZmZzZXQgPSBvZmZzZXQgfHwgMDtcclxuICAgIGhhc2ggPSBlbCA/ICcnIDogaGFzaC5yZXBsYWNlKCcjJywgJycpO1xyXG4gICAgYW5jaG9yRWwgPSBlbCB8fCAkKCdkaXZbZGF0YS1hbmNob3I9XCInICsgaGFzaCArICdcIl0nKTtcclxuXHJcbiAgICAkZWwgPSBhbmNob3JFbC5sZW5ndGggPyBhbmNob3JFbCA6ICQoJyMnICsgaGFzaCk7XHJcblxyXG4gICAgaWYgKCRlbC5sZW5ndGgpIHtcclxuICAgICAgdG9wID0gTWF0aC5mbG9vcigkZWwub2Zmc2V0KCkudG9wICsgMSk7XHJcbiAgICAgIHVwZGF0ZWROYXZIZWlnaHQgPSAkKCcubW9sZWN1bGUtbGItODM1Jykub3V0ZXJIZWlnaHQoKTtcclxuICAgICAgbmV3VG9wID0gK3RvcCAtICt1cGRhdGVkTmF2SGVpZ2h0ICsgb2Zmc2V0O1xyXG4gICAgICB0aGlzLl9hbmltYXRlKG5ld1RvcCwgZHVyYXRpb24sIGNiKTtcclxuICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gZmFsc2U7XHJcbiAgfVxyXG5cclxuICBuYXZpZ2F0ZVRvT2Zmc2V0KG9mZnNldCwgZHVyYXRpb24sIGNiLCBpbmNsdWRlTmF2SGVpZ2h0KSB7XHJcbiAgICBsZXQgc2Nyb2xsVG9wID0gJCh3aW5kb3cpLnNjcm9sbFRvcCgpLFxyXG4gICAgICBuZXdUb3AgPSBzY3JvbGxUb3AgKyBvZmZzZXQ7XHJcblxyXG4gICAgaW5jbHVkZU5hdkhlaWdodCAmJiAobmV3VG9wIC09ICQoJy5tb2xlY3VsZS1sYi04MzUnKS5vdXRlckhlaWdodCgpKTtcclxuICAgIHRoaXMuX2FuaW1hdGUobmV3VG9wLCBkdXJhdGlvbiwgY2IpO1xyXG4gIH1cclxuXHJcbiAgbmF2aWdhdGVUb1NlY3Rpb24oaGFzaCwgY2IsIGR1cmF0aW9uLCBlbCwgb2Zmc2V0KSB7XHJcbiAgICBsZXQgJGVsLFxyXG4gICAgICBhbmNob3JFbCxcclxuICAgICAgdG9wLFxyXG4gICAgICBuZXdUb3A7XHJcbiAgICBvZmZzZXQgPSBvZmZzZXQgfHwgMDtcclxuICAgIGhhc2ggPSBlbCA/ICcnIDogaGFzaC5yZXBsYWNlKCcjJywgJycpO1xyXG4gICAgYW5jaG9yRWwgPSBlbCB8fCAkKCdkaXZbZGF0YS1hbmNob3I9XCInICsgaGFzaCArICdcIl0nKTtcclxuXHJcbiAgICAkZWwgPSBhbmNob3JFbC5sZW5ndGggPyBhbmNob3JFbCA6ICQoJyMnICsgaGFzaCk7XHJcblxyXG4gICAgaWYgKCRlbC5sZW5ndGgpIHtcclxuICAgICAgdG9wID0gTWF0aC5mbG9vcigkZWwub2Zmc2V0KCkudG9wICsgMSk7XHJcbiAgICAgIG5ld1RvcCA9ICt0b3AgKyBvZmZzZXQ7XHJcbiAgICAgIHRoaXMuX2FuaW1hdGUobmV3VG9wLCBkdXJhdGlvbiwgY2IpO1xyXG4gICAgICByZXR1cm4gdHJ1ZTtcclxuICAgIH1cclxuICAgIHJldHVybiBmYWxzZTtcclxuICB9XHJcblxyXG4gIG5hdmlnYXRlVG8ob2Zmc2V0LCBkdXJhdGlvbiwgY2IpIHtcclxuICAgIHRoaXMuX2FuaW1hdGUob2Zmc2V0LCBkdXJhdGlvbiwgY2IpO1xyXG4gIH1cclxuXHJcbiAgX2FuaW1hdGUobmV3VG9wLCBkdXJhdGlvbiA9IDYwMCwgY2IpIHtcclxuICAgIHRoaXMuYW5pbWF0aW5nID0gdHJ1ZTtcclxuXHJcbiAgICBpZiAoR0xPQkFMUy5VdGlscy5kZXZpY2UuaXNNb2JpbGUpIHtcclxuICAgICAgdGhpcy5fYW5pbWF0ZU1vYmlsZShuZXdUb3AsIGR1cmF0aW9uLCAnZWFzZUluT3V0Q3ViaWMnLCBjYik7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLl9hbmltYXRlRGVmYXVsdChuZXdUb3AsIGR1cmF0aW9uLCAnZWFzZUluT3V0Q3ViaWMnLCBjYik7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBfYW5pbWF0ZURlZmF1bHQobmV3VG9wLCBkdXJhdGlvbiwgZWFzaW5nLCBjYikge1xyXG4gICAgJCgnaHRtbCwgYm9keScpLnN0b3AoKS5jbGVhclF1ZXVlKCkuYW5pbWF0ZSh7XHJcbiAgICAgIHNjcm9sbFRvcDogbmV3VG9wICsgJ3B4JyBcclxuICAgIH0sIGR1cmF0aW9uLCBlYXNpbmcsICgpID0+IHtcclxuICAgICAgKHR5cGVvZiBjYiA9PT0gJ2Z1bmN0aW9uJykgJiYgY2IoKTtcclxuICAgICAgdGhpcy5hbmltYXRpbmcgPSBmYWxzZTtcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgX2FuaW1hdGVNb2JpbGUobmV3VG9wLCBkdXJhdGlvbiwgZWFzaW5nLCBjYikge1xyXG4gICAgdGhpcy5fc2Nyb2xsVG9Nb2JpbGUobmV3VG9wLCBkdXJhdGlvbiwgZWFzaW5nLCBmdW5jdGlvbigpIHtcclxuICAgICAgdGhpcy5hbmltYXRpbmcgPSBmYWxzZTtcclxuICAgICAgY2IgJiYgY2IoKTtcclxuICAgIH0uYmluZCh0aGlzKSk7XHJcbiAgfVxyXG5cclxuICBfc2Nyb2xsVG9Nb2JpbGUoc2Nyb2xsVG9ZLCBkdXJhdGlvbiwgZWFzaW5nLCBjYikge1xyXG4gICAgbGV0IHNjcm9sbFkgPSB3aW5kb3cuc2Nyb2xsWSB8fCB3aW5kb3cucGFnZVlPZmZzZXQsXHJcbiAgICAgICAgc2Nyb2xsVG8gPSBzY3JvbGxUb1kgfHwgMCxcclxuICAgICAgICBjdXJyZW50VGltZSA9IDAsXHJcbiAgICAgICAgdGltZSA9IGR1cmF0aW9uIC8gMTAwMCxcclxuICAgICAgICBlYXNpbmdFcXVhdGlvbnMgPSB7XHJcbiAgICAgICAgICBlYXNlSW5PdXRDdWJpYzogZnVuY3Rpb24ocG9zKSB7XHJcbiAgICAgICAgICAgIGlmICgocG9zLz0wLjUpIDwgMSkgcmV0dXJuIDAuNSpNYXRoLnBvdyhwb3MsMyk7XHJcbiAgICAgICAgICAgIHJldHVybiAwLjUgKiAoTWF0aC5wb3coKHBvcy0yKSwzKSArIDIpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH07XHJcblxyXG4gICAgZWFzaW5nID0gZWFzaW5nIHx8ICdlYXNlSW5PdXRDdWJpYyc7XHJcblxyXG4gICAgZnVuY3Rpb24gdGljaygpIHtcclxuICAgICAgY3VycmVudFRpbWUgKz0gMSAvIDYwO1xyXG5cclxuICAgICAgbGV0IHAgPSBjdXJyZW50VGltZSAvIHRpbWUsXHJcbiAgICAgICAgICB0ID0gZWFzaW5nRXF1YXRpb25zW2Vhc2luZ10ocCk7XHJcblxyXG4gICAgICBpZiAocCA8IDEpIHtcclxuICAgICAgICByZXF1ZXN0QW5pbUZyYW1lKHRpY2spO1xyXG4gICAgICAgIHdpbmRvdy5zY3JvbGxUbygwLCBzY3JvbGxZICsgKChzY3JvbGxUbyAtIHNjcm9sbFkpICogdCkpO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHdpbmRvdy5zY3JvbGxUbygwLCBzY3JvbGxUbyk7XHJcbiAgICAgICAgY2IgJiYgY2IoKTtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gICAgdGljaygpO1xyXG4gIH1cclxuXHJcbiAgY2hlY2tBbmNob3JJblVybCgpIHtcclxuICAgIGxldCB1cmxBbmNob3IgPSB3aW5kb3cubG9jYXRpb24uaGFzaDtcclxuICAgIGlmICggdXJsQW5jaG9yICkge1xyXG4gICAgICAgIHVybEFuY2hvciA9IHVybEFuY2hvciAmJiB1cmxBbmNob3IucmVwbGFjZSgvXiMhLywnIycpO1xyXG4gICAgICAgICQod2luZG93KS5vbignbG9hZCcsICgpID0+IHRoaXMubmF2aWdhdGUoIHVybEFuY2hvciApKTtcclxuICAgIH1cclxuICB9XHJcbn1cclxuIiwiJ3VzZSBzdHJpY3QnO1xyXG5cclxuZXhwb3J0IGNsYXNzIFNsaWRlckhlbHBlciB7XHJcblxyXG4gIC8qKlxyXG4gICAqIEBwYXJhbSB7anF1ZXJ5IERPTSBlbH0gc2xpZGVyIC0gc2xpZGVyIHRvIHdvcmsgd2l0aFxyXG4gICAqIEBwYXJhbSB7b2JqZWN0fSBvcHRpb25zIC0gY29uZmlndXJhdGlvblxyXG4gICAqIEBkZXNjcmlwdGlvbiAtIGFsaWduZXMgcG9zaXRpb24gb2YgYXJyb3cgdG8gdGhlIG1pZGRsZSBvZiBhIGNlcnRhaW4gaXRlbSB3aXRoaW4gYSBzbGlkZXJcclxuICAgKiAgICAgICAgICAgICAgICBJdGVtIGlzIHNwZWNpZmllZCBpbiBvcHRpb25zLmFsaWduVG9FbFNlbGVjdG9yLiBJdGVtIGlzIGEgc2luZ2xlIERPTSBlbCBmcm9tIFxyXG4gICAqICAgICAgICAgICAgICAgIGZpcnN0IHNsaWRlIGZvciBhbGwgc2xpZGVzXHJcbiAgICovXHJcbiAgYWxpZ25BcnJvd3NUb0VsKHNsaWRlciwgb3B0aW9ucykge1xyXG4gICAgbGV0IGFycm93cyA9IHNsaWRlci5maW5kKCcuc2xpY2stYXJyb3cnKS5jc3MoJ21hcmdpbi10b3AnLCAwKSxcclxuICAgICAgICBhbGlnblRvRWwgPSBzbGlkZXIuZmluZCgnLnNsaWNrLWN1cnJlbnQgJyArIG9wdGlvbnMuYWxpZ25Ub0VsU2VsZWN0b3IpLmZpcnN0KCk7XHJcbiAgICBcclxuICAgIGZ1bmN0aW9uIF9hbGlnbigpIHtcclxuICAgICAgbGV0IGFycm93SGVpZ2h0ID0gYXJyb3dzLmZpcnN0KCkuaGVpZ2h0KCksXHJcbiAgICAgICAgICBhbGlnblRvRWxIZWlnaHQgPSBhbGlnblRvRWwuaGVpZ2h0KCk7XHJcblxyXG4gICAgICBhbGlnblRvRWxIZWlnaHQgJiYgYXJyb3dzLmNzcygndG9wJywgKGFsaWduVG9FbEhlaWdodCAtIGFycm93SGVpZ2h0KSAvIDIpO1xyXG4gICAgfVxyXG4gICAgX2FsaWduKCk7XHJcblxyXG4gICAgc2xpZGVyLm9uKCdzZXRQb3NpdGlvbicsIF8uZGVib3VuY2UoKCkgPT4gX2FsaWduKCksIDUwKSk7XHJcbiAgfVxyXG5cclxuXHJcbiAgLyoqXHJcbiAgICogQHBhcmFtIHtqcXVlcnkgRE9NIGVsfSB0YWtlSGVpZ2h0RnJvbUVsIC0gZWxlbWVudCwgd2hpY2ggaGVpZ2h0IHNob3VsZCBiZSB1c2VkIHRvIGFwcGx5IHRvIHRoZSBvdGhlciBlbFxyXG4gICAqIEBwYXJhbSB7anF1ZXJ5IERPTSBlbH0gYXBwbHlIZWlnaHRUb0VsIC0gZWxlbWVudCwgdGhhdCBzaG91bGQgYWNjZXB0IGEgbmV3IGhlaWdodCBmcm9tIHRha2VIZWlnaHRGcm9tRWxcclxuICAgKiBAZGVzY3JpcHRpb24gLSBhbGlnbmVzIGhlaWdodHMgb2YgbXVsdGlwbGUgZWxlbWVudHMge2FwcGx5SGVpZ2h0VG9FbHN9LCBiYXNlZCBvbiB7dGFrZUhlaWdodEZyb21FbH0gaGVpZ2h0XHJcbiAgICovXHJcbiAgYWxpZ25FbGVtZW50c0hlaWdodChzbGlkZXIsIHRha2VIZWlnaHRGcm9tRWwsIGFwcGx5SGVpZ2h0VG9FbHMpIHtcclxuICAgIGlmICghdGFrZUhlaWdodEZyb21FbCB8fCAhYXBwbHlIZWlnaHRUb0VscykgcmV0dXJuO1xyXG4gICAgZnVuY3Rpb24gX2FsaWduKCkge1xyXG4gICAgICBsZXQgaGVpZ2h0ID0gdGFrZUhlaWdodEZyb21FbC5oZWlnaHQoKTtcclxuXHJcbiAgICAgIGFwcGx5SGVpZ2h0VG9FbHMuaGVpZ2h0KGhlaWdodCk7XHJcbiAgICB9XHJcbiAgICBfYWxpZ24oKTtcclxuXHJcbiAgICBzbGlkZXIub24oJ3NldFBvc2l0aW9uJywgXy5kZWJvdW5jZSgoKSA9PiBfYWxpZ24oKSwgNTApKTtcclxuICB9XHJcblxyXG59IiwiJ3VzZSBzdHJpY3QnO1xyXG5cclxuZXhwb3J0IGNsYXNzIFN0aWNrYWJsZSB7XHJcbiAgZ2V0IG9wdGlvbnMoKSB7XHJcbiAgICByZXR1cm4ge1xyXG4gICAgICB0eXBlOiAncmVndWxhcicsIC8vIHJlZ3VsYXJ8Ym91bmRpbmcuIERlZmluZXMgdHlwZSBvZiBza2lja2FibGUgYmVoYXZpb3VyXHJcbiAgICAgIHN0aWNreUNsYXNzOiAnc3RpY2t5JyxcclxuICAgICAgb2Zmc2V0VG9wOiAxMjAsXHJcbiAgICAgIG9mZnNldEJvdHRvbTogMFxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgY29uc3RydWN0b3Ioc3RpY2thYmxlRWwsIHN0YXRpY1BhcmVudCwgb3B0KSB7XHJcbiAgICB0aGlzLm9wdHMgPSAkLmV4dGVuZCh7fSwgdGhpcy5vcHRpb25zLCBvcHQpO1xyXG4gICAgdGhpcy5lbCA9IHN0aWNrYWJsZUVsO1xyXG4gICAgdGhpcy5zdGF0aWNQYXJlbnQgPSBzdGF0aWNQYXJlbnQgfHwgc3RpY2thYmxlRWwuY2xvc2VzdCgnLnNlY3Rpb24nKTtcclxuICAgIHRoaXMuX2luaXQoKTtcclxuICB9XHJcbiAgXHJcbiAgX2luaXQoKSB7XHJcbiAgICB0aGlzLmNhbGN1bGF0ZSA9ICgpID0+IHRoaXMuX2NhbGN1bGF0ZSgpW3RoaXMub3B0cy50eXBlXSgpO1xyXG4gICAgdGhpcy5jYWxjdWxhdGUoKTtcclxuXHJcbiAgICAkKHdpbmRvdykub24oJ3Njcm9sbCcsIHRoaXMuY2FsY3VsYXRlKTtcclxuICAgIHRoaXMuX2Rlc3Ryb3kgPSAoKSA9PiAkKHdpbmRvdykub2ZmKCdzY3JvbGwnLCB0aGlzLmNhbGN1bGF0ZSk7XHJcbiAgfVxyXG5cclxuICBfY2FsY3VsYXRlKCkge1xyXG4gICAgcmV0dXJuIHtcclxuICAgICAgJ3JlZ3VsYXInOiAoKSA9PiB7XHJcbiAgICAgICAgbGV0IHRvcCA9IHRoaXMuc3RhdGljUGFyZW50Lm9mZnNldCgpLnRvcDtcclxuICAgICAgICB0aGlzLmVsWygkKHdpbmRvdykuc2Nyb2xsVG9wKCkgKyAxKSA+IHRvcCA/ICdhZGRDbGFzcycgOiAncmVtb3ZlQ2xhc3MnXSh0aGlzLm9wdHMuc3RpY2t5Q2xhc3MpO1xyXG4gICAgICB9LFxyXG4gICAgICAnYm91bmRpbmcnOiAoKSA9PiB7XHJcbiAgICAgICAgbGV0IGNsaWVudFJlY3QgPSB0aGlzLnN0YXRpY1BhcmVudFswXS5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKSxcclxuICAgICAgICAgIHdIZWlnaHQgPSAkKHdpbmRvdykuaGVpZ2h0KCk7XHJcbiAgICAgICAgdGhpcy5lbFsoKGNsaWVudFJlY3QudG9wICsgdGhpcy5vcHRzLm9mZnNldFRvcCkgPCB3SGVpZ2h0ICYmIChjbGllbnRSZWN0LmJvdHRvbSAtIHRoaXMub3B0cy5vZmZzZXRCb3R0b20pID4gd0hlaWdodCkgPyAnYWRkQ2xhc3MnIDogJ3JlbW92ZUNsYXNzJ10odGhpcy5vcHRzLnN0aWNreUNsYXNzKTtcclxuICAgICAgfSxcclxuICAgICAgJ2JvdHRvbSc6ICgpID0+IHtcclxuICAgICAgICBsZXQgc2Nyb2xsVG9wID0gJCh3aW5kb3cpLnNjcm9sbFRvcCgpO1xyXG4gICAgICAgIHRoaXMuZWxbc2Nyb2xsVG9wID4gdGhpcy5vcHRzLm9mZnNldFRvcCA/ICdhZGRDbGFzcycgOiAncmVtb3ZlQ2xhc3MnXSh0aGlzLm9wdHMuc3RpY2t5Q2xhc3MpO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBkZXN0cm95KCkge1xyXG4gICAgdGhpcy5lbC5yZW1vdmVDbGFzcyh0aGlzLm9wdHMuc3RpY2t5Q2xhc3MpO1xyXG4gICAgdGhpcy5fZGVzdHJveSgpO1xyXG4gIH1cclxufVxyXG4iLCIndXNlIHN0cmljdCc7XHJcblxyXG5leHBvcnQgY2xhc3MgVXRpbHMge1xyXG5cclxuICBjb25zdHJ1Y3RvciguLi5hcmdzKSB7XHJcbiAgICB0aGlzLl9pbml0KCk7XHJcbiAgfVxyXG5cclxuICBfaW5pdCgpIHtcclxuICAgIHRoaXMuZGV2aWNlID0ge1xyXG4gICAgICBpc01vYmlsZTogJ29udG91Y2hzdGFydCcgaW4gd2luZG93IHx8IG5hdmlnYXRvci5tc01heFRvdWNoUG9pbnRzIHx8IGZhbHNlLFxyXG4gICAgICBpc0xhbmRzY2FwZTogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgcmV0dXJuIHdpbmRvdy5tYXRjaE1lZGlhKFwiKG9yaWVudGF0aW9uOiBsYW5kc2NhcGUpXCIpLm1hdGNoZXNcclxuICAgICAgfVxyXG4gICAgfVxyXG4gICAgaWYgKHRoaXMuZGV2aWNlLmlzTW9iaWxlKSB7XHJcbiAgICAgICQoKCkgPT4gJCgnYm9keScpLmFkZENsYXNzKCdsYi1tb2JpbGUnKSk7XHJcbiAgICB9XHJcblxyXG4gICAgLy9FeHRlbmQgdmlkZW8gb3B0aW9ucyB0byBoYXZlIFwiaXNQbGF5aW5nXCIgcGFyYW1cclxuICAgIE9iamVjdC5kZWZpbmVQcm9wZXJ0eShIVE1MTWVkaWFFbGVtZW50LnByb3RvdHlwZSwgJ2lzUGxheWluZycsIHtcclxuICAgICAgZ2V0OiBmdW5jdGlvbigpe1xyXG4gICAgICAgIHJldHVybiAhISh0aGlzLmN1cnJlbnRUaW1lID4gMC4wMSAmJiAhdGhpcy5wYXVzZWQgJiYgIXRoaXMuZW5kZWQgJiYgdGhpcy5yZWFkeVN0YXRlID4gMik7XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gICAgdGhpcy5faW5pdENhY2hlKCk7XHJcbiAgfVxyXG5cclxuICBfcGFyc2VRdWVyeShxc3RyKSB7XHJcbiAgICBsZXQgcXVlcnkgPSB7fSxcclxuICAgICAgcGFpcnMgPSAocXN0clswXSA9PT0gJz8nID8gcXN0ci5zdWJzdHIoMSkgOiBxc3RyKS5zcGxpdCgnJicpO1xyXG5cclxuICAgIGZvciAobGV0IGkgPSAwOyBpIDwgcGFpcnMubGVuZ3RoOyBpKyspIHtcclxuICAgICAgbGV0IHBhaXIgPSBwYWlyc1tpXS5zcGxpdCgnPScpO1xyXG4gICAgICBxdWVyeVtkZWNvZGVVUklDb21wb25lbnQocGFpclswXSldID0gZGVjb2RlVVJJQ29tcG9uZW50KHBhaXJbMV0gfHwgJycpO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIHF1ZXJ5O1xyXG4gIH1cclxuXHJcbiAgX2luaXRDYWNoZSgpIHtcclxuICAgIHZhciBjYWNoZSA9IHt9O1xyXG4gICAgdGhpcy5jYWNoZSA9IHtcclxuICAgICAgc2V0OiAobmFtZSwgdmFsdWUpID0+IHtcclxuICAgICAgICBjYWNoZVtuYW1lXSA9IFt2YWx1ZV07XHJcbiAgICAgIH0sXHJcbiAgICAgIHB1c2g6IChuYW1lLCB2YWx1ZSkgPT4ge1xyXG4gICAgICAgIGlmIChjYWNoZVtuYW1lXSkge1xyXG4gICAgICAgICAgY2FjaGVbbmFtZV0ucHVzaCh2YWx1ZSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIHRoaXMuY2FjaGUuc2V0KG5hbWUsIHZhbHVlKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0sXHJcbiAgICAgIGdldDogKG5hbWUpID0+IHtcclxuICAgICAgICByZXR1cm4gY2FjaGVbbmFtZV0gfHwgW107XHJcbiAgICAgIH0sXHJcbiAgICAgIHJlbW92ZTogKG5hbWUpID0+IHtcclxuICAgICAgICBkZWxldGUgY2FjaGVbbmFtZV1cclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxufSIsImltcG9ydCB7IEJhc2VDb21wb25lbnQgfSBmcm9tICcuLi9iYXNlLWNvbXBvbmVudC5qcyc7XHJcblxyXG5leHBvcnQgY2xhc3MgQmFubmVycyBleHRlbmRzIEJhc2VDb21wb25lbnQge1xyXG5cclxuICBzdGF0aWMgZ2V0IHNlbGVjdG9yKCkgeyByZXR1cm4gJy5zZWN0aW9uLmZ1bGxbc3R5bGUqPVwidXJsXCJdW2RhdGEtc2Vjb25kYXJ5LWJhY2tncm91bmRdOm5vdCguY3VzdG9tLW1hc3Rlcik6bm90KC5jdXN0b20tc2xhdmUpJzsgfVxyXG5cclxuICBnZXQgZGVmYXVsdHMoKSB7XHJcbiAgICByZXR1cm4gJC5leHRlbmQoe30sIHN1cGVyLmRlZmF1bHRzLCB7XHJcbiAgICAgIERFRkFVTFRfRkxJUF9CUDogJzcyMCdcclxuICAgIH0pO1xyXG4gIH1cclxuICBcclxuICBjb25zdHJ1Y3RvciguLi5hcmdzKSB7XHJcbiAgICBzdXBlciguLi5hcmdzKTtcclxuICAgIHRoaXMuX2luaXQoKTtcclxuICB9XHJcblxyXG4gIF9pbml0KCkge1xyXG4gICAgdGhpcy5faW5pdEJnRmxpcCgpO1xyXG4gIH1cclxuXHJcbiAgX2luaXRCZ0ZsaXAoKSB7XHJcbiAgICBsZXQgY3VzdG9tQlAgPSB0aGlzLl9nZXRXcmFwQnJlYWtwb2ludCgpLFxyXG4gICAgICBtb2JpbGVRdWVyeSA9IHdpbmRvdy5tYXRjaE1lZGlhKCdzY3JlZW4gYW5kIChtYXgtd2lkdGg6ICcgKyBjdXN0b21CUCArICdweCknKSxcclxuICAgICAgdXJsID0gdGhpcy5lbC5jc3MoJ2JhY2tncm91bmQtaW1hZ2UnKS5yZXBsYWNlKC91cmxcXChbJ1wiXSooLio/KVsnXCJdKlxcKS8sICckMScpO1xyXG4gICAgdGhpcy5lbC5hdHRyKCdkYXRhLXByaW1hcnktYmFja2dyb3VuZCcsIHVybCk7XHJcbiAgICB0aGlzLl9jaGFuZ2VCYWNrZ3JvdW5kcyhtb2JpbGVRdWVyeSk7XHJcbiAgICBtb2JpbGVRdWVyeS5hZGRMaXN0ZW5lcih0aGlzLl9jaGFuZ2VCYWNrZ3JvdW5kcy5iaW5kKHRoaXMpKTtcclxuICB9XHJcblxyXG4gIF9jaGFuZ2VCYWNrZ3JvdW5kcyhtcWwpIHtcclxuICAgIGxldCBpbWFnZSA9IG1xbC5tYXRjaGVzID8gJ2RhdGEtc2Vjb25kYXJ5LWJhY2tncm91bmQnIDogJ2RhdGEtcHJpbWFyeS1iYWNrZ3JvdW5kJztcclxuICAgIHRoaXMuZWwuY3NzKCdiYWNrZ3JvdW5kLWltYWdlJywgJ3VybChcIicrIHRoaXMuZWwuYXR0cihpbWFnZSkgKyAnXCIpJyk7XHJcbiAgfVxyXG5cclxuICBfZ2V0V3JhcEJyZWFrcG9pbnQoKSB7XHJcbiAgICBsZXQgcmVzdWx0ID0gL2ZsaXAtYmdzLWJyZWFrcG9pbnQtKC4qPykoXFxEfCQpLy5leGVjKHRoaXMuZWwuYXR0cignY2xhc3MnKSk7XHJcbiAgICBpZiAocmVzdWx0KSB7XHJcbiAgICAgIHJldHVybiByZXN1bHRbMV07XHJcbiAgICB9XHJcbiAgICByZXR1cm4gdGhpcy5vcHRpb25zLkRFRkFVTFRfRkxJUF9CUDtcclxuICB9XHJcblxyXG59XHJcblxyXG4iLCJpbXBvcnQgeyBCYXNlQ29tcG9uZW50IH0gZnJvbSAnLi4vYmFzZS1jb21wb25lbnQuanMnO1xyXG5cclxuZXhwb3J0IGNsYXNzIEN1c3RvbUhlaWdodCBleHRlbmRzIEJhc2VDb21wb25lbnQge1xyXG5cclxuICBzdGF0aWMgZ2V0IHNlbGVjdG9yKCkgeyByZXR1cm4gJy5zZWN0aW9uLmN1c3RvbS1oZWlnaHQ6bm90KC5wYXJhbGxheC1wYW5lbCknOyB9XHJcblxyXG4gIGNvbnN0cnVjdG9yKC4uLmFyZ3MpIHtcclxuICAgIHN1cGVyKC4uLmFyZ3MpO1xyXG4gICAgdGhpcy5faW5pdCgpO1xyXG4gIH1cclxuXHJcbiAgX2luaXQoKSB7XHJcbiAgICB0aGlzLl9pbml0Q3VzdG9tSGVpZ2h0KCk7XHJcbiAgfVxyXG5cclxuICBfaW5pdEN1c3RvbUhlaWdodCgpIHtcclxuICAgIC8vcGljayB1cCBpbmxpbmUgaGVpZ2h0IGZyb20gLnNlY3Rpb24gYW5kIHJlcGxhY2UgaXQgd2l0aCBtaW4taGVpZ2h0IFxyXG4gICAgY29uc3Qgc3R5bGUgPSB0aGlzLmVsLmF0dHIoJ3N0eWxlJyk7XHJcbiAgICBzdHlsZSAmJiB0aGlzLmVsLmF0dHIoJ3N0eWxlJywgc3R5bGUucmVwbGFjZSgvaGVpZ2h0L2lnLCAnbWluLWhlaWdodCcpKTtcclxuICB9XHJcbn1cclxuXHJcbiIsImltcG9ydCB7IGV2ZW50QnVzIH0gZnJvbSAnLi4vY29tbW9uL3NjcmlwdHMvZXZlbnQtYnVzJztcclxuXHJcbmV4cG9ydCBjbGFzcyBCYXNlQ29tcG9uZW50IHtcclxuICBnZXQgZGVmYXVsdHMoKSB7XHJcbiAgICByZXR1cm4ge307XHJcbiAgfVxyXG5cclxuICBjb25zdHJ1Y3RvcihlbGVtZW50LCBvcHRpb25zKSB7XHJcbiAgICB0aGlzLmVsID0gJChlbGVtZW50KTtcclxuICAgIHRoaXMub3B0aW9ucyA9ICQuZXh0ZW5kKHt9LCB0aGlzLmRlZmF1bHRzLCBvcHRpb25zKTtcclxuXHJcbiAgICBpZiAodGhpcy5ST09UX0NMQVNTKSB7XHJcbiAgICAgIHRoaXMuZWwuYWRkQ2xhc3ModGhpcy5ST09UX0NMQVNTKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIG9uKGV2ZW50TmFtZSwgaGFuZGxlcikge1xyXG4gICAgdGhpcy5lbC5vbihldmVudE5hbWUsIGhhbmRsZXIpO1xyXG4gICAgLy8gUmV0dXJuaW5nIHVuYmluZCBmdW5jdGlvblxyXG4gICAgcmV0dXJuICgpID0+IHRoaXMub2ZmKGV2ZW50TmFtZSwgaGFuZGxlcik7XHJcbiAgfVxyXG5cclxuICBvZmYoZXZlbnROYW1lLCBoYW5kbGVyKSB7XHJcbiAgICB0aGlzLmVsLm9mZihldmVudE5hbWUsIGhhbmRsZXIpO1xyXG4gIH1cclxuXHJcbiAgdHJpZ2dlcihldmVudE5hbWUsIC4uLmRhdGEpIHtcclxuICAgIHRoaXMuZWwudHJpZ2dlcihldmVudE5hbWUsIFsuLi5kYXRhXSk7XHJcbiAgfVxyXG5cclxuICBicm9hZGNhc3QoZXZlbnROYW1lLCAuLi5kYXRhKSB7XHJcbiAgICB0aGlzLnRyaWdnZXIoZXZlbnROYW1lLCAuLi5kYXRhKTtcclxuICAgIGV2ZW50QnVzLnRyaWdnZXIoZXZlbnROYW1lLCB0aGlzLCAuLi5kYXRhKTtcclxuICB9XHJcblxyXG4gIG9uUmVzaXplKGNiLCBkZWxheSwgbWV0aG9kPSdkZWJvdW5jZScpIHtcclxuICAgIGxldCBoYW5kbGVyID0gXy5kZWJvdW5jZShjYiwgZGVib3VuY2VEZWxheSk7XHJcbiAgICAkKHdpbmRvdykub24oJ3Jlc2l6ZScsIGhhbmRsZXIpO1xyXG4gICAgcmV0dXJuICgpID0+ICQod2luZG93KS5vZmYoJ3Jlc2l6ZScsIGhhbmRsZXIpO1xyXG4gIH1cclxufVxyXG4iLCIndXNlIHN0cmljdCc7XHJcblxyXG5pbXBvcnQgeyBCYXNlQ29tcG9uZW50IH0gZnJvbSAnLi4vLi4vY29tcG9uZW50cy9iYXNlLWNvbXBvbmVudC5qcyc7XHJcblxyXG5leHBvcnQgY2xhc3MgQ3VzdG9tRm9udCBleHRlbmRzIEJhc2VDb21wb25lbnQge1xyXG5cclxuICBzdGF0aWMgZ2V0IHNlbGVjdG9yICgpIHsgcmV0dXJuICdbc3R5bGUqPVwidGFibGV0XCJdLCBbc3R5bGUqPVwibW9iaWxlXCJdJzsgfVxyXG5cclxuICBnZXQgZGVmYXVsdHMoKSB7XHJcbiAgICByZXR1cm4ge1xyXG4gICAgICBtZWRpYXM6IHtcclxuICAgICAgICBkZXNrdG9wOiAnc2NyZWVuIGFuZCAobWluLXdpZHRoOiAxMjgxcHgpJyxcclxuICAgICAgICB0YWJsZXQ6ICdzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDEyODBweCkgYW5kIChtaW4td2lkdGg6IDcyMXB4KScsXHJcbiAgICAgICAgbW9iaWxlOiAnc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA3MjBweCknXHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcblxyXG4gIGNvbnN0cnVjdG9yKC4uLmFyZ3MpIHtcclxuICAgIHN1cGVyKC4uLmFyZ3MpO1xyXG4gICAgdGhpcy5faW5pdCgpO1xyXG4gIH1cclxuXHJcbiAgX2luaXQoKSB7XHJcbiAgICB0aGlzLl9yZXRyaWV2ZUN1c3RvbVN0eWxlcygpO1xyXG4gICAgdGhpcy5fdXBkYXRlRGF0YUF0dHJzKCk7XHJcbiAgICBmb3IgKGxldCBpIGluIHRoaXMub3B0aW9ucy5tZWRpYXMpIHtcclxuICAgICAgbGV0IG1lZGlhID0gd2luZG93Lm1hdGNoTWVkaWEodGhpcy5vcHRpb25zLm1lZGlhc1tpXSk7XHJcbiAgICAgIG1lZGlhLmFkZExpc3RlbmVyKChtZWRpYSkgPT4gdGhpcy51cGRhdGUobWVkaWEpKTtcclxuICAgICAgdGhpcy51cGRhdGUobWVkaWEpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgX3JldHJpZXZlQ3VzdG9tU3R5bGVzKCkge1xyXG4gICAgbGV0IHN0eWxlID0gdGhpcy5lbC5hdHRyKCdzdHlsZScpO1xyXG4gICAgaWYgKHN0eWxlKSB7XHJcbiAgICAgIGxldCBkZXNrdG9wID0gc3R5bGUucmVwbGFjZSgvXFwvXFwqKC4qPylcXCpcXC8vaWcsICcnKSxcclxuICAgICAgICB0YWJsZXQgPSBzdHlsZS5tYXRjaCgvXFwvXFwqdGFibGV0KC4qPylcXCpcXC8vKSxcclxuICAgICAgICBtb2JpbGUgPSBzdHlsZS5tYXRjaCgvXFwvXFwqbW9iaWxlKC4qPylcXCpcXC8vKTtcclxuXHJcbiAgICAgIHRhYmxldCA9IHRhYmxldCA/IHRhYmxldFsxXSA6ICcnO1xyXG4gICAgICBtb2JpbGUgPSBtb2JpbGUgPyBtb2JpbGVbMV0gOiAnJztcclxuICAgICAgZGVza3RvcCAmJiB0aGlzLmVsLmF0dHIoJ2RhdGEtc3R5bGUtZGVza3RvcCcsIGRlc2t0b3ApO1xyXG4gICAgICB0YWJsZXQgJiYgdGhpcy5lbC5hdHRyKCdkYXRhLXN0eWxlLXRhYmxldCcsIHRhYmxldCk7XHJcbiAgICAgIG1vYmlsZSAmJiB0aGlzLmVsLmF0dHIoJ2RhdGEtc3R5bGUtbW9iaWxlJywgbW9iaWxlKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIF91cGRhdGVEYXRhQXR0cnMoKSB7XHJcbiAgICAhdGhpcy5lbC5hdHRyKCdkYXRhLXN0eWxlLWRlc2t0b3AnKSAmJiB0aGlzLmVsLmF0dHIoJ2RhdGEtc3R5bGUtZGVza3RvcCcsICcnKTtcclxuICAgIGlmICghdGhpcy5lbC5hdHRyKCdkYXRhLXN0eWxlLW1vYmlsZScpICYmIHRoaXMuZWwuYXR0cignZGF0YS1zdHlsZS10YWJsZXQnKSkge1xyXG4gICAgICB0aGlzLmVsLmF0dHIoJ2RhdGEtc3R5bGUtbW9iaWxlJywgdGhpcy5lbC5hdHRyKCdkYXRhLXN0eWxlLXRhYmxldCcpKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHVwZGF0ZShtZWRpYSkge1xyXG4gICAgaWYgKG1lZGlhLm1hdGNoZXMpIHtcclxuICAgICAgc3dpdGNoIChtZWRpYS5tZWRpYSkge1xyXG4gICAgICAgIGNhc2UgdGhpcy5vcHRpb25zLm1lZGlhcy5kZXNrdG9wOiB7XHJcbiAgICAgICAgICB0aGlzLnNldFN0eWxlKCdkZXNrdG9wJyk7XHJcbiAgICAgICAgICBicmVhaztcclxuICAgICAgICB9XHJcbiAgICAgICAgY2FzZSB0aGlzLm9wdGlvbnMubWVkaWFzLnRhYmxldDoge1xyXG4gICAgICAgICAgdGhpcy5zZXRTdHlsZSgndGFibGV0Jyk7XHJcbiAgICAgICAgICBicmVhaztcclxuICAgICAgICB9XHJcbiAgICAgICAgY2FzZSB0aGlzLm9wdGlvbnMubWVkaWFzLm1vYmlsZToge1xyXG4gICAgICAgICAgdGhpcy5zZXRTdHlsZSgnbW9iaWxlJyk7XHJcbiAgICAgICAgICBicmVhaztcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcblxyXG4gIHNldFN0eWxlKHNjcmVlbikge1xyXG4gICAgbGV0IHN0eWxlID0gdGhpcy5lbC5kYXRhKCdzdHlsZS0nICsgc2NyZWVuKSB8fCB0aGlzLmVsLmRhdGEoJ3N0eWxlLWRlc2t0b3AnKTtcclxuICAgIHRoaXMuZWwuYXR0cignc3R5bGUnLCBzdHlsZSk7XHJcbiAgfVxyXG59IiwiJ3VzZSBzdHJpY3QnO1xyXG5cclxuLypOT1RFLCBvcmRlciBtYXR0ZXJzLiBJZiB5b3UgbmVlZCB0byB1cGRhdGUgaHRtbCBvZiBzb21lIGVsZW1lbnQgYmVmb3JlIG90aGVyIFxyXG4gKmNvbXBvbmVudHMgd2lsbCBiZSBpbml0aWFsaXplZCAtIHB1dCBpdCdzIGxvZ2ljIG9uIHRvcFxyXG4gKi9cclxuZXhwb3J0ICogZnJvbSAnLi9jb21tb24vY3VzdG9tLWZvbnQnO1xyXG5leHBvcnQgKiBmcm9tICcuLi9ncmlkL3NjcmlwdHMvZ3JpZCc7XHJcbmV4cG9ydCAqIGZyb20gJy4vc2xpZGVycy9zbGlkZXJzJztcclxuZXhwb3J0ICogZnJvbSAnLi9iYW5uZXJzL2Jhbm5lcnMnO1xyXG5leHBvcnQgKiBmcm9tICcuL2Jhbm5lcnMvY3VzdG9tLWhlaWdodCc7XHJcbmV4cG9ydCAqIGZyb20gJy4vb3ZlcmxheXMvb3ZlcmxheXMnOyIsImltcG9ydCB7IEJhc2VDb21wb25lbnQgfSBmcm9tICcuLi8uLi9iYXNlLWNvbXBvbmVudC5qcyc7XHJcblxyXG5cclxuZXhwb3J0IGNsYXNzIE92ZXJsYXkgZXh0ZW5kcyBCYXNlQ29tcG9uZW50IHtcclxuXHJcbiAgc3RhdGljIGdldCBPUEVOX0VWRU5UKCkgeyByZXR1cm4gJ292ZXJsYXktb3BlbmVkJzsgfVxyXG4gIHN0YXRpYyBnZXQgQ0xPU0VfRVZFTlQoKSB7IHJldHVybiAnb3ZlcmxheS1jbG9zZWQnOyB9XHJcblxyXG4gIGdldCBkZWZhdWx0cygpIHtcclxuICAgIHJldHVybiAkLmV4dGVuZCh7fSwgc3VwZXIuZGVmYXVsdHMsIHtcclxuICAgICAgY2xvc2VCdXR0b25TZWxlY3RvcjogJy5qc19wb3BfY2xvc2UnLFxyXG4gICAgICBvdmVybGF5V3JhcHBlckNsYXNzOiAnb3ZlcmxheS13cmFwcGVyIGJvZHknLFxyXG4gICAgICBvcGVuZWRDbGFzczogJ29wZW5lZCcsXHJcbiAgICAgIG5vU2Nyb2xsQ2xhc3M6ICduby1zY3JvbGwnXHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIGNvbnN0cnVjdG9yKC4uLmFyZ3MpIHtcclxuICAgIHN1cGVyKC4uLmFyZ3MpO1xyXG5cclxuICAgIHRoaXMuY2xvc2VCdXR0b24gPSB0aGlzLmVsLmZpbmQodGhpcy5vcHRpb25zLmNsb3NlQnV0dG9uU2VsZWN0b3IpO1xyXG4gICAgdGhpcy5vcGVuZXJzID0gJCgnYVtyZWw9XCInICsgdGhpcy5lbC5hdHRyKCdpZCcpICsgJ1wiXScpO1xyXG4gICAgdGhpcy5lbC53cmFwKCc8ZGl2IGNsYXNzPVwiJyArIHRoaXMub3B0aW9ucy5vdmVybGF5V3JhcHBlckNsYXNzICsgJ1wiPjwvZGl2PicpLnBhcmVudCgpLmFwcGVuZFRvKGRvY3VtZW50LmJvZHkpO1xyXG4gICAgdGhpcy5faW5pdExpc3RlbmVycygpO1xyXG4gICAgdGhpcy5faW5uZXJNb2R1bGVzID0gdGhpcy5lbC5maW5kKCdbY2xhc3MqPVwiZmxleDItbW9sZWN1bGVcIl0nKTtcclxuICB9XHJcblxyXG4gIF9pbml0TGlzdGVuZXJzKCkge1xyXG4gICAgdGhpcy5vcGVuZXJzLm9uKCdjbGljaycsIChlKSA9PiB0aGlzLm9wZW4oZSkpO1xyXG4gICAgdGhpcy5jbG9zZUJ1dHRvbi5vbignY2xpY2snLCAoKSA9PiB0aGlzLmNsb3NlKCkpO1xyXG4gICAgdGhpcy5lbC5wYXJlbnQoKS5vbignY2xpY2snLCAoZSkgPT4ge1xyXG4gICAgICAkKGUudGFyZ2V0KS5oYXNDbGFzcygnb3ZlcmxheS13cmFwcGVyJykgJiYgdGhpcy5jbG9zZSgpO1xyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBvcGVuKGUpIHtcclxuICAgIHRoaXMuX2lubmVyTW9kdWxlcy50cmlnZ2VyKCdvdmVybGF5LmJlZm9yZW9wZW4nKTtcclxuICAgIHRoaXMuX29uT3BlbigpO1xyXG4gICAgdGhpcy5faW5uZXJNb2R1bGVzLnRyaWdnZXIoJ292ZXJsYXkub3BlbmVkJyk7XHJcblxyXG4gICAgLy9vbmUgdGltZSBsaXN0ZW4gZm9yIEVTQyBwcmVzc2VkIHRvIGNsb3NlIG92ZXJsYXlcclxuICAgICQoZG9jdW1lbnQpLm9uZSgna2V5dXAub3ZlcmxheScsIChlKSA9PiB7XHJcbiAgICAgIChlLmtleUNvZGUgPT0gMjcpICYmIHRoaXMuY2xvc2UoKTtcclxuICAgIH0pO1xyXG4gICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gIH1cclxuXHJcbiAgX29uT3BlbigpIHtcclxuICAgIHRoaXMuX3ByZXZlbnRTY3JvbGxpbmcoKTtcclxuXHJcbiAgICB0aGlzLmVsLmFkZENsYXNzKHRoaXMub3B0aW9ucy5vcGVuZWRDbGFzcyk7XHJcbiAgICB0aGlzLmVsLnBhcmVudCgpLmFkZENsYXNzKHRoaXMub3B0aW9ucy5vcGVuZWRDbGFzcyk7XHJcbiAgfVxyXG5cclxuICBjbG9zZSgpIHtcclxuICAgICQoZG9jdW1lbnQpLm9mZigna2V5dXAub3ZlcmxheScpO1xyXG4gICAgdGhpcy5fb25DbG9zZSgpO1xyXG4gIH1cclxuXHJcbiAgX29uQ2xvc2UoKSB7XHJcbiAgICB0aGlzLmVsLnJlbW92ZUNsYXNzKHRoaXMub3B0aW9ucy5vcGVuZWRDbGFzcyk7XHJcbiAgICB0aGlzLmVsLnBhcmVudCgpLnJlbW92ZUNsYXNzKHRoaXMub3B0aW9ucy5vcGVuZWRDbGFzcyk7XHJcbiAgICB0aGlzLl9pbm5lck1vZHVsZXMudHJpZ2dlcignb3ZlcmxheS5jbG9zZWQnKTtcclxuICAgIHRoaXMuX3JlbGVhc2VTY3JvbGxpbmcoKTtcclxuICB9XHJcblxyXG4gIF9wcmV2ZW50U2Nyb2xsaW5nKCkge1xyXG4gICAgJChkb2N1bWVudC5ib2R5KS5hZGRDbGFzcyh0aGlzLm9wdGlvbnMubm9TY3JvbGxDbGFzcyk7XHJcbiAgfVxyXG5cclxuICBfcmVsZWFzZVNjcm9sbGluZygpIHtcclxuICAgICQoZG9jdW1lbnQuYm9keSkucmVtb3ZlQ2xhc3ModGhpcy5vcHRpb25zLm5vU2Nyb2xsQ2xhc3MpO1xyXG4gIH1cclxufVxyXG4iLCIndXNlIHN0cmljdCc7XHJcblxyXG5pbXBvcnQgeyBCYXNlQ29tcG9uZW50IH0gZnJvbSAnLi4vYmFzZS1jb21wb25lbnQuanMnO1xyXG5pbXBvcnQgeyBPdmVybGF5IH0gZnJvbSAnLi9vdmVybGF5L292ZXJsYXkuanMnO1xyXG5pbXBvcnQgeyBWaWRlb092ZXJsYXkgfSBmcm9tICcuL3ZpZGVvLW92ZXJsYXkvdmlkZW8tb3ZlcmxheS5qcyc7XHJcblxyXG5leHBvcnQgY2xhc3MgT3ZlcmxheXNGYWN0b3J5IHtcclxuICBcclxuICBzdGF0aWMgZ2V0IHNlbGVjdG9yKCkgeyByZXR1cm4gJy5vdmVybGF5LXBvcHVwLCAuY29udGVudC1vdmVybGF5JzsgfVxyXG4gIGdldCBvcHRpb25zKCkge1xyXG4gICAgcmV0dXJuIHtcclxuICAgICAgY2xvc2VPdmVybGF5SWNvblRwbDogJycrXHJcbiAgICAgICAgJzxhIGhyZWY9XCJqYXZhc2NyaXB0OnZvaWQoMCk7XCIgY2xhc3M9XCJqc19wb3BfY2xvc2UgcG9wdXAtY2xvc2VcIiB0aXRsZT1cImNsb3NlIGJ1dHRvblwiPicrXHJcbiAgICAgICAgICAnPHNwYW4gY2xhc3M9XCJzY3JlZW5SZWFkaW5nXCI+Q2xvc2U8L3NwYW4+JytcclxuICAgICAgICAnPC9hPidcclxuICAgIH1cclxuICB9XHJcbiAgZ2V0IG92ZXJsYXlUeXBlcygpIHtcclxuICAgIHJldHVybiB7XHJcbiAgICAgIHZpZGVvT3ZlcmxheVNlbGVjdG9yOiAnLnZpZGVvLWNvbnRhaW5lcicsXHJcbiAgICAgIGNvbnRlbnRPdmVybGF5Q2xhc3M6ICdjb250ZW50LW92ZXJsYXknXHJcbiAgICB9XHJcbiAgfVxyXG4gIGNvbnN0cnVjdG9yKGVsKSB7XHJcbiAgICB0aGlzLl9pbml0KGVsKTtcclxuICB9XHJcblxyXG4gIF9pbml0KGVsKSB7XHJcbiAgICB0aGlzLmVsID0gJChlbCk7XHJcbiAgICAvL0luaXRpYXRlIHZpZGVvIE92ZXJsYXlcclxuICAgIGlmICh0aGlzLmVsLmZpbmQodGhpcy5vdmVybGF5VHlwZXMudmlkZW9PdmVybGF5U2VsZWN0b3IpLmxlbmd0aCAmJiAhdGhpcy5lbC5oYXNDbGFzcyh0aGlzLm92ZXJsYXlUeXBlcy5jb250ZW50T3ZlcmxheUNsYXNzKSkge1xyXG4gICAgICBuZXcgVmlkZW9PdmVybGF5KGVsKTtcclxuICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG4gICAgLy9Jbml0aWF0ZSBjb250ZW50IE92ZXJsYXlcclxuICAgIGlmICh0aGlzLmVsLmhhc0NsYXNzKHRoaXMub3ZlcmxheVR5cGVzLmNvbnRlbnRPdmVybGF5Q2xhc3MpKSB7XHJcbiAgICAgIHRoaXMuX3ByZXBhcmVDb250ZW50KCkgJiYgbmV3IE92ZXJsYXkodGhpcy5lbFswXSk7XHJcbiAgICAgIHJldHVybjtcclxuICAgIH1cclxuXHJcbiAgICAvL0luaXRpYXRlIHJlZ3VsYXIgT3ZlcmxheSwgYXNzdW1pbmcgdGhhdCBpdCBpcyBub3QgYSBzcGVjaWFsIG9uZVxyXG4gICAgbmV3IE92ZXJsYXkoZWwpO1xyXG4gIH1cclxuXHJcbiAgX3ByZXBhcmVDb250ZW50KCkge1xyXG4gICAgdGhpcy5fcmVtb3ZlSW5saW5lU2NyaXB0cygpO1xyXG4gICAgbGV0IHBvcHVwSWQgPSB0aGlzLmVsLmZpbmQoJ1tkYXRhLWNvbnRlbnQtb3ZlcmxheS1pZF0nKS5hdHRyKCdkYXRhLWNvbnRlbnQtb3ZlcmxheS1pZCcpLFxyXG4gICAgICAgIG9wZW5lcnMgPSAkKCdhW2hyZWY9XCIjJyArIHBvcHVwSWQgKyAnXCJdJyk7XHJcbiAgICBpZiAocG9wdXBJZCAmJiBvcGVuZXJzLmxlbmd0aCkge1xyXG4gICAgICB0aGlzLmVsID0gdGhpcy5lbC5yZW1vdmVDbGFzcyh0aGlzLm92ZXJsYXlUeXBlcy5jb250ZW50T3ZlcmxheUNsYXNzKVxyXG4gICAgICAgICAgICAgIC53cmFwKCc8ZGl2IGNsYXNzPVwib3ZlcmxheS1wb3B1cCAnICsgdGhpcy5vdmVybGF5VHlwZXMuY29udGVudE92ZXJsYXlDbGFzcyArICdcIj48L2Rpdj4nKVxyXG4gICAgICAgICAgICAgIC5wYXJlbnQoKVxyXG4gICAgICAgICAgICAgIC5hdHRyKCdpZCcsIHBvcHVwSWQpXHJcbiAgICAgICAgICAgICAgLmFwcGVuZCh0aGlzLm9wdGlvbnMuY2xvc2VPdmVybGF5SWNvblRwbCk7XHJcbiAgICAgIG9wZW5lcnMuYXR0cigncmVsJywgcG9wdXBJZCk7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gdGhpcy5lbDtcclxuICB9XHJcblxyXG4gIC8vcmVtb3ZlIGlubGluZSBzY3JpcHRzIHdpdGhpbiBvdmVybGF5J3MgY29udGVudCB0byBwcmV2ZW50IGRvdWJsZSBldmFsdWF0aW9uXHJcbiAgX3JlbW92ZUlubGluZVNjcmlwdHMoKSB7XHJcbiAgICB0aGlzLmVsLmZpbmQoJ3NjcmlwdCcpLnJlbW92ZSgpO1xyXG4gIH1cclxuXHJcbn0iLCJpbXBvcnQgeyBPdmVybGF5IH0gZnJvbSAnLi4vb3ZlcmxheS9vdmVybGF5LmpzJztcclxuaW1wb3J0IHsgWW91dHViZVBsYXllciB9IGZyb20gJy4uLy4uL3ZpZGVvL3lvdXR1YmUtcGxheWVyL3lvdXR1YmUtcGxheWVyLmpzJztcclxuaW1wb3J0IHsgQnJpZ2h0Y292ZVBsYXllciB9IGZyb20gJy4uLy4uL3ZpZGVvL2JyaWdodGNvdmUtcGxheWVyL2JyaWdodGNvdmUtcGxheWVyLmpzJztcclxuXHJcblxyXG5leHBvcnQgY2xhc3MgVmlkZW9PdmVybGF5IGV4dGVuZHMgT3ZlcmxheSB7XHJcbiAgLy8gVGhpcyBjbGFzcyBpcyBhZGRlZCBhdXRvbWF0aWNhbGx5IHRvIHRoaXMuZWwgY29tcG9uZW50XHJcbiAgZ2V0IFJPT1RfQ0xBU1MoKSB7IHJldHVybiAndmlkZW8tb3ZlcmxheSc7IH1cclxuXHJcbiAgZ2V0IGRlZmF1bHRzKCkge1xyXG4gICAgcmV0dXJuICQuZXh0ZW5kKHt9LCBzdXBlci5kZWZhdWx0cywge1xyXG4gICAgICB5b3V0dWJlVmlkZW9TZWxlY3RvcjogJy55b3V0dWJlLXZpZGVvLXRlbXBsYXRlJyxcclxuICAgICAgYnJpZ2h0Y292ZVZpZGVvU2VsZWN0b3I6ICcuYmMtdmlkZW8tdGVtcGxhdGUnLFxyXG4gICAgICB2aWRlb0NvbnRhaW5lclNlbGVjdG9yOiAnLnZpZGVvLWNvbnRhaW5lcidcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgY29uc3RydWN0b3IoLi4uYXJncykge1xyXG4gICAgc3VwZXIoLi4uYXJncyk7XHJcbiAgICB0aGlzLl9zcGlubmVyID0gdGhpcy5fY3JlYXRlVmlkZW9TcGlubmVyKCk7XHJcbiAgfVxyXG5cclxuICBfY3JlYXRlVmlkZW9QbGF5ZXIoKSB7XHJcbiAgICBsZXQgeW91dHViZVZpZGVvRWxlbWVudCA9IHRoaXMuZWwuZmluZCh0aGlzLm9wdGlvbnMueW91dHViZVZpZGVvU2VsZWN0b3IpO1xyXG4gICAgbGV0IGJyaWdodGNvdmVWaWRlb0VsZW1lbnQgPSB0aGlzLmVsLmZpbmQodGhpcy5vcHRpb25zLmJyaWdodGNvdmVWaWRlb1NlbGVjdG9yKTtcclxuICAgIGlmICh5b3V0dWJlVmlkZW9FbGVtZW50Lmxlbmd0aCkge1xyXG4gICAgICByZXR1cm4gbmV3IFlvdXR1YmVQbGF5ZXIoeW91dHViZVZpZGVvRWxlbWVudCk7XHJcbiAgICB9IGVsc2UgaWYgKGJyaWdodGNvdmVWaWRlb0VsZW1lbnQubGVuZ3RoKSB7XHJcbiAgICAgIHJldHVybiBuZXcgQnJpZ2h0Y292ZVBsYXllcihicmlnaHRjb3ZlVmlkZW9FbGVtZW50KTtcclxuICAgIH1cclxuXHJcbiAgICBjb25zb2xlLndhcm4oJ05vIHN1cHBvcnRlZCB2aWRlbyBlbGVtZW50IGluIFZpZGVvT3ZlcmxheScpO1xyXG4gICAgcmV0dXJuIHt9O1xyXG4gIH1cclxuXHJcbiAgX2NyZWF0ZVZpZGVvU3Bpbm5lcigpIHtcclxuICAgIGlmICghdGhpcy5fdmlkZW9TcGlubmVyKSB7XHJcbiAgICAgIHRoaXMuc3Bpbm5lciA9ICQoJzxkaXYvPicsIHsnY2xhc3MnOiAnc3Bpbm5lcid9KTtcclxuICAgICAgdGhpcy5lbC5maW5kKHRoaXMub3B0aW9ucy52aWRlb0NvbnRhaW5lclNlbGVjdG9yKS5hcHBlbmQodGhpcy5zcGlubmVyKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIF9vbk9wZW4oKSB7XHJcbiAgICBpZiAoIXRoaXMuX3ZpZGVvUGxheWVyKSB7XHJcbiAgICAgIHRoaXMuX3ZpZGVvUGxheWVyID0gdGhpcy5fY3JlYXRlVmlkZW9QbGF5ZXIoKTtcclxuICAgICAgdGhpcy5fdmlkZW9QbGF5ZXIuaW5pdCgoKSA9PiB0aGlzLl9oaWRlU3Bpbm5lcigpKTtcclxuICAgICAgLy9pZiBwbGF5ZXIgd2FzIG5vdCBpbml0aWFsaXplZCAtIGRvbid0IGF1dG9zdGFydCB2aWRlbyBvbiBtb2JpbGVzXHJcbiAgICAgIGlmIChHTE9CQUxTLlV0aWxzLmRldmljZS5pc01vYmlsZSkge1xyXG4gICAgICAgIHN1cGVyLl9vbk9wZW4oKTtcclxuICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIHRoaXMuX3N0YXJ0VmlkZW8oKTtcclxuICAgIHN1cGVyLl9vbk9wZW4oKTtcclxuICB9XHJcblxyXG4gIF9vbkNsb3NlKCkge1xyXG4gICAgdGhpcy5fcGF1c2VWaWRlbygpO1xyXG4gICAgc3VwZXIuX29uQ2xvc2UoKTtcclxuICB9XHJcblxyXG4gIF9zdGFydFZpZGVvKCkge1xyXG4gICAgdGhpcy5fdmlkZW9QbGF5ZXIucGxheSgpO1xyXG4gIH1cclxuXHJcbiAgX3BhdXNlVmlkZW8oKSB7XHJcbiAgICB0aGlzLl92aWRlb1BsYXllci5wYXVzZSgpO1xyXG4gIH1cclxuXHJcbiAgX2hpZGVTcGlubmVyKCkge1xyXG4gICAgdGhpcy5zcGlubmVyLmZhZGVPdXQoKTtcclxuICB9XHJcbn1cclxuIiwiLypDdXN0b20gTGJTbGlkZXIqL1xyXG5leHBvcnQgY2xhc3MgTGJTbGlkZXIge1xyXG5cclxuICBnZXQgZGVmYXVsdHMoKSB7XHJcbiAgICByZXR1cm4ge1xyXG4gICAgICBzbGlkZXJDbGFzczogJ2xiLXNsaWRlciBhbmltYXRlJyxcclxuICAgICAgbm9BcnJvd3NDbGFzczogJ25vLWFycm93cycsXHJcbiAgICAgIGN1cnJlbnRTbGlkZTogMCxcclxuICAgICAgdmlzaWJsZVNsaWRlczogMCxcclxuICAgICAgY3VzdG9tQ2xhc3M6ICcnLFxyXG4gICAgICBhbGlnbkFycm93c1RvRWw6IG51bGwsXHJcbiAgICAgIGhpZGVBcnJvd3NXaGVuQ2FudFNsaWRlOiB0cnVlLFxyXG4gICAgICBvblNsaWRlOiBmdW5jdGlvbigpIHt9XHJcbiAgICB9XHJcbiAgfVxyXG4gIGNvbnN0cnVjdG9yKGVsLCBvcHRpb25zKSB7XHJcbiAgICB0aGlzLm8gPSAkLmV4dGVuZCh7fSwgdGhpcy5kZWZhdWx0cywgb3B0aW9ucyk7XHJcbiAgICB0aGlzLl9pbml0KGVsKTtcclxuICB9XHJcblxyXG4gIF9pbml0KGVsKSB7XHJcbiAgICB0aGlzLl9ib290c3RyYXAoZWwpO1xyXG4gICAgdGhpcy5fb25SZXNpemUoKTtcclxuICAgIHRoaXMuX2FjdGl2YXRlU2xpZGUodGhpcy5vLmN1cnJlbnRTbGlkZSk7XHJcbiAgICB0aGlzLl9pbml0TGlzdGVuZXJzKCk7XHJcbiAgfVxyXG5cclxuICBfYm9vdHN0cmFwKGVsKSB7XHJcbiAgICB0aGlzLnMgPSB7XHJcbiAgICAgIGVsOiBlbCxcclxuICAgICAgc2xpZGVzOiAkKGVsKS5jaGlsZHJlbigpLFxyXG4gICAgICBjdXJyZW50U2xpZGU6IHRoaXMuby5jdXJyZW50U2xpZGUsXHJcbiAgICAgIG5leHRJbmRleDogMCxcclxuICAgICAgc2xpZGVzQ291bnQ6IGVsLmNoaWxkcmVuKCkubGVuZ3RoLFxyXG4gICAgICBzbGlkZVdpZHRoOiAwLFxyXG4gICAgICB0cmFja1dpZHRoOiAwLFxyXG4gICAgICB2aXNpYmxlU2xpZGVzOiB0aGlzLm8udmlzaWJsZVNsaWRlcyxcclxuICAgICAgY3VycmVudFRyYW5zbGF0ZTogMFxyXG4gICAgfVxyXG4gICAgdGhpcy5zLmVsLmFkZENsYXNzKHRoaXMuby5zbGlkZXJDbGFzcyArICcgJyArIHRoaXMuby5jdXN0b21DbGFzcyk7XHJcbiAgICB0aGlzLl9hZGRMYlNsaWRlclRyYWNrKCk7XHJcbiAgICB0aGlzLl9hZGRDb250cm9scygpO1xyXG4gIH1cclxuXHJcbiAgX2FkZExiU2xpZGVyVHJhY2soKSB7XHJcbiAgICB0aGlzLnMubGlzdCA9ICQoJzxkaXYgLz4nLCB7Y2xhc3M6ICdsYi1zbGlkZXItbGlzdCd9KTtcclxuICAgIHRoaXMucy50cmFjayA9ICQoJzxkaXYgLz4nLCB7Y2xhc3M6ICdsYi1zbGlkZXItdHJhY2snfSk7XHJcbiAgICB0aGlzLnMubGlzdC5hcHBlbmQodGhpcy5zLnRyYWNrLmFwcGVuZCh0aGlzLnMuc2xpZGVzKSk7XHJcbiAgICB0aGlzLnMuZWwuYXBwZW5kKHRoaXMucy5saXN0LmFwcGVuZCh0aGlzLnMudHJhY2spKTtcclxuICB9XHJcblxyXG4gIF9nZXRUcmFja1dpZHRoKCkge1xyXG4gICAgcmV0dXJuIHRoaXMucy5zbGlkZVdpZHRoICogdGhpcy5zLnNsaWRlc0NvdW50O1xyXG4gIH1cclxuXHJcbiAgX2dldFNsaWRlV2lkdGgoKSB7XHJcbiAgICBpZiAoIXRoaXMucy52aXNpYmxlU2xpZGVzKSB7XHJcbiAgICAgIGxldCByZWdleHByID0gbmV3IFJlZ0V4cCgvc3BhbihcXGQqKS9pZyksXHJcbiAgICAgICAgICBzbGlkZUNsYXNzID0gdGhpcy5zLnNsaWRlcy5maXJzdCgpLmF0dHIoJ2NsYXNzJyksXHJcbiAgICAgICAgICB0ZXN0ID0gcmVnZXhwci5leGVjKHNsaWRlQ2xhc3MpO1xyXG4gICAgICBcclxuICAgICAgdGhpcy5zLnZpc2libGVTbGlkZXMgPSAodGVzdCAmJiB0ZXN0WzFdKSA/IDI0IC8gKCt0ZXN0WzFdKSA6IDFcclxuICAgIH1cclxuICAgIFxyXG4gICAgcmV0dXJuIHRoaXMucy5lbC53aWR0aCgpIC8gdGhpcy5zLnZpc2libGVTbGlkZXM7XHJcbiAgfVxyXG5cclxuICBfYWRkQ29udHJvbHMoKSB7XHJcbiAgICB0aGlzLnMubmV4dEFycm93ID0gJCgnPGJ1dHRvbiAvPicsIHtjbGFzczogJ25leHQnLCB0ZXh0OiAnTmV4dCd9KTtcclxuICAgIHRoaXMucy5wcmV2QXJyb3cgPSAkKCc8YnV0dG9uIC8+Jywge2NsYXNzOiAncHJldicsIHRleHQ6ICdQcmV2J30pO1xyXG4gICAgdGhpcy5zLmVsLmFwcGVuZCh0aGlzLnMucHJldkFycm93KS5hcHBlbmQodGhpcy5zLm5leHRBcnJvdyk7XHJcblxyXG4gICAgLy9hbGlnbiBhcnJvd3MgdG8gc29tZSBlbGVtZW50IHdpdGhpbiBzbGlkZXIgKGUuZy4gdG8gdGhlIG1pZGRsZSBvZiB0aGUgaW1nKVxyXG4gICAgdGhpcy5vLmFsaWduQXJyb3dzVG9FbCAmJiBHTE9CQUxTLlNsaWRlckhlbHBlci5hbGlnbkFycm93c1RvRWwodGhpcy5zLmVsLCB7XHJcbiAgICAgIGFycm93czogdGhpcy5zLm5leHRBcnJvdy5hZGQodGhpcy5zLnByZXZBcnJvdyksXHJcbiAgICAgIGFsaWduVG9FbDogdGhpcy5zLnNsaWRlcy5maXJzdCgpLmZpbmQodGhpcy5vLmFsaWduQXJyb3dzVG9FbClcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgX29uUmVzaXplKCkge1xyXG4gICAgdGhpcy5fdXBkYXRlTW9kZWwoKTtcclxuICAgIHRoaXMuX3VwZGF0ZURPTSgpO1xyXG4gIH1cclxuXHJcbiAgX3VwZGF0ZU1vZGVsKCkge1xyXG4gICAgdGhpcy5zLnNsaWRlV2lkdGggPSB0aGlzLl9nZXRTbGlkZVdpZHRoKCk7XHJcbiAgICB0aGlzLnMudHJhY2tXaWR0aCA9IHRoaXMuX2dldFRyYWNrV2lkdGgoKTtcclxuICAgIHRoaXMucy5tYXhUcmFuc2xhdGUgPSB0aGlzLnMudHJhY2tXaWR0aCAtIHRoaXMucy5lbC53aWR0aCgpO1xyXG4gICAgdGhpcy5zLm1heFRyYW5zbGF0ZSA9ICh0aGlzLnMubWF4VHJhbnNsYXRlIDwgMCkgPyAwIDogdGhpcy5zLm1heFRyYW5zbGF0ZTtcclxuICB9XHJcblxyXG4gIF91cGRhdGVET00oKSB7XHJcbiAgICB0aGlzLnMuc2xpZGVzLndpZHRoKHRoaXMucy5zbGlkZVdpZHRoKTtcclxuICAgIHRoaXMucy50cmFjay53aWR0aCh0aGlzLnMudHJhY2tXaWR0aCk7XHJcbiAgICB0aGlzLnNsaWRlVG8odGhpcy5zLmN1cnJlbnRTbGlkZSk7XHJcblxyXG4gICAgLy9oaWRlIGFycm93cyB3aGVuIG5vIGNvbnRlbnQgdG8gc2Nyb2xsXHJcbiAgICBpZiAodGhpcy5vLmhpZGVBcnJvd3NXaGVuQ2FudFNsaWRlKSB7XHJcbiAgICAgIHRoaXMucy5lbFt0aGlzLnMuZWwud2lkdGgoKSA+PSB0aGlzLnMudHJhY2tXaWR0aCA/ICdhZGRDbGFzcycgOiAncmVtb3ZlQ2xhc3MnXSh0aGlzLm8ubm9BcnJvd3NDbGFzcyk7XHJcbiAgICB9XHJcblxyXG4gICAgdGhpcy5zLmVsLnRyaWdnZXIoJ3NldFBvc2l0aW9uJyk7XHJcbiAgfVxyXG5cclxuICBfaW5pdExpc3RlbmVycygpIHtcclxuICAgICQod2luZG93KS5vbigncmVzaXplIG9yaWVudGF0aW9uY2hhbmdlIGxvYWQnLCBfLmRlYm91bmNlKCgpID0+IHRoaXMuX29uUmVzaXplKCksIDUwKSk7XHJcbiAgICB0aGlzLnMubmV4dEFycm93IC5vbignY2xpY2snLCAoKSA9PiB0aGlzLnNsaWRlTmV4dCgpKTtcclxuICAgIHRoaXMucy5wcmV2QXJyb3cgLm9uKCdjbGljaycsICgpID0+IHRoaXMuc2xpZGVQcmV2KCkpO1xyXG4gICAgdGhpcy5zLnNsaWRlcy5vbignY2xpY2snLCAoZSkgPT4gdGhpcy5oYW5kbGVTbGlkZUNsaWNrKGUpKTtcclxuICAgIHRoaXMuX2luaXRNb2JpbGVFdmVudHMoKTtcclxuICB9XHJcblxyXG4gIGhhbmRsZVNsaWRlQ2xpY2soZSkge1xyXG4gICAgdGhpcy5zbGlkZVRvKCQoZS5jdXJyZW50VGFyZ2V0KS5pbmRleCgpKTtcclxuICB9XHJcblxyXG4gIHNsaWRlTmV4dCgpIHtcclxuICAgIHRoaXMucy5uZXh0SW5kZXggPSB0aGlzLnMuY3VycmVudFNsaWRlICsgMTtcclxuICAgICh0aGlzLnMubmV4dEluZGV4ID49IHRoaXMucy5zbGlkZXNDb3VudCkgJiYgKHRoaXMucy5uZXh0SW5kZXggPSAwKTtcclxuXHJcbiAgICB0aGlzLnNsaWRlVG8oKTtcclxuICB9XHJcblxyXG4gIHNsaWRlUHJldigpIHtcclxuICAgIHRoaXMucy5uZXh0SW5kZXggPSB0aGlzLnMuY3VycmVudFNsaWRlIC0gMTtcclxuICAgICh0aGlzLnMubmV4dEluZGV4IDwgMCkgJiYgKHRoaXMucy5uZXh0SW5kZXggPSB0aGlzLnMuc2xpZGVzQ291bnQgLSAxKTtcclxuXHJcbiAgICB0aGlzLnNsaWRlVG8oKTtcclxuICB9XHJcblxyXG4gIHNsaWRlVG8oaW5kZXgpIHtcclxuICAgIGxldCBpbmQgPSAoaW5kZXggIT09IHVuZGVmaW5lZCkgPyBpbmRleCA6IHRoaXMucy5uZXh0SW5kZXgsXHJcbiAgICAgICAgbW92ZVggPSB0aGlzLl9nZXRNb3ZlWChpbmQpO1xyXG5cclxuICAgIC8vc2Nyb2xsVG9FbGVtZW50IG9ubHkgaWYgaXQgaXMgbm90IGN1cnJlbnRseSB2aXNpYmxlXHJcbiAgICB0aGlzLnMuY3VycmVudFRyYW5zbGF0ZSA9IG1vdmVYO1xyXG4gICAgXHJcbiAgICAvL3NsaWRlIHRvIHB4XHJcbiAgICB0aGlzLl9zbGlkZShtb3ZlWCk7XHJcblxyXG4gICAgLy9yZXNldCBuZXh0SW5kZXggb25jZSBzbGlkaW5nIGluaXRpYWxpemVkXHJcbiAgICB0aGlzLnMubmV4dEluZGV4ID0gMDtcclxuXHJcbiAgICAvL3VwZGF0ZSBjdXJyZW50U2xpZGVcclxuICAgIHRoaXMucy5jdXJyZW50U2xpZGUgPSBpbmQ7XHJcblxyXG4gICAgLy9hY3RpdmF0ZSBzbGlkZVxyXG4gICAgdGhpcy5fYWN0aXZhdGVTbGlkZShpbmQpO1xyXG4gIH1cclxuXHJcbiAgX3NsaWRlKHRvLCBzdGlja1RvU2xpZGUsIGN1cnJlbnRUcmFuc2xhdGUpIHtcclxuICAgIGxldCBzY3JvbGxUbyA9IHN0aWNrVG9TbGlkZSA/IHRoaXMuX2dldENsb3Nlc3RJdGVtVHJhbnNsYXRlKHRvKSA6IHRvO1xyXG4gICAgdGhpcy5zLnRyYWNrLmNzcygndHJhbnNmb3JtJyxgdHJhbnNsYXRlM2QoJHtzY3JvbGxUb31weCwwLDApYCk7XHJcbiAgfVxyXG5cclxuICBfZ2V0Q2xvc2VzdEl0ZW1UcmFuc2xhdGUodG8pIHtcclxuICAgIHJldHVybiAoLTEpICogdGhpcy5zLnNsaWRlV2lkdGggKiBNYXRoLnJvdW5kKE1hdGguYWJzKHRvKSAvIHRoaXMucy5zbGlkZVdpZHRoKTtcclxuICB9XHJcblxyXG4gIF9nZXRNb3ZlWChpbmQpIHtcclxuICAgIGxldCBtb3ZlWCA9IHRoaXMuX2luZGV4SW50b1B4KGluZCk7XHJcblxyXG4gICAgaWYgKG1vdmVYIDwgMCkgcmV0dXJuIDA7XHJcbiAgICBpZiAobW92ZVggPj0gdGhpcy5zLm1heFRyYW5zbGF0ZSkgcmV0dXJuIHRoaXMucy5tYXhUcmFuc2xhdGUgKiAoLTEpO1xyXG5cclxuICAgIHJldHVybiBtb3ZlWCAqICgtMSk7XHJcbiAgfVxyXG5cclxuICBfaW5kZXhJbnRvUHgoaW5kZXgpIHtcclxuICAgIHJldHVybiBpbmRleCAqIHRoaXMucy5zbGlkZVdpZHRoO1xyXG4gIH1cclxuXHJcbiAgX2FjdGl2YXRlU2xpZGUoaW5kZXgpIHtcclxuICAgIGxldCBpID0gaW5kZXggfHwgdGhpcy5zLmN1cnJlbnRTbGlkZTtcclxuICAgIHRoaXMucy5zbGlkZXMucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpLmVxKGkpLmFkZENsYXNzKCdhY3RpdmUnKTtcclxuXHJcbiAgICAvL2NhbGxiYWNrXHJcbiAgICB0aGlzLm8ub25TbGlkZShpKTtcclxuICB9XHJcblxyXG4gIF9pbml0TW9iaWxlRXZlbnRzKCkge1xyXG4gICAgbGV0IHRyYW5zbGF0ZSA9IDAsXHJcbiAgICAgICAgdG91Y2ggPSB7XHJcbiAgICAgICAgICBpbkFjdGlvbjogZmFsc2UsXHJcbiAgICAgICAgICBzdGFydFNsaWRlclBvczogMCxcclxuICAgICAgICAgIHN0YXJ0UG9pbnRlclBvczogMCxcclxuICAgICAgICAgIHByZUxhc3RUaW1lOiAwLFxyXG4gICAgICAgICAgcHJlTGFzdFBvczogMCxcclxuICAgICAgICAgIGxhc3RUaW1lOiAwLFxyXG4gICAgICAgICAgbGFzdFBvczogMFxyXG4gICAgICAgIH1cclxuXHJcbiAgICB0aGlzLnMuZWwub24oJ3RvdWNoc3RhcnQnLCAoZSkgPT4ge1xyXG4gICAgICB0b3VjaC5pbkFjdGlvbiA9IGZhbHNlO1xyXG4gICAgICB0cmFuc2xhdGUgPSB0b3VjaC5zdGFydFNsaWRlclBvcyA9IHRoaXMuX2dldEN1cnJlbnRUcmFuc2xhdGUoKTtcclxuICAgICAgdG91Y2guc3RhcnRQb2ludGVyUG9zID0gZS5vcmlnaW5hbEV2ZW50LmNoYW5nZWRUb3VjaGVzWzBdLmNsaWVudFg7XHJcbiAgICB9KTtcclxuXHJcbiAgICB0aGlzLnMuZWwub24oJ3RvdWNobW92ZScsIChlKSA9PiB7XHJcbiAgICAgIHRoaXMucy5lbC5yZW1vdmVDbGFzcygnYW5pbWF0ZScpO1xyXG4gICAgICB0b3VjaC5pbkFjdGlvbiA9IHRydWU7XHJcblxyXG4gICAgICB0b3VjaC5wcmVMYXN0VGltZSA9IHRvdWNoLmxhc3RUaW1lO1xyXG4gICAgICB0b3VjaC5wcmVMYXN0UG9zID0gdG91Y2gubGFzdFBvcztcclxuICAgICAgdG91Y2gubGFzdFRpbWUgPSBlLnRpbWVTdGFtcDtcclxuICAgICAgdG91Y2gubGFzdFBvcyA9IGUub3JpZ2luYWxFdmVudC5jaGFuZ2VkVG91Y2hlc1swXS5jbGllbnRYO1xyXG4gICAgICB0cmFuc2xhdGUgPSB0b3VjaC5zdGFydFNsaWRlclBvcyArIHRvdWNoLmxhc3RQb3MgLSB0b3VjaC5zdGFydFBvaW50ZXJQb3M7XHJcblxyXG4gICAgICAvL3NsaWRpbmcgb3V0IG9mIGF2YWlsYWJsZSBhcmVhXHJcbiAgICAgIGlmICh0cmFuc2xhdGUgPiAwKSB7XHJcbiAgICAgICAgdGhpcy5fc2xpZGUodHJhbnNsYXRlLzQpO1xyXG4gICAgICB9IGVsc2UgaWYgKE1hdGguYWJzKHRyYW5zbGF0ZSkgPiB0aGlzLnMubWF4VHJhbnNsYXRlKSB7XHJcbiAgICAgICAgdGhpcy5fc2xpZGUoLXRoaXMucy5tYXhUcmFuc2xhdGUgKyAoKHRoaXMucy5tYXhUcmFuc2xhdGUgKyB0cmFuc2xhdGUpIC8gNCkpO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHRoaXMuX3NsaWRlKHRyYW5zbGF0ZSk7XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG5cclxuICAgIHRoaXMucy5lbC5vbigndG91Y2hlbmQnLCAoKSA9PiB7XHJcbiAgICAgIGlmICh0b3VjaC5pbkFjdGlvbikge1xyXG4gICAgICAgIGxldCB0aW1lID0gdG91Y2gubGFzdFRpbWUgLSB0b3VjaC5wcmVMYXN0VGltZSxcclxuICAgICAgICAgICAgZGlzdGFuY2UgPSB0b3VjaC5sYXN0UG9zIC0gdG91Y2gucHJlTGFzdFBvcyxcclxuICAgICAgICAgICAgaXNOZWdhdGl2ZSA9IGRpc3RhbmNlIDwgMCA/IC0xIDogMSxcclxuICAgICAgICAgICAgcG93ZXIgPSBNYXRoLmFicyhkaXN0YW5jZSAvIHRpbWUgKiAxMCksXHJcbiAgICAgICAgICAgIHNwID0gcG93ZXIgPiA1ID8gKChwb3dlciAqIDE3MjUgLSA2MjI1KSAvIDMyICogaXNOZWdhdGl2ZSkgOiAocG93ZXIgKiAxNSAqIGlzTmVnYXRpdmUpO1xyXG5cclxuICAgICAgICB0aGlzLnMuZWwuYWRkQ2xhc3MoJ2FuaW1hdGUnKTtcclxuXHJcbiAgICAgICAgbGV0IHNsaWRlVG8gPSB0cmFuc2xhdGUgKyBzcDtcclxuXHJcbiAgICAgICAgLy9oYW5kbGUgb3V0IG9mIGxlZnQgc2NyZWVuIGVkZ2VcclxuICAgICAgICBpZiAodHJhbnNsYXRlID4gMCB8fCBzbGlkZVRvID4gMCkge1xyXG4gICAgICAgICAgc2xpZGVUbyA9IDA7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvL2hhbmRsZSBvdXQgb2YgcmlnaHQgc2NyZWVuIGVkZ2VcclxuICAgICAgICBpZiAodHJhbnNsYXRlIDwgLXRoaXMucy5tYXhUcmFuc2xhdGUgfHwgc2xpZGVUbyA8IC10aGlzLnMubWF4VHJhbnNsYXRlKSB7XHJcbiAgICAgICAgICBzbGlkZVRvID0gLXRoaXMucy5tYXhUcmFuc2xhdGU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIFxyXG4gICAgICAgIHRoaXMuX3NsaWRlKHNsaWRlVG8sIHRydWUsIHRyYW5zbGF0ZSk7XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgX2dldEN1cnJlbnRUcmFuc2xhdGUoKSB7XHJcbiAgICBsZXQgcmVnRXhwciA9IG5ldyBSZWdFeHAoL3RyYW5zbGF0ZTNkXFwoKC4qPylweC8pLFxyXG4gICAgICAgIHN0eWxlID0gdGhpcy5zLnRyYWNrLmF0dHIoJ3N0eWxlJyksXHJcbiAgICAgICAgdGVzdCA9IHJlZ0V4cHIuZXhlYyhzdHlsZSk7XHJcblxyXG4gICAgaWYgKHRlc3QpIHJldHVybiArdGVzdFsxXTtcclxuXHJcbiAgICByZXR1cm4gMDtcclxuICB9XHJcblxyXG59IiwiaW1wb3J0IHsgQmFzZUNvbXBvbmVudCB9IGZyb20gJy4uL2Jhc2UtY29tcG9uZW50LmpzJztcclxuXHJcbi8qXHJcbiAqIEBkZXNjcmlwdGlvbiBJbnRyb2R1Y2VzIGZhY3RvcnkgdGhhdCBjcmVhdGVzIGEgY2VydGFpbiB0eXBlIG9mIHNsaWRlciBkZXBlbmRpbmcgb24gYSBzZWxlY3RvclxyXG4gKi9cclxuZXhwb3J0IGNsYXNzIFNsaWRlcnNGYWN0b3J5IHtcclxuXHJcbiAgc3RhdGljIGdldCBzZWxlY3RvcigpIHsgcmV0dXJuICcuanMtc2xpZGVyLCAuYWRhcHRpdmUtc2xpZGVyJzsgfVxyXG5cclxuICBnZXQgc2xpZGVyVHlwZXMoKSB7XHJcbiAgICByZXR1cm4ge1xyXG4gICAgICByZWd1bGFyOiAnanMtc2xpZGVyJyxcclxuICAgICAgYWRhcHRpdmU6ICdhZGFwdGl2ZS1zbGlkZXInXHJcbiAgICB9XHJcbiAgfVxyXG4gIGNvbnN0cnVjdG9yKGVsKSB7XHJcbiAgICB0aGlzLl9pbml0KGVsKTtcclxuICB9XHJcblxyXG4gIF9pbml0KGVsKSB7XHJcbiAgICBpZiAoJChlbCkuaGFzQ2xhc3ModGhpcy5zbGlkZXJUeXBlcy5yZWd1bGFyKSkge1xyXG4gICAgICBuZXcgUmVndWxhclNsaWRlcihlbCk7XHJcbiAgICB9XHJcbiAgICBpZiAoJChlbCkuaGFzQ2xhc3ModGhpcy5zbGlkZXJUeXBlcy5hZGFwdGl2ZSkpIHtcclxuICAgICAgbmV3IEFkYXB0aXZlU2xpZGVyKCQoZWwpLmZpbmQoJz4ucm93JykpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbn1cclxuXHJcbi8qXHJcbiAqIEBkZXNjcmlwdGlvbiBSZWd1bGFyIExvb2tib29rKEhQSSBsaWtlKSBzbGlkZXIsIHRoYXQgaXMgY3JlYXRlZCBmb3IgYW55IC5qcy1zbGlkZXIgc2VsZWN0b3JcclxuICovXHJcbmNsYXNzIFJlZ3VsYXJTbGlkZXIgZXh0ZW5kcyBCYXNlQ29tcG9uZW50IHtcclxuXHJcbiAgZ2V0IGRlZmF1bHRzKCkge1xyXG4gICAgcmV0dXJuICQuZXh0ZW5kKHt9LCBzdXBlci5kZWZhdWx0cywge1xyXG4gICAgICBpbmZpbml0ZTogdHJ1ZSxcclxuICAgICAgaW50ZXJ2YWw6IDUwMDAsXHJcbiAgICAgIGF1dG9zdGFydDogZmFsc2UsXHJcbiAgICAgIG5hdlR5cGU6ICdib3RoJyxcclxuICAgICAgcmVhZHlDbGFzczogJ3JlYWR5JyxcclxuICAgICAgYnVsbGV0ZWREb3RzQ2xhc3M6ICdidWxsZXRlZC1kb3RzJyxcclxuICAgICAgcm93U2xpZGVyQ2xhc3M6ICdyb3ctc2xpZGVyJyxcclxuICAgICAgbGlnaHRDb2xvckNsYXNzOiAnZm9udC1jb2xvci10aGVtZTEnLFxyXG4gICAgICBlbmFibGVBcnJvd3NNb2JpbGVDbGFzczogJ2VuYWJsZS1hcnJvd3MtbW9iaWxlJyxcclxuICAgICAgY2Fyb3VzZWxDb3VudGVyU2VsZWN0b3I6ICcuZGlzcGxheS1jYXJvdXNlbC1jb3VudGVyJyxcclxuICAgICAgYW1vdW50U2xpZGVzOiAwXHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIGNvbnN0cnVjdG9yKC4uLmFyZ3MpIHtcclxuICAgIHN1cGVyKC4uLmFyZ3MpO1xyXG4gICAgdGhpcy5faW5pdCgpO1xyXG4gIH1cclxuXHJcbiAgX2luaXQoKSB7XHJcbiAgICB0aGlzLmNvdW50ZXJFbCA9IG51bGw7XHJcbiAgICB0aGlzLl91cGRhdGVPcHRpb25zKCk7XHJcbiAgICB0aGlzLl9idWlsZCgpO1xyXG4gICAgdGhpcy5fc2V0Q291bnRlcigpO1xyXG4gICAgdGhpcy5fb25SZWFkeSgpO1xyXG4gIH1cclxuXHJcbiAgLy9NZXJnZSBkZWZhdWx0IG9wdGlvbnMgd2l0aCBkYXRhLW9wdGlvbnNcclxuICBfdXBkYXRlT3B0aW9ucygpIHtcclxuICAgIHRoaXMub3B0aW9ucyA9ICQuZXh0ZW5kKHt9LCB0aGlzLm9wdGlvbnMsIHRoaXMuZWwuZGF0YSgpKTtcclxuXHJcbiAgICAvL2luaXRpYWxpemUgYXV0b3JvdGF0ZSBtYW51YWxseSB2aWEgY2xhc3NuYW1lLiBVc2VmdWwgZm9yIGF1dG9yb3RhdGlvbiB3aXRoIG9ubHkgQVJST1dTXHJcbiAgICB0aGlzLmVsLmNsb3Nlc3QoJy5jYXJvdXNlbC1hdXRvLXJvdGF0ZScpLmxlbmd0aCAmJiAodGhpcy5vcHRpb25zLmF1dG9zdGFydCA9ICd0cnVlJyk7XHJcbiAgfVxyXG5cclxuICBfc2V0Q291bnRlcigpIHtcclxuICAgIGlmICh0aGlzLmVsLmNsb3Nlc3QodGhpcy5vcHRpb25zLmNhcm91c2VsQ291bnRlclNlbGVjdG9yKS5sZW5ndGggfHwgdGhpcy5lbC5maW5kKHRoaXMub3B0aW9ucy5jYXJvdXNlbENvdW50ZXJTZWxlY3RvcikubGVuZ3RoKSB7XHJcbiAgICAgIGxldCBzbGljayA9IHRoaXMuZWwuc2xpY2soJ2dldFNsaWNrJyk7XHJcbiAgICAgIHRoaXMub3B0aW9ucy5hbW91bnRTbGlkZXMgPSBzbGljay5zbGlkZUNvdW50O1xyXG4gICAgICB0aGlzLmNvdW50ZXJFbCA9ICQoJzxzcGFuIC8+Jywge2NsYXNzOiAncGFnaW5nSW5mbyd9KTtcclxuICAgICAgdGhpcy5lbC5vbigncmVJbml0IGFmdGVyQ2hhbmdlJywgKGUsIHNsaWNrLCBjdXJyZW50U2xpZGUpID0+IHRoaXMuX3VwZGF0ZUNvdW50ZXIoY3VycmVudFNsaWRlKSlcclxuICAgICAgICAgICAgIC5hcHBlbmQodGhpcy5jb3VudGVyRWwpO1xyXG5cclxuICAgICAgdGhpcy5fdXBkYXRlQ291bnRlcihzbGljay5jdXJyZW50U2xpZGUpO1xyXG4gICAgfTtcclxuICB9XHJcblxyXG4gIF91cGRhdGVDb3VudGVyKGN1cnJlbnRTbGlkZSkge1xyXG4gICAgdGhpcy5jb3VudGVyRWwudGV4dChgJHtjdXJyZW50U2xpZGUgKyAxfS8ke3RoaXMub3B0aW9ucy5hbW91bnRTbGlkZXN9YCk7XHJcbiAgfVxyXG5cclxuICBfYnVpbGQoKSB7XHJcbiAgICB0aGlzLmVsXHJcbiAgICAgIC5vbignaW5pdCcsIHRoaXMuX2FmdGVyQ2hhbmdlKVxyXG4gICAgICAub24oJ2JlZm9yZUNoYW5nZScsIHRoaXMuX2JlZm9yZUNoYW5nZSlcclxuICAgICAgLm9uKCdhZnRlckNoYW5nZScsIHRoaXMuX2FmdGVyQ2hhbmdlKVxyXG4gICAgICAuc2xpY2soe1xyXG4gICAgICAgIGRvdHM6IHRoaXMub3B0aW9ucy5uYXZUeXBlID09PSAnYm90aCcgfHwgdGhpcy5vcHRpb25zLm5hdlR5cGUgPT09ICdkb3R0ZWQnLFxyXG4gICAgICAgIGFycm93czogdGhpcy5vcHRpb25zLm5hdlR5cGUgPT09ICdib3RoJyB8fCB0aGlzLm9wdGlvbnMubmF2VHlwZSA9PT0gJ2Fycm93JyB8fCB0aGlzLmVsLmF0dHIoJ2RhdGEtYXV0b3N0YXJ0JykgPT09ICd0cnVlJyxcclxuICAgICAgICBhdXRvcGxheTogdGhpcy5vcHRpb25zLmF1dG9zdGFydCxcclxuICAgICAgICB0b3VjaE1vdmU6IGZhbHNlLFxyXG4gICAgICAgIGF1dG9wbGF5U3BlZWQ6IHRoaXMub3B0aW9ucy5pbnRlcnZhbCxcclxuICAgICAgICBpbmZpbml0ZTogdHJ1ZSxcclxuICAgICAgICB0b3VjaFRocmVzaG9sZDogMTAwXHJcbiAgICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgX2JlZm9yZUNoYW5nZShlLCBzbGljaykge1xyXG4gICAgc2xpY2suJHNsaWRlcy5lcShzbGljay5jdXJyZW50U2xpZGUpLmZpbmQoJy5mbGV4Mi1tb2xlY3VsZScpLnRyaWdnZXIoJ3NsaWRlci52aWV3cG9ydG91dCcpO1xyXG4gIH1cclxuXHJcbiAgX2FmdGVyQ2hhbmdlKGUsIHNsaWNrKSB7XHJcbiAgICBzbGljay4kc2xpZGVzLmVxKHNsaWNrLmN1cnJlbnRTbGlkZSkuZmluZCgnLmZsZXgyLW1vbGVjdWxlJykudHJpZ2dlcignc2xpZGVyLnZpZXdwb3J0aW4nKTtcclxuICB9XHJcblxyXG4gIF9vblJlYWR5KCkge1xyXG4gICAgdGhpcy5lbC5maW5kKCcuc2VjdGlvbi5zbGljay1zbGlkZScpLmxlbmd0aCAmJiB0aGlzLmVsLmFkZENsYXNzKHRoaXMub3B0aW9ucy5yb3dTbGlkZXJDbGFzcyk7XHJcbiAgICB0aGlzLmVsLmZpbmQoJy5zZWN0aW9uLmZvbnQtY29sb3ItdGhlbWUxJykubGVuZ3RoICYmIHRoaXMuZWwuYWRkQ2xhc3ModGhpcy5vcHRpb25zLmxpZ2h0Q29sb3JDbGFzcyk7XHJcbiAgICB0aGlzLmVsLmZpbmQoJy5zZWN0aW9uLnByZXNlcnZlLXJvdy1jb250ZW50JykubGVuZ3RoICYmIHRoaXMuZWwuYWRkQ2xhc3MoJ3ByZXNlcnZlLXJvdy1jb250ZW50Jyk7XHJcbiAgICB0aGlzLmVsLmFkZENsYXNzKHRoaXMub3B0aW9ucy5yZWFkeUNsYXNzKTtcclxuXHJcbiAgICAvL2FkZCBidWxsZXRlZCBkb3RzIGFwcGVyYW5jZSBvbmx5IGlmIE5PIEtXICdjYXJvdXNlbC1hZGQtbGluZXMnIGlzIGFwcGxpZWQgYW5kIE5PIGF1dG9yb3RhdGUuXHJcbiAgICBpZiAoIXRoaXMuZWwuY2xvc2VzdCgnLmNhcm91c2VsLWFkZC1saW5lcycpLmxlbmd0aCAmJiAhdGhpcy5vcHRpb25zLmF1dG9zdGFydCkge1xyXG4gICAgICB0aGlzLmVsLmFkZENsYXNzKHRoaXMub3B0aW9ucy5idWxsZXRlZERvdHNDbGFzcyk7XHJcbiAgICB9XHJcblxyXG4gICAgLyppZiBvbmx5IEFSUk9XUyBhcmUgdmlzaWJsZSBvbiBkZXNrdG9wIG9yIGEgJ2Nhcm91c2VsLWFycm93cy1vbi1tb2JpbGUnIEtXIGlzIGFwcGxpZWRcclxuICAgICAgLSBzaG93IGFycm93cyBvbiBtb2JpbGVzKi9cclxuICAgIGlmICh0aGlzLmVsLmNsb3Nlc3QoJy5jYXJvdXNlbC1hcnJvd3Mtb24tbW9iaWxlJykubGVuZ3RoIHx8IHRoaXMub3B0aW9ucy5uYXZUeXBlID09PSAnYXJyb3cnIHx8IHRoaXMuZWwuZmluZCgnLmNhcm91c2VsLWFycm93cy1vbi1tb2JpbGUnKS5sZW5ndGgpIHtcclxuICAgICAgdGhpcy5lbC5maW5kKCcuc2xpY2stYXJyb3cnKS5hZGRDbGFzcyh0aGlzLm9wdGlvbnMuZW5hYmxlQXJyb3dzTW9iaWxlQ2xhc3MpO1xyXG4gICAgfVxyXG5cclxuICAgICQod2luZG93KS5vbihcInJlc2l6ZVwiLCAoKSA9PiBfLmRlYm91bmNlKCgpID0+IHRoaXMuZWwuc2xpY2soJ3NldFBvc2l0aW9uJyksIDEwMCkpO1xyXG4gIH1cclxuXHJcbn1cclxuXHJcbi8qXHJcbiAqIEBkZXNjcmlwdGlvbiBBZGFwdGl2ZSBMb29rYm9vayBzbGlkZXIsIHRoYXQgaXMgY3JlYXRlZCBmb3IgYW55IC5hZGFwdGl2ZS1zbGlkZXIgc2VsZWN0b3IuXHJcbiAqICAgICAgICBUdXJucyBjZWxscyBpbiBhIHNsaWRlciBvbmNlXHJcbiAqL1xyXG5jbGFzcyBBZGFwdGl2ZVNsaWRlciBleHRlbmRzIEJhc2VDb21wb25lbnQge1xyXG4gIGdldCBkZWZhdWx0cygpIHtcclxuICAgIHJldHVybiB7XHJcbiAgICAgIGluaXRpYWxpemVkQ2xhc3NuYW1lOiAnc2xpY2staW5pdGlhbGl6ZWQnLFxyXG4gICAgICBzaG93TmV4dFByZXZTbGlkZXM6ICdzaG93LW5leHQtcHJldicsXHJcbiAgICAgIGluaXRCZWxvd0JyZWFrcG9pbnQ6IDcyMCxcclxuICAgICAgcmVhZHlDbGFzczogJ3JlYWR5JyxcclxuICAgICAgZGlzYWJsZUluZmluaXR5U2VsZWN0b3I6ICcubm9sb29wJyxcclxuICAgICAgaW5pdGlhbFNsaWRlU2VsZWN0b3I6ICdbY2xhc3MqPVwiaW5pdGlhbC1zbGlkZVwiXScsXHJcbiAgICAgIGJ1bGxldGVkRG90c0NsYXNzOiAnYnVsbGV0ZWQtZG90cycsXHJcbiAgICAgIHR5cGVzOiB7XHJcbiAgICAgICAgcmVndWxhcjogJ2FkYXB0aXZlJyxcclxuICAgICAgICBjdXN0b206ICduZXh0UHJldidcclxuICAgICAgfVxyXG4gICAgfTtcclxuICB9XHJcblxyXG4gIGNvbnN0cnVjdG9yKC4uLmFyZ3MpIHtcclxuICAgIHN1cGVyKC4uLmFyZ3MpO1xyXG4gICAgdGhpcy5faW5pdCgpO1xyXG4gIH1cclxuXHJcbiAgX2luaXQoKSB7XHJcbiAgICB0aGlzLm9wdHMgPSB7XHJcbiAgICAgIGRvdHM6IHRydWUsXHJcbiAgICAgIHRvdWNoTW92ZTogZmFsc2UsXHJcbiAgICAgIGluZmluaXRlOiAhdGhpcy5lbC5wYXJlbnRzKHRoaXMub3B0aW9ucy5kaXNhYmxlSW5maW5pdHlTZWxlY3RvcikubGVuZ3RoLFxyXG4gICAgICBpbml0aWFsU2xpZGU6IHRoaXMuX2dldEluaXRpYWxTbGlkZSgpLFxyXG4gICAgICB0b3VjaFRocmVzaG9sZDogMTAwLFxyXG4gICAgICBzbGlkZTogJ2Rpdjpub3QoLmlnbm9yZSknXHJcbiAgICB9XHJcbiAgICB0aGlzLnNob3dOZXh0UHJldk9wdHMgPSB7XHJcbiAgICAgIGNlbnRlck1vZGU6IHRydWUsXHJcbiAgICAgIGNlbnRlclBhZGRpbmc6ICcyOCUnLFxyXG4gICAgICBzbGlkZXNUb1Nob3c6IDEsXHJcbiAgICAgIHJlc3BvbnNpdmU6IFt7XHJcbiAgICAgICAgICBicmVha3BvaW50OiAxMzUwLFxyXG4gICAgICAgICAgc2V0dGluZ3M6IHtcclxuICAgICAgICAgICAgY2VudGVyUGFkZGluZzogJzIwJScsXHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSxcclxuICAgICAgICB7XHJcbiAgICAgICAgICBicmVha3BvaW50OiA3MjAsXHJcbiAgICAgICAgICBzZXR0aW5nczoge1xyXG4gICAgICAgICAgICBjZW50ZXJQYWRkaW5nOiAwXHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICBdXHJcbiAgICB9XHJcbiAgICB0aGlzLm9wdGlvbnMudHlwZSA9IHRoaXMuX2dldFNsaWRlclR5cGUoKTtcclxuICAgIHRoaXMuX2NoZWNrSW5pdGlhbGl6YXRpb24oKTtcclxuICAgIHRoaXMuX29uUmVhZHkoKTtcclxuXHJcbiAgfVxyXG5cclxuICAvL2RldGVjdHMgaW5pdGlhbCBzbGlkZSBpbmRleCBmcm9tIGNsYXNzbmFtZSBpbml0aWFsLXNsaWRlLVhYWFxyXG4gIF9nZXRJbml0aWFsU2xpZGUoKSB7XHJcbiAgICBsZXQgaW5pdGlhbEluZGV4ID0gMCxcclxuICAgICAgaW5pdGlhbFNsaWRlSW5kaWNhdG9yRWwgPSB0aGlzLmVsLnBhcmVudHModGhpcy5vcHRpb25zLmluaXRpYWxTbGlkZVNlbGVjdG9yKTtcclxuXHJcbiAgICBpZiAoaW5pdGlhbFNsaWRlSW5kaWNhdG9yRWwubGVuZ3RoKSB7XHJcbiAgICAgIGluaXRpYWxJbmRleCA9IC9pbml0aWFsLXNsaWRlLShcXGR7MSx9KS8uZXhlYyhpbml0aWFsU2xpZGVJbmRpY2F0b3JFbC5hdHRyKCdjbGFzcycpKVsxXTtcclxuICAgICAgaW5pdGlhbEluZGV4ID0gK2luaXRpYWxJbmRleCAtIDFcclxuICAgIH1cclxuICAgIHJldHVybiBpbml0aWFsSW5kZXg7XHJcbiAgfVxyXG5cclxuICBfZ2V0U2xpZGVyVHlwZSgpIHtcclxuICAgIHJldHVybiB0aGlzLmVsLnBhcmVudCgpLmhhc0NsYXNzKHRoaXMub3B0aW9ucy5zaG93TmV4dFByZXZTbGlkZXMpID8gdGhpcy5vcHRpb25zLnR5cGVzLmN1c3RvbSA6IHRoaXMub3B0aW9ucy50eXBlcy5yZWd1bGFyO1xyXG4gIH1cclxuXHJcbiAgX2NoZWNrSW5pdGlhbGl6YXRpb24oKSB7XHJcbiAgICAvL0N1c3RvbSBhcHBlcmFuY2UuIFNob3cgcGFydGlhbGx5IG5leHQgYW5kIHByZXYgc2xpZGVzXHJcbiAgICBpZiAodGhpcy5lbC5wYXJlbnQoKS5oYXNDbGFzcyh0aGlzLm9wdGlvbnMuc2hvd05leHRQcmV2U2xpZGVzKSkge1xyXG5cclxuICAgICAgLy9QYWlyZWQgc2xpZGVycyBTVEFSVFxyXG4gICAgICBpZiAodGhpcy5lbC5wYXJlbnQoKS5oYXNDbGFzcygnc2xpZGVyLW5hdicpICYmICQoJy5zbGlkZXItZm9yJykubGVuZ3RoKSB7XHJcbiAgICAgICAgdGhpcy5lbC5hZGRDbGFzcygnc2xpZGVyLW5hdicpLnBhcmVudCgpLnJlbW92ZUNsYXNzKCdzbGlkZXItbmF2Jyk7XHJcbiAgICAgICAgdGhpcy5zaG93TmV4dFByZXZPcHRzID0gJC5leHRlbmQoe30sIHRoaXMuc2hvd05leHRQcmV2T3B0cywge1xyXG4gICAgICAgICAgYXNOYXZGb3I6ICcuc2xpZGVyLWZvcicsXHJcbiAgICAgICAgICBkb3RzOiBmYWxzZVxyXG4gICAgICAgIH0pO1xyXG4gICAgICB9XHJcbiAgICAgIGlmICh0aGlzLmVsLnBhcmVudCgpLmhhc0NsYXNzKCdzbGlkZXItZm9yJykgJiYgJCgnLnNsaWRlci1uYXYnKS5sZW5ndGgpIHtcclxuICAgICAgICB0aGlzLmVsLmFkZENsYXNzKCdzbGlkZXItZm9yJykucGFyZW50KCkucmVtb3ZlQ2xhc3MoJ3NsaWRlci1mb3InKTtcclxuICAgICAgICB0aGlzLnNob3dOZXh0UHJldk9wdHMgPSAkLmV4dGVuZCh7fSwgdGhpcy5zaG93TmV4dFByZXZPcHRzLCB7XHJcbiAgICAgICAgICBhc05hdkZvcjogJy5zbGlkZXItbmF2JyxcclxuICAgICAgICAgIGFycm93czogZmFsc2UsXHJcbiAgICAgICAgICBzcGVlZDogMFxyXG4gICAgICAgIH0pO1xyXG4gICAgICB9XHJcbiAgICAgIC8vUGFpcmVkIHNsaWRlcnMgRU5EXHJcblxyXG4gICAgICB0aGlzLm9wdHMgPSAkLmV4dGVuZCh7fSwgdGhpcy5vcHRzLCB0aGlzLnNob3dOZXh0UHJldk9wdHMpO1xyXG4gICAgICB0aGlzLl9idWlsZCgpO1xyXG4gICAgICByZXR1cm47XHJcbiAgICB9XHJcbiAgICBpZiAoIXRoaXMuX2lzSW5pdGlhbGl6ZWQoKSAmJiB0aGlzLl9zaG91bGRCZUluaXRpYWxpemVkKCkpIHtcclxuICAgICAgdGhpcy5fYnVpbGQoKTtcclxuICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG4gICAgaWYgKHRoaXMuX2lzSW5pdGlhbGl6ZWQoKSAmJiAhdGhpcy5fc2hvdWxkQmVJbml0aWFsaXplZCgpKSB7XHJcbiAgICAgIHRoaXMuX2Rlc3Ryb3koKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIF9pc0luaXRpYWxpemVkKCkge1xyXG4gICAgcmV0dXJuIHRoaXMuZWwuaGFzQ2xhc3ModGhpcy5vcHRpb25zLmluaXRpYWxpemVkQ2xhc3NuYW1lKTtcclxuICB9XHJcblxyXG4gIF9zaG91bGRCZUluaXRpYWxpemVkKCkge1xyXG4gICAgcmV0dXJuIHdpbmRvdy5pbm5lcldpZHRoIDwgdGhpcy5vcHRpb25zLmluaXRCZWxvd0JyZWFrcG9pbnQ7XHJcbiAgfVxyXG5cclxuICBfYnVpbGQoKSB7XHJcbiAgICB0aGlzLmVsLnNsaWNrKHRoaXMub3B0cyk7XHJcbiAgICB0aGlzLm9wdHMuaW5pdGlhbFNsaWRlICYmIHNldFRpbWVvdXQoKCkgPT4gdGhpcy5lbC5zbGljaygnc2xpY2tHb1RvJywgdGhpcy5vcHRzLmluaXRpYWxTbGlkZSkpO1xyXG4gIH1cclxuXHJcbiAgX2Rlc3Ryb3koKSB7XHJcbiAgICB0aGlzLmVsLnNsaWNrKCd1bnNsaWNrJyk7XHJcbiAgfVxyXG5cclxuICBfb25SZWFkeSgpIHtcclxuICAgIHRoaXMuZWwuYWRkQ2xhc3ModGhpcy5vcHRpb25zLmJ1bGxldGVkRG90c0NsYXNzICsgJyAnICsgdGhpcy5vcHRpb25zLnJlYWR5Q2xhc3MpO1xyXG4gICAgaWYgKHRoaXMub3B0aW9ucy50eXBlID09PSB0aGlzLm9wdGlvbnMudHlwZXMucmVndWxhcikge1xyXG4gICAgICAkKHdpbmRvdykub24oJ3Jlc2l6ZScsIF8uZGVib3VuY2UoKCkgPT4gdGhpcy5fY2hlY2tJbml0aWFsaXphdGlvbigpLCAxMDApKTtcclxuICAgIH1cclxuICB9XHJcbn0iLCJpbXBvcnQgeyBCYXNlQ29tcG9uZW50IH0gZnJvbSAnLi4vLi4vYmFzZS1jb21wb25lbnQnO1xyXG5cclxubGV0IGJjQVBJTG9hZGVkID0gJC5EZWZlcnJlZCgpO1xyXG5cclxuZXhwb3J0IGNsYXNzIEJyaWdodGNvdmVQbGF5ZXIgZXh0ZW5kcyBCYXNlQ29tcG9uZW50IHtcclxuXHJcbiAgZ2V0IGRlZmF1bHREYXRhKCkge1xyXG4gICAgcmV0dXJuIHtcclxuICAgICAgXCJkYXRhLWVtYmVkXCI6XCJkZWZhdWx0XCIsXHJcbiAgICAgIFwiY29udHJvbHNcIjogXCJcIlxyXG4gICAgfVxyXG4gIH1cclxuICBjb25zdHJ1Y3RvcihlbGVtZW50KSB7XHJcbiAgICBzdXBlcihlbGVtZW50KTtcclxuICAgIHRoaXMuX3BsYXllckluaXRpYWxpemVkID0gJC5EZWZlcnJlZCgpO1xyXG4gICAgdGhpcy5fY2FwdGlvbnNBdmFpbGFibGUgPSAkLkRlZmVycmVkKCk7XHJcbiAgICB0aGlzLl9iY1BsYXllciA9IG51bGw7XHJcbiAgICB0aGlzLl9jYXB0aW9ucyA9IGZhbHNlO1xyXG4gICAgdGhpcy5fdmlkZW9VbmlxdWVJZCA9IF8udW5pcXVlSWQoJ2JjLWlkLScpO1xyXG4gIH1cclxuXHJcbiAgaW5pdChjYikge1xyXG4gICAgdGhpcy5faGFuZGxlQ2FwdGlvbnMoKTtcclxuICAgIHRoaXMuX3ByZXBhcmVQbGF5ZXJIdG1sKCk7XHJcblxyXG4gICAgcmV0dXJuIHRoaXMubG9hZEFQSUlmTmVlZGVkKClcclxuICAgICAgLnRoZW4oKCkgPT4gdGhpcy5jcmVhdGVOZXdQbGF5ZXIoKSlcclxuICAgICAgLnRoZW4oKHBsYXllcikgPT4ge1xyXG4gICAgICAgIHRoaXMuX2JjUGxheWVyID0gcGxheWVyO1xyXG4gICAgICAgIHRoaXMuX3BsYXllckluaXRpYWxpemVkLnJlc29sdmUodGhpcyk7XHJcbiAgICAgICAgdGhpcy5faW5pdE1ldHJpY3MoKTtcclxuICAgICAgICB0aGlzLl9iY1BsYXllci5vbigncGxheScsICgpID0+IHtcclxuICAgICAgICAgIHRoaXMuX2NhcHRpb25zQXZhaWxhYmxlLnJlc29sdmUoKTtcclxuICAgICAgICB9KTtcclxuICAgICAgICBjYiAmJiBjYigpO1xyXG4gICAgICAgXHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX3BsYXllckluaXRpYWxpemVkLnByb21pc2UoKTtcclxuICAgICAgfSk7XHJcbiAgfVxyXG5cclxuICBfcHJlcGFyZVBsYXllckh0bWwoKSB7XHJcbiAgICBsZXQgcGxheWVyQXR0cnMgPSAkLmV4dGVuZCh7fSwgdGhpcy5kZWZhdWx0RGF0YSwgdGhpcy5fZ2V0UGFyYW1zKCkpLFxyXG4gICAgICAgIHBsYXllckVsID0gJCgnPHZpZGVvIC8+JywgeydpZCc6IHRoaXMuX3ZpZGVvVW5pcXVlSWQsICdjbGFzcyc6IFwidmlkZW8tanNcIn0pO1xyXG5cclxuICAgIGZvciAobGV0IGkgaW4gcGxheWVyQXR0cnMpIHtcclxuICAgICAgcGxheWVyRWwuYXR0cihpLCBwbGF5ZXJBdHRyc1tpXSk7XHJcbiAgICB9XHJcbiAgICB0aGlzLmVsLnJlcGxhY2VXaXRoKHBsYXllckVsKTtcclxuXHJcbiAgICB0aGlzLmVsID0gcGxheWVyRWw7XHJcbiAgICB0aGlzLmFjY291bnRJZCA9IHBsYXllckF0dHJzWydkYXRhLWFjY291bnQnXTtcclxuICAgIHRoaXMucGxheWVySWQgPSBwbGF5ZXJBdHRyc1snZGF0YS1wbGF5ZXInXTtcclxuICB9XHJcblxyXG4gIF9oYW5kbGVDYXB0aW9ucygpIHtcclxuICAgIHRoaXMuX2NhcHRpb25zID0gdGhpcy5lbC5hdHRyKCdkYXRhLWNhcHRpb25zJykgPT09ICd0cnVlJztcclxuICAgIHRoaXMuX2NhcHRpb25zICYmIHRoaXMuX2NhcHRpb25zQXZhaWxhYmxlLnRoZW4oKCkgPT4ge1xyXG4gICAgICB0aGlzLl9zaG93Q2FwdGlvbnMoKTtcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgX2dldFBhcmFtcygpIHtcclxuICAgIC8vZmFsbGJhY2sgdG8gc3VwcG9ydCBhbHJlYWR5IGV4aXN0ZWQgSFRNTDUgQkMgdmlkZW9zIHdpdGggT0xEIGRhdGEgZm9ybWF0LlxyXG4gICAgaWYgKHRoaXMuZWwuYXR0cignZGF0YS12aWRlbycpKSB7XHJcbiAgICAgIHJldHVybiBHTE9CQUxTLlV0aWxzLl9wYXJzZVF1ZXJ5KHRoaXMuZWwuYXR0cignZGF0YS12aWRlbycpKVxyXG4gICAgfVxyXG4gICAgbGV0IHJlcXVpcmVkUGFyYW1zID0gW3tcclxuICAgICAgICAgIG5hbWU6ICdhY2NvdW50JyxcclxuICAgICAgICAgIHZhbDogJ2FjY291bnQnXHJcbiAgICAgICAgfSwge1xyXG4gICAgICAgICAgbmFtZTogJ3BsYXllcicsXHJcbiAgICAgICAgICB2YWw6ICdwbGF5ZXJJRCdcclxuICAgICAgICB9LCB7XHJcbiAgICAgICAgICBuYW1lOiAndmlkZW8taWQnLFxyXG4gICAgICAgICAgdmFsOiAndmlkZW8taWQnXHJcbiAgICAgICAgfV0sXHJcbiAgICAgICAgcGFyYW1zID0ge307XHJcblxyXG4gICAgcmVxdWlyZWRQYXJhbXMuZm9yRWFjaCgocGFyYW0sIGluZGV4KSA9PiB7XHJcbiAgICAgIHBhcmFtc1snZGF0YS0nICsgcGFyYW0ubmFtZV0gPSB0aGlzLmVsLmF0dHIoJ2RhdGEtJyArIHBhcmFtLnZhbCk7XHJcbiAgICB9KTtcclxuICAgIHJldHVybiBwYXJhbXM7XHJcbiAgfVxyXG5cclxuICBsb2FkQVBJSWZOZWVkZWQoKSB7XHJcbiAgICBsZXQgQVBJTG9hZGVkID0gJC5EZWZlcnJlZCgpLFxyXG4gICAgICAgIEFQSVNyYyA9IHRoaXMuX2dldFBsYXllckluaXRTY3JpcHQoKTtcclxuXHJcbiAgICAkLmdldFNjcmlwdChBUElTcmMsICgpID0+IHtcclxuICAgICAgQVBJTG9hZGVkLnJlc29sdmUoKTtcclxuICAgIH0pO1xyXG4gICAgcmV0dXJuIEFQSUxvYWRlZC5wcm9taXNlKCk7XHJcbiAgfVxyXG5cclxuICBjcmVhdGVOZXdQbGF5ZXIoZWxlbWVudCwgb3B0aW9ucykge1xyXG4gICAgdmFyIHBsYXllclJlYWR5ID0gJC5EZWZlcnJlZCgpO1xyXG4gICAgXHJcbiAgICBpZiAodHlwZW9mIHZpZGVvanMgIT0gJ3VuZGVmaW5lZCcpIHtcclxuICAgICAgdmlkZW9qcyh0aGlzLl92aWRlb1VuaXF1ZUlkKS5yZWFkeShmdW5jdGlvbigpIHtcclxuICAgICAgICAvL05PVEUsIGRvbid0IGNoYW5nZSB0byBhcnJvdyBzeW50YXgsIHRoaXMgaXMgcmVxdWlyZWRcclxuICAgICAgICBwbGF5ZXJSZWFkeS5yZXNvbHZlKHRoaXMpO1xyXG4gICAgICB9KTtcclxuICAgIH07XHJcblxyXG4gICAgcmV0dXJuIHBsYXllclJlYWR5LnByb21pc2UoKTtcclxuICB9XHJcblxyXG4gIF9nZXRQbGF5ZXJJbml0U2NyaXB0KCkge1xyXG4gICAgcmV0dXJuIFwiLy9wbGF5ZXJzLmJyaWdodGNvdmUubmV0L1wiICsgdGhpcy5hY2NvdW50SWQgKyBcIi9cIiArIHRoaXMucGxheWVySWQgKyBcIl9kZWZhdWx0L2luZGV4Lm1pbi5qc1wiO1xyXG4gIH1cclxuXHJcbiAgX3Nob3dDYXB0aW9ucygpIHtcclxuICAgIGxldCB0cmFja3MgPSB0aGlzLl9iY1BsYXllci50ZXh0VHJhY2tzKCk7XHJcbiAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRyYWNrcy5sZW5ndGg7IGkrKykge1xyXG4gICAgICBsZXQgdHJhY2sgPSB0cmFja3NbaV07XHJcbiAgICAgIGlmICh0cmFjay5raW5kID09PSAnY2FwdGlvbnMnICYmIHRoaXMuX3RyYWNrTGFuZ01hdGNoZXNQYWdlKHRyYWNrLmxhbmd1YWdlKSkge1xyXG4gICAgICAgIHRyYWNrLm1vZGUgPSAnc2hvd2luZyc7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcblxyXG4gIF90cmFja0xhbmdNYXRjaGVzUGFnZShsYW5ndWFnZSkge1xyXG4gICAgaWYgKGxhbmd1YWdlKSB7XHJcbiAgICAgIGxldCB0cmFja0xhbmcgPSBsYW5ndWFnZS50b0xvd2VyQ2FzZSgpLFxyXG4gICAgICAgICAgdHJhY2tMYW5nU3BsaXRlZCA9IHRyYWNrTGFuZy5zcGxpdCgnLScpLFxyXG4gICAgICAgICAgcGFnZUxhbmcgPSAkKCdodG1sJykuYXR0cignbGFuZycpID8gJCgnaHRtbCcpLmF0dHIoJ2xhbmcnKS50b0xvd2VyQ2FzZSgpIDogJ2VuLXVzJztcclxuXHJcbiAgICAgIGlmICh0cmFja0xhbmdTcGxpdGVkLmxlbmd0aCA+IDEpIHtcclxuICAgICAgICByZXR1cm4gdHJhY2tMYW5nID09PSBwYWdlTGFuZztcclxuICAgICAgfVxyXG4gICAgICByZXR1cm4gdHJhY2tMYW5nU3BsaXRlZFswXSA9PT0gcGFnZUxhbmcuc3BsaXQoJy0nKVswXTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIF9nZXRDdXJyZW50TGFuZ3VhZ2UoKSB7XHJcbiAgICBsZXQgbGFuZyA9ICQoJ2h0bWwnKS5hdHRyKCdsYW5nJykgfHwgJ2VuJztcclxuICAgIHJldHVybiBsYW5nLnNwbGl0KCctJylbMF07XHJcbiAgfVxyXG5cclxuICBwbGF5KGNiKSB7XHJcbiAgICB0aGlzLl9wbGF5ZXJJbml0aWFsaXplZC50aGVuKCgpID0+IHtcclxuICAgICAgdGhpcy5fYmNQbGF5ZXIucGxheSgpO1xyXG4gICAgICBjYiAmJiBjYigpO1xyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBwYXVzZSgpIHtcclxuICAgIHRoaXMuX3BsYXllckluaXRpYWxpemVkLnRoZW4oKCkgPT4ge1xyXG4gICAgICB0aGlzLl9iY1BsYXllci5wYXVzZSgpO1xyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBzdG9wKCkge1xyXG4gICAgdGhpcy5fcGxheWVySW5pdGlhbGl6ZWQudGhlbigoKSA9PiB7XHJcbiAgICAgIHRoaXMuX2JjUGxheWVyLnBhdXNlKCk7XHJcbiAgICAgIHRoaXMuX2JjUGxheWVyLmN1cnJlbnRUaW1lKDAuMDEpO1xyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBfaW5pdE1ldHJpY3MoKSB7XHJcbiAgICBsZXQgZXZlbnRzID0gWydwbGF5aW5nJywgJ3BhdXNlJywgJ2VuZGVkJ10sXHJcbiAgICAgICAgdmlkZW9UaXRsZSA9ICcnLFxyXG4gICAgICAgIHZpZGVvRHVyYXRpb25JblNlY29uZHMsXHJcbiAgICAgICAgdmlkZW9RdWV1ZVBvaW50SW5TZWNvbmRzLFxyXG4gICAgICAgIGhhc0JlZW5FbmRlZCA9IHRydWU7XHJcblxyXG4gICAgLy9kdXJhdGlvbiBpcyBhdmFpbGFibGUgb25seSBvbmNlIG1ldGFkYXRhIGlzIGxvYWRlZFxyXG4gICAgdGhpcy5fYmNQbGF5ZXIub24oJ2xvYWRlZG1ldGFkYXRhJywgKCkgPT4ge1xyXG4gICAgICB2aWRlb0R1cmF0aW9uSW5TZWNvbmRzID0gTWF0aC5mbG9vcih0aGlzLl9iY1BsYXllci5kdXJhdGlvbigpIHx8IDApO1xyXG4gICAgICB2aWRlb1RpdGxlID0gdGhpcy5fYmNQbGF5ZXIubWVkaWFpbmZvID8gdGhpcy5fYmNQbGF5ZXIubWVkaWFpbmZvLm5hbWUgOiAnJztcclxuICAgIH0pO1xyXG5cclxuICAgIC8vYXR0YWNoIGV2ZW50cyB0byBjYXB0dXJlXHJcbiAgICBldmVudHMuZm9yRWFjaCgoZXZlbnQpID0+IHtcclxuICAgICAgdGhpcy5fYmNQbGF5ZXIub24oZXZlbnQsIChldmVudCkgPT4gaGFuZGxlRXZlbnQuY2FsbCh0aGlzLCBldmVudCkpO1xyXG4gICAgfSk7XHJcblxyXG4gICAgZnVuY3Rpb24gaGFuZGxlRXZlbnQoZXZlbnQpIHtcclxuICAgICAgLy9oYW5kbGUgZXZlbnRzIG9ubHkgaWYgbWV0YWRhdGEgaXMgbG9hZGVkXHJcbiAgICAgIGlmICh2aWRlb0R1cmF0aW9uSW5TZWNvbmRzKSB7XHJcbiAgICAgICAgdmlkZW9RdWV1ZVBvaW50SW5TZWNvbmRzID0gTWF0aC5mbG9vcih0aGlzLl9iY1BsYXllci5jdXJyZW50VGltZSgpIHx8IDApO1xyXG4gICAgICAgIHN3aXRjaCAoZXZlbnQudHlwZSkge1xyXG4gICAgICAgICAgY2FzZSAncGxheWluZyc6IHtcclxuICAgICAgICAgICAgLy9pZiB2aWRlbyBoYXMgYmVlZCBlbmRlZCAtIHRoZW4gZmlyZSAnb3BlbicgYmVmb3JlIG5leHQgJ3BsYXknIGV2ZW50XHJcbiAgICAgICAgICAgIGlmIChoYXNCZWVuRW5kZWQpIHtcclxuICAgICAgICAgICAgICBoYXNCZWVuRW5kZWQgPSBmYWxzZTtcclxuICAgICAgICAgICAgICBzZW5kKCdvcGVuJywgdmlkZW9UaXRsZSwgdmlkZW9EdXJhdGlvbkluU2Vjb25kcyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgc2VuZCgncGxheScsIHZpZGVvVGl0bGUsIHZpZGVvUXVldWVQb2ludEluU2Vjb25kcyk7XHJcbiAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgY2FzZSAncGF1c2UnOiB7XHJcbiAgICAgICAgICAgIC8vcHJldmVudCBzZW5kaW5nICdzdG9wJyBldmVudCBvbiB2aWRlbyAnZW5kZWQnIGV2ZW50XHJcbiAgICAgICAgICAgICh2aWRlb1F1ZXVlUG9pbnRJblNlY29uZHMgIT0gdmlkZW9EdXJhdGlvbkluU2Vjb25kcykgJiYgc2VuZCgnc3RvcCcsIHZpZGVvVGl0bGUsIHZpZGVvUXVldWVQb2ludEluU2Vjb25kcyk7XHJcbiAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgY2FzZSAnZW5kZWQnOiB7XHJcbiAgICAgICAgICAgIC8qIGlmIHZpZGVvIGhhcyBiZWVkIGVuZGVkIC0gY2hhbmdlIGZsYWcgaGFzQmVlbkVuZGVkIHRvIHRydWUsIHRvIGJlIGFibGUgdHJpZ2dlciAnb3BlbidcclxuICAgICAgICAgICAgICAgYmVmb3JlIHRoZSBuZXh0ICdwbGF5IGV2ZW50JyovXHJcbiAgICAgICAgICAgIGhhc0JlZW5FbmRlZCA9IHRydWU7XHJcbiAgICAgICAgICAgIHNlbmQoJ2Nsb3NlJywgdmlkZW9UaXRsZSwgdmlkZW9RdWV1ZVBvaW50SW5TZWNvbmRzKTtcclxuICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgZnVuY3Rpb24gc2VuZChldmVudFR5cGUsIHRpdGxlLCBkdXJhdGlvbikge1xyXG4gICAgICBjb25zb2xlLmxvZyhldmVudFR5cGUsIHRpdGxlLCBkdXJhdGlvbik7XHJcbiAgICAgIHRyeSB7XHJcbiAgICAgICAgdHJhY2tWaWRlb01ldHJpY3MoZXZlbnRUeXBlLCB0aXRsZSwgZHVyYXRpb24pO1xyXG4gICAgICB9IGNhdGNoKGUpIHtcclxuICAgICAgICBjb25zb2xlLmxvZygnTWV0cmljcyBhcmUgTk9UIHNlbnQ6ICcgKyBlLm1lc3NhZ2UpO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG59IiwiaW1wb3J0IHsgQmFzZUNvbXBvbmVudCB9IGZyb20gJy4uLy4uL2Jhc2UtY29tcG9uZW50JztcclxuXHJcbmNvbnN0IFlPVVRVQkVfQVBJX0VORFBPSU5UID0gJ2h0dHBzOi8vd3d3LnlvdXR1YmUuY29tL2lmcmFtZV9hcGknO1xyXG5cclxubGV0IHlvdXR1YmVBUElMb2FkZWQgPSAkLkRlZmVycmVkKCk7XHJcblxyXG5leHBvcnQgY2xhc3MgWW91dHViZVBsYXllciBleHRlbmRzIEJhc2VDb21wb25lbnQge1xyXG5cclxuICBzdGF0aWMgZ2V0IFBMQVlfRVZFTlQoKSB7IHJldHVybiAneW91dHViZS1wbGF5ZXItcGxheSc7IH1cclxuICBzdGF0aWMgZ2V0IFBBVVNFX0VWRU5UKCkgeyByZXR1cm4gJ3lvdXR1YmUtcGxheWVyLXBhdXNlJzsgfVxyXG4gIHN0YXRpYyBnZXQgU1RPUF9FVkVOVCgpIHsgcmV0dXJuICd5b3V0dWJlLXBsYXllci1zdG9wJzsgfVxyXG5cclxuICBnZXQgeXRQbGF5ZXJEZWZhdWx0cygpIHtcclxuICAgIHJldHVybiB7XHJcbiAgICAgIHBsYXllclZhcnM6IHtcclxuICAgICAgICByZWw6IDBcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgZ2V0IGRhdGFBdHRyaWJ1dGVzKCkge1xyXG4gICAgcmV0dXJuIHtcclxuICAgICAgdmlkZW9JZCA6ICdkYXRhLWlkJ1xyXG4gICAgfTtcclxuICB9XHJcblxyXG4gIGdldCB2aWRlb1N0YXRlcygpIHtcclxuICAgIHJldHVybiB7XHJcbiAgICAgIHBsYXllckxhc3RTdGF0ZTogLTEsXHJcbiAgICAgIFVOU1RBUlRFRDogLTEsXHJcbiAgICAgIEVOREVEOiAwLFxyXG4gICAgICBQTEFZSU5HOiAxLFxyXG4gICAgICBQQVVTRUQ6IDIsXHJcbiAgICAgIEJVRkZFUklORzogMyxcclxuICAgICAgQ1VFRDogNVxyXG4gICAgfTtcclxuICB9XHJcblxyXG4gIGNvbnN0cnVjdG9yKGVsZW1lbnQpIHtcclxuICAgIHN1cGVyKGVsZW1lbnQpO1xyXG4gICAgdGhpcy5zdGF0ZXMgPSAkLmV4dGVuZCh7fSwgdGhpcy52aWRlb1N0YXRlcyk7XHJcbiAgICB0aGlzLl9wbGF5ZXJJbml0aWFsaXplZCA9ICQuRGVmZXJyZWQoKTtcclxuICAgIHRoaXMuX3lvdXR1YmVQbGF5ZXIgPSBudWxsO1xyXG5cclxuICAgIC8vIFJldHJpZXZlIG9wdGlvbnMgZnJvbSBET00gZWxlbWVudFxyXG4gICAgdGhpcy5vcHRpb25zID0gdGhpcy5fcmV0cmlldmVPcHRpb25zKHRoaXMuZWwsIHRoaXMuZGF0YUF0dHJpYnV0ZXMpO1xyXG4gICAgdGhpcy5vcHRpb25zID0gJC5leHRlbmQoe30sIHRoaXMub3B0aW9ucywgdGhpcy55dFBsYXllckRlZmF1bHRzKTtcclxuICB9XHJcblxyXG4gIF9yZXRyaWV2ZU9wdGlvbnMoZWxlbWVudCwgYXR0cmlidXRlcykge1xyXG4gICAgT2JqZWN0LmtleXMoYXR0cmlidXRlcykuZm9yRWFjaCgoaXRlbSkgPT4ge1xyXG4gICAgICBhdHRyaWJ1dGVzW2l0ZW1dID0gZWxlbWVudC5hdHRyKGF0dHJpYnV0ZXNbaXRlbV0pO1xyXG4gICAgfSk7XHJcbiAgICByZXR1cm4gYXR0cmlidXRlcztcclxuICB9XHJcblxyXG4gIGluaXQoY2IpIHtcclxuICAgIGNvbnN0IHZpZGVvRWxlbWVudCA9IHRoaXMuZWxbMF07XHJcblxyXG4gICAgcmV0dXJuIGxvYWRBUElJZk5lZWRlZCgpXHJcbiAgICAgIC50aGVuKCgpID0+IGNyZWF0ZU5ld1BsYXllcih2aWRlb0VsZW1lbnQsIHRoaXMub3B0aW9ucykpXHJcbiAgICAgIC50aGVuKChwbGF5ZXIpID0+IHtcclxuICAgICAgICB0aGlzLl95b3V0dWJlUGxheWVyID0gcGxheWVyO1xyXG4gICAgICAgIHRoaXMuX3lvdXR1YmVQbGF5ZXIuYWRkRXZlbnRMaXN0ZW5lcihcIm9uU3RhdGVDaGFuZ2VcIiwgKGRhdGEpID0+IHRoaXMuX29uUGxheWVyU3RhdGVDaGFuZ2UoZGF0YSkpO1xyXG4gICAgICAgIHRoaXMuX3BsYXllckluaXRpYWxpemVkLnJlc29sdmUodGhpcyk7XHJcbiAgICAgICAgY2IgJiYgY2IoKTtcclxuICAgICAgICByZXR1cm4gdGhpcy5fcGxheWVySW5pdGlhbGl6ZWQucHJvbWlzZSgpO1xyXG4gICAgICB9KTtcclxuICB9XHJcblxyXG4gIF9vblBsYXllclN0YXRlQ2hhbmdlKGV2ZW50KSB7XHJcbiAgICBsZXQgdmlkZW9EdXJhdGlvbkluU2Vjb25kcyA9IE1hdGguZmxvb3IoKHRoaXMuX3lvdXR1YmVQbGF5ZXIuZ2V0RHVyYXRpb24oKSAtIDAuMDEpIHx8IDApLFxyXG4gICAgICAgIHZpZGVvUXVldWVQb2ludEluU2Vjb25kcyA9IE1hdGgucm91bmQodGhpcy5feW91dHViZVBsYXllci5nZXRDdXJyZW50VGltZSgpIHx8IDApLFxyXG4gICAgICAgIHZpZGVvVGl0bGUgPSB0aGlzLl95b3V0dWJlUGxheWVyLmdldFZpZGVvRGF0YSgpLnRpdGxlO1xyXG4gICAgdHJ5IHtcclxuICAgICAgICBpZiAodGhpcy5zdGF0ZXMucGxheWVyTGFzdFN0YXRlID09IHRoaXMuc3RhdGVzLlVOU1RBUlRFRCkge1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZygnKioqKioqKioqKipMQjogWW91dHViZSBwbGF5ZXIgVU5TVEFSVEVEICoqKioqKioqKionLCAnb3BlbicgKyAnLCcgKyB2aWRlb1RpdGxlICsgJywnICsgdmlkZW9EdXJhdGlvbkluU2Vjb25kcyk7XHJcbiAgICAgICAgICAgIHRyYWNrVmlkZW9NZXRyaWNzKCdvcGVuJywgdmlkZW9UaXRsZSwgdmlkZW9EdXJhdGlvbkluU2Vjb25kcyk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmIChldmVudC5kYXRhID09IHRoaXMuc3RhdGVzLlBMQVlJTkcpIHtcclxuICAgICAgICAgICAgY29uc29sZS5sb2coJyoqKioqKioqKioqTEI6IFlvdXR1YmUgcGxheWVyIFBMQVlJTkcgKioqKioqKioqKicsICdwbGF5JyArICcsJyArIHZpZGVvVGl0bGUgKyAnLCcgKyB2aWRlb1F1ZXVlUG9pbnRJblNlY29uZHMpO1xyXG4gICAgICAgICAgICB0cmFja1ZpZGVvTWV0cmljcygncGxheScsIHZpZGVvVGl0bGUsIHZpZGVvUXVldWVQb2ludEluU2Vjb25kcyk7XHJcbiAgICAgICAgfSBlbHNlIGlmIChldmVudC5kYXRhID09IHRoaXMuc3RhdGVzLlBBVVNFRCkge1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZygnKioqKioqKioqKipMQjogWW91dHViZSBwbGF5ZXIgUEFVU0VEICoqKioqKioqKionLCAnc3RvcCcgKyAnLCcgKyB2aWRlb1RpdGxlICsgJywnICsgdmlkZW9RdWV1ZVBvaW50SW5TZWNvbmRzKTtcclxuICAgICAgICAgICAgdHJhY2tWaWRlb01ldHJpY3MoJ3N0b3AnLCB2aWRlb1RpdGxlLCB2aWRlb1F1ZXVlUG9pbnRJblNlY29uZHMpO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoZXZlbnQuZGF0YSA9PSB0aGlzLnN0YXRlcy5FTkRFRCkge1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZygnKioqKioqKioqKipMQjogWW91dHViZSBwbGF5ZXIgRU5ERUQgKioqKioqKioqKicsICdjbG9zZScgKyAnLCcgKyB2aWRlb1RpdGxlKTtcclxuICAgICAgICAgICAgdHJhY2tWaWRlb01ldHJpY3MoJ2Nsb3NlJywgdmlkZW9UaXRsZSk7XHJcbiAgICAgICAgfVxyXG4gICAgfSBjYXRjaChlKSB7XHJcbiAgICAgICAgY29uc29sZS5sb2coZS5tZXNzYWdlKTtcclxuICAgIH1cclxuICAgIGlmIChldmVudC5kYXRhID09IHRoaXMuc3RhdGVzLkVOREVEKSB7XHJcbiAgICAgIHRoaXMuc3RhdGVzLnBsYXllckxhc3RTdGF0ZSA9IHRoaXMuc3RhdGVzLlVOU1RBUlRFRFxyXG4gICAgfSAgZWxzZSB7XHJcbiAgICAgIHRoaXMuc3RhdGVzLnBsYXllckxhc3RTdGF0ZSA9IGV2ZW50LmRhdGE7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBwbGF5KGNiKSB7XHJcbiAgICB0aGlzLl9wbGF5ZXJJbml0aWFsaXplZC50aGVuKCgpID0+IHtcclxuICAgICAgdGhpcy5feW91dHViZVBsYXllci5wbGF5VmlkZW8oKTtcclxuICAgICAgdGhpcy5icm9hZGNhc3QoWW91dHViZVBsYXllci5QTEFZX0VWRU5UKTtcclxuICAgICAgY2IgJiYgY2IoKTtcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgcGF1c2UoKSB7XHJcbiAgICB0aGlzLl9wbGF5ZXJJbml0aWFsaXplZC50aGVuKCgpID0+IHtcclxuICAgICAgdGhpcy5feW91dHViZVBsYXllci5wYXVzZVZpZGVvKCk7XHJcbiAgICAgIHRoaXMuYnJvYWRjYXN0KFlvdXR1YmVQbGF5ZXIuUEFVU0VfRVZFTlQpO1xyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBzdG9wKCkge1xyXG4gICAgdGhpcy5fcGxheWVySW5pdGlhbGl6ZWQudGhlbigoKSA9PiB7XHJcbiAgICAgIHRoaXMuX3lvdXR1YmVQbGF5ZXIuc3RvcFZpZGVvKCk7XHJcbiAgICAgIHRoaXMuYnJvYWRjYXN0KFlvdXR1YmVQbGF5ZXIuU1RPUF9FVkVOVCk7XHJcbiAgICB9KTtcclxuICB9XHJcbn1cclxuXHJcbmZ1bmN0aW9uIGxvYWRBUElJZk5lZWRlZCgpIHtcclxuICBpZiAoIXdpbmRvdy5ZVCkge1xyXG4gICAgd2luZG93Lm9uWW91VHViZUlmcmFtZUFQSVJlYWR5ID0gKCkgPT4geW91dHViZUFQSUxvYWRlZC5yZXNvbHZlKCk7XHJcbiAgICAkLmdldFNjcmlwdChZT1VUVUJFX0FQSV9FTkRQT0lOVCk7XHJcbiAgfSBlbHNlIHtcclxuICAgIGlmICghd2luZG93LllULmxvYWRlZCkge1xyXG4gICAgICB3aW5kb3cub25Zb3VUdWJlSWZyYW1lQVBJUmVhZHkgPSAoKSA9PiB5b3V0dWJlQVBJTG9hZGVkLnJlc29sdmUoKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHlvdXR1YmVBUElMb2FkZWQucmVzb2x2ZSgpO1xyXG4gICAgfVxyXG4gIH1cclxuICByZXR1cm4geW91dHViZUFQSUxvYWRlZC5wcm9taXNlKCk7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIGNyZWF0ZU5ld1BsYXllcihlbGVtZW50LCBvcHRpb25zKSB7XHJcbiAgdmFyIHBsYXllclJlYWR5ID0gJC5EZWZlcnJlZCgpO1xyXG5cclxuICBvcHRpb25zLmV2ZW50cyA9IG9wdGlvbnMuZXZlbnRzIHx8IHt9O1xyXG4gIG9wdGlvbnMuZXZlbnRzLm9uUmVhZHkgPSAoZXZlbnQpID0+IHtcclxuICAgIHBsYXllclJlYWR5LnJlc29sdmUoZXZlbnQudGFyZ2V0KTtcclxuICB9O1xyXG4gIG9wdGlvbnMuZXZlbnRzLm9uRXJyb3IgPSAoKSA9PiBwbGF5ZXJSZWFkeS5yZWplY3QoKTtcclxuICBjb25zb2xlLmxvZyhvcHRpb25zKTtcclxuICBuZXcgd2luZG93LllULlBsYXllcihlbGVtZW50LCBvcHRpb25zKTtcclxuXHJcbiAgcmV0dXJuIHBsYXllclJlYWR5LnByb21pc2UoKTtcclxufVxyXG4iLCJleHBvcnQgY2xhc3MgR3JpZCB7XHJcblxyXG4gIGdldCBzZWxlY3RvcnMoKSB7IFxyXG4gICAgcmV0dXJuIHtcclxuICAgICAgZXF1YWxDZWxsczogJy5lcXVhbC1jZWxscycsXHJcbiAgICAgIGNvbGxhZ2VBcHBlYXJhbmNlOiAnLmNvbGxhZ2UtbGF5b3V0J1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgY29uc3RydWN0b3IoLi4uYXJncykge1xyXG4gICAgdGhpcy5faW5pdCgpO1xyXG4gIH1cclxuXHJcbiAgX2luaXQoKSB7XHJcbiAgICAkKHRoaXMuc2VsZWN0b3JzLmVxdWFsQ2VsbHMpLmVhY2goKGluZGV4LCBlbCkgPT4ge1xyXG4gICAgICBsZXQgY2VsbHMgPSAkKGVsKS5maW5kKCc+IC5yb3cgPiBbY2xhc3MqPVwic3BhblwiXScpO1xyXG4gICAgICBsZXQgd3JhcHBlciA9ICQoZWwpO1xyXG4gICAgICB0aGlzLl91cGRhdGVFcXVhbENlbGxzKGNlbGxzLCB3cmFwcGVyKTtcclxuICAgICAgdGhpcy5fY29ycmVjdFZlcnRpY2FsU3BhY2luZ3MoY2VsbHMpO1xyXG4gICAgICAkKHdpbmRvdykub24oJ3Jlc2l6ZSBsb2FkJywgXy50aHJvdHRsZSgoKSA9PiB0aGlzLl91cGRhdGVFcXVhbENlbGxzKGNlbGxzLCB3cmFwcGVyKSwgNTApKTtcclxuICAgIH0pO1xyXG4gICAgJCh0aGlzLnNlbGVjdG9ycy5jb2xsYWdlQXBwZWFyYW5jZSkuZWFjaCgoaW5kZXgsIGVsKSA9PiB7XHJcbiAgICAgIGxldCBzbGlkZXJFbCA9ICQoZWwpLmZpbmQoJz4gLnJvdycpLFxyXG4gICAgICAgIGNlbGxzID0gc2xpZGVyRWwuZmluZCgnPiBbY2xhc3MqPVwic3BhblwiXScpLFxyXG4gICAgICAgIHdpZGVDZWxsID0gY2VsbHMuZmlsdGVyKChpbmRleCwgZWwpID0+ICQoZWwpLmhhc0NsYXNzKCdzcGFuMTYnKSk7XHJcbiAgICAgIFxyXG4gICAgICB3aWRlQ2VsbC5sZW5ndGggJiYgJChlbCkuYWRkQ2xhc3MoJ3dpZGUtY2VsbC1wb3NpdGlvbi0nICsgd2lkZUNlbGwuaW5kZXgoKSk7XHJcbiAgICAgIGNlbGxzLmVhY2goKGluZGV4LCBlbCkgPT4gJChlbCkuYXR0cignZGF0YS1pbmRleCcsICQoZWwpLmluZGV4KCkpKTtcclxuICAgICAgc2xpZGVyRWwub24oJ2Rlc3Ryb3knLCAoKSA9PiB7XHJcbiAgICAgICAgY2VsbHNcclxuICAgICAgICAgIC5zb3J0KChhLCBiKSA9PiArJChhKS5hdHRyKCdkYXRhLWluZGV4JykgLSArJChiKS5hdHRyKCdkYXRhLWluZGV4JykpXHJcbiAgICAgICAgICAuYXBwZW5kVG8oc2xpZGVyRWwpO1xyXG4gICAgICB9KTtcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgX3VwZGF0ZUVxdWFsQ2VsbHMoY2VsbHMsIHdyYXBwZXIpIHtcclxuXHJcbiAgICAvKmlmIGVxdWFsIGNlbGxzIGlzIHVzZWQgd2l0aCBhZGFwdGl2ZSBzbGlkZXIgLSBpZ25vcmUgaGVpZ2h0IHJlY2FsY3VsYXRpb24uXHJcbiAgICBoZWlnaHQgaXMgc2V0IHRvIGF1dG8haW1wb3J0YW50IGluIENTUyovXHJcbiAgICBpZiAod2luZG93LmlubmVyV2lkdGggPCA3MjAgJiYgd3JhcHBlci5oYXNDbGFzcygnYWRhcHRpdmUtc2xpZGVyJykpIHtcclxuICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfVxyXG5cclxuICAgIGxldCBncm91cHMgPSBfLmdyb3VwQnkoY2VsbHMsIChlbCkgPT4gJChlbCkub2Zmc2V0KCkudG9wKTtcclxuICAgIGlmIChPYmplY3Qua2V5cyhncm91cHMpLmxlbmd0aCAhPSBjZWxscy5sZW5ndGgpIHtcclxuICAgICAgZm9yIChsZXQgaSBpbiBncm91cHMpIHtcclxuICAgICAgICBjYWxjdWxhdGVIZWlnaHQoJChncm91cHNbaV0pKTtcclxuICAgICAgfVxyXG4gICAgfSBlbHNlIHtcclxuICAgICAgY2VsbHMuaGVpZ2h0KCdhdXRvJyk7XHJcbiAgICB9XHJcbiAgICBcclxuXHJcbiAgICBmdW5jdGlvbiBjYWxjdWxhdGVIZWlnaHQoY2VsbHMpIHtcclxuICAgICAgbGV0IG1heCA9IDA7XHJcbiAgICAgIGNlbGxzLmhlaWdodCgnYXV0bycpO1xyXG4gICAgICBtYXggPSBjZWxscy5lcSgwKS5oZWlnaHQoKTtcclxuICAgICAgZm9yIChsZXQgaSA9IDE7IGkgPCBjZWxscy5sZW5ndGg7IGkrKykge1xyXG4gICAgICAgIG1heCA9IGNlbGxzLmVxKGkpLmhlaWdodCgpID4gbWF4ID8gY2VsbHMuZXEoaSkuaGVpZ2h0KCkgOiBtYXg7XHJcbiAgICAgIH1cclxuICAgICAgY2VsbHMuaGVpZ2h0KG1heCk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBfY29ycmVjdFZlcnRpY2FsU3BhY2luZ3MoY2VsbHMpIHtcclxuICAgIGNlbGxzLmVhY2goKGluZGV4LCBlbCkgPT4ge1xyXG4gICAgICB2YXIgY2hpbGRNb2xlY3VsZSA9ICQoZWwpLmZpbmQoJz5bY2xhc3MqPVwibW9sZWN1bGVcIl0nKTtcclxuICAgICAgaWYgKGNoaWxkTW9sZWN1bGUuaGFzQ2xhc3MoJ210b3AtbW9sJykpIHtcclxuICAgICAgICAkKGVsKS5hZGRDbGFzcygnbXRvcC1tb2wnKTtcclxuICAgICAgfVxyXG4gICAgICBpZiAoY2hpbGRNb2xlY3VsZS5oYXNDbGFzcygnbWJvdC1tb2wnKSkge1xyXG4gICAgICAgICQoZWwpLmFkZENsYXNzKCdtYm90LW1vbCcpO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG59XHJcblxyXG4iLCIndXNlIHN0cmljdCc7XHJcblxyXG5pbXBvcnQgeyBydW5Bc3luYyB9IGZyb20gJy4vbW9sZWN1bGVzL2hlbHBlcnMnO1xyXG5pbXBvcnQgKiBhcyBNb2xlY3VsZXNMaXN0IGZyb20gJy4vbW9sZWN1bGVzJztcclxuaW1wb3J0ICogYXMgQ29tcG9uZW50c0xpc3QgZnJvbSAnLi9jb21wb25lbnRzL2luZGV4LmpzJztcclxuaW1wb3J0ICogYXMgQ29tbW9uIGZyb20gJy4vY29tbW9uL3NjcmlwdHMvY29tbW9uLmpzJztcclxuXHJcblxyXG4vL0luaXRpYWxpemUgY29tbW9uIGZ1bmN0aW9uYWxpdHlcclxud2luZG93LkdMT0JBTFMgPSB7fTtcclxuZm9yIChsZXQgY29tbW9uSXRlbSBpbiBDb21tb24pIHtcclxuICBHTE9CQUxTW2NvbW1vbkl0ZW1dID0gbmV3IENvbW1vbltjb21tb25JdGVtXSgpO1xyXG59XHJcblxyXG5cclxuJCgoKSA9PiB7XHJcbiAgLy8gSW5pdGlhbGl6ZSBhbGwgbW9sZWN1bGVzXHJcbiAgZm9yIChsZXQgbW9sZWN1bGVOYW1lIGluIE1vbGVjdWxlc0xpc3QpIHtcclxuICAgICgoTW9sZWN1bGUpID0+IHtcclxuICAgICAgJChNb2xlY3VsZS5zZWxlY3RvcikuZWFjaCgoXywgJGVsKSA9PiB7XHJcbiAgICAgICAgbmV3IE1vbGVjdWxlKCRlbCk7XHJcbiAgICAgIH0pO1xyXG4gICAgfSkoTW9sZWN1bGVzTGlzdFttb2xlY3VsZU5hbWVdKTtcclxuICB9XHJcblxyXG4gICQod2luZG93KS50cmlnZ2VyKCdtb2R1bGVzLmluaXRpYWxpemVkJyk7XHJcblxyXG4gIC8vIEluaXRpYWxpemUgYWxsIGNvbXBvbmVudHNcclxuICBmb3IgKGxldCBjb21wb25lbnROYW1lIGluIENvbXBvbmVudHNMaXN0KSB7XHJcbiAgICAoKENvbXBvbmVudCkgPT4ge1xyXG4gICAgICBpZiAoQ29tcG9uZW50LnNlbGVjdG9yKSB7XHJcbiAgICAgICAgJChDb21wb25lbnQuc2VsZWN0b3IpLmVhY2goKF8sICRlbCkgPT4gcnVuQXN5bmMoKCkgPT4ge1xyXG4gICAgICAgICAgbmV3IENvbXBvbmVudCgkZWwpO1xyXG4gICAgICAgIH0pKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICBuZXcgQ29tcG9uZW50KCk7XHJcbiAgICAgIH1cclxuICAgICAgXHJcbiAgICB9KShDb21wb25lbnRzTGlzdFtjb21wb25lbnROYW1lXSk7XHJcbiAgfVxyXG59KTsiLCJpbXBvcnQgeyBCYXNlQ29tcG9uZW50IH0gZnJvbSAnLi4vLi4vLi4vY29tcG9uZW50cy9iYXNlLWNvbXBvbmVudC5qcyc7XHJcbmltcG9ydCB7IFN0aWNrYWJsZSB9IGZyb20gJy4uLy4uLy4uL2NvbW1vbi9zY3JpcHRzL3N0aWNrYWJsZS5qcyc7XHJcblxyXG5cclxuZXhwb3J0IGNsYXNzIENvbGxhcHNpYmxlUm93cyBleHRlbmRzIEJhc2VDb21wb25lbnQge1xyXG5cclxuICBzdGF0aWMgZ2V0IHNlbGVjdG9yICgpIHsgcmV0dXJuICcubW9sZWN1bGUtOTk5IC5jb2xsYXBzaWJsZS1yb3cnOyB9XHJcblxyXG4gIGdldCBkZWZhdWx0cygpIHtcclxuICAgIHJldHVybiAkLmV4dGVuZCh7fSwgc3VwZXIuZGVmYXVsdHMsIHtcclxuICAgICAgY29sbGFwc2VJY29uQ2xhc3M6ICcuY29sbGFwc2libGUtcm93LWNvbGxhcHNlLWljb24nLFxyXG4gICAgICBjb2xsYXBzZUljb25UcGw6IHRoaXMuZWwuZmluZCgnLmNvbGxhcHNpYmxlLXJvdy1jb2xsYXBzZS1pY29uLXdyYXAnKS5sZW5ndGggPyB0aGlzLmVsLmZpbmQoJy5jb2xsYXBzaWJsZS1yb3ctY29sbGFwc2UtaWNvbi13cmFwJykgOiAnPGRpdiBjbGFzcz1cImNvbGxhcHNpYmxlLXJvdy1jb2xsYXBzZS1pY29uLXdyYXBcIj48YSBjbGFzcz1cImNvbGxhcHNpYmxlLXJvdy1jb2xsYXBzZS1pY29uXCIgaHJlZj1cImphdmFzY3JpcHQ6dm9pZCgwKVwiIGRhdGEtbWV0cmljcy1pZGVudGlmaWVyPVwiZHJhd2VyIG1vZHVsZVwiIGRhdGEtbWV0cmljcy10aXRsZT1cInNob3cgbGVzc1wiIGRhdGEtbWV0cmljcy1saW5rLXR5cGU9XCJsaW5rXCI+PC9hPjwvZGl2PicsICAgICAgXHJcbiAgICAgIGxpZ2h0VGhlbWVLZXl3b3JkOiAnYXBwZWFyYW5jZS10aGVtZS0yJyxcclxuICAgICAgbGlnaHRUaGVtZUNsYXNzOiAnbGlnaHQtdGhlbWUnXHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIGNvbnN0cnVjdG9yKC4uLmFyZ3MpIHtcclxuICAgIHN1cGVyKC4uLmFyZ3MpO1xyXG4gICAgdGhpcy5faW5pdCgpO1xyXG4gIH1cclxuXHJcbiAgX2luaXQoKSB7XHJcbiAgICB0aGlzLnRyaWdnZXIgPSB0aGlzLmVsLmZpbmQoJz5hJyk7XHJcbiAgICB0aGlzLnRyaWdnZXJXcmFwcGVyID0gdGhpcy50cmlnZ2VyLmNsb3Nlc3QoJ1tjbGFzcyo9XCJmbGV4Mi1tb2xlY3VsZVwiXScpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLmFkZENsYXNzKCdjb2xsYXBzaWJsZS1yb3ctdHJpZ2dlci13cmFwcGVyJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXHJcbiAgICB0aGlzLnRyaWdnZXJXcmFwcGVyLmNsb3Nlc3QoJy5yb3cnKS5jc3MoJ2xpbmUtaGVpZ2h0JywgJzAnKTtcclxuICAgIHRoaXMuX3ByZXBhcmVDb2xsYXBzaWJsZVJvdygpO1xyXG4gICAgdGhpcy5fcHJlcGFyZUNvbGxhcHNlSWNvbigpO1xyXG4gICAgdGhpcy5faW5pdExpc3RlbmVycygpO1xyXG4gIH1cclxuXHJcbiAgX3ByZXBhcmVDb2xsYXBzaWJsZVJvdygpIHtcclxuICAgIGxldCBsYXN0Um93UGFyZW50ID0gdGhpcy5lbC5wYXJlbnRzKCcucm93JykubGFzdCgpLFxyXG4gICAgICAgIGxpZ2h0VGhlbWUgPSB0aGlzLmVsLmNsb3Nlc3QoJy5mbGV4Mi1tb2xlY3VsZScpLmhhc0NsYXNzKHRoaXMub3B0aW9ucy5saWdodFRoZW1lS2V5d29yZCkgPyB0aGlzLm9wdGlvbnMubGlnaHRUaGVtZUNsYXNzIDogJyc7XHJcbiAgICAgIFxyXG4gICAgLy9kZXRlY3Qgd2hldGhlciBzY3J1Y3R1cmUgaXMgLnNlY3Rpb24gPiAucm93ICsgLnNlY3Rpb24gPiAucm93IE9SIC5zZWN0aW9uID4gLnJvdyArIC5yb3c7XHJcbiAgICAvL2NvbmRpZGl0aW9uIGlzIHRydXRoIGZvciAuc2VjdGlvbiA+IC5yb3cgKyAucm93IHN0cnVjdHVyZVxyXG4gICAgaWYgKGxhc3RSb3dQYXJlbnQubmV4dCgnLnJvdycpLmxlbmd0aCkge1xyXG4gICAgICB0aGlzLnJvdyA9IGxhc3RSb3dQYXJlbnQubmV4dCgnLnJvdycpO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5yb3cgPSBsYXN0Um93UGFyZW50LnBhcmVudCgpLm5leHQoJy5zZWN0aW9uJyk7XHJcbiAgICB9XHJcbiAgICB0aGlzLnJvdy5hZGRDbGFzcygnY29sbGFwc2libGUtcm93LWNvbnRhaW5lciAnICsgbGlnaHRUaGVtZSk7XHJcbiAgfVxyXG5cclxuICBfcHJlcGFyZUNvbGxhcHNlSWNvbigpIHtcclxuICAgIHRoaXMucm93LmFwcGVuZCh0aGlzLm9wdGlvbnMuY29sbGFwc2VJY29uVHBsKTtcclxuICAgIHRoaXMuY29sbGFwc2VJY29uID0gdGhpcy5yb3cuZmluZCh0aGlzLm9wdGlvbnMuY29sbGFwc2VJY29uQ2xhc3MpO1xyXG4gIH1cclxuXHJcbiAgX2luaXRMaXN0ZW5lcnMoKSB7XHJcbiAgICBcclxuICAgIHRoaXMudHJpZ2dlci5vbignY2xpY2snLCAoKSA9PiB7XHJcbiAgICAgIHRoaXMuX2V4cGFuZFJvdygpO1xyXG4gICAgfSk7XHJcblxyXG4gICAgdGhpcy5jb2xsYXBzZUljb24ub24oJ2NsaWNrJywgKCkgPT4ge1xyXG4gICAgICB0aGlzLmNvbGxhcHNlSWNvbi5yZW1vdmVDbGFzcygnaW5pdGlhbGl6ZWQnKTtcclxuICAgICAgdGhpcy5fY29sbGFwc2VSb3coKTtcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgX2V4cGFuZFJvdygpIHtcclxuICAgIHRoaXMudHJpZ2dlcldyYXBwZXJIZWlnaHQgPSB0aGlzLnRyaWdnZXJXcmFwcGVyLm91dGVySGVpZ2h0KHRydWUpO1xyXG5cclxuICAgIHRoaXMucm93Lm9uKCd0cmFuc2l0aW9uZW5kIHdlYmtpdFRyYW5zaXRpb25FbmQnLCAoZSkgPT4ge1xyXG4gICAgICAgaWYgKGUub3JpZ2luYWxFdmVudC5wcm9wZXJ0eU5hbWUgPT0gJ21heC1oZWlnaHQnICYmICQoZS50YXJnZXQpLmlzKHRoaXMucm93KSkge1xyXG4gICAgICAgIHRoaXMucm93Lm9mZigndHJhbnNpdGlvbmVuZCB3ZWJraXRUcmFuc2l0aW9uRW5kJyk7XHJcbiAgICAgICAgdGhpcy5yb3cuY3NzKCdtYXgtaGVpZ2h0JywgJ25vbmUnKTtcclxuICAgICAgIH1cclxuICAgIH0pO1xyXG5cclxuICAgIHRoaXMucm93LmNzcygnbWF4LWhlaWdodCcsIHRoaXMucm93WzBdLnNjcm9sbEhlaWdodCk7XHJcblxyXG4gICAgdGhpcy50cmlnZ2VyV3JhcHBlci5vbigndHJhbnNpdGlvbmVuZCB3ZWJraXRUcmFuc2l0aW9uRW5kJywgKGUpID0+IHtcclxuICAgICAgaWYgKGUub3JpZ2luYWxFdmVudC5wcm9wZXJ0eU5hbWUgPT0gJ21hcmdpbi10b3AnICYmICQoZS50YXJnZXQpLmhhc0NsYXNzKCdjb2xsYXBzaWJsZS1yb3ctdHJpZ2dlci13cmFwcGVyJykpIHtcclxuICAgICAgICB0aGlzLnRyaWdnZXJXcmFwcGVyLm9mZigndHJhbnNpdGlvbmVuZCB3ZWJraXRUcmFuc2l0aW9uRW5kJyk7XHJcbiAgICAgICAgdGhpcy50cmlnZ2VyV3JhcHBlci5hZGRDbGFzcygnZGlzYWJsZWQnKTtcclxuICAgICAgICB0aGlzLnN0aWNrYWJsZUNvbGxhcHNlQnRuID0gbmV3IFN0aWNrYWJsZSh0aGlzLmNvbGxhcHNlSWNvbiwgbnVsbCwge3R5cGU6ICdib3VuZGluZyd9KTtcclxuICAgICAgICB0aGlzLmNvbGxhcHNlSWNvbi5hZGRDbGFzcygnaW5pdGlhbGl6ZWQnKTtcclxuICAgICAgfVxyXG4gICAgfSk7XHJcblxyXG4gICAgdGhpcy50cmlnZ2VyV3JhcHBlci5hZGRDbGFzcygnaW5hY3RpdmUnKS5jc3MoeydtYXJnaW4tdG9wJzogLXRoaXMudHJpZ2dlcldyYXBwZXJIZWlnaHQgKyAncHgnfSk7XHJcbiAgICBHTE9CQUxTLlNjcm9sbFRvLm5hdmlnYXRlKG51bGwsIG51bGwsIDEwMDAsIHRoaXMucm93LCAtdGhpcy50cmlnZ2VyV3JhcHBlckhlaWdodCk7XHJcbiAgfVxyXG5cclxuICBfY29sbGFwc2VSb3coKSB7XHJcbiAgICBzZXRUaW1lb3V0KCgpID0+IHRoaXMuc3RpY2thYmxlQ29sbGFwc2VCdG4uZGVzdHJveSgpLCAzMDApO1xyXG4gICAgbGV0IG5hdmlnYXRlT2Zmc2V0ID0gdGhpcy5yb3cub2Zmc2V0KCkudG9wIC0gJCh3aW5kb3cpLnNjcm9sbFRvcCgpICsgdGhpcy50cmlnZ2VyV3JhcHBlckhlaWdodCAtIDMwMDtcclxuXHJcbiAgICB0aGlzLnRyaWdnZXJXcmFwcGVyLm9uKCd0cmFuc2l0aW9uZW5kIHdlYmtpdFRyYW5zaXRpb25FbmQnLCAoZSkgPT4ge1xyXG4gICAgICBpZiAoZS5vcmlnaW5hbEV2ZW50LnByb3BlcnR5TmFtZSA9PSAnbWFyZ2luLXRvcCcgJiYgJChlLnRhcmdldCkuaGFzQ2xhc3MoJ2NvbGxhcHNpYmxlLXJvdy10cmlnZ2VyLXdyYXBwZXInKSkge1xyXG4gICAgICAgIHRoaXMudHJpZ2dlcldyYXBwZXIub2ZmKCd0cmFuc2l0aW9uZW5kIHdlYmtpdFRyYW5zaXRpb25FbmQnKTtcclxuICAgICAgICB0aGlzLnRyaWdnZXJXcmFwcGVyLnJlbW92ZUNsYXNzKCdpbmFjdGl2ZSBkaXNhYmxlZCcpO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICAgIHRoaXMucm93LmNzcygnbWF4LWhlaWdodCcsIHRoaXMucm93LmhlaWdodCgpKVxyXG4gICAgc2V0VGltZW91dCgoKSA9PiB7XHJcbiAgICAgIHRoaXMucm93LmNzcygnbWF4LWhlaWdodCcsIDApO1xyXG4gICAgICB0aGlzLnRyaWdnZXJXcmFwcGVyLmNzcygnbWFyZ2luLXRvcCcsIDApO1xyXG4gICAgfSk7XHJcblxyXG4gICAgR0xPQkFMUy5TY3JvbGxUby5uYXZpZ2F0ZVRvT2Zmc2V0KG5hdmlnYXRlT2Zmc2V0LCAxMDAwLCBudWxsLCB0cnVlKTtcclxuICB9XHJcbiAgXHJcbn1cclxuIiwiaW1wb3J0IHsgQmFzZUNvbXBvbmVudCB9IGZyb20gJy4uLy4uLy4uL2NvbXBvbmVudHMvYmFzZS1jb21wb25lbnQuanMnO1xyXG5cclxuZXhwb3J0IGNsYXNzIElubGluZVZpZGVvIGV4dGVuZHMgQmFzZUNvbXBvbmVudCB7XHJcblxyXG4gIHN0YXRpYyBnZXQgc2VsZWN0b3IgKCkgeyByZXR1cm4gJy5tb2xlY3VsZS05OTkgLnZpZGVvLW1vZHVsZSc7IH1cclxuXHJcbiAgZ2V0IGRlZmF1bHRzKCkge1xyXG4gICAgcmV0dXJuIHtcclxuICAgICAgb2Zmc2V0VG9wOiAxMDAsXHJcbiAgICAgIGxvb3A6IGZhbHNlLFxyXG4gICAgICBvZmZzZXRCb3R0b206IDEwMCxcclxuICAgICAgcGxheUluVmlld3BvcnRDbGFzc25hbWU6ICdwbGF5LW9uY2UtaW4tdmlld3BvcnQnLFxyXG4gICAgICBsb29wSW5WaXdwb3J0Q2xhc3NuYW1lOiAnbG9vcC1pbi12aXdwb3J0JyxcclxuICAgICAgcmVwbGF5SWNvblNlbGVjdG9yOiAnLnJlcGxheS12aWRlbycsXHJcbiAgICAgIHBsYXlJY29uVHBsOiAnPGEgY2xhc3M9XCJwbGF5IGpzX292ZXJsYXlfdHJpZ2dlclwiIGhyZWY9XCJqYXZhc2NyaXB0OnZvaWQoMClcIiB0aXRsZT1cInBsYXkgdmlkZW9cIiBkYXRhLW1ldHJpY3MtdGNtaWQ9XCIyNDUtMTkwOTc2MFwiIGRhdGEtbWV0cmljcy10YWdzPVwiYXNzZXQsXCIgZGF0YS1tZXRyaWNzLWxpbmstdHlwZT1cIlwiIGRhdGEtbWV0cmljcy10aXRsZT1cIlZpZGVvIHcvbyByZWxhdGVkIHZpZGVvcyBhdCB0aGUgZW5kXCI+PC9hPidcclxuICAgIH1cclxuICB9XHJcbiAgY29uc3RydWN0b3IoLi4uYXJncykge1xyXG4gICAgc3VwZXIoLi4uYXJncyk7XHJcbiAgICB0aGlzLm1vYmlsZSA9IEdMT0JBTFMuVXRpbHMuZGV2aWNlLmlzTW9iaWxlO1xyXG4gICAgdGhpcy5faW5pdCgpO1xyXG4gIH1cclxuXHJcbiAgX2luaXQoKSB7XHJcbiAgICB0aGlzLnZpZGVvID0gdGhpcy5lbC5maW5kKCd2aWRlbycpLmxlbmd0aCAmJiB0aGlzLmVsLmZpbmQoJ3ZpZGVvJylbMF07XHJcbiAgICBpZiAodGhpcy52aWRlbykge1xyXG4gICAgICB0aGlzLnBhcmVudE1vZHVsZSA9IHRoaXMuZWwuY2xvc2VzdCgnLmZsZXgyLW1vbGVjdWxlJyk7XHJcbiAgICAgIHRoaXMucmVwbGF5SWNvbiA9IHRoaXMuZWwuZmluZCh0aGlzLm9wdGlvbnMucmVwbGF5SWNvblNlbGVjdG9yKTtcclxuXHJcbiAgICAgIC8qIGZvciBtb2JpbGVzIHdlIG5lZWQgdG8gc2hvdyBwbGF5IGljb24gd2l0aCBwb3N0ZXIgaW1hZ2UgY2F1c2UgYXV0b3BsYXlcclxuICAgICAgICogaXMgbm90IHdvcmtpbmcgYW5kIHdlIG5lZWQgdG8gdHJpZ2dlciB2aWRlbyB0byBwbGF5IG1hbnVhbGx5Ki9cclxuICAgICAgaWYgKHRoaXMubW9iaWxlKSB7XHJcbiAgICAgICAgdGhpcy5yZXBsYXlJY29uLmFmdGVyKHRoaXMub3B0aW9ucy5wbGF5SWNvblRwbCk7XHJcbiAgICAgICAgdGhpcy5wbGF5SWNvbiA9IHRoaXMuZWwuZmluZCgnYS5wbGF5Jyk7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgdGhpcy5wbGF5SW5WaWV3cG9ydCA9IHRoaXMuZWwuaGFzQ2xhc3ModGhpcy5vcHRpb25zLnBsYXlJblZpZXdwb3J0Q2xhc3NuYW1lKTtcclxuICAgICAgICB0aGlzLmxvb3BJblZpZXdwb3J0ID0gdGhpcy5lbC5oYXNDbGFzcyh0aGlzLm9wdGlvbnMubG9vcEluVml3cG9ydENsYXNzbmFtZSk7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIC8vSWdub3JlIHBsYXlpbmcgaW4gYSB2aWV3cG9ydCBmb3IgbW9iaWxlIGRldmljZXNcclxuICAgICAgaWYgKCF0aGlzLm1vYmlsZSAmJiB0aGlzLnBsYXlJblZpZXdwb3J0KSB7XHJcbiAgICAgICAgdGhpcy5vdXRPZlZpZXdwb3J0ID0gdHJ1ZTtcclxuXHJcbiAgICAgICAgdGhpcy52aWV3cG9ydFBhcmVudCA9IHRoaXMuZWwuY2xvc2VzdCgnLnNsaWNrLXNsaWRlcicpO1xyXG4gICAgICAgIHRoaXMudmlld3BvcnRQYXJlbnQgPSB0aGlzLnZpZXdwb3J0UGFyZW50Lmxlbmd0aCA/IHRoaXMudmlld3BvcnRQYXJlbnQgOiB0aGlzLmVsO1xyXG4gICAgICAgIHRoaXMudmlld3BvcnRQYXJlbnQudmlld3BvcnRDaGVja2VyKHtcclxuICAgICAgICAgIHJlcGVhdDogdHJ1ZSxcclxuICAgICAgICAgIG9mZnNldDogMTAwLFxyXG4gICAgICAgICAgY2FsbGJhY2tGdW5jdGlvbjogKG9iaiwgZSkgPT4ge1xyXG4gICAgICAgICAgICBpZiAoZSA9PT0gJ2FkZCcpIHtcclxuICAgICAgICAgICAgICBpZiAodGhpcy5vdXRPZlZpZXdwb3J0KSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl9wbGF5VmlkZW8oKTtcclxuICAgICAgICAgICAgICAgIHRoaXMub3V0T2ZWaWV3cG9ydCA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICB0aGlzLl9wYXVzZVZpZGVvKCk7XHJcbiAgICAgICAgICAgICAgdGhpcy5vdXRPZlZpZXdwb3J0ID0gdHJ1ZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgICB9XHJcbiAgICAgIC8vcGxheSB2aWRlbyBpbiBhY3RpdmUgc2xpZGUgb25seSBmb3IgZGVza3RvcHNcclxuICAgICAgaWYgKCF0aGlzLm1vYmlsZSkge1xyXG4gICAgICAgIHRoaXMucGFyZW50TW9kdWxlLm9uKCdzbGlkZXIudmlld3BvcnRpbicsICgpID0+IHNldFRpbWVvdXQoKCkgPT4gdGhpcy5fcGxheVZpZGVvKCksIDEwMCkpO1xyXG4gICAgICAgIC8vcGF1c2UgdmlkZW8gaW4gaW5hY3RpdmUgc2xpZGUgZm9yIGJvdGggZGVza3RvcHMgYW5kIG1vYmlsZXNcclxuICAgICAgICB0aGlzLnBhcmVudE1vZHVsZS5vbignc2xpZGVyLnZpZXdwb3J0b3V0JywgKCkgPT4gc2V0VGltZW91dCgoKSA9PiB0aGlzLl9wYXVzZVZpZGVvKCksIDEwMCkpO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHRoaXMucGFyZW50TW9kdWxlLm9uKCdzbGlkZXIudmlld3BvcnRvdXQnLCAoKSA9PiBzZXRUaW1lb3V0KCgpID0+IHtcclxuICAgICAgICAgIGlmICh0aGlzLnZpZGVvLmlzUGxheWluZykge1xyXG4gICAgICAgICAgICB0aGlzLl9zaG93UGxheUljb24oKTtcclxuICAgICAgICAgIH1cclxuICAgICAgICAgIHRoaXMuX3BhdXNlVmlkZW8oKTtcclxuICAgICAgICB9LCAxMDApKTtcclxuICAgICAgfVxyXG5cclxuICAgICAgdGhpcy5faW5pdFZpZGVvRXZlbnRzKCk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBfcGxheVZpZGVvKCkge1xyXG4gICAgaWYgKCF0aGlzLm1vYmlsZSAmJiB0aGlzLnBsYXlJblZpZXdwb3J0KSB7XHJcbiAgICAgIGlmICh0aGlzLnZpZXdwb3J0UGFyZW50Lmhhc0NsYXNzKCd2aXNpYmxlJykpIHtcclxuICAgICAgICBpZiAoIXRoaXMudmlkZW8uaXNQbGF5aW5nKSB7XHJcbiAgICAgICAgICBpZiAodGhpcy5lbC5jbG9zZXN0KCcuc2xpY2stc2xpZGVyJykubGVuZ3RoKSB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLmVsLmNsb3Nlc3QoJy5zbGljay1hY3RpdmUnKS5sZW5ndGgpIHtcclxuICAgICAgICAgICAgICBzZXRUaW1lb3V0KCgpID0+IHRoaXMuX3BsYXkoKSwgNTApO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBzZXRUaW1lb3V0KCgpID0+IHRoaXMuX3BsYXkoKSwgNTApO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgXHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBpZiAoIXRoaXMudmlkZW8uaXNQbGF5aW5nKSB7XHJcbiAgICAgICAgdGhpcy5fcGxheSgpO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBfcGxheSgpIHtcclxuICAgIHRoaXMuX2hpZGVSZXBsYXlJY29uKCk7XHJcbiAgICB0aGlzLl9oaWRlUGxheUljb24oKTtcclxuICAgIHRoaXMudmlkZW8ucGxheSgpO1xyXG4gIH1cclxuXHJcbiAgX3BhdXNlVmlkZW8oKSB7XHJcbiAgICBpZiAodGhpcy52aWRlby5pc1BsYXlpbmcpIHtcclxuICAgICAgdGhpcy52aWRlby5wYXVzZSgpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgX2luaXRWaWRlb0V2ZW50cygpIHtcclxuXHJcbiAgICBpZiAoIXRoaXMubW9iaWxlKSB7XHJcbiAgICAgICQodGhpcy52aWRlbylcclxuICAgICAgICAub24oJ2xvYWRlZG1ldGFkYXRhJywgKCkgPT4ge1xyXG4gICAgICAgICAgdGhpcy52aWRlby5jdXJyZW50VGltZSA9IDAuMDE7XHJcbiAgICAgICAgfSlcclxuICAgICAgICAub24oJ2VuZGVkJywgKCkgPT4ge1xyXG4gICAgICAgICAgaWYgKCF0aGlzLmxvb3BJblZpZXdwb3J0KSB7XHJcbiAgICAgICAgICAgIHRoaXMuX3Nob3dSZXBsYXlJY29uKCk7XHJcbiAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLl9wbGF5VmlkZW8oKTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMuX3Nob3dQbGF5SWNvbigpO1xyXG4gICAgICAkKHRoaXMudmlkZW8pLm9uKCdlbmRlZCcsICgpID0+IHRoaXMuX3Nob3dSZXBsYXlJY29uKCkpO1xyXG4gICAgfVxyXG5cclxuICAgIHRoaXMucmVwbGF5SWNvbiAmJiB0aGlzLnJlcGxheUljb24ub24oJ2NsaWNrJywgKGUpID0+IHtcclxuICAgICAgaWYgKCF0aGlzLnZpZGVvLmlzUGxheWluZykge1xyXG4gICAgICAgIHRoaXMuX3BsYXlWaWRlbygpO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuXHJcbiAgICB0aGlzLnBsYXlJY29uICYmIHRoaXMucGxheUljb24ub24oJ2NsaWNrJywgKGUpID0+IHtcclxuICAgICAgdGhpcy5fcGxheVZpZGVvKCk7XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIF9zaG93UGxheUljb24oKSB7XHJcbiAgICB0aGlzLnBsYXlJY29uICYmIHRoaXMucGxheUljb24uYWRkQ2xhc3MoJ3Zpc2libGUnKTtcclxuICB9XHJcblxyXG4gIF9oaWRlUGxheUljb24oKSB7XHJcbiAgICB0aGlzLnBsYXlJY29uICYmIHRoaXMucGxheUljb24ucmVtb3ZlQ2xhc3MoJ3Zpc2libGUnKTtcclxuICB9XHJcblxyXG4gIF9zaG93UmVwbGF5SWNvbigpIHtcclxuICAgIHRoaXMucmVwbGF5SWNvbiAmJiB0aGlzLnJlcGxheUljb24uYWRkQ2xhc3MoJ3Zpc2libGUnKTtcclxuICB9XHJcblxyXG4gIF9oaWRlUmVwbGF5SWNvbigpIHtcclxuICAgIHRoaXMucmVwbGF5SWNvbiAmJiB0aGlzLnJlcGxheUljb24ucmVtb3ZlQ2xhc3MoJ3Zpc2libGUnKTtcclxuICB9XHJcblxyXG59XHJcbiIsImltcG9ydCB7QmFzZUNvbXBvbmVudH0gZnJvbSAnLi4vLi4vLi4vY29tcG9uZW50cy9iYXNlLWNvbXBvbmVudC5qcyc7XHJcblxyXG5leHBvcnQgY2xhc3MgU2Nyb2xsVG9OZXh0IGV4dGVuZHMgQmFzZUNvbXBvbmVudCB7XHJcblxyXG4gIHN0YXRpYyBnZXQgc2VsZWN0b3IoKSB7XHJcbiAgICByZXR1cm4gJy5tb2xlY3VsZS05OTkgYS5zY3JvbGwtdG8tbmV4dCc7XHJcbiAgfVxyXG5cclxuICBnZXQgZGVmYXVsdHMoKSB7XHJcbiAgICByZXR1cm4ge1xyXG4gICAgICBzY3JvbGxEdXJhdGlvbjogMTAwMCxcclxuICAgICAgc2Nyb2xsVG9TZWN0aW9uQ2xhc3NOYW1lOiAnc2Nyb2xsLXRvLXNlY3Rpb24tYXJyb3cnLFxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgY29uc3RydWN0b3IoLi4uYXJncykge1xyXG4gICAgc3VwZXIoLi4uYXJncyk7XHJcbiAgICB0aGlzLl9pbml0KCk7XHJcbiAgfVxyXG5cclxuICBfaW5pdCgpIHtcclxuICAgIHRoaXMuJHNlY3Rpb24gPSB0aGlzLmVsLmNsb3Nlc3QoJy5zZWN0aW9uJyk7XHJcbiAgICB0aGlzLiR3cmFwcGVyID0gdGhpcy5lbC5jbG9zZXN0KCcubW9sZWN1bGUtOTk5Jyk7XHJcbiAgICBpZiAodGhpcy4kc2VjdGlvbi5sZW5ndGgpIHtcclxuICAgICAgdGhpcy4kc2VjdGlvbi5hZGRDbGFzcyh0aGlzLm9wdGlvbnMuc2Nyb2xsVG9TZWN0aW9uQ2xhc3NOYW1lKTtcclxuICAgICAgdGhpcy5fcHJlcGFyZVNlY3Rpb24oKTtcclxuICAgIH1cclxuXHJcbiAgICB0aGlzLl9pbml0TGlzdGVuZXJzKCk7XHJcbiAgfVxyXG5cclxuICBfcHJlcGFyZVNlY3Rpb24oKSB7XHJcbiAgICBpZiAodGhpcy4kd3JhcHBlci5oYXNDbGFzcygnc2hvdy1hYm92ZS0xMjgwJykpe1xyXG4gICAgICB0aGlzLiRzZWN0aW9uLmFkZENsYXNzKCdhcnJvdy1hYm92ZS0xMjgwJyk7XHJcbiAgICB9XHJcbiAgICBpZiAodGhpcy4kd3JhcHBlci5oYXNDbGFzcygnc2hvdy1iZWxvdy0xMjgwJykpe1xyXG4gICAgICB0aGlzLiRzZWN0aW9uLmFkZENsYXNzKCdhcnJvdy1iZWxvdy0xMjgwJyk7XHJcbiAgICB9XHJcbiAgICBpZiAodGhpcy4kd3JhcHBlci5oYXNDbGFzcygnc2hvdy1hYm92ZS03MjAnKSl7XHJcbiAgICAgIHRoaXMuJHNlY3Rpb24uYWRkQ2xhc3MoJ2Fycm93LWFib3ZlLTcyMCcpO1xyXG4gICAgfVxyXG4gICAgaWYgKHRoaXMuJHdyYXBwZXIuaGFzQ2xhc3MoJ3Nob3ctYmVsb3ctNzIwJykpe1xyXG4gICAgICB0aGlzLiRzZWN0aW9uLmFkZENsYXNzKCdhcnJvdy1iZWxvdy03MjAnKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIF9pbml0TGlzdGVuZXJzKCkge1xyXG4gICAgdGhpcy5lbC5vbignY2xpY2snLCAoZSkgPT4ge1xyXG4gICAgICBHTE9CQUxTLlNjcm9sbFRvLm5hdmlnYXRlVG9TZWN0aW9uKCQoZS5jdXJyZW50VGFyZ2V0KS5hdHRyKCdocmVmJyksIG51bGwsIHRoaXMub3B0aW9ucy5zY3JvbGxEdXJhdGlvbik7XHJcbiAgICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuICAgIH0pO1xyXG4gIH1cclxufVxyXG4iLCJpbXBvcnQgeyBCYXNlQ29tcG9uZW50IH0gZnJvbSAnLi4vLi4vLi4vY29tcG9uZW50cy9iYXNlLWNvbXBvbmVudC5qcyc7XHJcbmltcG9ydCB7IFN0aWNrYWJsZSB9IGZyb20gJy4uLy4uLy4uL2NvbW1vbi9zY3JpcHRzL3N0aWNrYWJsZS5qcyc7XHJcblxyXG5leHBvcnQgY2xhc3MgU2Nyb2xsVG9Ub3AgZXh0ZW5kcyBCYXNlQ29tcG9uZW50IHtcclxuXHJcbiAgc3RhdGljIGdldCBzZWxlY3RvciAoKSB7IHJldHVybiAnLm1vbGVjdWxlLTk5OSBhLnNjcm9sbC10by10b3AnOyB9XHJcblxyXG4gIGdldCBkZWZhdWx0cygpIHtcclxuICAgIHJldHVybiB7XHJcbiAgICAgIG9mZnNldFRvcDogNTAwLFxyXG4gICAgICBzY3JvbGxEdXJhdGlvbjogNjAwXHJcbiAgICB9XHJcbiAgfVxyXG4gIGNvbnN0cnVjdG9yKC4uLmFyZ3MpIHtcclxuICAgIHN1cGVyKC4uLmFyZ3MpO1xyXG4gICAgdGhpcy5faW5pdCgpO1xyXG4gIH1cclxuXHJcbiAgX2luaXQoKSB7XHJcbiAgICBuZXcgU3RpY2thYmxlKHRoaXMuZWwsIG51bGwsIHt0eXBlOiAnYm90dG9tJywgb2Zmc2V0VG9wOiArdGhpcy5lbC5hdHRyKCdkYXRhLXNob3ctb2Zmc2V0JykgfHwgdGhpcy5vcHRpb25zLm9mZnNldFRvcH0pO1xyXG4gICAgdGhpcy5faW5pdExpc3RlbmVycygpO1xyXG4gIH1cclxuXHJcbiAgX2luaXRMaXN0ZW5lcnMoKSB7XHJcbiAgICB0aGlzLmVsLm9uKCdjbGljaycsICgpID0+IEdMT0JBTFMuU2Nyb2xsVG8ubmF2aWdhdGVUbygwLCArdGhpcy5lbC5hdHRyKCdkYXRhLXNjcm9sbC1kdXJhdGlvbicpIHx8IHRoaXMub3B0aW9ucy5zY3JvbGxEdXJhdGlvbikpO1xyXG4gIH1cclxufVxyXG4iLCJleHBvcnQgZnVuY3Rpb24gcnVuQXN5bmMoZm4pIHtcclxuICB3aW5kb3cuc2V0VGltZW91dChmbiwgMCk7XHJcbn1cclxuIiwiJ3VzZSBzdHJpY3QnO1xyXG4vKk5PVEUgb3JkZXIgbWF0dGVycy4gU29tZSBtb2R1bGVzIHNob3VsZCBiZSBpbml0aWFsaXplZCBwcmlvciB0byB0aGUgb3RoZXJzLiBEb24ndCBjaGFuZ2UgaXQgdW5sZXNzIHZlcnkgc3VyZSovXHJcbmV4cG9ydCAqIGZyb20gJy4vbGItMjA5L21vbGVjdWxlLWxiLTIwOSdcclxuZXhwb3J0ICogZnJvbSAnLi9sYi0zMDAvbW9sZWN1bGUtbGItMzAwJztcclxuZXhwb3J0ICogZnJvbSAnLi9sYi0zMDIvbW9sZWN1bGUtbGItMzAyJztcclxuZXhwb3J0ICogZnJvbSAnLi9sYi00MTMvbW9sZWN1bGUtbGItNDEzJztcclxuZXhwb3J0ICogZnJvbSAnLi9sYi01MDEvbW9sZWN1bGUtbGItNTAxJztcclxuZXhwb3J0ICogZnJvbSAnLi9sYi01MTAvbW9sZWN1bGUtbGItNTEwJztcclxuZXhwb3J0ICogZnJvbSAnLi9sYi04MjMvbW9sZWN1bGUtbGItODIzJztcclxuZXhwb3J0ICogZnJvbSAnLi9sYi04MjcvbW9sZWN1bGUtbGItODI3JztcclxuZXhwb3J0ICogZnJvbSAnLi9sYi04MzAvbW9sZWN1bGUtbGItODMwJztcclxuZXhwb3J0ICogZnJvbSAnLi9sYi04MzUvbW9sZWN1bGUtbGItODM1JztcclxuZXhwb3J0ICogZnJvbSAnLi9sYi04MzYvbW9sZWN1bGUtbGItODM2JztcclxuZXhwb3J0ICogZnJvbSAnLi9sYi04MzYvbW9sZWN1bGUtbGItODM2LXRoZW1lLTInO1xyXG5leHBvcnQgKiBmcm9tICcuL2xiLTcwMS9tb2xlY3VsZS1sYi03MDEnO1xyXG5leHBvcnQgKiBmcm9tICcuL2xiLTcwMi9tb2xlY3VsZS1sYi03MDInO1xyXG5leHBvcnQgKiBmcm9tICcuLzk5OS9jb2xsYXBzaWJsZS1yb3cvY29sbGFwc2libGUtcm93JztcclxuZXhwb3J0ICogZnJvbSAnLi85OTkvc2Nyb2xsLXRvLXRvcC9zY3JvbGwtdG8tdG9wJztcclxuZXhwb3J0ICogZnJvbSAnLi85OTkvaW5saW5lLXZpZGVvL2lubGluZS12aWRlbyc7XHJcbmV4cG9ydCAqIGZyb20gJy4vOTk5L3Njcm9sbC10by1uZXh0L3Njcm9sbC10by1uZXh0JztcclxuZXhwb3J0ICogZnJvbSAnLi9sYi00MDYvbW9sZWN1bGUtbGItNDA2JzsiLCJpbXBvcnQgeyBydW5Bc3luYyB9IGZyb20gJy4uL2hlbHBlcnMnO1xyXG5pbXBvcnQgeyBCYXNlQ29tcG9uZW50IH0gZnJvbSAnLi4vLi4vY29tcG9uZW50cy9iYXNlLWNvbXBvbmVudC5qcyc7XHJcblxyXG5cclxuZXhwb3J0IGNsYXNzIE1vbGVjdWxlMjA5IGV4dGVuZHMgQmFzZUNvbXBvbmVudCB7XHJcblxyXG4gIHN0YXRpYyBnZXQgc2VsZWN0b3IgKCkgeyByZXR1cm4gJy5tb2xlY3VsZS1sYi0yMDknOyB9XHJcblxyXG4gIGdldCBkZWZhdWx0cygpIHtcclxuICAgIHJldHVybiAkLmV4dGVuZCh7fSwgc3VwZXIuZGVmYXVsdHMsIHtcclxuICAgICAgc2VjdGlvblNlbGVjdG9yOiAnLnNlY3Rpb24nLFxyXG4gICAgICB3cmFwcGVyU2VsZWN0b3I6ICcubW9sZWN1bGUtb2NtaHBpZm9vdG5vdGVzX193cmFwcGVyJyxcclxuICAgICAgZm9vdG5vdGVzU2VjdGlvbkNsYXNzOiAnbW9sZWN1bGUtb2NtaHBpZm9vdG5vdGVzLXNlY3Rpb24nLFxyXG4gICAgICBsYXlvdXRXaWR0aDogMTI4MFxyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBjb25zdHJ1Y3RvciguLi5hcmdzKSB7XHJcbiAgICBzdXBlciguLi5hcmdzKTtcclxuICAgIHRoaXMuX2luaXQoKTtcclxuICB9XHJcblxyXG4gIF9pbml0KCkge1xyXG4gICAgXHJcbiAgICB0aGlzLl9zZWN0aW9uID0gdGhpcy5lbC5wYXJlbnRzKHRoaXMub3B0aW9ucy5zZWN0aW9uU2VsZWN0b3IpO1xyXG5cclxuICAgIHJ1bkFzeW5jKCgpID0+IHtcclxuICAgICAgdGhpcy5fYWxpZ25XaXRoTGF5b3V0KCk7XHJcbiAgICAgIHRoaXMuX3NlY3Rpb24uYWRkQ2xhc3ModGhpcy5vcHRpb25zLmZvb3Rub3Rlc1NlY3Rpb25DbGFzcyk7XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIF9hbGlnbldpdGhMYXlvdXQoKSB7XHJcbiAgICAgIGxldCBtYXhXaWR0aCA9IHRoaXMuX2dldExheW91dFdpZHRoKCksXHJcbiAgICAgICAgICB3cmFwcGVyID0gdGhpcy5lbC5maW5kKHRoaXMub3B0aW9ucy53cmFwcGVyU2VsZWN0b3IpO1xyXG5cclxuICAgICAgd3JhcHBlci5jc3MoJ21heC13aWR0aCcsIG1heFdpZHRoKTtcclxuICB9XHJcblxyXG4gIF9nZXRMYXlvdXRXaWR0aCgpIHtcclxuICAgICAgbGV0IHJlc3VsdCA9IC9sYXlvdXQtKC4qPykoXFxEfCQpLy5leGVjKHRoaXMuX3NlY3Rpb24uYXR0cignY2xhc3MnKSk7XHJcbiAgICAgIGlmIChyZXN1bHQpIHtcclxuICAgICAgICAgIHJldHVybiArcmVzdWx0WzFdO1xyXG4gICAgICB9XHJcbiAgICAgIHJldHVybiB0aGlzLm9wdGlvbnMubGF5b3V0V2lkdGg7XHJcbiAgfVxyXG5cclxufVxyXG4iLCJpbXBvcnQgeyBCYXNlQ29tcG9uZW50IH0gZnJvbSAnLi4vLi4vY29tcG9uZW50cy9iYXNlLWNvbXBvbmVudC5qcyc7XHJcblxyXG5cclxuZXhwb3J0IGNsYXNzIE1vbGVjdWxlMzAwIGV4dGVuZHMgQmFzZUNvbXBvbmVudCB7XHJcblxyXG4gIHN0YXRpYyBnZXQgc2VsZWN0b3IgKCkgeyByZXR1cm4gJy5tb2xlY3VsZS1sYi0zMDAnOyB9XHJcblxyXG4gIGdldCBkZWZhdWx0cygpIHtcclxuICAgIHJldHVybiAkLmV4dGVuZCh7fSwgc3VwZXIuZGVmYXVsdHMsIHt9KTtcclxuICB9XHJcblxyXG4gIGNvbnN0cnVjdG9yKC4uLmFyZ3MpIHtcclxuICAgIHN1cGVyKC4uLmFyZ3MpO1xyXG4gICAgdGhpcy5faW5pdCgpO1xyXG4gIH1cclxuXHJcbiAgX2luaXQoKSB7XHJcbiAgICBpZiAodGhpcy5lbC5maW5kKCdhLmJ1dHRvbicpLmxlbmd0aCA+IDEpIHtcclxuICAgICAgdGhpcy5lbC5maW5kKCdhLmJ1dHRvbicpLndyYXAoJzxkaXYgY2xhc3M9XCJjdGEtd3JhcHBlclwiPjwvZGl2PicpO1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG4iLCJpbXBvcnQgeyBCYXNlQ29tcG9uZW50IH0gZnJvbSAnLi4vLi4vY29tcG9uZW50cy9iYXNlLWNvbXBvbmVudC5qcyc7XHJcblxyXG5cclxuZXhwb3J0IGNsYXNzIE1vbGVjdWxlMzAyIGV4dGVuZHMgQmFzZUNvbXBvbmVudCB7XHJcblxyXG4gIHN0YXRpYyBnZXQgc2VsZWN0b3IgKCkgeyByZXR1cm4gJy5tb2xlY3VsZS1sYi0zMDInOyB9XHJcblxyXG4gIGdldCBkZWZhdWx0cygpIHtcclxuICAgIHJldHVybiAkLmV4dGVuZCh7fSwgc3VwZXIuZGVmYXVsdHMsIHtcclxuICAgICAgcm93U2VsZWN0b3I6ICcucm93JyxcclxuICAgICAgY2hpbGRyZW5TZWxlY3RvcjogJz4gKicsXHJcbiAgICAgIGltYWdlU2VsZWN0b3I6ICdpbWcnLFxyXG4gICAgICBpbWFnZUhvbGRlclNlbGVjdG9yOiAnLmltYWdlLWhvbGRlcicsXHJcbiAgICAgIGNvbnRlbnRXcmFwcGVyU2VsZWN0b3I6ICcuY29udGVudC13cmFwcGVyJyxcclxuXHJcbiAgICAgIGNsb25lZENsYXNzOiAnY2xvbmVkJyxcclxuICAgICAgaGlkZGVuQ2xhc3M6ICdoaWRkZW4tY29udGFpbmVyJyxcclxuICAgICAgbm9JbWFnZUNsYXNzOiAnbm8taW1hZ2UnLFxyXG4gICAgICBub0FsbFNwYWNpbmdzQ2xhc3M6ICdkcm9wLWFsbC1zcGFjaW5ncydcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgY29uc3RydWN0b3IoLi4uYXJncykge1xyXG4gICAgc3VwZXIoLi4uYXJncyk7XHJcbiAgICB0aGlzLl9pbml0KCk7XHJcbiAgfVxyXG5cclxuICBfaW5pdCgpIHtcclxuICAgIGxldCBpbWFnZSA9IHRoaXMuZWwuZmluZCh0aGlzLm9wdGlvbnMuaW1hZ2VTZWxlY3Rvcik7XHJcbiAgICBsZXQgY29udGVudFdyYXBwZXIgPSB0aGlzLmVsLmZpbmQodGhpcy5vcHRpb25zLmNvbnRlbnRXcmFwcGVyU2VsZWN0b3IpO1xyXG4gICAgbGV0IGhhc0FueVRleHRDb250ZW50ID0gY29udGVudFdyYXBwZXIuZmluZCh0aGlzLm9wdGlvbnMuY2hpbGRyZW5TZWxlY3RvcikubGVuZ3RoO1xyXG5cclxuICAgIGlmIChpbWFnZS5sZW5ndGgpIHtcclxuICAgICAgdGhpcy5lbC5maW5kKHRoaXMub3B0aW9ucy5pbWFnZUhvbGRlclNlbGVjdG9yKS5jc3Moe1xyXG4gICAgICAgICdiYWNrZ3JvdW5kLWltYWdlJzogYHVybChcIiR7aW1hZ2UuYXR0cignc3JjJyl9XCIpYFxyXG4gICAgICB9KTtcclxuXHJcbiAgICAgIC8vIE5PVEU6IEFwcGVuZCBjbG9uZWQgY29weSBvbmx5IGlmIHRoZXJlIGlzIGFuIGltYWdlIHRvZ2V0aGVyIHdpdGggY29udGVudFxyXG4gICAgICBpZiAoaGFzQW55VGV4dENvbnRlbnQpIHtcclxuICAgICAgICBjb250ZW50V3JhcHBlci5maW5kKCcgPiBhLCA+IC5tb2xlY3VsZS1sYi00MDYnKS53cmFwKCc8ZGl2IGNsYXNzPVwiY3RhLXdyYXBwZXJcIj48L2Rpdj4nKTtcclxuICAgICAgICBsZXQgY29udGVudENvcHkgPSBjb250ZW50V3JhcHBlclxyXG4gICAgICAgICAgICAuY2xvbmUodHJ1ZSwgdHJ1ZSlcclxuICAgICAgICAgICAgLmFkZENsYXNzKGAke3RoaXMub3B0aW9ucy5jbG9uZWRDbGFzc30gJHt0aGlzLm9wdGlvbnMuaGlkZGVuQ2xhc3N9YCk7XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgIGNvbnRlbnRDb3B5LmZpbmQoJy5vdmVybGF5LXBvcHVwJykucmVtb3ZlKCk7XHJcbiAgICAgICAgdGhpcy5lbC5hcHBlbmQoY29udGVudENvcHkpO1xyXG4gICAgICB9XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLmVsLmFkZENsYXNzKHRoaXMub3B0aW9ucy5ub0ltYWdlQ2xhc3MpO1xyXG4gICAgfVxyXG4gICAgLy9pbiBjYXNlIGlmIG1vZHVsZSBoYXMgYm90aCBjbGFzc25hbWVzIC0gdGhlcmUgc2hvdWxkIG5vdCBiZSBub0FsbFNwYWNpbmdzQ2xhc3Mgb24gYSBST1dcclxuICAgIGxldCBwcmVzZXJ2ZVdpZHRoID0gdGhpcy5lbC5hdHRyKCdjbGFzcycpLm1hdGNoKC9pbWFnZS1hcy1pc3xyZXNwZWN0LWltYWdlLW1ldGFkYXRhLXdpZHRoL2cpO1xyXG4gICAgaWYgKCFwcmVzZXJ2ZVdpZHRoIHx8IChwcmVzZXJ2ZVdpZHRoICYmIHByZXNlcnZlV2lkdGgubGVuZ3RoIDwgMikpIHtcclxuICAgICAgdGhpcy5lbC5wYXJlbnRzKHRoaXMub3B0aW9ucy5yb3dTZWxlY3RvcikuYWRkQ2xhc3ModGhpcy5vcHRpb25zLm5vQWxsU3BhY2luZ3NDbGFzcylcclxuICAgIH1cclxuICB9XHJcbn1cclxuIiwiaW1wb3J0IHsgQmFzZUNvbXBvbmVudCB9IGZyb20gJy4uLy4uL2NvbXBvbmVudHMvYmFzZS1jb21wb25lbnQuanMnO1xyXG5cclxuZXhwb3J0IGNsYXNzIE1vbGVjdWxlNDA2IGV4dGVuZHMgQmFzZUNvbXBvbmVudCB7XHJcblxyXG4gIHN0YXRpYyBnZXQgc2VsZWN0b3IgKCkgeyByZXR1cm4gJy5tb2xlY3VsZS1sYi00MDYnOyB9XHJcblxyXG4gIGdldCBkZWZhdWx0cygpIHtcclxuICAgIHJldHVybiAkLmV4dGVuZCh7fSwgc3VwZXIuZGVmYXVsdHMsIHtcclxuICAgICAgbW9kdWxlU2VsZWN0b3I6ICcubW9sZWN1bGUtbGItNDA2JyxcclxuICAgICAgZHJvcFNlbGVjdG9yOiAnLmRyb3Bkb3duLW1lbnUnLFxyXG4gICAgICBjZW50ZXJNb2R1bGVDbGFzc25hbWU6ICdhbGlnbi1tb2R1bGUtY2VudGVyJyxcclxuICAgICAgYWxpZ25SaWdodENsYXNzbmFtZTogJ2FsaWduLW1vZHVsZS1yaWdodCcsXHJcbiAgICAgIHRyaWdnZXJTZWxlY3RvcjogJy5kcm9wZG93bi10b2dnbGUnLFxyXG4gICAgICBjbGlja2FibGVIZWxwZXJTZWxlY3RvcjogJy5jbGlja2FibGUnXHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIGNvbnN0cnVjdG9yKC4uLmFyZ3MpIHtcclxuICAgIHN1cGVyKC4uLmFyZ3MpO1xyXG4gICAgdGhpcy5faW5pdCgpO1xyXG4gIH1cclxuXHJcbiAgX2luaXQoKSB7XHJcbiAgICB0aGlzLl9oYW5kbGVBbGlnbm1lbnQoKTtcclxuICAgIHRoaXMuX2luaXRMaXN0ZW5lcnMoKTtcclxuICAgIHRoaXMuY2xvc2VUaW1lb3V0ID0gbnVsbDtcclxuICB9XHJcblxyXG4gIF9oYW5kbGVBbGlnbm1lbnQoKSB7XHJcbiAgICBjb25zdCBjbGlja2FibGVFbCA9ICQoJzxkaXYgLz4nLCB7Y2xhc3M6ICdjbGlja2FibGUnfSk7XHJcbiAgICB0aGlzLmVsLmZpbmQodGhpcy5vcHRpb25zLnRyaWdnZXJTZWxlY3RvcikuYmVmb3JlKGNsaWNrYWJsZUVsKTtcclxuICAgIGlmICh0aGlzLmVsLmhhc0NsYXNzKHRoaXMub3B0aW9ucy5jZW50ZXJNb2R1bGVDbGFzc25hbWUpKSB7XHJcbiAgICAgIHRoaXMuZWwucGFyZW50KCkuYWRkQ2xhc3MoJ3RleHQtY2VudGVyJyk7XHJcbiAgICB9XHJcbiAgICBpZiAodGhpcy5lbC5oYXNDbGFzcyh0aGlzLm9wdGlvbnMuYWxpZ25SaWdodENsYXNzbmFtZSkpIHtcclxuICAgICAgdGhpcy5lbC5wYXJlbnQoKS5hZGRDbGFzcygndGV4dC1yaWdodCcpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgX3ByZXBhcmVNb2JpbGUoKSB7XHJcbiAgICBsZXQgdHBsID0gXy50ZW1wbGF0ZSh0aGlzLm9wdGlvbnMubW9iaWxlVHBsKSxcclxuICAgICAgICBpdGVtcyA9IHRoaXMuZWwuZmluZCgndWwnKS5jbG9uZSgpLmh0bWwoKSxcclxuICAgICAgICBidG4gPSB0aGlzLmVsLmZpbmQoJ2xpLnNtYWxsLWJ0bicpLmNsb25lKCkuaHRtbCgpLFxyXG4gICAgICAgIGZpcnN0QW5jaG9yID0gdGhpcy5lbC5maW5kKCdsaTpmaXJzdC1jaGlsZCBhJyksXHJcbiAgICAgICAgZHJvcFRyaWdnZXIgPSAnPGEgY2xhc3M9XCJkcm9wLXRyaWdnZXJcIiBocmVmPVwiamF2YXNjcmlwdDogdm9pZCgwKVwiPicgKyBmaXJzdEFuY2hvci50ZXh0KCkgKyAnPC9hPidcclxuXHJcbiAgICB0aGlzLmVsLmFwcGVuZCh0cGwoe2l0ZW1zLCBidG4sIGRyb3BUcmlnZ2VyfSkpO1xyXG4gIH1cclxuXHJcbiAgX2luaXRMaXN0ZW5lcnMoKSB7XHJcbiAgICB0aGlzLmVsLmZpbmQodGhpcy5vcHRpb25zLmNsaWNrYWJsZUhlbHBlclNlbGVjdG9yKS5vbignY2xpY2snLCAoZSkgPT4ge1xyXG4gICAgICAkKHRoaXMub3B0aW9ucy5tb2R1bGVTZWxlY3Rvcikubm90KHRoaXMuZWwpLnJlbW92ZUNsYXNzKCdhY3RpdmUnKTtcclxuICAgICAgaWYgKHRoaXMuZWwuaGFzQ2xhc3MoJ2FjdGl2ZScpKSB7XHJcbiAgICAgICAgdGhpcy5lbC5yZW1vdmVDbGFzcygnYWN0aXZlJyk7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgdGhpcy5lbC5hZGRDbGFzcygnYWN0aXZlJyk7XHJcbiAgICAgICAgdGhpcy5faW5pdENsb3NlT25Cb2R5Q0xpY2soKTtcclxuICAgICAgfVxyXG4gICAgfSk7XHJcbiAgICB0aGlzLmVsLm9uKCdmb2N1c291dCcsICgpID0+IHtcclxuICAgICAgdGhpcy5jbG9zZVRpbWVvdXQgPSBzZXRUaW1lb3V0KCgpID0+IHtcclxuICAgICAgICB0aGlzLmVsLnJlbW92ZUNsYXNzKCdhY3RpdmUnKTtcclxuICAgICAgfSwgNDAwKTtcclxuICAgIH0pO1xyXG4gICAgdGhpcy5lbC5maW5kKHRoaXMub3B0aW9ucy4gZHJvcFNlbGVjdG9yKS5vbignY2xpY2snLCAnYScsICgpID0+IHtcclxuICAgICAgdGhpcy5lbC50cmlnZ2VyKCdmb2N1c291dCcpO1xyXG4gICAgfSk7XHJcbiAgfVxyXG4gIF9pbml0Q2xvc2VPbkJvZHlDTGljaygpIHtcclxuICAgICQoJ2JvZHknKS5vZmYoJ2NsaWNrLmRkJykub24oJ2NsaWNrLmRkJywgKGUpID0+IHtcclxuICAgICAgaWYgKCEkKGUudGFyZ2V0KS5jbG9zZXN0KHRoaXMub3B0aW9ucy5tb2R1bGVTZWxlY3RvcikubGVuZ3RoKSB7XHJcbiAgICAgICAgJCh0aGlzLm9wdGlvbnMubW9kdWxlU2VsZWN0b3IpLnJlbW92ZUNsYXNzKCdhY3RpdmUnKTtcclxuICAgICAgICAkKCdib2R5Jykub2ZmKCdjbGljay5kZCcpO1xyXG4gICAgICAgIGNsZWFyVGltZW91dCh0aGlzLmNsb3NlVGltZW91dCk7XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gIH1cclxuICBcclxufVxyXG4iLCJpbXBvcnQgeyBCYXNlQ29tcG9uZW50IH0gZnJvbSAnLi4vLi4vY29tcG9uZW50cy9iYXNlLWNvbXBvbmVudC5qcyc7XHJcblxyXG5leHBvcnQgY2xhc3MgTW9sZWN1bGU0MTMgZXh0ZW5kcyBCYXNlQ29tcG9uZW50IHtcclxuXHJcbiAgc3RhdGljIGdldCBzZWxlY3RvciAoKSB7IHJldHVybiAnLm1vbGVjdWxlLWxiLTQxMyc7IH1cclxuXHJcbiAgZ2V0IGRlZmF1bHRzKCkge1xyXG4gICAgcmV0dXJuICQuZXh0ZW5kKHt9LCBzdXBlci5kZWZhdWx0cywge1xyXG4gICAgICBjdXN0b21BcHBlYXJhbmNlQ2xhc3NuYW1lOiAnYXBwZWFyYW5jZS10aGVtZS0yJ1xyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBjb25zdHJ1Y3RvciguLi5hcmdzKSB7XHJcbiAgICBzdXBlciguLi5hcmdzKTtcclxuICAgIHRoaXMuX2luaXQoKTtcclxuICB9XHJcblxyXG4gIF9pbml0KCkge1xyXG4gICAgaWYgKHRoaXMuZWwuaGFzQ2xhc3ModGhpcy5vcHRpb25zLmN1c3RvbUFwcGVhcmFuY2VDbGFzc25hbWUpKSB7XHJcbiAgICAgIHRoaXMuX2luaXRDdXN0b21BcHBlYXJhbmNlKCk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLl93cmFwSW1hZ2UoKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIF9pbml0Q3VzdG9tQXBwZWFyYW5jZSgpIHtcclxuICAgIGxldCBpbWcgPSB0aGlzLmVsLmZpbmQoJ2ltZycpLFxyXG4gICAgICBoZWlnaHQgPSArdGhpcy5lbC5maW5kKCdpbWcnKS5hdHRyKCdoZWlnaHQnKSxcclxuICAgICAgYmFja2dyb3VuZCA9ICd1cmwoJyArIGltZy5hdHRyKCdzcmMnKSArICcpJztcclxuICAgIGltZy5wYXJlbnQoKS5jc3Moe1xyXG4gICAgICAnYmFja2dyb3VuZC1pbWFnZSc6IGJhY2tncm91bmQsXHJcbiAgICAgIGhlaWdodDogaGVpZ2h0XHJcbiAgICB9KTtcclxuXHJcbiAgICB0aGlzLl9zZXRJbml0aWFsaXplZCgpO1xyXG4gIH1cclxuXHJcbiAgX3dyYXBJbWFnZSgpIHtcclxuICAgIHRoaXMuZWwuZmluZCgnaW1nJykuZWFjaCgoaSwgZWwpID0+ICQoZWwpLndyYXAoJzxkaXYgLz4nKSk7XHJcbiAgfVxyXG5cclxuICBfc2V0SW5pdGlhbGl6ZWQoKSB7XHJcbiAgICB0aGlzLmVsLmFkZENsYXNzKCdpbml0aWFsaXplZCcpO1xyXG4gIH1cclxuXHJcbn1cclxuIiwiaW1wb3J0IHsgQmFzZUNvbXBvbmVudCB9IGZyb20gJy4uLy4uL2NvbXBvbmVudHMvYmFzZS1jb21wb25lbnQuanMnO1xyXG5cclxuXHJcbmV4cG9ydCBjbGFzcyBNb2xlY3VsZTUwMSBleHRlbmRzIEJhc2VDb21wb25lbnQge1xyXG5cclxuICBzdGF0aWMgZ2V0IHNlbGVjdG9yKCkgeyByZXR1cm4gJy5tb2xlY3VsZS1sYi01MDEnOyB9XHJcblxyXG4gIGdldCBkZWZhdWx0cygpIHtcclxuICAgIHJldHVybiAkLmV4dGVuZCh7fSwgc3VwZXIuZGVmYXVsdHMsIHtcclxuICAgICAgdGV4dE92ZXJJbWFnZUNsYXNzbmFtZTogJ3RleHQtb3Zlci1pbWFnZSdcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgY29uc3RydWN0b3IoLi4uYXJncykge1xyXG4gICAgc3VwZXIoLi4uYXJncyk7XHJcbiAgICB0aGlzLl9pbml0KCk7XHJcbiAgfVxyXG5cclxuICBfaW5pdChlbCkge1xyXG4gICAgaWYgKHRoaXMuZWwuaGFzQ2xhc3ModGhpcy5vcHRpb25zLnRleHRPdmVySW1hZ2VDbGFzc25hbWUpKSB7XHJcbiAgICAgIHRoaXMuZWwuZmluZCgnPiAqOm5vdCgudmlkZW9pdGVtKTpub3QoLmNsZiknKS53cmFwQWxsKCc8ZGl2IGNsYXNzPVwibW9kdWxlLWNvbnRlbnRcIj48L2Rpdj4nKTtcclxuICAgIH1cclxuICB9XHJcbn1cclxuIiwiaW1wb3J0IHsgQmFzZUNvbXBvbmVudCB9IGZyb20gJy4uLy4uL2NvbXBvbmVudHMvYmFzZS1jb21wb25lbnQuanMnO1xyXG5cclxuXHJcbmV4cG9ydCBjbGFzcyBNb2xlY3VsZTUxMCBleHRlbmRzIEJhc2VDb21wb25lbnQge1xyXG5cclxuICBzdGF0aWMgZ2V0IHNlbGVjdG9yICgpIHsgcmV0dXJuICcubW9sZWN1bGUtbGItNTEwJzsgfVxyXG5cclxuICBjb25zdHJ1Y3RvciguLi5hcmdzKSB7XHJcbiAgICBzdXBlciguLi5hcmdzKTtcclxuICAgIHRoaXMuX2luaXQoKTtcclxuICB9XHJcblxyXG4gIF9pbml0KCkge1xyXG4gICAgdGhpcy5fc2V0Q29sbGFwc2libGVJbmRpY2F0b3IoKTtcclxuICAgIHRoaXMuX3VwZGF0ZUh0bWwoKTtcclxuICAgIHRoaXMuX2luaXRMaXN0ZW5lcnMoKTtcclxuICB9XHJcblxyXG4gIF9zZXRDb2xsYXBzaWJsZUluZGljYXRvcigpIHtcclxuICAgIGlmICgkLnRyaW0odGhpcy5lbC5maW5kKCcuZGVzY3JpcHRpb24nKS50ZXh0KCkpKSB7XHJcbiAgICAgIHRoaXMuZWwuZmluZCgnLmRlc2NyaXB0aW9uJykud3JhcCgnPGRpdiBjbGFzcz1cImRlc2NyaXB0aW9uLXdyYXBwZXJcIi8+Jyk7XHJcbiAgICAgIHRoaXMuZWwuYWRkQ2xhc3MoJ3dpdGgtaWNvbicpO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgaWYgKCF0aGlzLmVsLmZpbmQoJy50aXRsZSAqJykubGVuZ3RoKSB7XHJcbiAgICAgICAgdGhpcy5lbC5wYXJlbnQoKS5hZGRDbGFzcygnaWdub3JlJyk7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcblxyXG4gIF91cGRhdGVIdG1sKCkge1xyXG4gICAgbGV0IHNyYyA9IHRoaXMuZWwuZmluZCgnaW1nJykuYXR0cignc3JjJyksXHJcbiAgICAgICAgaW1nV3JhcHBlciA9IHRoaXMuZWwuZmluZCgnLmltZy13cmFwcGVyJyk7XHJcblxyXG4gICAgaW1nV3JhcHBlci5jc3MoJ2JhY2tncm91bmQtaW1hZ2UnLCAndXJsKCcgKyBzcmMgKyAnKScpO1xyXG4gICAgdGhpcy5lbC5hZGRDbGFzcygnaW5pdGlhbGl6ZWQnKTtcclxuICB9XHJcblxyXG4gIF9pbml0TGlzdGVuZXJzKCkge1xyXG4gICAgY29uc3QgcGFyZW50ID0gdGhpcy5lbC5jbG9zZXN0KCcuc2VjdGlvbi5jb2xsYWdlLWxheW91dCcpO1xyXG4gICAgdGhpcy5lbC5maW5kKCcuZXhwYW5kLWNvbGxhcHNlLXdyYXBwZXInKS5vbignY2xpY2snLCAoKSA9PiB7XHJcbiAgICAgIHBhcmVudC5maW5kKCcubW9sZWN1bGUtbGItNTEwJykubm90KHRoaXMuZWwpLmVhY2goKGluZGV4LCBlbCkgPT4gdGhpcy5faGlkZURlc2NyaXB0aW9uKCQoZWwpKSk7XHJcbiAgICAgIGlmICh0aGlzLmVsLmhhc0NsYXNzKCdhY3RpdmUnKSkge1xyXG4gICAgICAgIHRoaXMuX2hpZGVEZXNjcmlwdGlvbih0aGlzLmVsKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICB0aGlzLl9zaG93RGVzY3JpcHRpb24odGhpcy5lbCk7XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgX2hpZGVEZXNjcmlwdGlvbigkZWwpIHtcclxuICAgIGlmICgkZWwuaGFzQ2xhc3MoJ2FjdGl2ZScpKSB7XHJcbiAgICAgICRlbC5maW5kKCcuZGVzY3JpcHRpb24nKS5zdG9wKCkuY2xlYXJRdWV1ZSgpLmFuaW1hdGUoe29wYWNpdHk6IDB9LCAxNTApO1xyXG4gICAgICAkZWwucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpLmZpbmQoJy5kZXNjcmlwdGlvbi13cmFwcGVyJykuc2xpZGVVcCgzMDApO1xyXG4gICAgICB0aGlzLl9lbmFibGVNZXRyaWNzKCRlbCk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBfc2hvd0Rlc2NyaXB0aW9uKCRlbCkge1xyXG4gICAgJGVsLmFkZENsYXNzKCdhY3RpdmUnKTtcclxuICAgICRlbC5maW5kKCcuZGVzY3JpcHRpb24td3JhcHBlcicpLnNsaWRlRG93bigzMDAsICgpID0+IHtcclxuICAgICAgJGVsLmZpbmQoJy5kZXNjcmlwdGlvbicpLnN0b3AoKS5jbGVhclF1ZXVlKCkuYW5pbWF0ZSh7J29wYWNpdHknOiAxfSk7XHJcbiAgICB9KTtcclxuICAgIHRoaXMuX2Rpc2FibGVNZXRyaWNzKCRlbCk7XHJcbiAgfVxyXG5cclxuICBfZW5hYmxlTWV0cmljcygkZWwpIHtcclxuICAgIHNldFRpbWVvdXQoKCkgPT4gJGVsLmZpbmQoJy5hY3Rpb24tdHJpZ2dlcicpLnJlbW92ZUF0dHIoJ2RhdGEtZGlzYWJsZS1tZXRyaWNzJykpO1xyXG4gIH1cclxuXHJcbiAgX2Rpc2FibGVNZXRyaWNzKCRlbCkge1xyXG4gICAgc2V0VGltZW91dCgoKSA9PiAkZWwuZmluZCgnLmFjdGlvbi10cmlnZ2VyJykuYXR0cignZGF0YS1kaXNhYmxlLW1ldHJpY3MnLCAnZGlzYWJsZWQnKSk7XHJcbiAgfVxyXG59XHJcbiIsImltcG9ydCB7IEJhc2VDb21wb25lbnQgfSBmcm9tICcuLi8uLi9jb21wb25lbnRzL2Jhc2UtY29tcG9uZW50LmpzJztcclxuXHJcblxyXG5leHBvcnQgY2xhc3MgTW9sZWN1bGU3MDEgZXh0ZW5kcyBCYXNlQ29tcG9uZW50IHtcclxuXHJcbiAgc3RhdGljIGdldCBzZWxlY3RvciAoKSB7IHJldHVybiAnLm1vbGVjdWxlLWxiLTcwMSc7IH1cclxuXHJcbiAgZ2V0IGRlZmF1bHRzKCkge1xyXG4gICAgcmV0dXJuICQuZXh0ZW5kKHt9LCBzdXBlci5kZWZhdWx0cywge1xyXG4gICAgICBwYWdlUm93U2VsZWN0b3I6ICcuc2VjdGlvbicsXHJcbiAgICAgIHBhZ2VTZWN0aW9uU2VsZWN0b3I6ICcuZmxleC1jb250ZW50JyxcclxuICAgICAgY29uZmlnRWxTZWxlY3RvcjogJy5yb3ctZ3JhZGllbnQtY29uZmlnJ1xyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBjb25zdHJ1Y3RvciguLi5hcmdzKSB7XHJcbiAgICBzdXBlciguLi5hcmdzKTtcclxuICAgIHRoaXMuX2luaXQoKTtcclxuICB9XHJcblxyXG4gIF9pbml0KCkge1xyXG4gICAgdGhpcy5fY29sbGVjdE9wdGlvbnMoKTtcclxuICAgIHRoaXMuX2dldEJnKCk7XHJcbiAgICB0aGlzLl9zZXRCZygpO1xyXG4gIH1cclxuXHJcbiAgX2NvbGxlY3RPcHRpb25zKCkge1xyXG4gICAgdGhpcy5jb25maWdFbCA9IHRoaXMuZWwuZmluZCh0aGlzLm9wdGlvbnMuY29uZmlnRWxTZWxlY3Rvcik7XHJcbiAgICB0aGlzLm9wdGlvbnMgPSAkLmV4dGVuZCh7fSwgdGhpcy5vcHRpb25zLCB0aGlzLmNvbmZpZ0VsLmRhdGEoKSk7XHJcbiAgfVxyXG5cclxuICBfZ2V0QmcoKSB7XHJcbiAgICB0aGlzLm9wdGlvbnMuYmcgPSAnJztcclxuICAgIGlmICh0aGlzLm9wdGlvbnMuc3RhcnQpIHtcclxuICAgICAgdGhpcy5vcHRpb25zLmJnICs9ICdiYWNrZ3JvdW5kOiAnICsgdGhpcy5vcHRpb25zLnN0YXJ0ICsgJzsnO1xyXG4gICAgICBpZiAodGhpcy5vcHRpb25zLmVuZCkge1xyXG4gICAgICAgIGxldCBkaXJlY3Rpb24gPSAodGhpcy5vcHRpb25zLmRpcmVjdGlvbiA9PT0gJ3ZlcnRpY2FsJykgPyAnYm90dG9tJyA6ICdyaWdodCc7XHJcbiAgICAgICAgdGhpcy5vcHRpb25zLmJnICs9ICdiYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQodG8gJyArIGRpcmVjdGlvbiArICcsICcgKyB0aGlzLm9wdGlvbnMuc3RhcnQgKyAnIDAlLCcgKyB0aGlzLm9wdGlvbnMuZW5kICsgJyAxMDAlKTsnXHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcblxyXG4gIF9zZXRCZygpIHtcclxuICAgIGNvbnN0IGFwcGx5VG8gPSAodGhpcy5vcHRpb25zLmFyZWEgIT09ICdhcmVhLXBhZ2Utcm93JykgPyAncGFnZVNlY3Rpb24nIDogJ3BhZ2VSb3cnO1xyXG4gICAgY29uc3QgYXBwbHlUb0VsID0gdGhpcy5lbC5jbG9zZXN0KHRoaXMub3B0aW9uc1thcHBseVRvICsgJ1NlbGVjdG9yJ10pO1xyXG4gICAgdGhpcy5vcHRpb25zLmJnICYmIGFwcGx5VG9FbC5hdHRyKCdzdHlsZScsIHRoaXMub3B0aW9ucy5iZyk7XHJcbiAgfVxyXG4gIFxyXG59XHJcbiIsImltcG9ydCB7IEJhc2VDb21wb25lbnQgfSBmcm9tICcuLi8uLi9jb21wb25lbnRzL2Jhc2UtY29tcG9uZW50LmpzJztcclxuXHJcblxyXG5leHBvcnQgY2xhc3MgTW9sZWN1bGU3MDIgZXh0ZW5kcyBCYXNlQ29tcG9uZW50IHtcclxuXHJcbiAgc3RhdGljIGdldCBzZWxlY3RvciAoKSB7IHJldHVybiAnLm1vbGVjdWxlLWxiLTcwMic7IH1cclxuXHJcbiAgY29uc3RydWN0b3IoLi4uYXJncykge1xyXG4gICAgc3VwZXIoLi4uYXJncyk7XHJcbiAgICB0aGlzLl9pbml0KCk7XHJcbiAgfVxyXG5cclxuICBfaW5pdCgpIHtcclxuICAgIGlmICghR0xPQkFMUy5VdGlscy5kZXZpY2UuaXNNb2JpbGUpIHtcclxuICAgICAgdGhpcy52aWRlb1dyYXBwZXIgPSB0aGlzLmVsLmZpbmQoJy5hbWJpZW50LXZpZGVvLXdyYXBwZXInKTtcclxuICAgICAgdGhpcy5jb25maWcgPSB0aGlzLnZpZGVvV3JhcHBlci5maW5kKCcudmlkZW8tY29uZmlnJykuZGF0YSgpO1xyXG4gICAgICB0aGlzLmVsLmNsb3Nlc3QoJy5zZWN0aW9uJykucHJlcGVuZCh0aGlzLnZpZGVvV3JhcHBlcikuYWRkQ2xhc3MoJ2FtYmllbnQtdmlkZW8tc2VjdGlvbicpO1xyXG4gICAgICBzZXRUaW1lb3V0KCgpID0+IHRoaXMuX2NyZWF0ZVZpZGVvKCkpO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5lbC5yZW1vdmUoKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIF9jcmVhdGVWaWRlbygpIHtcclxuICAgIHRoaXMudmlkZW8gPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCd2aWRlbycpO1xyXG4gICAgdGhpcy5jb25maWcubXA0ICYmIHRoaXMuX2FkZFNvdXJjZSh0aGlzLmNvbmZpZy5tcDQsICd2aWRlby9tcDQnKTtcclxuICAgIHRoaXMuY29uZmlnLndlYm0gJiYgdGhpcy5fYWRkU291cmNlKHRoaXMuY29uZmlnLndlYm0sICd2aWRlby93ZWJtJyk7XHJcbiAgICB0aGlzLnZpZGVvLnNldEF0dHJpYnV0ZSgnYXV0b3BsYXknLCAnYXV0b3BsYXknKTtcclxuICAgIHRoaXMudmlkZW8uc2V0QXR0cmlidXRlKCdtdXRlZCcsICdtdXRlZCcpO1xyXG4gICAgdGhpcy52aWRlby5zZXRBdHRyaWJ1dGUoJ2xvb3AnLCAnbG9vcCcpO1xyXG4gICAgdGhpcy52aWRlb1dyYXBwZXIuYXBwZW5kKHRoaXMudmlkZW8pO1xyXG4gICAgdGhpcy52aWRlby5hZGRFdmVudExpc3RlbmVyKCdsb2FkZWRkYXRhJywgKCkgPT4ge1xyXG4gICAgICB0aGlzLmVsLmNsb3Nlc3QoJy5zZWN0aW9uJykuYWRkQ2xhc3MoJ3ZpZGVvLWxvYWRlZCcpO1xyXG4gICAgICB0aGlzLl9pbml0QXNwZWN0UmF0aW8oKTtcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgX2luaXRBc3BlY3RSYXRpbygpIHtcclxuICAgIHRoaXMucmF0aW8gPSB0aGlzLnZpZGVvLnZpZGVvV2lkdGggLyB0aGlzLnZpZGVvLnZpZGVvSGVpZ2h0O1xyXG4gICAgJCh3aW5kb3cpLm9uKCdyZXNpemUnLCBfLmRlYm91bmNlKCgpID0+IHRoaXMuX2NhbGNWaWRlb1dpZHRoKCksIDEwMCkpO1xyXG4gICAgdGhpcy5fY2FsY1ZpZGVvV2lkdGgoKTtcclxuICB9XHJcblxyXG4gIF9jYWxjVmlkZW9XaWR0aCgpIHtcclxuICAgIGxldCBtaW5XaWR0aCA9IHRoaXMucmF0aW8gKiB0aGlzLnZpZGVvV3JhcHBlci5oZWlnaHQoKTtcclxuICAgICQodGhpcy52aWRlbykuY3NzKCdtaW4td2lkdGgnLCBtaW5XaWR0aCk7XHJcbiAgfVxyXG5cclxuICBfYWRkU291cmNlKHNyYywgdHlwZSkge1xyXG4gICAgY29uc3Qgc291cmNlID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnc291cmNlJyk7XHJcbiAgICBzb3VyY2Uuc3JjID0gc3JjO1xyXG4gICAgc291cmNlLnR5cGUgPSB0eXBlO1xyXG4gICAgdGhpcy52aWRlby5hcHBlbmRDaGlsZChzb3VyY2UpO1xyXG4gIH1cclxuXHJcbiAgX3BsYXlWaWRlbygpIHtcclxuICAgIHRoaXMudmlkZW8ucGxheSgpO1xyXG4gIH1cclxuXHJcbn1cclxuIiwiaW1wb3J0IHsgQmFzZUNvbXBvbmVudCB9IGZyb20gJy4uLy4uL2NvbXBvbmVudHMvYmFzZS1jb21wb25lbnQuanMnO1xyXG5cclxuXHJcbmV4cG9ydCBjbGFzcyBNb2xlY3VsZTgyMyBleHRlbmRzIEJhc2VDb21wb25lbnQge1xyXG5cclxuICBzdGF0aWMgZ2V0IHNlbGVjdG9yICgpIHsgcmV0dXJuICcubW9sZWN1bGUtODIzLCAubW9sZWN1bGUtbGItODIzJzsgfVxyXG5cclxuICAvKmdldCBkZWZhdWx0cygpIHtcclxuICAgIHJldHVybiAkLmV4dGVuZCh7fSwgc3VwZXIuZGVmYXVsdHMsIHtcclxuICAgICAgZml4ZWRDb2x1bW5TZWxlY3RvcjogJ2ZpeGVkLWNvbHVtbidcclxuICAgIH0pO1xyXG4gIH0qL1xyXG5cclxuICBjb25zdHJ1Y3RvciguLi5hcmdzKSB7XHJcbiAgICBzdXBlciguLi5hcmdzKTtcclxuICAgIHRoaXMuX2luaXQoKTtcclxuICB9XHJcblxyXG4gIF9pbml0KCkge1xyXG4gICAgbGV0IGNlbGxzTGVuZ3RoID0gdGhpcy5lbC5maW5kKCcucm93cy13cmFwcGVyIC5maXJzdF9yb3cgPiBkaXYnKS5sZW5ndGg7XHJcbiAgICB0aGlzLmVsLmFkZENsYXNzKCdjb2x1bW5zLScgKyBjZWxsc0xlbmd0aCk7XHJcbiAgICAvKmlmICh0aGlzLmVsLmhhc0NsYXNzKHRoaXMub3B0aW9ucy5maXhlZENvbHVtblNlbGVjdG9yKSkge1xyXG4gICAgXHR0aGlzLmVsLmNsb3Nlc3QoJy5zZWN0aW9uJykuYWRkQ2xhc3MoJ3RhYmxlLWNvbHVtbnMtJyArIGNlbGxzTGVuZ3RoKTtcclxuICAgIFx0dGhpcy5lbC5maW5kKCc+LnJvd3Mtd3JhcHBlcicpLnN0aWNreVRhYmxlQ29sdW1ucyh7XHJcbiAgICBcdFx0Y29sdW1uczogMixcclxuICAgIFx0XHRzdGFydEZyb206IDFcclxuICAgIFx0fSk7XHJcbiAgICB9Ki9cclxuICB9XHJcbn1cclxuIiwiaW1wb3J0IHsgcnVuQXN5bmMgfSBmcm9tICcuLi9oZWxwZXJzJztcclxuaW1wb3J0IHsgQmFzZUNvbXBvbmVudCB9IGZyb20gJy4uLy4uL2NvbXBvbmVudHMvYmFzZS1jb21wb25lbnQuanMnO1xyXG5cclxuZXhwb3J0IGNsYXNzIEltYWdlR2FsbGVyeSBleHRlbmRzIEJhc2VDb21wb25lbnQge1xyXG5cclxuICAvLyBUaGlzIGNsYXNzIGlzIGFkZGVkIGF1dG9tYXRpY2FsbHkgdG8gdGhpcy5lbCBjb21wb25lbnRcclxuICBnZXQgUk9PVF9DTEFTUygpIHsgcmV0dXJuICdpbWFnZS1nYWxsZXJ5JyB9XHJcblxyXG4gIGdldCBkZWZhdWx0cygpIHtcclxuICAgIHJldHVybiAkLmV4dGVuZCh7fSwgc3VwZXIuZGVmYXVsdHMsIHtcclxuICAgICAgZW5hYmxlQXJyb3dzTW9iaWxlQ2xhc3M6ICdlbmFibGUtYXJyb3dzLW1vYmlsZScsXHJcbiAgICAgIGNhcm91c2VsQ291bnRlckNsYXNzOiAnZGlzcGxheS1jYXJvdXNlbC1jb3VudGVyJyxcclxuICAgICAgYW1vdW50U2xpZGVzOiAwXHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIGNvbnN0cnVjdG9yKC4uLmFyZ3MpIHtcclxuICAgIHN1cGVyKC4uLmFyZ3MpO1xyXG4gICAgdGhpcy5faW5pdCgpO1xyXG4gIH1cclxuXHJcbiAgX2luaXQoKSB7XHJcbiAgICAvL2xhenkgaW5pdCB3aGVuIGluIGNvbnRlbnQtb3ZlcmxheVxyXG4gICAgaWYgKHRoaXMuZWwuY2xvc2VzdCgnLmNvbnRlbnQtb3ZlcmxheScpLmxlbmd0aCkge1xyXG4gICAgICB0aGlzLl9sYXp5SW5pdCgpO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5faW5zdGFudEluaXQoKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIF9sYXp5SW5pdCgpIHtcclxuICAgIHRoaXMuZWwub25lKCdvdmVybGF5Lm9wZW5lZCcsICgpID0+IHRoaXMuX2luc3RhbnRJbml0KCkpO1xyXG4gICAgLy9nZXQgcmlkIG9mIGV4dHJhIGdyaWQgc3BhY2luZ3NcclxuICAgIHRoaXMuZWwub25lKCdvdmVybGF5LmJlZm9yZW9wZW4nLCAoKSA9PiB0aGlzLmVsLmNsb3Nlc3QoJy5jb250ZW50LW92ZXJsYXknKS5maW5kKCc+LnNlY3Rpb24nKS5hZGRDbGFzcygnZHJvcC1hbGwtc3BhY2luZ3MnKSk7XHJcbiAgfVxyXG5cclxuICBfaW5zdGFudEluaXQoKSB7XHJcbiAgICB0aGlzLnN3aXRjaGVyRWwgPSBudWxsO1xyXG4gICAgdGhpcy5jb3VudGVyRWwgPSBudWxsO1xyXG4gICAgdGhpcy5mYWRlckhlbHBlckVsID0gbnVsbDtcclxuICAgIHRoaXMudGh1bWJuYWlsTGlzdCA9IHRoaXMuZWwuZmluZCgnLnRodW1ibmFpbHMtbGlzdCcpO1xyXG4gICAgdGhpcy5pbWFnZUNvbnRhaW5lciA9IHRoaXMuZWwuZmluZCgnLmltYWdlLWNvbnRhaW5lcicpO1xyXG4gICAgdGhpcy5nYWxsZXJ5U2xpZGVyID0gdGhpcy5lbC5maW5kKCcuY29udGVudC1saXN0Jyk7XHJcbiAgICB0aGlzLmluaXRpYWxNZWRpYSA9IHRoaXMuX2NoZWNrTWVkaWEoKTtcclxuICAgIHRoaXMuX3NldEltYWdlcygpO1xyXG4gICAgdGhpcy5fcmVuZGVySW1hZ2VzKCk7XHJcbiAgICB0aGlzLl93cmFwRm9yU2Nyb2xsKCk7XHJcbiAgICB0aGlzLl9wcmVwYXJlSFRNTCgpO1xyXG4gICAgdGhpcy5pbml0TGlzdGVuZXJzKCk7XHJcbiAgICB0aGlzLl9hZGRTd2l0Y2hlcigpO1xyXG4gICAgdGhpcy5fYnVpbGRTbGlkZXIoKTtcclxuICAgIHRoaXMuX3NldENvdW50ZXIoKTtcclxuICAgIHRoaXMub25SZWFkeSgpO1xyXG4gIH1cclxuXHJcbiAgaW5pdExpc3RlbmVycygpIHtcclxuXHJcbiAgICB0aGlzLnRodW1ibmFpbExpc3QuZmluZCgnLml0ZW0nKS5vbignbW91c2VlbnRlcicsIChlKSA9PiB7XHJcbiAgICAgIHRoaXMuX3Njcm9sbFRvKCQoZS5jdXJyZW50VGFyZ2V0KSk7XHJcbiAgICB9KTtcclxuXHJcbiAgICB0aGlzLnRodW1ibmFpbExpc3QuZmluZCgnLml0ZW0nKS5vbignY2xpY2snLCAoZSkgPT4ge1xyXG4gICAgICBsZXQgZWwgPSAkKGUuY3VycmVudFRhcmdldCk7XHJcblxyXG4gICAgICB0aGlzLnRodW1ibmFpbExpc3QuZmluZCgnLml0ZW0nKS5yZW1vdmVDbGFzcyhcImFjdGl2ZVwiKTtcclxuICAgICAgZWwuYWRkQ2xhc3MoXCJhY3RpdmVcIik7XHJcbiAgICAgIGNvbnNvbGUubG9nKGVsKTtcclxuICAgICAgdGhpcy5nYWxsZXJ5U2xpZGVyLnNsaWNrKCdzbGlja0dvVG8nLCBlbC5pbmRleCgpKTtcclxuICAgIH0pO1xyXG5cclxuICAgIHRoaXMuZ2FsbGVyeVNsaWRlci5vbignaW5pdCByZUluaXQnLCAoZSwgc2xpY2ssIGN1cnJlbnRTbGlkZSkgPT4gdGhpcy5fc2V0QWN0aXZlKHNsaWNrLmN1cnJlbnRTbGlkZSkpO1xyXG4gICAgdGhpcy5nYWxsZXJ5U2xpZGVyLm9uKCdiZWZvcmVDaGFuZ2UnLCAoZSwgc2xpY2ssIGN1cnJlbnRTbGlkZSwgbmV4dFNsaWRlKSA9PiB0aGlzLl9zZXRBY3RpdmUobmV4dFNsaWRlKSk7XHJcbiAgfVxyXG5cclxuICBfcHJlcGFyZUhUTUwoKSB7XHJcbiAgICBpZiAodGhpcy5lbC5wYXJlbnRzKCcuY29udGVudC1vdmVybGF5JykubGVuZ3RoKSB7XHJcbiAgICAgIHRoaXMuZmFkZXJIZWxwZXJFbCA9ICQoJzxkaXYgLz4nLCB7Y2xhc3M6ICdmYWRlci1oZWxwZXInfSk7XHJcbiAgICAgIHRoaXMuZWwuZmluZCgnLmNvbnRlbnQtd3JhcHBlcicpLnByZXBlbmQodGhpcy5mYWRlckhlbHBlckVsKTtcclxuICAgIH1cclxuXHJcbiAgfVxyXG5cclxuICBfc2Nyb2xsVG8odGFiKSB7XHJcbiAgICBsZXQgc2Nyb2xsaWdQYW5lbCA9IHRoaXMuZWwuZmluZCgnLnNjcm9sbGluZy1wYW5lbCcpO1xyXG5cclxuICAgICAgaWYgKHNjcm9sbGlnUGFuZWwud2lkdGgoKSA8IHNjcm9sbGlnUGFuZWwuZ2V0KDApLnNjcm9sbFdpZHRoKSB7XHJcbiAgICAgICAgbGV0IGVsUG9zTGVmdCA9IE1hdGguZmxvb3IodGFiLnBvc2l0aW9uKCkubGVmdCksXHJcbiAgICAgICAgICAgIHBvc0xlZnQgPSBlbFBvc0xlZnQgLSBwYXJzZUludCh0aGlzLnRodW1ibmFpbExpc3QuY3NzKFwicGFkZGluZy1sZWZ0XCIpKTtcclxuXHJcbiAgICAgICAgc2Nyb2xsaWdQYW5lbC5zdG9wKHRydWUsIHRydWUpLmRlbGF5KDEwMCkuYW5pbWF0ZSh7XHJcbiAgICAgICAgICBzY3JvbGxMZWZ0OiBwb3NMZWZ0XHJcbiAgICAgICAgfSwgMzAwKTtcclxuICAgICAgfVxyXG4gIH1cclxuXHJcbiAgX2NoZWNrTWVkaWEoKSB7XHJcbiAgICB2YXIgbWVkaWE7XHJcbiAgICBpZiAod2luZG93LmlubmVyV2lkdGggPiA3MjApIHtcclxuICAgICAgbWVkaWEgPSBcImRlc2t0b3BcIjtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIG1lZGlhID0gXCJtb2JpbGVcIjtcclxuICAgIH1cclxuICAgIHJldHVybiBtZWRpYTtcclxuICB9XHJcblxyXG4gIF9jcm9zc0JyZWFrcG9pbnQoKSB7XHJcbiAgICBsZXQgbWVkaWEgPSB0aGlzLl9jaGVja01lZGlhKCk7XHJcbiAgICBpZiAobWVkaWEgPT0gdGhpcy5pbml0aWFsTWVkaWEpIHtcclxuICAgICAgcmV0dXJuO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5pbml0aWFsTWVkaWEgPSBtZWRpYTtcclxuICAgICAgdGhpcy5fcmVuZGVySW1hZ2VzKCk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBfc2V0SW1hZ2VzKCkge1xyXG4gICAgdGhpcy5pbWFnZUNvbnRhaW5lci5lYWNoKChpbmRleCwgZWwpID0+IHtcclxuICAgICAgbGV0IGVsZW0gPSAkKGVsKSxcclxuICAgICAgICBjb250YWluZXIgPSBlbGVtLmZpbmQoXCJkaXZcIiksXHJcbiAgICAgICAgb3B0aW9ucyA9IHt9O1xyXG5cclxuICAgICAgY29udGFpbmVyLmVhY2goZnVuY3Rpb24oaW5kZXgsIGl0KSB7XHJcbiAgICAgICAgbGV0IGl0ZW0gPSAkKGl0KTtcclxuICAgICAgICBpZiAoaXRlbS5oYXNDbGFzcyhcImRlc2t0b3BcIikpIHtcclxuICAgICAgICAgIG9wdGlvbnMuc3JjRGVza3RvcCA9IGl0ZW0uZGF0YShcInNyY1wiKTtcclxuICAgICAgICB9IGVsc2UgaWYgKGl0ZW0uaGFzQ2xhc3MoXCJtb2JpbGVcIikpIHtcclxuICAgICAgICAgIG9wdGlvbnMuc3JjTW9iaWxlID0gaXRlbS5kYXRhKFwic3JjXCIpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBvcHRpb25zLmFsdCA9IGl0ZW0uZGF0YShcImFsdFwiKSB8fCBcIlwiO1xyXG4gICAgICAgIGl0ZW0ucmVtb3ZlKCk7XHJcbiAgICAgIH0pO1xyXG5cclxuICAgICAgZWxlbS5hcHBlbmQodGhpcy5fY3JlYXRlSW1hZ2Uob3B0aW9ucykpO1xyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBfcmVuZGVySW1hZ2VzKCkge1xyXG4gICAgdGhpcy5pbWFnZUNvbnRhaW5lci5maW5kKCdpbWcnKS5lYWNoKChpbmRleCwgaW1hZ2UpID0+IHtcclxuICAgICAgbGV0IGltYWdlSXRlbSA9ICQoaW1hZ2UpLFxyXG4gICAgICAgIGRlc2t0b3BTcmMgPSBpbWFnZUl0ZW0uZGF0YShcInNyYy1kZXNrdG9wXCIpLFxyXG4gICAgICAgIG1vYmlsZVNyYyA9IGltYWdlSXRlbS5kYXRhKFwic3JjLW1vYmlsZVwiKSxcclxuICAgICAgICBzcmMgPSBcIlwiO1xyXG4gICAgICBpZiAod2luZG93LmlubmVyV2lkdGggPiA3MjApIHtcclxuICAgICAgICBzcmMgPSBkZXNrdG9wU3JjO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHNyYyA9IG1vYmlsZVNyYyA/IG1vYmlsZVNyYyA6IGRlc2t0b3BTcmM7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGltYWdlSXRlbS5hdHRyKFwic3JjXCIsIHNyYyk7XHJcbiAgICB9KTtcclxuXHJcbiAgfVxyXG5cclxuICBfY3JlYXRlSW1hZ2UoeyBzcmNEZXNrdG9wLCBzcmNNb2JpbGUsIGFsdCB9KSB7XHJcbiAgICBsZXQgaW1hZ2UgPSAkKCc8aW1nIC8+Jywge1xyXG4gICAgICBhbHQ6IGFsdCxcclxuICAgICAgXCJkYXRhLXNyYy1kZXNrdG9wXCI6IHNyY0Rlc2t0b3AsXHJcbiAgICAgIFwiZGF0YS1zcmMtbW9iaWxlXCI6IHNyY01vYmlsZSxcclxuICAgICAgY2xhc3M6IHNyY01vYmlsZSA/IFwiaGFzTW9iaWxlSW1hZ2VcIiA6IFwiXCJcclxuICAgIH0pO1xyXG4gICAgcmV0dXJuIGltYWdlO1xyXG4gIH1cclxuXHJcbiAgX3dyYXBGb3JTY3JvbGwoKSB7XHJcbiAgICBsZXQgcGFuZWwgPSAkKCc8ZGl2IC8+Jywge1xyXG4gICAgICBjbGFzczogJ3Njcm9sbGluZy1wYW5lbCdcclxuICAgIH0pO1xyXG4gICAgdGhpcy50aHVtYm5haWxMaXN0LnBhcmVudHMoJy50aHVtYm5haWxzLXdyYXBwZXInKS53cmFwSW5uZXIocGFuZWwpO1xyXG4gIH1cclxuXHJcbiAgX2J1aWxkU2xpZGVyKCkge1xyXG4gICAgdGhpcy5nYWxsZXJ5U2xpZGVyXHJcbiAgICAub24oJ2luaXQnLCAoZSwgc2xpY2spID0+IHRoaXMuX29uSW5pdChlLCBzbGljaykpXHJcbiAgICAuc2xpY2soe1xyXG4gICAgICB0b3VjaE1vdmU6IGZhbHNlLFxyXG4gICAgICBpbmZpbml0ZTogZmFsc2UsXHJcbiAgICAgIHRvdWNoVGhyZXNob2xkOiAyMCxcclxuICAgICAgaW5pdGlhbFNsaWRlOiAwLFxyXG4gICAgICBmYWRlOiB0cnVlXHJcbiAgICB9KTtcclxuICAgIHRoaXMuZWwuZmluZCgnLnNsaWNrLWFycm93JykuYWRkQ2xhc3ModGhpcy5vcHRpb25zLmVuYWJsZUFycm93c01vYmlsZUNsYXNzKTtcclxuICB9XHJcblxyXG4gIF9vbkluaXQoZSwgc2xpY2spIHtcclxuICAgIGlmKHRoaXMuZmFkZXJIZWxwZXJFbCkge1xyXG4gICAgICBHTE9CQUxTLlNsaWRlckhlbHBlci5hbGlnbkVsZW1lbnRzSGVpZ2h0KHRoaXMuZ2FsbGVyeVNsaWRlciwgdGhpcy5nYWxsZXJ5U2xpZGVyLmZpbmQoJy5zbGljay1jdXJyZW50IC5pbWFnZS1jb250YWluZXInKSwgdGhpcy5mYWRlckhlbHBlckVsKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIF9zZXRDb3VudGVyKCkge1xyXG4gICAgaWYgKHRoaXMuZWwuaGFzQ2xhc3ModGhpcy5vcHRpb25zLmNhcm91c2VsQ291bnRlckNsYXNzKSkge1xyXG4gICAgICBsZXQgc2xpY2sgPSB0aGlzLmdhbGxlcnlTbGlkZXIuc2xpY2soJ2dldFNsaWNrJyk7XHJcbiAgICAgIHRoaXMub3B0aW9ucy5hbW91bnRTbGlkZXMgPSBzbGljay5zbGlkZUNvdW50O1xyXG4gICAgICB0aGlzLmNvdW50ZXJFbCA9ICQoJzxzcGFuIC8+Jywge2NsYXNzOiAncGFnaW5nSW5mbyd9KTtcclxuICAgICAgdGhpcy5nYWxsZXJ5U2xpZGVyLm9uKCdyZUluaXQgYWZ0ZXJDaGFuZ2UnLCAoZSwgc2xpY2ssIGN1cnJlbnRTbGlkZSkgPT4gdGhpcy5fdXBkYXRlQ291bnRlcihjdXJyZW50U2xpZGUpKVxyXG4gICAgICAgICAgICAgLmFwcGVuZCh0aGlzLmNvdW50ZXJFbCk7XHJcblxyXG4gICAgICB0aGlzLl91cGRhdGVDb3VudGVyKHNsaWNrLmN1cnJlbnRTbGlkZSk7XHJcbiAgICB9O1xyXG4gIH1cclxuXHJcbiAgIF91cGRhdGVDb3VudGVyKGN1cnJlbnRTbGlkZSkge1xyXG4gICAgdGhpcy5jb3VudGVyRWwudGV4dChgJHtjdXJyZW50U2xpZGUgKyAxfS8ke3RoaXMub3B0aW9ucy5hbW91bnRTbGlkZXN9YCk7XHJcbiAgfVxyXG5cclxuICBfc2V0QWN0aXZlKHNsaWRlKSB7XHJcbiAgICB0aGlzLnRodW1ibmFpbExpc3QuZmluZCgnLml0ZW0nKS5yZW1vdmVDbGFzcyhcImFjdGl2ZVwiKTtcclxuICAgIHZhciBhY3RpdmVUYWIgPSAkKHRoaXMudGh1bWJuYWlsTGlzdC5maW5kKCcuaXRlbScpLmVxKHNsaWRlKSkuYWRkQ2xhc3MoJ2FjdGl2ZScpO1xyXG5cclxuICAgIHRoaXMuX3Njcm9sbFRvKGFjdGl2ZVRhYik7XHJcbiAgICBzZXRUaW1lb3V0KCgpID0+IHRoaXMuX3VwZGF0ZVVuZGVybGluZShhY3RpdmVUYWIpLCAxMDApO1xyXG4gIH1cclxuXHJcbiAgX3VwZGF0ZVVuZGVybGluZShhY3RpdmVUYWIpIHtcclxuICAgIHZhciBhY3RpdmUgPSBhY3RpdmVUYWIgPyBhY3RpdmVUYWIgOiB0aGlzLnRodW1ibmFpbExpc3QuZmluZCgnLml0ZW0uYWN0aXZlJyksXHJcbiAgICAgIHBvc0xlZnQgPSBhY3RpdmUucG9zaXRpb24oKS5sZWZ0IC0gKHRoaXMudGh1bWJuYWlsTGlzdC5vdXRlcldpZHRoKCkgLSB0aGlzLnRodW1ibmFpbExpc3Qud2lkdGgoKSkgLyAyO1xyXG4gICAgdGhpcy5zd2l0Y2hlckVsLmNzcyh7d2lkdGg6IGFjdGl2ZS53aWR0aCgpfSkuY3NzKHsnbWFyZ2luLWxlZnQnOiBwb3NMZWZ0fSk7XHJcbiAgfVxyXG5cclxuICBfYWRkU3dpdGNoZXIoKSB7XHJcbiAgICB0aGlzLnN3aXRjaGVyRWwgPSAkKCc8ZGl2Lz4nLCB7Y2xhc3M6ICdzd2l0Y2hlci1pbm5lcid9KTtcclxuICAgIGxldCBzd2l0Y2hlcldyYXBwZXIgPSAkKCc8ZGl2IC8+Jywge2NsYXNzOiAnc3dpdGNoZXItd3JhcHBlcid9KS5hcHBlbmQodGhpcy5zd2l0Y2hlckVsKTtcclxuICAgIHRoaXMudGh1bWJuYWlsTGlzdC5hcHBlbmQoc3dpdGNoZXJXcmFwcGVyKTtcclxuICB9XHJcblxyXG4gIG9uUmVhZHkoKSB7XHJcbiAgICB0aGlzLmdhbGxlcnlTbGlkZXIuc2xpY2soJ3NldFBvc2l0aW9uJyk7XHJcblxyXG4gICAgJCh3aW5kb3cpLm9uKCdyZXNpemUnLCBfLmRlYm91bmNlKCgpID0+IHtcclxuICAgICAgdGhpcy5fY3Jvc3NCcmVha3BvaW50KCk7XHJcbiAgICAgIHRoaXMuZ2FsbGVyeVNsaWRlci5zbGljaygnc2V0UG9zaXRpb24nKTtcclxuICAgICAgdGhpcy5fdXBkYXRlVW5kZXJsaW5lKCk7XHJcbiAgICB9LCAyNTApKTtcclxuICB9XHJcblxyXG59XHJcbiIsImltcG9ydCB7IEJhc2VDb21wb25lbnQgfSBmcm9tICcuLi8uLi9jb21wb25lbnRzL2Jhc2UtY29tcG9uZW50JztcclxuaW1wb3J0IHsgWW91dHViZVBsYXllciB9IGZyb20gJy4uLy4uL2NvbXBvbmVudHMvdmlkZW8veW91dHViZS1wbGF5ZXIveW91dHViZS1wbGF5ZXInO1xyXG5pbXBvcnQgeyBCcmlnaHRjb3ZlUGxheWVyIH0gZnJvbSAnLi4vLi4vY29tcG9uZW50cy92aWRlby9icmlnaHRjb3ZlLXBsYXllci9icmlnaHRjb3ZlLXBsYXllcic7XHJcbmltcG9ydCB7IExiU2xpZGVyIH0gZnJvbSAnLi4vLi4vY29tcG9uZW50cy9zbGlkZXJzL2xiLXNsaWRlcic7XHJcblxyXG5leHBvcnQgY2xhc3MgVmlkZW9HYWxsZXJ5IGV4dGVuZHMgQmFzZUNvbXBvbmVudCB7XHJcblxyXG4gIC8vIFRoaXMgY2xhc3MgaXMgYWRkZWQgYXV0b21hdGljYWxseSB0byB0aGlzLmVsIGNvbXBvbmVudFxyXG4gIGdldCBST09UX0NMQVNTKCkgeyByZXR1cm4gJ3ZpZGVvLWdhbGxlcnknIH1cclxuXHJcbiAgZ2V0IGRlZmF1bHRzKCkge1xyXG4gICAgcmV0dXJuICQuZXh0ZW5kKHt9LCBzdXBlci5kZWZhdWx0cywge1xyXG4gICAgICBlbmFibGVBcnJvd3NNb2JpbGVDbGFzczogJ2VuYWJsZS1hcnJvd3MtbW9iaWxlJyxcclxuICAgICAgcGxheUljb25UcGw6ICgpID0+ICc8YSBjbGFzcz1cInBsYXkganNfb3ZlcmxheV90cmlnZ2VyXCIgaHJlZj1cImphdmFzY3JpcHQ6dm9pZCgwKVwiIHRpdGxlPVwicGxheSB2aWRlb1wiIGRhdGEtbWV0cmljcy1saW5rLXR5cGU9XCJsaW5rXCIgZGF0YS1tZXRyaWNzLXRpdGxlPVwiUGxheSB2aWRlb1wiPjwvYT4nXHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIGNvbnN0cnVjdG9yKC4uLmFyZ3MpIHtcclxuICAgIHN1cGVyKC4uLmFyZ3MpO1xyXG4gICAgdGhpcy5fcG9zdGVycyA9IFtdO1xyXG4gICAgdGhpcy5fcGxheWVycyA9IFtdO1xyXG4gICAgdGhpcy5fdmlkZW9HYWxsZXJ5ID0gbnVsbDtcclxuICAgIHRoaXMuX2luaXQoKTtcclxuICB9XHJcblxyXG4gIF9pbml0KCkge1xyXG4gICAgLy9sYXp5IGluaXQgd2hlbiBpbiBjb250ZW50LW92ZXJsYXlcclxuICAgIGlmICh0aGlzLmVsLmNsb3Nlc3QoJy5jb250ZW50LW92ZXJsYXknKS5sZW5ndGgpIHtcclxuICAgICAgdGhpcy5fbGF6eUluaXQoKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMuX2luc3RhbnRJbml0KCk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBfbGF6eUluaXQoKSB7XHJcbiAgICB0aGlzLmVsLm9uZSgnb3ZlcmxheS5vcGVuZWQnLCAoKSA9PiB0aGlzLl9pbnN0YW50SW5pdCgpKTtcclxuICAgIC8vZ2V0IHJpZCBvZiBleHRyYSBncmlkIHNwYWNpbmdzXHJcbiAgICB0aGlzLmVsLm9uZSgnb3ZlcmxheS5iZWZvcmVvcGVuJywgKCkgPT4gdGhpcy5lbC5jbG9zZXN0KCcuY29udGVudC1vdmVybGF5JykuZmluZCgnPi5zZWN0aW9uJykuYWRkQ2xhc3MoJ2Ryb3AtYWxsLXNwYWNpbmdzJykpO1xyXG4gICAgdGhpcy5lbC5vbignb3ZlcmxheS5jbG9zZWQnLCAoKSA9PiB0aGlzLl9yZWZyZXNoR2FsbGVyeSgpKTtcclxuICB9XHJcblxyXG4gIF9pbnN0YW50SW5pdCgpIHtcclxuICAgIHRoaXMuX2NvbnRlbnRJdGVtcyA9IHRoaXMuZWwuZmluZCgnLmNvbnRlbnQtbGlzdCAuaXRlbScpO1xyXG4gICAgdGhpcy5fcHJlcGFyZUhUTUwoKTtcclxuICAgIHRoaXMuX2luaXRWaWRlb0l0ZW1zKCk7XHJcbiAgICB0aGlzLl9idWlsZFNsaWRlcnMoKTtcclxuICB9XHJcblxyXG4gIF9wcmVwYXJlSFRNTCgpIHtcclxuICAgIHRoaXMuZmFkZXJIZWxwZXJFbCA9ICQoJzxkaXYgLz4nLCB7Y2xhc3M6ICdmYWRlci1oZWxwZXInfSk7XHJcbiAgICB0aGlzLmVsLmZpbmQoJy5jb250ZW50LXdyYXBwZXInKS5wcmVwZW5kKHRoaXMuZmFkZXJIZWxwZXJFbCk7XHJcbiAgfVxyXG5cclxuICBfaW5pdFZpZGVvSXRlbXMoKSB7XHJcbiAgICB0aGlzLl92aWRlb0dhbGxlcnkgPSBuZXcgVmlkZW9JdGVtc0ZhY3RvcnkodGhpcy5fY29udGVudEl0ZW1zKTtcclxuICB9XHJcblxyXG4gIF9idWlsZFNsaWRlcnMoKSB7XHJcbiAgICB0aGlzLl9tYWluU2xpZGVyID0gdGhpcy5lbC5maW5kKCcuY29udGVudC1saXN0Jyk7XHJcbiAgICB0aGlzLl90aHVtYlNsaWRlciA9IHRoaXMuZWwuZmluZCgnLnRodW1ibmFpbHMtbGlzdCcpLmFkZENsYXNzKCdkZWZhdWx0Jyk7XHJcblxyXG4gICAgdGhpcy5fbWFpblNsaWRlclxyXG4gICAgICAub24oJ2luaXQnLCAoZSwgc2xpY2spID0+IHRoaXMuX29uSW5pdChlLCBzbGljaykpXHJcbiAgICAgIC5vbignYmVmb3JlQ2hhbmdlJywgKC4uLmFyZ3MpID0+IHRoaXMuX2JlZm9yZUNoYW5nZSguLi5hcmdzKSlcclxuICAgICAgLm9uKCdhZnRlckNoYW5nZScsIChlLCBzbGljaykgPT4gdGhpcy5fYWZ0ZXJDaGFuZ2UoZSwgc2xpY2spKVxyXG4gICAgICAuc2xpY2soe1xyXG4gICAgICAgIHRvdWNoTW92ZTogZmFsc2UsXHJcbiAgICAgICAgaW5maW5pdGU6IHRydWUsXHJcbiAgICAgICAgdG91Y2hUaHJlc2hvbGQ6IDEwMCxcclxuICAgICAgICBzd2lwZTogZmFsc2UsXHJcbiAgICAgICAgZmFkZTogdHJ1ZVxyXG4gICAgICB9KTtcclxuXHJcbiAgICBsZXQgbWFpblNsaWRlclNsaWNrID0gdGhpcy5fbWFpblNsaWRlci5zbGljaygnZ2V0U2xpY2snKTtcclxuXHJcbiAgICAvL2luaXQgdGh1bWJzIHNsaWRlclxyXG4gICAgdGhpcy5fdGh1bWJzTGJTbGlkZXIgPSBuZXcgTGJTbGlkZXIodGhpcy5fdGh1bWJTbGlkZXIsIHtcclxuICAgICAgdmlzaWJsZVNsaWRlczogMyxcclxuICAgICAgb25TbGlkZTogKCkgPT4ge30sLy9tYWluU2xpZGVyU2xpY2suc2xpY2tHb1RvLmJpbmQobWFpblNsaWRlclNsaWNrKSxcclxuICAgICAgY3VzdG9tQ2xhc3M6ICdzbWFsbC1hcnJvd3MnLFxyXG4gICAgICBhbGlnbkFycm93c1RvRWw6ICdhID4gaW1nJ1xyXG4gICAgfSk7XHJcblxyXG4gIH1cclxuXHJcbiAgX3JlZnJlc2hHYWxsZXJ5KCkge1xyXG4gICAgLy9wYXVzZSBhbGwgdmlkZW9zIGFuZCBzaG93IHBvc3RlcnNcclxuICAgIHRoaXMuX3ZpZGVvR2FsbGVyeS5wYXVzZUFsbCh0cnVlKTtcclxuXHJcbiAgICAvL2FjdGl2YXRlIGZpcnN0IHNsaWRlXHJcbiAgICB0aGlzLl9tYWluU2xpZGVyLnNsaWNrKCdzbGlja0dvVG8nLCAwLCB0cnVlKTtcclxuICB9XHJcblxyXG4gIF9vbkluaXQoZSwgc2xpY2spIHtcclxuICAgIC8vdXBkYXRlIHBvc2l0aW9uICYgc2l6ZSBvbiBpbml0aWFsaXphdGlvblxyXG4gICAgc2V0VGltZW91dCgoKSA9PiBzbGljay5zZXRQb3NpdGlvbigpLCAxMDApO1xyXG5cclxuICAgIC8vcG9zaXRpb24gYXJyb3dzIHJlbGF0aXZlbHkgdG8gaW1hZ2VcclxuICAgIEdMT0JBTFMuU2xpZGVySGVscGVyLmFsaWduQXJyb3dzVG9FbCh0aGlzLl9tYWluU2xpZGVyLCB7YWxpZ25Ub0VsU2VsZWN0b3I6ICcudmlkZW8tY29udGFpbmVyJ30pO1xyXG5cclxuICAgIC8vYWxpZ24gYmxhY2sgaGVscGVyIGVsZW1lbnQncyBoZWlnaHQgd2l0aCBhIGhlaWdodCBvZiB2aWRlb3NcclxuICAgIEdMT0JBTFMuU2xpZGVySGVscGVyLmFsaWduRWxlbWVudHNIZWlnaHQodGhpcy5fbWFpblNsaWRlciwgdGhpcy5fbWFpblNsaWRlci5maW5kKCcuc2xpY2stY3VycmVudCAudmlkZW8tY29udGFpbmVyJyksIHRoaXMuZmFkZXJIZWxwZXJFbCk7XHJcblxyXG4gICAgLy9zaG93IGFycm93cyBmb3IgbW9iaWxlc1xyXG4gICAgdGhpcy5fbWFpblNsaWRlci5maW5kKCcuc2xpY2stYXJyb3cnKS5hZGRDbGFzcyh0aGlzLm9wdGlvbnMuZW5hYmxlQXJyb3dzTW9iaWxlQ2xhc3MgKyAnIGFuaW1hdGUtdG9wJyk7XHJcblxyXG4gICAgLy9zZXQgY291bnRlclxyXG4gICAgdGhpcy5fbWFpblNsaWRlci5hcHBlbmQoYDxzcGFuIGNsYXNzPVwicGFnaW5nSW5mb1wiIGNsYXNzPVwicGFnaW5nSW5mb1wiPjEvJHtzbGljay5zbGlkZUNvdW50fTwvc3Bhbj5gKTtcclxuICAgIHRoaXMuX2NvdW50ZXIgPSB0aGlzLl9tYWluU2xpZGVyLmZpbmQoJy5wYWdpbmdJbmZvJyk7XHJcblxyXG4gICAgLy9jb3VudGVyIHBsYWNlbWVudCBpcyB1cGRhdGVkIGFmdGVyIGVhY2ggc2V0UG9zaXRpb24gY2FsbFxyXG4gICAgdGhpcy5fbWFpblNsaWRlci5vbignc2V0UG9zaXRpb24nLCAobG9sbykgPT4gdGhpcy5fdXBkYXRlQ291bnRlclBsYWNlbWVudCgpKTtcclxuICB9XHJcblxyXG4gIC8vVE9ETyBoYW5kbGUgYXJyb3dzIHByb3Blcmx5IGZvciBhIG5ldyBzbGlkZXJcclxuICBfb25Jbml0VGh1bWJzU2xpZGVyKGUsIHNsaWNrKSB7XHJcbiAgICBHTE9CQUxTLlNsaWRlckhlbHBlci5hbGlnbkFycm93c1RvRWwodGhpcy5fdGh1bWJTbGlkZXIsIHthbGlnblRvRWxTZWxlY3RvcjogJ2EgPiBpbWcnfSk7XHJcbiAgICB0aGlzLl90aHVtYlNsaWRlci5maW5kKCcuc2xpY2stYXJyb3cnKS5hZGRDbGFzcyh0aGlzLm9wdGlvbnMuZW5hYmxlQXJyb3dzTW9iaWxlQ2xhc3MgKyAnIGFuaW1hdGUtdG9wJyk7XHJcbiAgfVxyXG5cclxuICBfYmVmb3JlQ2hhbmdlKGUsIHNsaWNrLCBwcmV2SW5kZXgsIG5leHRJbmRleCkge1xyXG4gICAgdGhpcy5fdmlkZW9HYWxsZXJ5LnBhdXNlQWxsKCk7XHJcbiAgICB0aGlzLl90aHVtYnNMYlNsaWRlci5zbGlkZVRvKG5leHRJbmRleCk7XHJcbiAgICB0aGlzLl91cGRhdGVDb3VudGVyKHNsaWNrLnNsaWRlQ291bnQsIG5leHRJbmRleCk7XHJcbiAgfVxyXG5cclxuICBfYWZ0ZXJDaGFuZ2UoZSwgc2xpY2spIHtcclxuICAgIHRoaXMuX3ZpZGVvR2FsbGVyeS5zaG93UG9zdGVycyhzbGljay5jdXJyZW50U2xpZGUpO1xyXG4gIH1cclxuXHJcbiAgX3VwZGF0ZUNvdW50ZXIoc2xpZGVDb3VudCwgY3VycmVudFNsaWRlKSB7XHJcbiAgICB0aGlzLl9jb3VudGVyLnRleHQoYCR7Y3VycmVudFNsaWRlICsgMX0vJHtzbGlkZUNvdW50fWApO1xyXG4gIH1cclxuXHJcbiAgX3VwZGF0ZUNvdW50ZXJQbGFjZW1lbnQoKSB7XHJcbiAgICBsZXQgbmV3VG9wID0gdGhpcy5fbWFpblNsaWRlci5maW5kKCcuc2xpY2stY3VycmVudCAudmlkZW8tY29udGFpbmVyJykuZmlyc3QoKS5oZWlnaHQoKTtcclxuXHJcbiAgICBuZXdUb3AgJiYgdGhpcy5fY291bnRlci5jc3MoJ3RvcCcsIG5ld1RvcCk7XHJcbiAgfVxyXG5cclxufVxyXG5cclxuY2xhc3MgVmlkZW9JdGVtc0ZhY3Rvcnkge1xyXG4gIGNvbnN0cnVjdG9yKGl0ZW1zKSB7XHJcbiAgICB0aGlzLl9pbml0KGl0ZW1zKTtcclxuICB9XHJcblxyXG4gIF9pbml0KGl0ZW1zKSB7XHJcbiAgICB0aGlzLl9pdGVtcyA9IGl0ZW1zO1xyXG4gICAgdGhpcy5fdmlkZW9zID0gW107XHJcbiAgICB0aGlzLl9sYXp5SW5pdCgwKTtcclxuICB9XHJcblxyXG4gIF9sYXp5SW5pdChpbmRleCkge1xyXG4gICAgbGV0IGVsID0gdGhpcy5faXRlbXMuZXEoaW5kZXgpO1xyXG4gICAgaWYgKGVsLmxlbmd0aCkge1xyXG4gICAgICB0aGlzLl92aWRlb3MucHVzaChuZXcgVmlkZW9JdGVtKGVsKSk7XHJcbiAgICAgIHNldFRpbWVvdXQoKCkgPT4gdGhpcy5fbGF6eUluaXQoKytpbmRleCksIDEwMCk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBwYXVzZUFsbChzaG93UG9zdGVyKSB7XHJcbiAgICB0aGlzLl92aWRlb3MuZm9yRWFjaCgodmlkZW8pID0+IHZpZGVvLnBhdXNlVmlkZW8oc2hvd1Bvc3RlcikpO1xyXG4gIH1cclxuXHJcbiAgc2hvd1Bvc3RlcnMoZXhjbHVkZUluZGV4KSB7XHJcbiAgICB0aGlzLl92aWRlb3MuZm9yRWFjaCgodmlkZW8sIGkpID0+IChpICE9PSBleGNsdWRlSW5kZXgpICYmIHZpZGVvLnNob3dQb3N0ZXIoKSk7XHJcbiAgfVxyXG5cclxufVxyXG5cclxuY2xhc3MgVmlkZW9JdGVtIHtcclxuICBnZXQgb3B0aW9ucygpIHtcclxuICAgIHJldHVybiB7XHJcbiAgICAgIHlvdXR1YmVWaWRlb1NlbGVjdG9yOiAnLnlvdXR1YmUtdmlkZW8tdGVtcGxhdGUnLFxyXG4gICAgICBicmlnaHRjb3ZlVmlkZW9TZWxlY3RvcjogJy5iYy12aWRlby10ZW1wbGF0ZScsXHJcbiAgICAgIG1vYmlsZVF1ZXJ5OiB3aW5kb3cubWF0Y2hNZWRpYSgnc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA5NjBweCknKSxcclxuICAgICAgcGxheUljb25UcGw6ICgpID0+ICc8YSBjbGFzcz1cInBsYXkganNfb3ZlcmxheV90cmlnZ2VyXCIgaHJlZj1cImphdmFzY3JpcHQ6dm9pZCgwKVwiIHRpdGxlPVwicGxheSB2aWRlb1wiIGRhdGEtbWV0cmljcy1saW5rLXR5cGU9XCJsaW5rXCIgZGF0YS1tZXRyaWNzLXRpdGxlPVwiUGxheSB2aWRlb1wiPjwvYT4nXHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBjb25zdHJ1Y3RvcihlbCkge1xyXG4gICAgdGhpcy5faW5pdChlbCk7XHJcbiAgfVxyXG5cclxuICBfaW5pdChlbCkge1xyXG4gICAgdGhpcy5lbCA9IGVsO1xyXG4gICAgdGhpcy5fcG9zdGVyID0gbnVsbDtcclxuICAgIHRoaXMuX3BsYXllciA9IG51bGw7XHJcbiAgICB0aGlzLl9pbml0UG9zdGVyKCk7XHJcbiAgICB0aGlzLl9pbml0UGxheWVyKCk7XHJcbiAgICB0aGlzLl9pbml0TGlzdGVuZXJzKCk7XHJcbiAgfVxyXG5cclxuICBfaW5pdFBvc3RlcigpIHtcclxuICAgIGxldCBkZXNrdG9wQmcgPSB0aGlzLmVsLmZpbmQoJy5kZXNrdG9wJykuYXR0cignZGF0YS1zcmMnKSxcclxuICAgICAgICBtb2JpbGVCZyA9IHRoaXMuZWwuZmluZCgnLm1vYmlsZScpLmF0dHIoJ2RhdGEtc3JjJykgfHwgZGVza3RvcEJnLFxyXG4gICAgICAgIHBvc3RlckVsID0gJCgnPGRpdiAvPicsIHtcclxuICAgICAgICAgICdjbGFzcyc6ICdpbWcnLFxyXG4gICAgICAgICAgJ3N0eWxlJzogdGhpcy5fZ2V0QmdTdHlsZShkZXNrdG9wQmcsIG1vYmlsZUJnKVxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgIHRoaXMuX3Bvc3RlciA9IHRoaXMuZWwuZmluZCgnLmltYWdlLWNvbnRhaW5lcicpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgLmFwcGVuZChwb3N0ZXJFbClcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAuYXBwZW5kKHRoaXMub3B0aW9ucy5wbGF5SWNvblRwbCgpKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIC5kYXRhKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJnczogW2Rlc2t0b3BCZywgbW9iaWxlQmddXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgfVxyXG5cclxuICBfdXBkYXRlUG9zdGVySW1nKCkge1xyXG4gICAgdGhpcy5fcG9zdGVyLmZpbmQoJy5pbWcnKS5hdHRyKCdzdHlsZScsIHRoaXMuX2dldEJnU3R5bGUoLi4udGhpcy5fcG9zdGVyLmRhdGEoKS5iZ3MpKTtcclxuICB9XHJcblxyXG4gIF9pbml0UGxheWVyKGVsKSB7XHJcbiAgICB0aGlzLl9wbGF5ZXIgPSB0aGlzLl9jcmVhdGVWaWRlb1BsYXllcigpO1xyXG4gICAgdGhpcy5fcGxheWVyLmluaXQoKTtcclxuICB9XHJcblxyXG4gIF9pbml0TGlzdGVuZXJzKCkge1xyXG4gICAgdGhpcy5lbC5maW5kKCcucGxheScpLm9uKCdjbGljaycsICgpID0+IHRoaXMucGxheVZpZGVvKCkpO1xyXG4gICAgdGhpcy5vcHRpb25zLm1vYmlsZVF1ZXJ5LmFkZExpc3RlbmVyKCgpID0+IHRoaXMuX3VwZGF0ZVBvc3RlckltZygpKTtcclxuICB9XHJcblxyXG4gIF9jcmVhdGVWaWRlb1BsYXllcigpIHtcclxuICAgIGxldCB5b3V0dWJlVmlkZW9FbGVtZW50ID0gdGhpcy5lbC5maW5kKHRoaXMub3B0aW9ucy55b3V0dWJlVmlkZW9TZWxlY3RvciksXHJcbiAgICAgICAgYnJpZ2h0Y292ZVZpZGVvRWxlbWVudCA9IHRoaXMuZWwuZmluZCh0aGlzLm9wdGlvbnMuYnJpZ2h0Y292ZVZpZGVvU2VsZWN0b3IpO1xyXG5cclxuICAgIGlmICh5b3V0dWJlVmlkZW9FbGVtZW50Lmxlbmd0aCkge1xyXG4gICAgICByZXR1cm4gbmV3IFlvdXR1YmVQbGF5ZXIoeW91dHViZVZpZGVvRWxlbWVudCk7XHJcbiAgICB9IGVsc2UgaWYgKGJyaWdodGNvdmVWaWRlb0VsZW1lbnQubGVuZ3RoKSB7XHJcbiAgICAgIHJldHVybiBuZXcgQnJpZ2h0Y292ZVBsYXllcihicmlnaHRjb3ZlVmlkZW9FbGVtZW50KTtcclxuICAgIH1cclxuICAgIHJldHVybiB7fTtcclxuICB9XHJcblxyXG4gIF9nZXRCZ1N0eWxlKGltZ1VybERlc2t0b3AsIGltZ1VybE1vYmlsZSkge1xyXG4gICAgbGV0IGltZ1VybCA9IHRoaXMub3B0aW9ucy5tb2JpbGVRdWVyeS5tYXRjaGVzID8gaW1nVXJsTW9iaWxlIDogaW1nVXJsRGVza3RvcFxyXG4gICAgcmV0dXJuIGBiYWNrZ3JvdW5kOiB1cmwoJHtpbWdVcmx9KSA1MCUgNTAlIC8gY292ZXIgbm8tcmVwZWF0IHNjcm9sbDtgO1xyXG4gIH1cclxuXHJcbiAgX2hpZGVQb3N0ZXIoKSB7XHJcbiAgICB0aGlzLl9wb3N0ZXIuZmFkZU91dCg4MDApO1xyXG4gIH1cclxuXHJcbiAgc2hvd1Bvc3RlcigpIHtcclxuICAgIHRoaXMuX3Bvc3Rlci5mYWRlSW4oNDAwKTtcclxuICB9XHJcblxyXG4gIHBsYXlWaWRlbygpIHtcclxuICAgIHRoaXMuX3BsYXllci5wbGF5KCk7XHJcbiAgICB0aGlzLl9oaWRlUG9zdGVyKCk7XHJcbiAgfVxyXG5cclxuICAvKlxyXG4gICAqIEBwYXJhbSB7Ym9vbGVhbn0gc2hvd1Bvc3RlciAtIGluZGljYXRlcyB3aGV0aGVyIHRvIHNob3cgcG9zdGVyIGFmdGVyIHZpZGVvIGhhcyBiZWVuIHBhdXNlZFxyXG4gICAqICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFuZCByZXdpbmRzIHZpZGVvIHRvIHRoZSBiZWdpbm5pbmdcclxuICAgKi9cclxuICBwYXVzZVZpZGVvKHNob3dQb3N0ZXIpIHtcclxuICAgIGlmICh0aGlzLl9wbGF5ZXIpIHtcclxuICAgICAgaWYgKCFzaG93UG9zdGVyKSB7XHJcbiAgICAgICAgdGhpcy5fcGxheWVyLnBhdXNlKCk7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgdGhpcy5fcGxheWVyLnN0b3AoKTtcclxuICAgICAgICB0aGlzLnNob3dQb3N0ZXIoKTtcclxuICAgICAgfVxyXG4gICAgfVxyXG5cclxuICB9XHJcblxyXG59IiwiaW1wb3J0IHsgQmFzZUNvbXBvbmVudCB9IGZyb20gJy4uLy4uL2NvbXBvbmVudHMvYmFzZS1jb21wb25lbnQuanMnO1xyXG5pbXBvcnQgeyBWaWRlb0dhbGxlcnkgfSBmcm9tICcuL21vbGVjdWxlLWxiLTgyNy12aWRlby1nYWxsZXJ5LmpzJztcclxuaW1wb3J0IHsgSW1hZ2VHYWxsZXJ5IH0gZnJvbSAnLi9tb2xlY3VsZS1sYi04MjctaW1hZ2UtZ2FsbGVyeS5qcyc7XHJcblxyXG5cclxuZXhwb3J0IGNsYXNzIE1vbGVjdWxlODI3RmFjdG9yeSBleHRlbmRzIEJhc2VDb21wb25lbnQge1xyXG5cclxuICBzdGF0aWMgZ2V0IHNlbGVjdG9yICgpIHsgcmV0dXJuICcubW9sZWN1bGUtbGItODI3JzsgfVxyXG5cclxuICBnZXQgZGVmYXVsdHMoKSB7XHJcbiAgICByZXR1cm4gJC5leHRlbmQoe30sIHN1cGVyLmRlZmF1bHRzLCB7XHJcbiAgICAgIHZpZGVvR2FsbGVyeUluZGljYXRvcjogJy52aWRlby1jb250YWluZXInXHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIGNvbnN0cnVjdG9yKC4uLmFyZ3MpIHtcclxuICAgIHN1cGVyKC4uLmFyZ3MpO1xyXG4gICAgdGhpcy5faW5pdCgpO1xyXG4gIH1cclxuXHJcbiAgX2luaXQoKSB7XHJcbiAgICBpZiAodGhpcy5lbC5maW5kKHRoaXMub3B0aW9ucy52aWRlb0dhbGxlcnlJbmRpY2F0b3IpLmxlbmd0aCkge1xyXG4gICAgICBuZXcgVmlkZW9HYWxsZXJ5KHRoaXMuZWwpO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgbmV3IEltYWdlR2FsbGVyeSh0aGlzLmVsKTtcclxuICAgIH1cclxuICB9XHJcbn0iLCJpbXBvcnQgeyBCYXNlQ29tcG9uZW50IH0gZnJvbSAnLi4vLi4vY29tcG9uZW50cy9iYXNlLWNvbXBvbmVudC5qcyc7XHJcbmltcG9ydCB7IFN0aWNrYWJsZSB9IGZyb20gJy4uLy4uL2NvbW1vbi9zY3JpcHRzL3N0aWNrYWJsZS5qcyc7XHJcblxyXG5cclxuZXhwb3J0IGNsYXNzIE1vbGVjdWxlODMwbm9uciBleHRlbmRzIEJhc2VDb21wb25lbnQge1xyXG5cclxuICBzdGF0aWMgZ2V0IHNlbGVjdG9yKCkgeyByZXR1cm4gJy5tb2xlY3VsZS1sYi04MzAnOyB9XHJcblxyXG4gIGdldCBkZWZhdWx0cygpIHtcclxuICAgIHJldHVybiAkLmV4dGVuZCh7fSwgc3VwZXIuZGVmYXVsdHMsIHtcclxuICAgICAgZGlzYWJsZVN0aWNrYWJsZUNsYXNzbmFtZTogJ25vbi1zdGlja2FibGUnLFxyXG4gICAgICBzd2FwQXRCcmVha3BvaW50OiAnNzIwJyxcclxuICAgICAgbW9iaWxlUXVlcnk6IHdpbmRvdy5tYXRjaE1lZGlhKCdzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDcyMHB4KScpXHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIGNvbnN0cnVjdG9yKC4uLmFyZ3MpIHtcclxuICAgIHN1cGVyKC4uLmFyZ3MpO1xyXG4gICAgdGhpcy5faW5pdCgpO1xyXG4gIH1cclxuXHJcbiAgX2luaXQoKSB7XHJcbiAgICB0aGlzLm5hdiA9IHRoaXMuZWwuZmluZCgnbmF2Jyk7XHJcblxyXG4gICAgaWYgKHRoaXMuX2hhc1N1Ym1lbnUoKSkge1xyXG4gICAgICB0aGlzLl9wcmVwYXJlQ29udGVudCgpO1xyXG4gICAgICB0aGlzLmRyb3BUcmlnZ2VyID0gdGhpcy5lbC5maW5kKCcudGl0bGUgYScpO1xyXG4gICAgICB0aGlzLl9pbml0TGlzdGVuZXJzKCk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLmVsLmFkZENsYXNzKCduby1zdWJtZW51Jyk7XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKCF0aGlzLmVsLmhhc0NsYXNzKHRoaXMub3B0aW9ucy5kaXNhYmxlU3RpY2thYmxlQ2xhc3NuYW1lKSkge1xyXG4gICAgICB0aGlzLl9pbml0U3RpY2thYmxlQmVoYXZpb3VyKCk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBfaGFzU3VibWVudSgpIHtcclxuICAgIHJldHVybiAhIXRoaXMuZWwuZmluZCgndWwubGV2ZWwyJykubGVuZ3RoO1xyXG4gIH1cclxuXHJcbiAgX3ByZXBhcmVDb250ZW50KCkge1xyXG4gICAgbGV0IGNvcHlDb250ZW50ID0gJyc7XHJcbiAgICBsZXQgd3JhcE5hdiA9ICQoJzxkaXYgLz4nLCB7IGNsYXNzOiAnbmF2LXdyYXBwZXInIH0pO1xyXG5cclxuICAgIHRoaXMubGV2ZWwzQ29udGFpbmVyID0gJCgnPGRpdiAvPicsIHsgY2xhc3M6ICdsZXZlbDMtY29udGFpbmVyJyB9KTtcclxuICAgIHRoaXMuZWwuZmluZCgnLmxldmVsMiA+IC5zZWxlY3RhYmxlID4gLmV4cGFuZC1hcmVhJykuZWFjaCgoaSwgaXRlbSkgPT4ge1xyXG4gICAgICBsZXQgZWwgPSAkKGl0ZW0pLFxyXG4gICAgICAgIGlkID0gZWwuYXR0cignaWQnKTtcclxuICAgICAgY29weUNvbnRlbnQgKz0gJzxkaXYgaWQ9XCInICsgaWQgKyAnLWRlc2t0b3BcIiBjbGFzcz1cImV4cGFuZC1hcmVhXCI+JyArIGVsLmh0bWwoKSArICc8L2Rpdj4nO1xyXG4gICAgfSk7XHJcbiAgICB0aGlzLmxldmVsM0NvbnRhaW5lci5odG1sKGNvcHlDb250ZW50KTtcclxuICAgIHdyYXBOYXYuYXBwZW5kKHRoaXMuZWwuZmluZCgnLmxldmVsMicpKTtcclxuICAgIHdyYXBOYXYuZmluZCgnLmxldmVsMicpLndyYXAoJzxkaXYgY2xhc3M9XCJsZXZlbDEtY29udGFpbmVyXCIvPicpO1xyXG4gICAgd3JhcE5hdi5hcHBlbmQodGhpcy5sZXZlbDNDb250YWluZXIpO1xyXG4gICAgdGhpcy5uYXYuYXBwZW5kKHdyYXBOYXYpO1xyXG5cclxuICAgIHRoaXMuZHJvcCA9IHdyYXBOYXY7XHJcbiAgICB0aGlzLmFsbERlc2t0b3BEcm9wcyA9IHdyYXBOYXYuZmluZCgnLmxldmVsMy1jb250YWluZXIgLmV4cGFuZC1hcmVhJyk7XHJcbiAgICB0aGlzLm1vYmlsZURyb3BzID0gd3JhcE5hdi5maW5kKCcuc2VsZWN0YWJsZSAuZXhwYW5kLWFyZWEnKTtcclxuICAgIHRoaXMuYWxsVHJpZ2dlcnMgPSB3cmFwTmF2LmZpbmQoJy5sZXZlbDIgPmxpLnNlbGVjdGFibGUgPiBhJyk7XHJcbiAgICB0aGlzLmxldmVsMkNvbnRhaW5lciA9IHRoaXMuZWwuZmluZCgnLmxldmVsMicpO1xyXG4gIH1cclxuXHJcbiAgX2luaXRMaXN0ZW5lcnMoKSB7XHJcbiAgICB0aGlzLmVsLmZpbmQoJy5sZXZlbDIgPiAuc2VsZWN0YWJsZSBhJykub24oJ2NsaWNrJywgKGUpID0+IHtcclxuICAgICAgbGV0IGVsID0gJChlLmN1cnJlbnRUYXJnZXQpLFxyXG4gICAgICAgIHByZWZmaXggPSB0aGlzLl9nZXRQcmVmZml4KCksXHJcbiAgICAgICAgcmVsSWQgPSAkKGUuY3VycmVudFRhcmdldCkuYXR0cigncmVsJykgKyBwcmVmZml4LFxyXG4gICAgICAgIGRyb3AgPSAkKCcjJyArIHJlbElkKTtcclxuXHJcbiAgICAgIGlmIChlbC5oYXNDbGFzcygnYWN0aXZlJykpIHtcclxuICAgICAgICB0aGlzLmFsbERlc2t0b3BEcm9wcy5yZW1vdmVDbGFzcygnYWN0aXZlJyk7XHJcbiAgICAgICAgdGhpcy5tb2JpbGVEcm9wcy5zbGlkZVVwKCk7XHJcbiAgICAgICAgdGhpcy5hbGxUcmlnZ2Vycy5yZW1vdmVDbGFzcygnYWN0aXZlJyk7XHJcbiAgICAgICAgdGhpcy5sZXZlbDJDb250YWluZXIucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIC8vcHJlZmZpeCB0cnV0aHkgb25seSBmb3IgZGVza3RvcHNcclxuICAgICAgICBpZiAocHJlZmZpeCkge1xyXG4gICAgICAgICAgdGhpcy5hbGxEZXNrdG9wRHJvcHMucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpO1xyXG4gICAgICAgICAgdGhpcy5sZXZlbDJDb250YWluZXIuYWRkQ2xhc3MoJ2FjdGl2ZScpO1xyXG4gICAgICAgICAgZHJvcC5hZGRDbGFzcygnYWN0aXZlJyk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIHRoaXMubW9iaWxlRHJvcHMuc2xpZGVVcCgpO1xyXG4gICAgICAgICAgZHJvcC5zbGlkZURvd24oKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5hbGxUcmlnZ2Vycy5yZW1vdmVDbGFzcygnYWN0aXZlJyk7XHJcbiAgICAgICAgZWwuYWRkQ2xhc3MoJ2FjdGl2ZScpO1xyXG4gICAgICB9XHJcblxyXG4gICAgfSk7XHJcbiAgICB0aGlzLmRyb3BUcmlnZ2VyLm9uKCdjbGljaycsIChlKSA9PiB7XHJcbiAgICAgIGxldCBlbCA9ICQoZS5jdXJyZW50VGFyZ2V0KTtcclxuICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICBpZiAoZWwuaGFzQ2xhc3MoJ2FjdGl2ZScpKSB7XHJcbiAgICAgICAgdGhpcy5fY2xvc2VBbGwodHJ1ZSk7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgZWwuYWRkQ2xhc3MoJ2FjdGl2ZScpO1xyXG4gICAgICAgIHRoaXMuZHJvcC5zbGlkZURvd24oKCkgPT4ge1xyXG4gICAgICAgICAgdGhpcy5kcm9wLmNzcygnZGlzcGxheScsICdmbGV4Jyk7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG5cclxuICAgICQoJ2JvZHknKS5vbignY2xpY2snLCAoZSkgPT4ge1xyXG4gICAgICB0aGlzLl9jbGlja091dHNpZGVDYihlKTtcclxuICAgIH0pO1xyXG4gICAgdGhpcy5vcHRpb25zLm1vYmlsZVF1ZXJ5LmFkZExpc3RlbmVyKChlKSA9PiB0aGlzLl9jbG9zZUFsbChlKSk7XHJcbiAgfVxyXG5cclxuICBfZ2V0UHJlZmZpeCgpIHtcclxuICAgIGxldCBwcmVmZml4ID0gd2luZG93Lm1hdGNoTWVkaWEoJ3NjcmVlbiBhbmQgKG1pbi13aWR0aDogJyArIHRoaXMub3B0aW9ucy5zd2FwQXRCcmVha3BvaW50ICsgJ3B4KScpLm1hdGNoZXMgPyAnLWRlc2t0b3AnIDogJyc7XHJcbiAgICByZXR1cm4gcHJlZmZpeDtcclxuICB9XHJcblxyXG4gIF9pbml0U3RpY2thYmxlQmVoYXZpb3VyKCkge1xyXG4gICAgdGhpcy5zdGlja2FibGVFbCA9IHRoaXMuZWwuZmluZCgnbmF2Jyk7XHJcbiAgICBuZXcgU3RpY2thYmxlKHRoaXMuc3RpY2thYmxlRWwsIHRoaXMuc3RpY2thYmxlRWwucGFyZW50KCkpO1xyXG4gIH1cclxuXHJcbiAgX2Nsb3NlQWxsKCkge1xyXG4gICAgdGhpcy5tb2JpbGVEcm9wcy5zbGlkZVVwKCk7XHJcbiAgICB0aGlzLmRyb3Auc2xpZGVVcCgoKSA9PiB7XHJcbiAgICAgIHRoaXMuYWxsVHJpZ2dlcnMucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpO1xyXG4gICAgICB0aGlzLmFsbERlc2t0b3BEcm9wcy5yZW1vdmVDbGFzcygnYWN0aXZlJyk7XHJcbiAgICAgIHRoaXMubGV2ZWwyQ29udGFpbmVyLnJlbW92ZUNsYXNzKCdhY3RpdmUnKTtcclxuICAgIH0pO1xyXG5cclxuICAgIGlmIChhcmd1bWVudHMubGVuZ3RoKSB7XHJcbiAgICAgIHRoaXMuZHJvcFRyaWdnZXIucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgX2NsaWNrT3V0c2lkZUNiKGUpIHtcclxuICAgIGlmICghJChlLnRhcmdldCkuY2xvc2VzdCgnLm1vbGVjdWxlLWxiLTgzMCcpLmxlbmd0aCkge1xyXG4gICAgICB0aGlzLl9jbG9zZUFsbCh0cnVlKTtcclxuICAgIH1cclxuICB9XHJcblxyXG59XHJcbiIsImltcG9ydCB7IEJhc2VDb21wb25lbnQgfSBmcm9tICcuLi8uLi9jb21wb25lbnRzL2Jhc2UtY29tcG9uZW50LmpzJztcclxuaW1wb3J0IHsgU3RpY2thYmxlIH0gZnJvbSAnLi4vLi4vY29tbW9uL3NjcmlwdHMvc3RpY2thYmxlLmpzJztcclxuXHJcblxyXG5leHBvcnQgY2xhc3MgTW9sZWN1bGU4MzUgZXh0ZW5kcyBCYXNlQ29tcG9uZW50IHtcclxuXHJcbiAgc3RhdGljIGdldCBzZWxlY3RvciAoKSB7IHJldHVybiAnLm1vbGVjdWxlLWxiLTgzNSc7IH1cclxuXHJcbiAgZ2V0IGRlZmF1bHRzKCkge1xyXG4gICAgcmV0dXJuICQuZXh0ZW5kKHt9LCBzdXBlci5kZWZhdWx0cywge1xyXG4gICAgICByb3dTZWxlY3RvcjogJy5zZWN0aW9uJyxcclxuICAgICAgYWx0ZXJuYXRpdmVUaGVtZUNsYXNzOiAnYXBwZWFyYW5jZS10aGVtZS0yJyxcclxuICAgICAgYWx0ZXJuYXRpdmVUaGVtZVJvd0NsYXNzOiAnbWFpbi1uYXYtODM1LXRoZW1lLTInLFxyXG4gICAgICByb3dDbGFzczogJ21haW4tbmF2LTgzNScsXHJcbiAgICAgIG1vYmlsZVRwbDogXHJcbiAgICAgICAgJzxkaXYgY2xhc3M9XCJtb2JpbGUtbmF2XCI+JytcclxuICAgICAgICAgICc8ZGl2IGNsYXNzPVwiYW5jaG9yc1wiPicrXHJcbiAgICAgICAgICAgICc8JT0gZHJvcFRyaWdnZXIgJT4nK1xyXG4gICAgICAgICAgICAnPGRpdiBjbGFzcz1cImRyb3BcIj48dWw+PCU9IGl0ZW1zICU+PC91bD48L2Rpdj4nK1xyXG4gICAgICAgICAgJzwvZGl2PicrXHJcbiAgICAgICAgICAnPCUgaWYgKGJ0bikgeyAlPicrXHJcbiAgICAgICAgICAgICc8ZGl2IGNsYXNzPVwiYnRuLXdyYXBwZXIgc21hbGwtYnRuXCI+JytcclxuICAgICAgICAgICAgICAnPCU9IGJ0biAlPicrXHJcbiAgICAgICAgICAgICc8L2Rpdj4nK1xyXG4gICAgICAgICAgJzwlIH0gJT4nK1xyXG4gICAgICAgICc8L2Rpdj4nXHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIGNvbnN0cnVjdG9yKC4uLmFyZ3MpIHtcclxuICAgIHN1cGVyKC4uLmFyZ3MpO1xyXG4gICAgdGhpcy5faW5pdCgpO1xyXG4gIH1cclxuXHJcbiAgX2luaXQoKSB7XHJcbiAgICBpZiAodGhpcy5lbC5oYXNDbGFzcyh0aGlzLm9wdGlvbnMuYWx0ZXJuYXRpdmVUaGVtZUNsYXNzKSkge1xyXG4gICAgICB0aGlzLnBhcmVudFNlY3Rpb24gPSB0aGlzLmVsLmNsb3Nlc3QodGhpcy5vcHRpb25zLnJvd1NlbGVjdG9yKS5hZGRDbGFzcyh0aGlzLm9wdGlvbnMuYWx0ZXJuYXRpdmVUaGVtZVJvd0NsYXNzKTtcclxuICAgICAgdGhpcy5fYWN0aXZhdGVMaW5rKCk7XHJcbiAgICAgICQodGhpcy5lbCkud3JhcElubmVyKFwiPGRpdiBjbGFzcz0naW5uZXItd3JhcHBlcic+PC9kaXY+XCIpXHJcbiAgICAgIGlmICghdGhpcy5lbC5oYXNDbGFzcygnbm9uLXN0aWNrYWJsZScpKSB7XHJcbiAgICAgICAgdGhpcy5faW5pdFN0aWNrYWJsZUJlaGF2aW91cigpO1xyXG4gICAgICAgIHRoaXMuX2luaXRIZWlnaHRVcGRhdGUoKTtcclxuICAgICAgfVxyXG4gICAgICByZXR1cm47XHJcbiAgICB9XHJcbiAgICBsZXQgbGVuZ3RoID0gdGhpcy5lbC5maW5kKCcubGlua3MgPiBsaScpLmxlbmd0aDtcclxuICAgIHRoaXMuZWwuYWRkQ2xhc3MoJ3dpZHRoLScgKyBsZW5ndGgpO1xyXG4gICAgdGhpcy5lbC5jbG9zZXN0KHRoaXMub3B0aW9ucy5yb3dTZWxlY3RvcikuYWRkQ2xhc3ModGhpcy5vcHRpb25zLnJvd0NsYXNzKTtcclxuICAgIHRoaXMuX21hcmtFeHRlcm5hbExpbmtzKCk7XHJcbiAgICB0aGlzLl9wcmVwYXJlQnV5QnV0dG9uKCk7XHJcbiAgICB0aGlzLl9wcmVwYXJlTW9iaWxlKCk7XHJcbiAgICB0aGlzLl9pbml0TGlzdGVuZXJzKCk7XHJcbiAgICBpZiAoIXRoaXMuZWwuaGFzQ2xhc3MoJ25vbi1zdGlja2FibGUnKSkge1xyXG4gICAgICB0aGlzLl9pbml0U3RpY2thYmxlQmVoYXZpb3VyKCk7XHJcbiAgICB9XHJcbiAgICB0aGlzLmFjdGl2ZU5hdkluZGV4ID0gMDtcclxuICAgIHRoaXMuX2NsaWNrT3V0c2lkZUNiID0gdGhpcy5fY2xpY2tPdXRzaWRlQ2IuYmluZCh0aGlzKTtcclxuICB9XHJcblxyXG4gIF9hY3RpdmF0ZUxpbmsoKSB7XHJcbiAgICB2YXIgY3VycmVudExvY2F0aW9uID0gd2luZG93LmxvY2F0aW9uLmhyZWY7XHJcbiAgICB0aGlzLmVsLmZpbmQoJy5saW5rcyA+IGxpID4gYScpLmVhY2goKGluZGV4LCBlbCkgPT4ge1xyXG4gICAgICBpZiAoJChlbCkuYXR0cignaHJlZicpID09PSBjdXJyZW50TG9jYXRpb24pIHtcclxuICAgICAgICAkKGVsKS5hZGRDbGFzcygnYWN0aXZlJyk7XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgX21hcmtFeHRlcm5hbExpbmtzKCkge1xyXG4gICAgdGhpcy5lbC5maW5kKCcubGlua3MgPiBsaSA+IGEnKS5lYWNoKChpbmRleCwgZWwpID0+IHtcclxuICAgICAgaWYgKCQoZWwpLmF0dHIoJ2hyZWYnKS5pbmRleE9mKFwiI1wiKSAhPT0gMCkge1xyXG4gICAgICAgICQoZWwpLmFkZENsYXNzKCdleHRlcm5hbC1wYWdlLWxpbmsnKTsgICAgICAgIFxyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIF9wcmVwYXJlQnV5QnV0dG9uKCkge1xyXG4gICAgbGV0IGxhc3RMaW5rID0gdGhpcy5lbC5maW5kKCcubGlua3MgPiBsaTpsYXN0LWNoaWxkID4gYScpLFxyXG4gICAgICAgIGRyb3AgPSB0aGlzLmVsLmZpbmQoJy5tb2xlY3VsZS1sYi00MDYnKTtcclxuXHJcbiAgICBpZiAobGFzdExpbmsubGVuZ3RoICYmIGxhc3RMaW5rLmF0dHIoJ2hyZWYnKS5pbmRleE9mKCcjJykgIT09IDApIHtcclxuICAgICAgbGFzdExpbmsuYWRkQ2xhc3MoJ2J1dHRvbiBwcmltYXJ5JykuY2xvc2VzdCgnbGknKS5hZGRDbGFzcygnc21hbGwtYnRuJyk7XHJcbiAgICB9XHJcbiAgICBpZiAoZHJvcC5sZW5ndGgpIHtcclxuICAgICAgZHJvcC5wYXJlbnQoKS5hZGRDbGFzcygnZHJvcC1saW5rJyk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBfcHJlcGFyZU1vYmlsZSgpIHtcclxuICAgIGxldCB0cGwgPSBfLnRlbXBsYXRlKHRoaXMub3B0aW9ucy5tb2JpbGVUcGwpLFxyXG4gICAgICAgIGl0ZW1zID0gdGhpcy5lbC5maW5kKCcubGlua3MnKS5jbG9uZSgpLmh0bWwoKSxcclxuICAgICAgICBidG4gPSB0aGlzLmVsLmZpbmQoJ2xpLnNtYWxsLWJ0biwgbGkuZHJvcC1saW5rJykuY2xvbmUoKS5odG1sKCksXHJcbiAgICAgICAgZmlyc3RBbmNob3IgPSB0aGlzLmVsLmZpbmQoJy5saW5rcyA+IGxpOmZpcnN0LWNoaWxkID4gYScpLFxyXG4gICAgICAgIGRyb3BUcmlnZ2VyID0gJzxhIGNsYXNzPVwiZHJvcC10cmlnZ2VyXCIgaHJlZj1cImphdmFzY3JpcHQ6IHZvaWQoMClcIj4nICsgZmlyc3RBbmNob3IudGV4dCgpICsgJzwvYT4nXHJcblxyXG4gICAgdGhpcy5lbC5hcHBlbmQodHBsKHtpdGVtcywgYnRuLCBkcm9wVHJpZ2dlcn0pKTtcclxuICB9XHJcblxyXG4gIF9pbml0TGlzdGVuZXJzKCkge1xyXG4gICAgdGhpcy5kcm9wVHJpZ2dlciA9IHRoaXMuZWwuZmluZCgnLmFuY2hvcnMgPiBhJyk7XHJcbiAgICB0aGlzLmRyb3AgPSB0aGlzLmVsLmZpbmQoJy5kcm9wJyk7XHJcbiAgICB0aGlzLm1vYmlsZU5hdiA9IHRoaXMuZWwuZmluZCgnLm1vYmlsZS1uYXYnKTtcclxuICAgIHRoaXMuYW5jaG9yVHJpZ2dlcnMgPSB0aGlzLmVsLmZpbmQoJy5saW5rcyA+IGxpID4gYTpub3QoLmJ1dHRvbiwgLmV4dGVybmFsLXBhZ2UtbGluayksIC5tb2JpbGUtbmF2IC5kcm9wID4gdWwgPiBsaSA+IGE6bm90KC5idXR0b24sIC5leHRlcm5hbC1wYWdlLWxpbmspJyk7XHJcbiAgICB0aGlzLmFuY2hvclNlY3Rpb25zID0gdGhpcy5fY29sbGVjdFNlY3Rpb25zKCk7XHJcbiAgICBcclxuICAgIHRoaXMuZHJvcFRyaWdnZXIub24oJ2NsaWNrJywgKGVsKSA9PiB7XHJcbiAgICAgIGlmICh0aGlzLm1vYmlsZU5hdi5oYXNDbGFzcygnYWN0aXZlJykpIHtcclxuICAgICAgICB0aGlzLl9jb2xsYXBzZURyb3AoKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICB0aGlzLl9leHBhbmREcm9wKCk7XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG5cclxuICAgIHRoaXMuYW5jaG9yVHJpZ2dlcnMub24oJ2NsaWNrJywgKGUpID0+IHtcclxuICAgICAgdGhpcy5kcm9wVHJpZ2dlci5odG1sKCQoZS5jdXJyZW50VGFyZ2V0KS50ZXh0KCkpO1xyXG4gICAgICB0aGlzLl9jb2xsYXBzZURyb3AoKTtcclxuICAgICAgR0xPQkFMUy5TY3JvbGxUby5uYXZpZ2F0ZSgkKGUuY3VycmVudFRhcmdldCkuYXR0cignaHJlZicpLCB0aGlzLl91cGRhdGVBY3RpdmVBbmNob3IuYmluZCh0aGlzKSwgMTAwMCk7XHJcbiAgICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuICAgIH0pO1xyXG5cclxuICAgICQod2luZG93KS5vbignc2Nyb2xsJywgKCkgPT4ge1xyXG4gICAgICBpZiAoIUdMT0JBTFMuU2Nyb2xsVG8uYW5pbWF0aW5nKSB7XHJcbiAgICAgICAgdGhpcy5fdXBkYXRlQWN0aXZlQW5jaG9yKCk7XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgX3VwZGF0ZUFjdGl2ZUFuY2hvcigpIHtcclxuICAgIGxldCBjdXJyZW50TmF2SGVpZ2h0ID0gdGhpcy5lbC5vdXRlckhlaWdodCgpLFxyXG4gICAgICAgIGFjdGl2ZVNlY3Rpb247XHJcbiAgICBmb3IgKGxldCBpID0gdGhpcy5hbmNob3JTZWN0aW9ucy5sZW5ndGggLSAxOyBpID49IDA7IGktLSkge1xyXG4gICAgICBsZXQgY2xpZW50UmVjdCA9IHRoaXMuYW5jaG9yU2VjdGlvbnNbaV1bMF0uZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCk7XHJcblxyXG4gICAgICBpZiAoY2xpZW50UmVjdC50b3AgPCBjdXJyZW50TmF2SGVpZ2h0KSB7XHJcbiAgICAgICAgaWYgKCBpICE9PSB0aGlzLmFuY2hvclNlY3Rpb25zLmxlbmd0aCAtIDEgKSB7XHJcbiAgICAgICAgICBhY3RpdmVTZWN0aW9uID0gdGhpcy5hbmNob3JTZWN0aW9uc1tpXTtcclxuICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICBpZiAoY2xpZW50UmVjdC5ib3R0b20gPiBjdXJyZW50TmF2SGVpZ2h0KSB7XHJcbiAgICAgICAgICAgIGFjdGl2ZVNlY3Rpb24gPSB0aGlzLmFuY2hvclNlY3Rpb25zW2ldO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgfVxyXG4gICAgICB9O1xyXG4gICAgfVxyXG4gICAgaWYgKCBhY3RpdmVTZWN0aW9uICkge1xyXG4gICAgICBsZXQgYWN0aXZlU2VjdGlvbkFuY2hvciA9IGFjdGl2ZVNlY3Rpb24uZGF0YSgnYW5jaG9yJyksXHJcbiAgICAgICAgICBhY3RpdmVUcmlnZ2VycyA9IHRoaXMuYW5jaG9yVHJpZ2dlcnMuZmlsdGVyKChpbmRleCwgZWwpID0+IHtcclxuICAgICAgICAgICAgcmV0dXJuICQoZWwpLmF0dHIoJ2hyZWYnKSA9PT0gJyMnICsgYWN0aXZlU2VjdGlvbkFuY2hvcjtcclxuICAgICAgICAgIH0pO1xyXG4gICAgICB0aGlzLmFuY2hvclRyaWdnZXJzLnJlbW92ZUNsYXNzKCdhY3RpdmUnKTtcclxuICAgICAgdGhpcy5kcm9wVHJpZ2dlci5odG1sKGFjdGl2ZVRyaWdnZXJzLmVxKDApLnRleHQoKSk7XHJcbiAgICAgIGFjdGl2ZVRyaWdnZXJzLmFkZENsYXNzKCdhY3RpdmUnKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMuYW5jaG9yVHJpZ2dlcnMucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgX2V4cGFuZERyb3AoKSB7XHJcbiAgICAkKCdib2R5Jykub24oJ2NsaWNrJywgdGhpcy5fY2xpY2tPdXRzaWRlQ2IpO1xyXG4gICAgdGhpcy5tb2JpbGVOYXYuYWRkQ2xhc3MoJ2FjdGl2ZScpO1xyXG4gICAgdGhpcy5kcm9wLnNsaWRlRG93bigyMDApO1xyXG4gIH1cclxuXHJcbiAgX2NvbGxhcHNlRHJvcCgpIHtcclxuICAgICQoJ2JvZHknKS5vZmYoJ2NsaWNrJywgdGhpcy5fY2xpY2tPdXRzaWRlQ2IpO1xyXG4gICAgdGhpcy5tb2JpbGVOYXYucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpO1xyXG4gICAgdGhpcy5kcm9wLnNsaWRlVXAoMjAwKTtcclxuICB9XHJcblxyXG4gIF9pbml0U3RpY2thYmxlQmVoYXZpb3VyKCkge1xyXG4gICAgbmV3IFN0aWNrYWJsZSh0aGlzLmVsKTtcclxuICB9XHJcblxyXG4gIF9pbml0SGVpZ2h0VXBkYXRlKCkge1xyXG4gICAgY29uc3Qgcm93ID0gdGhpcy5wYXJlbnRTZWN0aW9uLmZpbmQoJz4ucm93Jyk7XHJcbiAgICBjb25zdCB1cGRhdGVIZWlnaHQgPSAoKSA9PiB7XHJcbiAgICAgIHJvdy5oZWlnaHQodGhpcy5lbC5oZWlnaHQoKSk7XHJcbiAgICB9XHJcbiAgICAkKHdpbmRvdykub24oJ2xvYWQgcmVzaXplJywgXy5kZWJvdW5jZSgoKSA9PiB1cGRhdGVIZWlnaHQoKSwgMTAwKSk7XHJcbiAgICB1cGRhdGVIZWlnaHQoKTtcclxuICB9XHJcblxyXG4gIF9jbGlja091dHNpZGVDYihlKSB7XHJcbiAgICBpZiAoISQoZS50YXJnZXQpLmNsb3Nlc3QoJy5tb2xlY3VsZS1sYi04MzUnKS5sZW5ndGgpIHtcclxuICAgICAgdGhpcy5fY29sbGFwc2VEcm9wKCk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBfY29sbGVjdFNlY3Rpb25zKCkge1xyXG4gICAgbGV0IHNlY3Rpb25zID0gW107XHJcbiAgICB0aGlzLmVsLmZpbmQoJy5saW5rcyA+IGxpID4gYTpub3QoLmJ1dHRvbiwgLmV4dGVybmFsLXBhZ2UtbGluayknKS5lYWNoKChpbmRleCwgZWwpID0+IHtcclxuICAgICAgbGV0IGFuY2hvcklkID0gJChlbCkuYXR0cignaHJlZicpLnJlcGxhY2UoJyMnLCAnJyksXHJcbiAgICAgICAgICBhbmNob3JTZWFjdGlvbiA9ICQoJ1tkYXRhLWFuY2hvcj1cIicgKyBhbmNob3JJZCArICdcIl0nKTtcclxuICAgICAgYW5jaG9yU2VhY3Rpb24ubGVuZ3RoICYmIHNlY3Rpb25zLnB1c2goYW5jaG9yU2VhY3Rpb24pO1xyXG4gICAgfSk7XHJcbiAgICByZXR1cm4gc2VjdGlvbnM7XHJcbiAgfVxyXG4gIFxyXG59XHJcbiIsImltcG9ydCB7IEJhc2VDb21wb25lbnQgfSBmcm9tICcuLi8uLi9jb21wb25lbnRzL2Jhc2UtY29tcG9uZW50LmpzJztcclxuaW1wb3J0IHsgU3RpY2thYmxlIH0gZnJvbSAnLi4vLi4vY29tbW9uL3NjcmlwdHMvc3RpY2thYmxlLmpzJztcclxuXHJcblxyXG5leHBvcnQgY2xhc3MgTW9sZWN1bGU4MzZUaGVtZTIgZXh0ZW5kcyBCYXNlQ29tcG9uZW50IHtcclxuXHJcbiAgc3RhdGljIGdldCBzZWxlY3RvcigpIHsgcmV0dXJuICcubW9sZWN1bGUtbGItODM2LmFwcGVhcmFuY2UtdGhlbWUtMic7IH1cclxuXHJcbiAgZ2V0IGRlZmF1bHRzKCkge1xyXG4gICAgcmV0dXJuICQuZXh0ZW5kKHt9LCBzdXBlci5kZWZhdWx0cywge1xyXG4gICAgICByb3dTZWxlY3RvcjogJy5zZWN0aW9uJyxcclxuICAgICAgcm93Q2xhc3M6ICdzZWN0aW9uLXdpdGgtdGFicy10aGVtZS0yJyxcclxuICAgICAgc3RpY2t5V3JhcHBlclNlbGVjdG9yOiAnLnN0aWNreS13cmFwcGVyJyxcclxuICAgICAgY29udGVudENsYXNzOiAndGFiLWNvbnRlbnQgY29udGVudC10aGVtZS0yJyxcclxuICAgICAgYWN0aXZlQ2xhc3M6ICd0YWItY29udGVudC1hY3RpdmUnLFxyXG4gICAgICBhY3RpdmVUYWJDbGFzczogJ2FjdGl2ZS10YWInLFxyXG4gICAgICBjb250ZW50U2VsZWN0b3I6ICcuZmxleC1jb250ZW50JyxcclxuICAgICAgbGlua3NDb250YWluZXJTZWxlY3RvcjogJy50YWJzLWNvbnRhaW5lcicsXHJcbiAgICAgIGxpbmtzU2VsZWN0b3I6ICdhJyxcclxuICAgICAgc2xpZGVyU2VsZWN0b3I6ICcuc2xpZGVyLWNhcm91c2VsLWFycm93cy1hbmQtZG90cycsXHJcbiAgICAgIHVuZGVybGluZVNsaWRlclRwbDogJzxkaXYgY2xhc3M9XCJ1bmRlcmxpbmUtc2xpZGVyXCI+JyArXHJcbiAgICAgICAgJzxzcGFuIGNsYXNzPVwic3dpdGNoZXJcIj48L3NwYW4+JyArXHJcbiAgICAgICAgJzwvZGl2PidcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgY29uc3RydWN0b3IoLi4uYXJncykge1xyXG4gICAgc3VwZXIoLi4uYXJncyk7XHJcbiAgICB0aGlzLl9pbml0KCk7XHJcbiAgfVxyXG5cclxuICBfaW5pdCgpIHtcclxuXHJcbiAgICB0aGlzLl9hZGRVbmRlcmxpbmVTbGlkZXIoKTtcclxuICAgIHRoaXMuX3dyYXBGb3JTY3JvbGxpbmcoKTtcclxuICAgIHRoaXMuX2NvbGxlY3RDb250ZW50cygpO1xyXG4gICAgdGhpcy5fd3JhcExpbmtzKCk7XHJcbiAgICB0aGlzLmlzU2Nyb2xsYWJsZSA9IGZhbHNlO1xyXG4gICAgdGhpcy5faW5pdExpc3RlbmVycygpO1xyXG4gICAgdGhpcy5fd3JhcFN0aWNreSgpO1xyXG4gICAgdGhpcy5faW5pdFN0aWNrYWJsZUJlaGF2aW91cigpO1xyXG4gICAgdGhpcy5fY2hlY2tTY3JvbGxhYmxlKCk7XHJcbiAgICB0aGlzLnByb2Nlc3NDb250ZW50c0JhY2tncm91bmQoKTsgICAgICAgIFxyXG4gICAgdmFyIHNlbGYgPSB0aGlzO1xyXG4gICAgdGhpcy50YWJIYXNoZXMgPSB7fTtcclxuXHJcbiAgICB0aGlzLmxpbmtzLmVhY2goZnVuY3Rpb24oaW5kZXgsIGxpbmspIHtcclxuICAgICAgaWYgKGxpbmsuaGFzaCkge1xyXG4gICAgICAgIHNlbGYudGFiSGFzaGVzW2xpbmsuaGFzaF0gPSB0cnVlO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuXHJcbiAgICBpZiAoIXRoaXMuX29wZW5UYWJCeUhhc2goKSkge1xyXG4gICAgICB0aGlzLmxpbmtzLmZpcnN0KCkudHJpZ2dlcignY2xpY2snLCB7IGluaXRpYWxDbGljazogdHJ1ZSB9KTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIC8vaGVyZSBkZXRlY3RpbmcgdGFicyBzZWN0aW9ucyB3aXRoIGNvbnRlbnRcclxuICBfY29sbGVjdENvbnRlbnRzKCkge1xyXG4gICAgdGhpcy5jb250YWluZXIgPSB0aGlzLmVsLmZpbmQodGhpcy5vcHRpb25zLmxpbmtzQ29udGFpbmVyU2VsZWN0b3IpO1xyXG4gICAgdGhpcy5saW5rcyA9IHRoaXMuY29udGFpbmVyLmZpbmQodGhpcy5vcHRpb25zLmxpbmtzU2VsZWN0b3IpO1xyXG4gICAgdGhpcy5jb250ZW50cyA9IHRoaXMuZWxcclxuICAgICAgLnBhcmVudHModGhpcy5vcHRpb25zLmNvbnRlbnRTZWxlY3RvcilcclxuICAgICAgLm5leHRBbGwodGhpcy5vcHRpb25zLmNvbnRlbnRTZWxlY3RvcilcclxuICAgICAgLnNsaWNlKDAsIHRoaXMubGlua3MubGVuZ3RoKVxyXG4gICAgICAuYWRkQ2xhc3ModGhpcy5vcHRpb25zLmNvbnRlbnRDbGFzcyk7XHJcblxyXG4gICAgdGhpcy5lbC5jbG9zZXN0KHRoaXMub3B0aW9ucy5yb3dTZWxlY3RvcikuYWRkQ2xhc3ModGhpcy5vcHRpb25zLnJvd0NsYXNzKTtcclxuICB9XHJcblxyXG4gIF93cmFwTGlua3MoKSB7XHJcbiAgICB0aGlzLmxpbmtzLmVhY2goKGluZGV4LCBlbCkgPT4ge1xyXG4gICAgICAkKGVsKS53cmFwSW5uZXIoXCI8c3Bhbj48L3NwYW4+XCIpO1xyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBfb3BlblRhYkJ5SGFzaChoYXNoLCBmb3JjZVNjcm9sbCkge1xyXG4gICAgdmFyIHRhYiA9IHRoaXMuX2ZpbmRUYWJCeUhhc2goaGFzaCksXHJcbiAgICAgIHNlbGYgPSB0aGlzO1xyXG5cclxuICAgIGlmICghdGFiLmxlbmd0aCkge1xyXG4gICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKCF0YWIucGFyZW50KCkuaGFzQ2xhc3ModGhpcy5vcHRpb25zLmFjdGl2ZVRhYkNsYXNzKSkge1xyXG4gICAgICB0YWIuZWFjaChmdW5jdGlvbihpLCAkZWwpIHtcclxuICAgICAgICBzZWxmLl91cGRhdGVBY3RpdmVUYWIoJGVsKTtcclxuICAgICAgICBzZWxmLl91cGRhdGVBY3RpdmVDb250ZW50KCRlbCk7XHJcbiAgICAgICAgc2VsZi5fdXBkYXRlVW5kZXJsaW5lKCRlbCk7ICAgICAgICBcclxuICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgdmFyIHRhcmdldCA9ICQodGhpcy5lbCksXHJcbiAgICAgIHN0aWNreU5hdkJlZm9yZSA9IHNlbGYuX2dldFByZXZTdGlja3lOYXZIZWlnaHQoKSB8fCAwLFxyXG4gICAgICB0b3BOdW1iZXIgPSB0YXJnZXQub2Zmc2V0KCkudG9wIC0gc3RpY2t5TmF2QmVmb3JlO1xyXG4gICAgaWYgKCF0YXJnZXQuaGFzQ2xhc3MoJ25vLWluaXRpYWwtc2Nyb2xsJykgfHwgZm9yY2VTY3JvbGwpIHtcclxuICAgICAgd2luZG93LnNldFRpbWVvdXQoZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgR0xPQkFMUy5TY3JvbGxUby5uYXZpZ2F0ZVRvKHRvcE51bWJlciwgMTAwMCk7XHJcbiAgICAgIH0sIDIwMCk7XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIHRydWU7XHJcbiAgfVxyXG5cclxuICBfZmluZFRhYkJ5SGFzaChoYXNoKSB7XHJcbiAgICBoYXNoID0gaGFzaCB8fCB3aW5kb3cubG9jYXRpb24uaGFzaDtcclxuICAgIHJldHVybiAkKCQuZ3JlcCh0aGlzLnRhYlRyaWdnZXJzLCBmdW5jdGlvbihpdGVtKSB7XHJcbiAgICAgIHJldHVybiBoYXNoICYmIChpdGVtLmhhc2ggPT09IGhhc2gpO1xyXG4gICAgfSkpO1xyXG4gIH1cclxuXHJcbiAgX2FkZFVuZGVybGluZVNsaWRlcigpIHtcclxuICAgIGxldCB0cGwgPSBfLnRlbXBsYXRlKHRoaXMub3B0aW9ucy51bmRlcmxpbmVTbGlkZXJUcGwpO1xyXG4gICAgdGhpcy5lbC5maW5kKCd1bCcpLmFwcGVuZCh0cGwpO1xyXG4gIH1cclxuXHJcbiAgX3dyYXBGb3JTY3JvbGxpbmcoKSB7XHJcbiAgICB0aGlzLmVsLmZpbmQoJ3VsJykud3JhcCgnPGRpdiBjbGFzcz1cInNjcm9sbGluZy1wYW5lbFwiPjwvZGl2PicpO1xyXG4gICAgdGhpcy5zY3JvbGxhYmxlRGl2ID0gdGhpcy5lbC5maW5kKCcuc2Nyb2xsaW5nLXBhbmVsJyk7XHJcbiAgfVxyXG5cclxuICBfY2hlY2tTY3JvbGxhYmxlKCkge1xyXG4gICAgbGV0IHBhcmVudFVsID0gdGhpcy5lbC5maW5kKCd1bCcpXHJcbiAgICBpZiAoZG9jdW1lbnQuYm9keS5jbGllbnRXaWR0aCA8IHBhcmVudFVsLndpZHRoKCkpIHtcclxuICAgICAgdGhpcy5pc1Njcm9sbGFibGUgPSB0cnVlO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5pc1Njcm9sbGFibGUgPSBmYWxzZTtcclxuICAgIH0gICAgXHJcbiAgfVxyXG5cclxuICBfZ2V0UHJldlN0aWNreU5hdkhlaWdodCgpIHtcclxuICAgIHZhciBwcmV2UGFyZW50ID0gdGhpcy5lbC5wYXJlbnRzKHRoaXMub3B0aW9ucy5jb250ZW50U2VsZWN0b3IpLnByZXZBbGwoJy5mbGV4LWNvbnRlbnQnKS5ub3QoJy50YWItY29udGVudCcpLFxyXG4gICAgICBwcmV2UGFyZW50TmVhciA9IHByZXZQYXJlbnQuY2hpbGRyZW4oKS5maW5kKFwiLm1vbGVjdWxlLWxiLTgzNSwgLm1vbGVjdWxlLWxiLTgzNiwgLm1vbGVjdWxlLWxiLTgzMFwiKS5ub3QoXCIubm9uLXN0aWNrYWJsZVwiKSxcclxuICAgICAgbWF4SGVpZ2h0ID0gMDtcclxuXHJcbiAgICBpZiAocHJldlBhcmVudE5lYXIubGVuZ3RoID4gMCkge1xyXG4gICAgICBtYXhIZWlnaHQgPSAkKHByZXZQYXJlbnROZWFyLmVxKDApKS5oZWlnaHQoKTtcclxuICAgIH0gICAgXHJcbiAgICByZXR1cm4gbWF4SGVpZ2h0O1xyXG4gIH1cclxuXHJcbiAgX2luaXRMaXN0ZW5lcnMoKSB7XHJcbiAgICB0aGlzLnRhYlRyaWdnZXJzID0gdGhpcy5lbC5maW5kKCdhJyk7XHJcbiAgICB2YXIgc2VsZiA9IHRoaXMsXHJcbiAgICAgICRodG1sT3JCb2R5ID0gJCgnaHRtbCwgYm9keScpO1xyXG5cclxuICAgIC8vIEFueSBsaW5rIG9uIGEgcGFnZSBtYXkgYmUgdGFyZ2V0aW5nIHRhYnNcclxuICAgICQoJ2JvZHknKS5vbignY2xpY2snLCAnYScsIChlKSA9PiB7XHJcbiAgICAgIHZhciBsaW5rID0gZS5jdXJyZW50VGFyZ2V0O1xyXG4gICAgICBpZiAoc2VsZi50YWJUcmlnZ2Vycy5pbmRleChsaW5rKSA9PSAtMSkge1xyXG4gICAgICAgIGlmIChsaW5rLmhhc2ggJiYgKGxpbmsuaGFzaCBpbiBzZWxmLnRhYkhhc2hlcykpIHtcclxuICAgICAgICAgIHNlbGYuX29wZW5UYWJCeUhhc2gobGluay5oYXNoLCB0cnVlKTtcclxuICAgICAgICAgIHdpbmRvdy5sb2NhdGlvbi5oYXNoID0gJChsaW5rKS5hdHRyKCdocmVmJyk7XHJcbiAgICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9KTtcclxuXHJcbiAgICB0aGlzLnRhYlRyaWdnZXJzLm9uKCdjbGljaycsIChlLCBwYXJhbXMpID0+IHtcclxuICAgICAgbGV0IGNsaWNrZWQgPSAkKGUuY3VycmVudFRhcmdldCk7XHJcbiAgICAgIHRoaXMuX3VwZGF0ZUFjdGl2ZVRhYihjbGlja2VkKTtcclxuICAgICAgdGhpcy5fdXBkYXRlQWN0aXZlQ29udGVudChjbGlja2VkKTtcclxuICAgICAgdGhpcy5fdXBkYXRlVW5kZXJsaW5lKGNsaWNrZWQpOyAgICAgIFxyXG5cclxuICAgICAgaWYgKCFwYXJhbXMgfHwgIXBhcmFtcy5pbml0aWFsQ2xpY2spIHtcclxuICAgICAgICBpZiAoY2xpY2tlZC5hdHRyKCdocmVmJykubGVuZ3RoID4gMSkge1xyXG4gICAgICAgICAgd2luZG93LmxvY2F0aW9uLmhhc2ggPSBjbGlja2VkLmF0dHIoJ2hyZWYnKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdmFyIHRhcmdldCA9ICQodGhpcy5lbCksXHJcbiAgICAgICAgICBzdGlja3lOYXZCZWZvcmUgPSBzZWxmLl9nZXRQcmV2U3RpY2t5TmF2SGVpZ2h0KCkgfHwgMCxcclxuICAgICAgICAgIHRvcE51bWJlciA9IHRhcmdldC5vZmZzZXQoKS50b3AgLSBzdGlja3lOYXZCZWZvcmU7XHJcbiAgICAgICAgd2luZG93LnNldFRpbWVvdXQoZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICBHTE9CQUxTLlNjcm9sbFRvLm5hdmlnYXRlVG8odG9wTnVtYmVyLCAxMDAwKTtcclxuICAgICAgICB9LCAwKTtcclxuICAgICAgfVxyXG5cclxuICAgICAgJCh3aW5kb3cpLnRyaWdnZXIoXCJyZXNpemVcIik7XHJcblxyXG4gICAgfSk7XHJcblxyXG4gICAgdGhpcy50YWJUcmlnZ2Vycy5vbignbW91c2VlbnRlcicsIChlLCBwYXJhbXMpID0+IHtcclxuICAgICAgbGV0IGZvY3VzZWQgPSAkKGUuY3VycmVudFRhcmdldCk7ICAgICAgIFxyXG4gICAgICBcclxuICAgICAgaWYgKHNlbGYuaXNTY3JvbGxhYmxlKSB7ICAgICAgICAgICAgICAgIFxyXG4gICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICBzZWxmLl9zY3JvbGxUbyhmb2N1c2VkKTsgICAgICAgICAgXHJcbiAgICAgICAgfSwgNTAwKTtcclxuICAgICAgfVxyXG4gICAgICBcclxuICAgIH0pO1xyXG5cclxuXHJcbiAgICAkKHdpbmRvdykub24oXCJyZXNpemVcIiwgZnVuY3Rpb24oKSB7XHJcbiAgICAgIHZhciBzZWxmID0gdGhpcztcclxuICAgICAgc2V0VGltZW91dChmdW5jdGlvbigpIHtcclxuICAgICAgICBsZXQgYWN0aXZlTGluayA9IHNlbGYuZWwuZmluZChcIi5cIiArIHNlbGYub3B0aW9ucy5hY3RpdmVUYWJDbGFzcykuZmluZChcImFcIik7XHJcbiAgICAgICAgc2VsZi5fY2hlY2tTY3JvbGxhYmxlKCk7XHJcbiAgICAgICAgc2VsZi5fdXBkYXRlTWFyZ2lucygpO1xyXG4gICAgICAgIHNlbGYuX2NlbnRlckFjdGl2ZUl0ZW0oYWN0aXZlTGluayk7XHJcbiAgICAgIH0sIDI1MCk7XHJcbiAgICB9LmJpbmQodGhpcykpO1xyXG5cclxuICAgICQod2luZG93KS5iaW5kKFwibG9hZFwiLCBmdW5jdGlvbigpIHtcclxuICAgICAgbGV0IGFjdGl2ZUxpbmsgPSB0aGlzLmVsLmZpbmQoXCIuXCIgKyB0aGlzLm9wdGlvbnMuYWN0aXZlVGFiQ2xhc3MpLmZpbmQoXCJhXCIpO1xyXG5cclxuICAgICAgdGhpcy5fdXBkYXRlVW5kZXJsaW5lKGFjdGl2ZUxpbmspO1xyXG4gICAgICB0aGlzLl9jaGVja1Njcm9sbGFibGUoKTtcclxuICAgICAgdGhpcy5fdXBkYXRlTWFyZ2lucygpO1xyXG4gICAgICB0aGlzLl9jZW50ZXJBY3RpdmVJdGVtKGFjdGl2ZUxpbmspO1xyXG4gICAgfS5iaW5kKHRoaXMpKTtcclxuXHJcbiAgICAkaHRtbE9yQm9keS5vbihbXHJcbiAgICAgICdzY3JvbGwnLFxyXG4gICAgICAnbW91c2Vkb3duJyxcclxuICAgICAgJ3doZWVsJyxcclxuICAgICAgJ0RPTU1vdXNlU2Nyb2xsJyxcclxuICAgICAgJ21vdXNld2hlZWwnLFxyXG4gICAgICAna2V5dXAnLFxyXG4gICAgICAndG91Y2htb3ZlJ1xyXG4gICAgXS5qb2luKCcgJyksIGZ1bmN0aW9uIHN0b3BUYWJzQW5pbWF0aW9ucygpIHtcclxuICAgICAgJGh0bWxPckJvZHkuc3RvcCgpO1xyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBfdXBkYXRlQWN0aXZlVGFiKGVsKSB7XHJcbiAgICBsZXQgYWN0aXZlVGFiID0gJChlbCksXHJcbiAgICAgIGluZGV4ID0gdGhpcy5saW5rcy5pbmRleChhY3RpdmVUYWIpO1xyXG5cclxuICAgIGlmIChpbmRleCAhPSAtMSkge1xyXG4gICAgICB0aGlzLnRhYlRyaWdnZXJzXHJcbiAgICAgICAgLnBhcmVudHMoJ2xpJylcclxuICAgICAgICAucmVtb3ZlQ2xhc3ModGhpcy5vcHRpb25zLmFjdGl2ZVRhYkNsYXNzKTtcclxuICAgICAgJCh0aGlzLmxpbmtzLnBhcmVudCgnbGknKS5lcShpbmRleCkpXHJcbiAgICAgICAgLmFkZENsYXNzKHRoaXMub3B0aW9ucy5hY3RpdmVUYWJDbGFzcyk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBfdXBkYXRlQWN0aXZlQ29udGVudChlbCkge1xyXG4gICAgbGV0IGNsaWNrZWRMaW5rID0gJChlbCksXHJcbiAgICAgIGluZGV4ID0gdGhpcy5saW5rcy5pbmRleChjbGlja2VkTGluayk7XHJcblxyXG4gICAgaWYgKGluZGV4ICE9IC0xKSB7XHJcbiAgICAgIHRoaXMuY29udGVudHNcclxuICAgICAgICAucmVtb3ZlQ2xhc3ModGhpcy5vcHRpb25zLmFjdGl2ZUNsYXNzKVxyXG4gICAgICAgIC5lcShpbmRleClcclxuICAgICAgICAuYWRkQ2xhc3ModGhpcy5vcHRpb25zLmFjdGl2ZUNsYXNzKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIF91cGRhdGVVbmRlcmxpbmUoZWwpIHtcclxuICAgIGxldCBjbGlja2VkTGluayA9ICQoZWwpLFxyXG4gICAgICBpbmRleCA9IHRoaXMubGlua3MuaW5kZXgoY2xpY2tlZExpbmspLFxyXG4gICAgICBzd2l0Y2hlciA9IHRoaXMuZWwuZmluZCgnLnVuZGVybGluZS1zbGlkZXIgLnN3aXRjaGVyJyksXHJcbiAgICAgIHBhcmVudFVsID0gdGhpcy5lbC5maW5kKCd1bCcpLFxyXG4gICAgICBvZmZzZXRUYXJnZXQgPSBwYXJzZUludChjbGlja2VkTGluay5vZmZzZXQoKS5sZWZ0IC0gcGFyZW50VWxbMF0uZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCkubGVmdCAtIHBhcmVudFVsLmNzcyhcInBhZGRpbmctbGVmdFwiKS5yZXBsYWNlKFwicHhcIiwgXCJcIikpICsgXCJweFwiLFxyXG4gICAgICB3aWR0aFRhcmdldCA9IHBhcnNlSW50KGNsaWNrZWRMaW5rLndpZHRoKCkpICsgXCJweFwiO1xyXG5cclxuICAgIGlmIChpbmRleCAhPSAtMSkge1xyXG4gICAgICBzd2l0Y2hlci5jc3MoXCJ3aWR0aFwiLCB3aWR0aFRhcmdldCkuY3NzKFwibWFyZ2luLWxlZnRcIiwgb2Zmc2V0VGFyZ2V0KTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIF9zY3JvbGxUbyhlbCkge1xyXG4gICAgbGV0ICRlbCA9ICQoZWwpLFxyXG4gICAgICBwb3NMZWZ0O1xyXG5cclxuICAgIHBvc0xlZnQgPSBwYXJzZUludCgkKCRlbCkub2Zmc2V0KCkubGVmdCAtICgodGhpcy5zY3JvbGxhYmxlRGl2LndpZHRoKCkgLSAkZWwub3V0ZXJXaWR0aCh0cnVlKSkgLyAyKSArIHRoaXMuc2Nyb2xsYWJsZURpdi5zY3JvbGxMZWZ0KCkpO1xyXG4gICAgXHJcbiAgICB0aGlzLnNjcm9sbGFibGVEaXYuc3RvcCgpLmFuaW1hdGUoe1xyXG4gICAgICBzY3JvbGxMZWZ0OiBwb3NMZWZ0ICAgICAgICAgICBcclxuICAgIH0sIDMwMCk7XHJcblxyXG4gIH1cclxuXHJcbiAgX2dldENlbnRlcihlbCkge1xyXG4gICAgbGV0ICRlbCA9ICQoZWwpLFxyXG4gICAgICBjZW50ZXJpbmcgPSBwYXJzZUludChkb2N1bWVudC5ib2R5LmNsaWVudFdpZHRoIC8gMiAtICRlbC53aWR0aCgpIC8gMik7XHJcbiAgICByZXR1cm4gY2VudGVyaW5nO1xyXG4gIH1cclxuXHJcbiAgX3VwZGF0ZU1hcmdpbnMoKSB7XHJcbiAgICBsZXQgcGFyZW50VWwgPSB0aGlzLmVsLmZpbmQoXCJ1bFwiKSxcclxuICAgICAgbGFzdEVsSW5kZXggPSBwYXJlbnRVbC5maW5kKFwibGlcIikubGFzdCgpLmluZGV4KCksXHJcbiAgICAgIHNwYWNlRm9yQ2VudGVyaW5nLFxyXG4gICAgICBwYWRkaW5nUHJvcCxcclxuICAgICAgbWFyZ2luUHJvcCxcclxuICAgICAgbWFyZ2luU3BhY2UsXHJcbiAgICAgIGl0ZW1zID0gcGFyZW50VWwuZmluZChcImxpXCIpO1xyXG5cclxuICAgIGlmICh0aGlzLmlzU2Nyb2xsYWJsZSkge1xyXG4gICAgICBpdGVtcy5lYWNoKGZ1bmN0aW9uKGluZGV4LCBlbCkge1xyXG4gICAgICAgIGlmIChpbmRleCA9PSAwKSB7XHJcbiAgICAgICAgICBwYWRkaW5nUHJvcCA9IFwicGFkZGluZy1sZWZ0XCI7XHJcbiAgICAgICAgICBtYXJnaW5Qcm9wID0gXCJtYXJnaW4tbGVmdFwiO1xyXG4gICAgICAgICAgc3BhY2VGb3JDZW50ZXJpbmcgPSB0aGlzLl9nZXRDZW50ZXIoZWwpO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoaW5kZXggPT0gbGFzdEVsSW5kZXgpIHtcclxuICAgICAgICAgIHBhZGRpbmdQcm9wID0gXCJwYWRkaW5nLXJpZ2h0XCI7XHJcbiAgICAgICAgICBtYXJnaW5Qcm9wID0gXCJtYXJnaW4tcmlnaHRcIjtcclxuICAgICAgICAgIHNwYWNlRm9yQ2VudGVyaW5nID0gdGhpcy5fZ2V0Q2VudGVyKGVsKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgbWFyZ2luU3BhY2UgPSBwYXJzZUludChzcGFjZUZvckNlbnRlcmluZyAtIHBhcmVudFVsLmNzcyhwYWRkaW5nUHJvcCkucmVwbGFjZShcInB4XCIsIFwiXCIpKSArIFwicHhcIjtcclxuICAgICAgICBwYXJlbnRVbC5jc3MobWFyZ2luUHJvcCwgbWFyZ2luU3BhY2UpO1xyXG5cclxuICAgICAgfS5iaW5kKHRoaXMpKTtcclxuXHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICAkKHBhcmVudFVsKS5jc3MoXCJtYXJnaW5cIiwgMCk7XHJcbiAgICB9XHJcblxyXG4gIH1cclxuXHJcbiAgX2NlbnRlckFjdGl2ZUl0ZW0oaXRlbSkge1xyXG4gICAgbGV0IGFjdGl2ZUxpbmsgPSAkKGl0ZW0pLFxyXG4gICAgICBzZWxmID0gdGhpcztcclxuXHJcbiAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xyXG4gICAgICBzZWxmLl9zY3JvbGxUbyhhY3RpdmVMaW5rKTtcclxuICAgIH0sIDMwMCk7XHJcbiAgfVxyXG5cclxuICBfd3JhcFN0aWNreSgpIHtcclxuICAgICQodGhpcy5lbCkud3JhcElubmVyKCc8ZGl2IGNsYXNzPVwic3RpY2t5LXdyYXBwZXJcIj48L2Rpdj4nKTtcclxuICB9XHJcblxyXG4gIF9pbml0U3RpY2thYmxlQmVoYXZpb3VyKCkge1xyXG4gICAgaWYgKCF0aGlzLmVsLmhhc0NsYXNzKCdub24tc3RpY2thYmxlJykpIHtcclxuICAgICAgbmV3IFN0aWNrYWJsZSh0aGlzLmVsLmZpbmQodGhpcy5vcHRpb25zLnN0aWNreVdyYXBwZXJTZWxlY3RvcikpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgcHJvY2Vzc0NvbnRlbnRzQmFja2dyb3VuZCgpIHtcclxuICAgIHZhciBzZWxmID0gdGhpcztcclxuICAgIHRoaXMuY29udGVudHMuZWFjaChmdW5jdGlvbigpIHtcclxuICAgICAgdmFyICR0YWJDb250ZW50ID0gJCh0aGlzKSxcclxuICAgICAgICBzZWN0aW9uSW5TbGlkZXIgPSAkdGFiQ29udGVudC5maW5kKHNlbGYub3B0aW9ucy5zbGlkZXJTZWxlY3RvciksXHJcbiAgICAgICAgY2xhc3NOYW1lID0gJyc7XHJcblxyXG4gICAgICBpZiAoc2VjdGlvbkluU2xpZGVyLmxlbmd0aCAmJiBzZWN0aW9uSW5TbGlkZXJbMF0uY2xhc3NOYW1lLmluZGV4T2YoJ2JrZy1jb2xvci0nKSA+IC0xKSB7XHJcbiAgICAgICAgc2VjdGlvbkluU2xpZGVyLmVhY2goZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICB2YXIgY2xhc3NlcyA9IHRoaXMuY2xhc3NOYW1lLnNwbGl0KCcgJyk7XHJcbiAgICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IGNsYXNzZXMubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgaWYgKGNsYXNzZXNbaV0uaW5kZXhPZignYmtnLWNvbG9yLScpID4gLTEpIHtcclxuICAgICAgICAgICAgICBjbGFzc05hbWUgPSBjbGFzc2VzW2ldO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgJHRhYkNvbnRlbnQuZmluZCgnLnNlY3Rpb24nKS5hZGRDbGFzcyhjbGFzc05hbWUpO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICB9XHJcbn1cclxuIiwiaW1wb3J0IHsgQmFzZUNvbXBvbmVudCB9IGZyb20gJy4uLy4uL2NvbXBvbmVudHMvYmFzZS1jb21wb25lbnQuanMnO1xyXG5pbXBvcnQgeyBTdGlja2FibGUgfSBmcm9tICcuLi8uLi9jb21tb24vc2NyaXB0cy9zdGlja2FibGUuanMnO1xyXG5cclxuXHJcbmV4cG9ydCBjbGFzcyBNb2xlY3VsZTgzNiBleHRlbmRzIEJhc2VDb21wb25lbnQge1xyXG5cclxuICBzdGF0aWMgZ2V0IHNlbGVjdG9yKCkgeyByZXR1cm4gJy5tb2xlY3VsZS1sYi04MzY6bm90KC5hcHBlYXJhbmNlLXRoZW1lLTIpJzsgfVxyXG5cclxuICBnZXQgZGVmYXVsdHMoKSB7XHJcbiAgICByZXR1cm4gJC5leHRlbmQoe30sIHN1cGVyLmRlZmF1bHRzLCB7XHJcbiAgICAgIHJvd1NlbGVjdG9yOiAnLnNlY3Rpb24nLFxyXG4gICAgICByb3dDbGFzczogJ3NlY3Rpb24td2l0aC10YWJzJywgICAgICBcclxuICAgICAgc3RpY2t5V3JhcHBlclNlbGVjdG9yOiAnLnN0aWNreS13cmFwcGVyJyxcclxuICAgICAgY29udGVudENsYXNzOiAndGFiLWNvbnRlbnQnLFxyXG4gICAgICBhY3RpdmVDbGFzczogJ3RhYi1jb250ZW50LWFjdGl2ZScsXHJcbiAgICAgIGFjdGl2ZVRhYkNsYXNzOiAnYWN0aXZlLXRhYicsXHJcbiAgICAgIGNvbnRlbnRTZWxlY3RvcjogJy5mbGV4LWNvbnRlbnQnLFxyXG4gICAgICBsaW5rc0NvbnRhaW5lclNlbGVjdG9yOiAnLnRhYnMtY29udGFpbmVyJyxcclxuICAgICAgbGlua3NTZWxlY3RvcjogJ2EnLFxyXG4gICAgICBhY3RpdmVNb2JpbGU6ICdtb2ItYWN0aXZlJyxcclxuICAgICAgbGlua3NDb250YWluZXJNb2JpbGU6ICcubW9iaWxlLXRhYnMtY29udGFpbmVyIC5kcm9wJyxcclxuICAgICAgc2xpZGVyU2VsZWN0b3I6ICcuc2xpZGVyLWNhcm91c2VsLWFycm93cy1hbmQtZG90cycsXHJcbiAgICAgIG1vYmlsZVRwbDogJzxkaXYgY2xhc3M9XCJtb2JpbGUtdGFic1wiPicgK1xyXG4gICAgICAgICc8ZGl2IGNsYXNzPVwibW9iaWxlLXRhYnMtY29udGFpbmVyXCI+JyArXHJcbiAgICAgICAgJzwlPSBkcm9wVHJpZ2dlciAlPicgK1xyXG4gICAgICAgICc8ZGl2IGNsYXNzPVwiZHJvcFwiPjx1bD48JT0gaXRlbXMgJT48L3VsPjwvZGl2PicgK1xyXG4gICAgICAgICc8L2Rpdj4nICtcclxuICAgICAgICAnPC9kaXY+JyAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFxyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBjb25zdHJ1Y3RvciguLi5hcmdzKSB7XHJcbiAgICBzdXBlciguLi5hcmdzKTtcclxuICAgIHRoaXMuX2luaXQoKTtcclxuICB9XHJcblxyXG4gIF9pbml0KCkge1xyXG4gICAgXHJcbiAgICB0aGlzLl9wcmVwYXJlTW9iaWxlKCk7ICAgICAgICBcclxuICAgIHRoaXMuX2NvbGxlY3RDb250ZW50cygpO1xyXG4gICAgdGhpcy5fd3JhcExpbmtzKCk7XHJcbiAgICB0aGlzLmlzTW9iaWxlID0gZmFsc2U7XHJcbiAgICB0aGlzLl9pbml0TGlzdGVuZXJzKCk7XHJcbiAgICB0aGlzLl93cmFwU3RpY2t5KCk7XHJcbiAgICB0aGlzLl9pbml0U3RpY2thYmxlQmVoYXZpb3VyKCk7XHJcbiAgICB0aGlzLl9jbGlja091dHNpZGVDYiA9IHRoaXMuX2NsaWNrT3V0c2lkZUNiLmJpbmQodGhpcyk7XHJcbiAgICB0aGlzLl9jaGVja01vYmlsZSgpO1xyXG4gICAgdGhpcy5wcm9jZXNzQ29udGVudHNCYWNrZ3JvdW5kKCk7XHJcblxyXG4gICAgdmFyIHNlbGYgPSB0aGlzO1xyXG4gICAgdGhpcy50YWJIYXNoZXMgPSB7fTtcclxuXHJcbiAgICB0aGlzLmxpbmtzLmVhY2goZnVuY3Rpb24oaW5kZXgsIGxpbmspIHtcclxuICAgICAgaWYgKGxpbmsuaGFzaCkge1xyXG4gICAgICAgIHNlbGYudGFiSGFzaGVzW2xpbmsuaGFzaF0gPSB0cnVlO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuXHJcbiAgICBpZiAoIXRoaXMuX29wZW5UYWJCeUhhc2goKSkge1xyXG4gICAgICB0aGlzLmxpbmtzLmZpcnN0KCkudHJpZ2dlcignY2xpY2snLCB7IGluaXRpYWxDbGljazogdHJ1ZSB9KTtcclxuICAgICAgdGhpcy5saW5rc01vYmlsZS5maXJzdCgpLnRyaWdnZXIoJ2NsaWNrJywgeyBpbml0aWFsQ2xpY2s6IHRydWUgfSk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAvL2hlcmUgZGV0ZWN0aW5nIHRhYnMgc2VjdGlvbnMgd2l0aCBjb250ZW50XHJcbiAgX2NvbGxlY3RDb250ZW50cygpIHtcclxuICAgIHRoaXMuY29udGFpbmVyID0gdGhpcy5lbC5maW5kKHRoaXMub3B0aW9ucy5saW5rc0NvbnRhaW5lclNlbGVjdG9yKTtcclxuICAgIHRoaXMubGlua3MgPSB0aGlzLmNvbnRhaW5lci5maW5kKHRoaXMub3B0aW9ucy5saW5rc1NlbGVjdG9yKTtcclxuICAgIHRoaXMuY29udGFpbmVyTW9iaWxlID0gdGhpcy5lbC5maW5kKHRoaXMub3B0aW9ucy5saW5rc0NvbnRhaW5lck1vYmlsZSk7XHJcbiAgICB0aGlzLmxpbmtzTW9iaWxlID0gdGhpcy5jb250YWluZXJNb2JpbGUuZmluZCh0aGlzLm9wdGlvbnMubGlua3NTZWxlY3Rvcik7XHJcbiAgICB0aGlzLmNvbnRlbnRzID0gdGhpcy5lbFxyXG4gICAgICAucGFyZW50cyh0aGlzLm9wdGlvbnMuY29udGVudFNlbGVjdG9yKVxyXG4gICAgICAubmV4dEFsbCh0aGlzLm9wdGlvbnMuY29udGVudFNlbGVjdG9yKVxyXG4gICAgICAuc2xpY2UoMCwgdGhpcy5saW5rcy5sZW5ndGgpXHJcbiAgICAgIC5hZGRDbGFzcyh0aGlzLm9wdGlvbnMuY29udGVudENsYXNzKTsgICAgICAgIFxyXG4gICAgICB0aGlzLmVsLmNsb3Nlc3QodGhpcy5vcHRpb25zLnJvd1NlbGVjdG9yKS5hZGRDbGFzcyh0aGlzLm9wdGlvbnMucm93Q2xhc3MpOyAgICBcclxuICB9XHJcblxyXG4gIF93cmFwTGlua3MoKSB7XHJcbiAgICB0aGlzLmxpbmtzLmVhY2goKGluZGV4LCBlbCkgPT4ge1xyXG4gICAgICAkKGVsKS53cmFwSW5uZXIoXCI8c3Bhbj48L3NwYW4+XCIpO1xyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBfb3BlblRhYkJ5SGFzaChoYXNoLCBmb3JjZVNjcm9sbCkge1xyXG4gICAgdmFyIHRhYiA9IHRoaXMuX2ZpbmRUYWJCeUhhc2goaGFzaCksXHJcbiAgICAgIHNlbGYgPSB0aGlzO1xyXG5cclxuICAgIGlmICghdGFiLmxlbmd0aCkge1xyXG4gICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKCF0YWIucGFyZW50KCkuaGFzQ2xhc3ModGhpcy5vcHRpb25zLmFjdGl2ZVRhYkNsYXNzKSkge1xyXG4gICAgICB0YWIuZWFjaChmdW5jdGlvbihpLCAkZWwpIHtcclxuICAgICAgICBzZWxmLl91cGRhdGVBY3RpdmVUYWIoJGVsKTtcclxuICAgICAgICBzZWxmLl91cGRhdGVBY3RpdmVDb250ZW50KCRlbCk7XHJcbiAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIHZhciB0YXJnZXQgPSAkKHRoaXMuZWwpLFxyXG4gICAgICBzdGlja3lOYXZCZWZvcmUgPSBzZWxmLl9nZXRQcmV2U3RpY2t5TmF2SGVpZ2h0KCkgfHwgMCxcclxuICAgICAgdG9wTnVtYmVyID0gdGFyZ2V0Lm9mZnNldCgpLnRvcCAtIHN0aWNreU5hdkJlZm9yZTtcclxuICAgIGlmICghdGFyZ2V0Lmhhc0NsYXNzKCduby1pbml0aWFsLXNjcm9sbCcpIHx8IGZvcmNlU2Nyb2xsKSB7XHJcbiAgICAgIHdpbmRvdy5zZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIEdMT0JBTFMuU2Nyb2xsVG8ubmF2aWdhdGVUbyh0b3BOdW1iZXIsIDEwMDApO1xyXG4gICAgICB9LCAyMDApO1xyXG4gICAgfVxyXG5cclxuXHJcbiAgICByZXR1cm4gdHJ1ZTtcclxuICB9XHJcblxyXG4gIF9maW5kVGFiQnlIYXNoKGhhc2gpIHtcclxuICAgIGhhc2ggPSBoYXNoIHx8IHdpbmRvdy5sb2NhdGlvbi5oYXNoO1xyXG4gICAgcmV0dXJuICQoJC5ncmVwKHRoaXMudGFiVHJpZ2dlcnMsIGZ1bmN0aW9uKGl0ZW0pIHtcclxuICAgICAgcmV0dXJuIGhhc2ggJiYgKGl0ZW0uaGFzaCA9PT0gaGFzaCk7XHJcbiAgICB9KSk7XHJcbiAgfVxyXG4gXHJcbiAgX3ByZXBhcmVNb2JpbGUoKSB7XHJcbiAgICBsZXQgdHBsID0gXy50ZW1wbGF0ZSh0aGlzLm9wdGlvbnMubW9iaWxlVHBsKSxcclxuICAgICAgaXRlbXMgPSB0aGlzLmVsLmZpbmQoJ3VsJykuY2xvbmUodHJ1ZSkuaHRtbCgpLFxyXG4gICAgICBmaXJzdFRhYiA9IHRoaXMuZWwuZmluZCgnbGk6Zmlyc3QtY2hpbGQgYScpLFxyXG4gICAgICBkcm9wVHJpZ2dlciA9ICc8YSBjbGFzcz1cImRyb3AtdHJpZ2dlclwiIGhyZWY9XCJqYXZhc2NyaXB0OiB2b2lkKDApXCI+PHNwYW4+JyArIGZpcnN0VGFiLnRleHQoKSArICc8L3NwYW4+PC9hPidcclxuXHJcbiAgICB0aGlzLmVsLmFwcGVuZCh0cGwoeyBpdGVtcywgZHJvcFRyaWdnZXIgfSkpO1xyXG4gIH1cclxuXHJcbiAgX2NoZWNrTW9iaWxlKCkge1xyXG4gICAgaWYgKHdpbmRvdy5pbm5lcldpZHRoID4gOTYwKSB7XHJcbiAgICAgIHRoaXMuaXNNb2JpbGUgPSBmYWxzZTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMuaXNNb2JpbGUgPSB0cnVlO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgX2luaXRMaXN0ZW5lcnMoKSB7XHJcbiAgICB0aGlzLmRyb3BUcmlnZ2VyID0gdGhpcy5lbC5maW5kKCcubW9iaWxlLXRhYnMtY29udGFpbmVyID4gYScpO1xyXG4gICAgdGhpcy5kcm9wID0gdGhpcy5lbC5maW5kKCcuZHJvcCcpO1xyXG4gICAgdGhpcy5tb2JpbGVOYXYgPSB0aGlzLmVsLmZpbmQoJy5tb2JpbGUtdGFicycpO1xyXG4gICAgdGhpcy50YWJUcmlnZ2VycyA9IHRoaXMuZWwuZmluZCgnYTpub3QoLmRyb3AtdHJpZ2dlciknKTtcclxuICAgIHZhciBzZWxmID0gdGhpcyxcclxuICAgICAgJGh0bWxPckJvZHkgPSAkKCdodG1sLCBib2R5Jyk7XHJcblxyXG4gICAgLy8gQW55IGxpbmsgb24gYSBwYWdlIG1heSBiZSB0YXJnZXRpbmcgdGFic1xyXG4gICAgJCgnYm9keScpLm9uKCdjbGljaycsICdhJywgKGUpID0+IHtcclxuICAgICAgdmFyIGxpbmsgPSBlLmN1cnJlbnRUYXJnZXQ7XHJcbiAgICAgIGlmIChzZWxmLnRhYlRyaWdnZXJzLmluZGV4KGxpbmspID09IC0xKSB7XHJcbiAgICAgICAgaWYgKGxpbmsuaGFzaCAmJiAobGluay5oYXNoIGluIHNlbGYudGFiSGFzaGVzKSkge1xyXG4gICAgICAgICAgc2VsZi5fb3BlblRhYkJ5SGFzaChsaW5rLmhhc2gsIHRydWUpO1xyXG4gICAgICAgICAgd2luZG93LmxvY2F0aW9uLmhhc2ggPSAkKGxpbmspLmF0dHIoJ2hyZWYnKTtcclxuICAgICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG5cclxuICAgIHRoaXMuZHJvcFRyaWdnZXIub24oJ2NsaWNrJywgKGVsKSA9PiB7XHJcbiAgICAgIGlmICh0aGlzLm1vYmlsZU5hdi5oYXNDbGFzcyh0aGlzLm9wdGlvbnMuYWN0aXZlTW9iaWxlKSkge1xyXG4gICAgICAgIHRoaXMuX2NvbGxhcHNlRHJvcCgpO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHRoaXMuX2V4cGFuZERyb3AoKTtcclxuICAgICAgfVxyXG4gICAgfSk7XHJcblxyXG4gICAgdGhpcy50YWJUcmlnZ2Vycy5vbignY2xpY2snLCAoZSwgcGFyYW1zKSA9PiB7XHJcbiAgICAgIGxldCBjbGlja2VkID0gJChlLmN1cnJlbnRUYXJnZXQpO1xyXG4gICAgICB0aGlzLl9jb2xsYXBzZURyb3AoKTtcclxuICAgICAgdGhpcy5fdXBkYXRlQWN0aXZlVGFiKGNsaWNrZWQpO1xyXG4gICAgICB0aGlzLl91cGRhdGVBY3RpdmVDb250ZW50KGNsaWNrZWQpO1xyXG5cclxuICAgICAgaWYgKCFwYXJhbXMgfHwgIXBhcmFtcy5pbml0aWFsQ2xpY2spIHtcclxuICAgICAgICBpZiAoY2xpY2tlZC5hdHRyKCdocmVmJykubGVuZ3RoID4gMSkge1xyXG4gICAgICAgICAgd2luZG93LmxvY2F0aW9uLmhhc2ggPSBjbGlja2VkLmF0dHIoJ2hyZWYnKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdmFyIHRhcmdldCA9ICQodGhpcy5lbCksXHJcbiAgICAgICAgICBzdGlja3lOYXZCZWZvcmUgPSBzZWxmLl9nZXRQcmV2U3RpY2t5TmF2SGVpZ2h0KCkgfHwgMCxcclxuICAgICAgICAgIHRvcE51bWJlciA9IHRhcmdldC5vZmZzZXQoKS50b3AgLSBzdGlja3lOYXZCZWZvcmU7XHJcbiAgICAgICAgd2luZG93LnNldFRpbWVvdXQoZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICBHTE9CQUxTLlNjcm9sbFRvLm5hdmlnYXRlVG8odG9wTnVtYmVyLCAxMDAwKTtcclxuICAgICAgICB9LCAwKTtcclxuICAgICAgfVxyXG4gICAgXHJcbiAgICAgIC8vdG8gZm9yY2Ugc2xpZGVyIHJlY2FsY3VsYXRpb25cclxuICAgICAgJCh3aW5kb3cpLnRyaWdnZXIoXCJyZXNpemVcIik7XHJcbiAgICAgICAgICAgIFxyXG4gICAgfSk7XHJcblxyXG4gICAgJCh3aW5kb3cpLm9uKFwibG9hZCByZXNpemVcIiwgZnVuY3Rpb24oKSB7XHJcbiAgICAgIHNlbGYuX2NoZWNrTW9iaWxlKCk7XHJcbiAgICB9KTtcclxuXHJcbiAgICAkaHRtbE9yQm9keS5vbihbXHJcbiAgICAgICdzY3JvbGwnLFxyXG4gICAgICAnbW91c2Vkb3duJyxcclxuICAgICAgJ3doZWVsJyxcclxuICAgICAgJ0RPTU1vdXNlU2Nyb2xsJyxcclxuICAgICAgJ21vdXNld2hlZWwnLFxyXG4gICAgICAna2V5dXAnLFxyXG4gICAgICAndG91Y2htb3ZlJ1xyXG4gICAgXS5qb2luKCcgJyksIGZ1bmN0aW9uIHN0b3BUYWJzQW5pbWF0aW9ucygpIHtcclxuICAgICAgJGh0bWxPckJvZHkuc3RvcCgpO1xyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBfdXBkYXRlQWN0aXZlVGFiKGVsKSB7XHJcbiAgICBsZXQgYWN0aXZlVGFiID0gJChlbCksXHJcbiAgICAgIGluZGV4ID0gdGhpcy5pc01vYmlsZSA/IHRoaXMubGlua3NNb2JpbGUuaW5kZXgoYWN0aXZlVGFiKSA6IHRoaXMubGlua3MuaW5kZXgoYWN0aXZlVGFiKTtcclxuXHJcbiAgICBpZiAoaW5kZXggIT0gLTEpIHtcclxuICAgICAgdGhpcy50YWJUcmlnZ2Vyc1xyXG4gICAgICAgIC5wYXJlbnRzKCdsaScpXHJcbiAgICAgICAgLnJlbW92ZUNsYXNzKHRoaXMub3B0aW9ucy5hY3RpdmVUYWJDbGFzcyk7XHJcbiAgICAgICQodGhpcy5saW5rcy5wYXJlbnQoJ2xpJykuZXEoaW5kZXgpKVxyXG4gICAgICAgIC5hZGRDbGFzcyh0aGlzLm9wdGlvbnMuYWN0aXZlVGFiQ2xhc3MpO1xyXG4gICAgICAkKHRoaXMubGlua3NNb2JpbGUucGFyZW50KCdsaScpLmVxKGluZGV4KSlcclxuICAgICAgICAuYWRkQ2xhc3ModGhpcy5vcHRpb25zLmFjdGl2ZVRhYkNsYXNzKTtcclxuICAgICAgdGhpcy5kcm9wVHJpZ2dlci5maW5kKCdzcGFuJykudGV4dChhY3RpdmVUYWIudGV4dCgpKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIF91cGRhdGVBY3RpdmVDb250ZW50KGVsKSB7XHJcbiAgICBsZXQgY2xpY2tlZExpbmsgPSAkKGVsKSwgICAgICBcclxuICAgICAgaW5kZXggPSB0aGlzLmlzTW9iaWxlID8gdGhpcy5saW5rc01vYmlsZS5pbmRleChjbGlja2VkTGluaykgOiB0aGlzLmxpbmtzLmluZGV4KGNsaWNrZWRMaW5rKTtcclxuXHJcbiAgICBpZiAoaW5kZXggIT0gLTEpIHsgICAgICBcclxuICAgICAgICB0aGlzLmNvbnRlbnRzXHJcbiAgICAgICAgICAucmVtb3ZlQ2xhc3ModGhpcy5vcHRpb25zLmFjdGl2ZUNsYXNzKVxyXG4gICAgICAgICAgLmVxKGluZGV4KVxyXG4gICAgICAgICAgLmFkZENsYXNzKHRoaXMub3B0aW9ucy5hY3RpdmVDbGFzcyk7ICAgICAgICAgICAgICAgICAgICAgICBcclxuICAgIH1cclxuICB9XHJcblxyXG4gIF9leHBhbmREcm9wKCkge1xyXG4gICAgJCgnYm9keScpLm9uKCdjbGljaycsIHRoaXMuX2NsaWNrT3V0c2lkZUNiKTtcclxuICAgIHRoaXMubW9iaWxlTmF2LmFkZENsYXNzKHRoaXMub3B0aW9ucy5hY3RpdmVNb2JpbGUpO1xyXG4gICAgdGhpcy5kcm9wLnNsaWRlRG93bigyMDApO1xyXG4gIH1cclxuXHJcbiAgX2NvbGxhcHNlRHJvcCgpIHtcclxuICAgICQoJ2JvZHknKS5vZmYoJ2NsaWNrJywgdGhpcy5fY2xpY2tPdXRzaWRlQ2IpO1xyXG4gICAgdGhpcy5tb2JpbGVOYXYucmVtb3ZlQ2xhc3ModGhpcy5vcHRpb25zLmFjdGl2ZU1vYmlsZSk7XHJcbiAgICB0aGlzLmRyb3Auc2xpZGVVcCgyMDApO1xyXG4gIH1cclxuXHJcbiAgX3dyYXBTdGlja3koKSB7XHJcbiAgICAkKHRoaXMuZWwpLndyYXBJbm5lcihcIjxkaXYgY2xhc3M9J3N0aWNreS13cmFwcGVyJz48L2Rpdj5cIik7XHJcbiAgfVxyXG5cclxuICBfaW5pdFN0aWNrYWJsZUJlaGF2aW91cigpIHtcclxuICAgIGlmICghdGhpcy5lbC5oYXNDbGFzcygnbm9uLXN0aWNrYWJsZScpKSB7XHJcbiAgICAgIG5ldyBTdGlja2FibGUodGhpcy5lbC5maW5kKHRoaXMub3B0aW9ucy5zdGlja3lXcmFwcGVyU2VsZWN0b3IpKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIF9jbGlja091dHNpZGVDYihlKSB7XHJcbiAgICBpZiAoISQoZS50YXJnZXQpLmNsb3Nlc3QoJy5tb2xlY3VsZS1sYi04MzYnKS5sZW5ndGgpIHtcclxuICAgICAgdGhpcy5fY29sbGFwc2VEcm9wKCk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBwcm9jZXNzQ29udGVudHNCYWNrZ3JvdW5kKCkge1xyXG4gICAgdmFyIHNlbGYgPSB0aGlzO1xyXG4gICAgdGhpcy5jb250ZW50cy5lYWNoKGZ1bmN0aW9uKCkge1xyXG4gICAgICB2YXIgJHRhYkNvbnRlbnQgPSAkKHRoaXMpLFxyXG4gICAgICAgIHNlY3Rpb25JblNsaWRlciA9ICR0YWJDb250ZW50LmZpbmQoc2VsZi5vcHRpb25zLnNsaWRlclNlbGVjdG9yKSxcclxuICAgICAgICBjbGFzc05hbWUgPSAnJztcclxuXHJcbiAgICAgIGlmIChzZWN0aW9uSW5TbGlkZXIubGVuZ3RoICYmIHNlY3Rpb25JblNsaWRlclswXS5jbGFzc05hbWUuaW5kZXhPZignYmtnLWNvbG9yLScpID4gLTEpIHtcclxuICAgICAgICBzZWN0aW9uSW5TbGlkZXIuZWFjaChmdW5jdGlvbigpIHtcclxuICAgICAgICAgIHZhciBjbGFzc2VzID0gdGhpcy5jbGFzc05hbWUuc3BsaXQoJyAnKTtcclxuICAgICAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgY2xhc3Nlcy5sZW5ndGg7IGkrKykge1xyXG4gICAgICAgICAgICBpZiAoY2xhc3Nlc1tpXS5pbmRleE9mKCdia2ctY29sb3ItJykgPiAtMSkge1xyXG4gICAgICAgICAgICAgIGNsYXNzTmFtZSA9IGNsYXNzZXNbaV07XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgICAgICAkdGFiQ29udGVudC5maW5kKCcuc2VjdGlvbicpLmFkZENsYXNzKGNsYXNzTmFtZSk7XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgX2dldFByZXZTdGlja3lOYXZIZWlnaHQoKSB7XHJcbiAgICB2YXIgcHJldlBhcmVudCA9IHRoaXMuZWwucGFyZW50cyh0aGlzLm9wdGlvbnMuY29udGVudFNlbGVjdG9yKS5wcmV2QWxsKCcuZmxleC1jb250ZW50Jykubm90KCcudGFiLWNvbnRlbnQnKSxcclxuICAgICAgcHJldlBhcmVudE5lYXIgPSBwcmV2UGFyZW50LmNoaWxkcmVuKCkuZmluZChcIi5tb2xlY3VsZS1sYi04MzUsIC5tb2xlY3VsZS1sYi04MzYsIC5tb2xlY3VsZS1sYi04MzBcIikubm90KFwiLm5vbi1zdGlja2FibGVcIiksXHJcbiAgICAgIG1heEhlaWdodCA9IDA7XHJcblxyXG4gICAgaWYgKHByZXZQYXJlbnROZWFyLmxlbmd0aCA+IDApIHtcclxuICAgICAgbWF4SGVpZ2h0ID0gJChwcmV2UGFyZW50TmVhci5lcSgwKSkuaGVpZ2h0KCk7XHJcbiAgICB9ICAgIFxyXG4gICAgcmV0dXJuIG1heEhlaWdodDtcclxuICB9XHJcbn1cclxuIl19
