var path = require('path'),
    _ = require('lodash'),
    browserSync = require('browser-sync'),
    watch = require('gulp-watch'),
    gulp = require('gulp'),
    globby = require('globby'),
    fs = require('fs-extra'),
    deploymentTools = require('deployment-tools'),

    paths = {
        root: '.'
    };

//--------------------------------------------------------------------------------
_.extend(paths, {
    html: path.join(paths.root, '*.html'),
    js: path.join(paths.root, 'js', '*.js'),
    css: path.join(paths.root, 'css', '**', '*.css'),
    font: path.join(paths.root, 'font', '**', '*'),
    img: path.join(paths.root, 'img', '**', '*.{jpg,gif,png,svg}'),
});

//--------------------------------------------------------------------------------
gulp.task('browser-sync', function() {
    browserSync({
        server: {
            directory: true,
            baseDir: paths.root
        },
        files: [
            '**/*', 
            '!.git', 
            '!node_modules', 
            '!dist'
        ],
        host: 'localhost',
    });
});


//--------------------------------------------------------------------------------
gulp.task('dist', function() {
    var files = globby.sync(['main_files', 'js', 'img', 'font', 'css', 'index.html']).map(function (item) {
        return path.join(__dirname, item);
    });

    try {
        fs.removeSync(path.join(__dirname, 'dist'));
    } catch (err) {
        console.error(err);
    }

    files.forEach(function (file) {
        try {
            fs.copySync(file, file.replace(__dirname, path.join(__dirname, 'dist')));
        } catch (err) {
            console.error(err);
        }
    });
});

//--------------------------------------------------------------------------------
gulp.task('deploy', ['dist'], function(cb) {
    deploymentTools({
        project: 'slice-full',
        dirToUpload: 'dist',
        onEnd: cb
    });
});

//--------------------------------------------------------------------------------
gulp.task('watch', function() {
    gulp.watch([paths.js, paths.html, paths.img, paths.css, paths.font]).on('change', browserSync.reload);
});
gulp.task('default', ['watch', 'browser-sync']);
