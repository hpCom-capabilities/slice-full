import glob, os

def inverse(dir, pattern, titlePattern):
  i = 1
  obj = glob.glob(os.path.join(dir, pattern))

  for pathAndFilename in obj:

    gl = len(obj)

    num = gl - i

    title, ext = os.path.splitext(os.path.basename(pathAndFilename))
    number = "%05d" % (num)

    print num

    os.rename(pathAndFilename,
              os.path.join(dir, titlePattern % number + ext))
    i += 1

inverse(r'inverse', r'*.jpg', r'aaa-%s')
